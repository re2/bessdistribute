

################################################################################
#
# This script transforms exoskeleton load cell force/torque data into
# an external loads STO file.
#
################################################################################

import os
import sys
from threading import Thread

# Add the parent "bess" directory to the Python module search path so that we
# can import modules from different directories.  Subsequent imports of BESS
# scripts may use reload() to ensure that any changes during development get
# reloaded by the interpreter.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
if bessDir not in sys.path:
    sys.path.append(bessDir)

import utilities.toolsCommon as TC; reload(TC)
import configure_forces.configureForces_Support as CF; reload(CF)

if __name__ == '__main__':
    """
    Main logic.
    """
    
    # This version of the scripts requires that we browse for the XML config
    # file to be used.
    inputFile = TC.getSingleFilePath(
        'Select configure forces setup file', 
        TC.FileFilter('Configure forces file (.xml)', 'xml'))
            
    # Run in a worker thread.  This means we don't block the GUI thread when
    # doing a lot of data processing.
    Thread(target=lambda: CF.mainWorker(inputFile, CF.TimeRange())).start()

# End main.


