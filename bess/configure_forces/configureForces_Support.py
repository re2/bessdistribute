

################################################################################
#
# The guts of the code to transform load cell force/torque data and produce
# an external loads data file.
#
################################################################################

from collections import defaultdict
import copy
import itertools
import os
import sys
from xml.etree import cElementTree as ET

from java.io import IOException

import org.opensim.modeling as modeling

# Add the parent "bess" directory to the Python module search path so that we
# can import modules from different directories.  Subsequent imports of BESS
# scripts may use reload() to ensure that any changes during development get
# reloaded by the interpreter.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
if bessDir not in sys.path:
    sys.path.append(bessDir)

import utilities.handleStoMot as HSM; reload(HSM)
import utilities.modelManipulation as MM; reload(MM)
import utilities.strideTracker as ST; reload(ST)
import utilities.toolsCommon as TC; reload(TC)

class Objects:
    """
    An old C-style struct, just for bundling all the global objects we're going
    to use.
    """
    def __init__(self):
        # A collection of file paths for input and output.
        self.filePaths = None
        
        # Some config parameters used when we generate our dictionary of
        # stride trackers.
        self.strideConfig = None
        
        # A collection of SimTK objects instantiated from the SimObjects type
        # in the modelManipulation script.
        self.simObjects = None
        
        # Dictionary read from the IK file.
        self.ikDict = None
        
        # Dictionary read from the load cell force file.
        self.lcDict = None
        
        # Dictionary read from the ground reaction force file.
        self.grfDict = None
        
        # A list of times that are common to all of our input datasets.
        self.mainTimeList = None
    
        # A dictionary of ground reaction force sets.  More than likely, just
        # a "right" and "left".
        self.grfForceSets = None
        
        # A dictionary of tracked positions.
        self.trackedPositions = None
        
        # A dictionary of load cell force sets that actually exist (and have
        # data for them).
        self.realLoadCellForceSets = None
        
        # A dictionary of load cell force sets that need to have data
        # synthesized for them.
        self.syntheticLoadCellForceSets = None
        
        # A dictionary of objects defining how each synthetic load cell should
        # generate its data.
        self.syntheticDataConfig = None
    
# End objects.

class TimeRange:
    """
    Used to specify a time range to run the configure forces tool.
    """
    def __init__(self, 
        minTime=None, 
        maxTime=None, 
        errorIfTimeNotInMinMaxRange=False):
        
        self.minTime = minTime
        self.maxTime = maxTime
        
        # If minTime and/or maxTime are given, and we then get a common time
        # base for our inpupt files (IK, GRF, load cell) and find the common
        # time base doesn't span the range of [minTime, maxTime] we error
        # if the user tells us to.
        self.errorIfTimeNotInMinMaxRange = errorIfTimeNotInMinMaxRange
        
    # End __init__.
    
# End TimeRange.

class TimeRangeError(Exception):
    """
    Used when the specified min or max time lies outside the time range 
    calculated from the input files.
    """
    pass

class Config:
    """
    Whether from BESS or from a standalone script, the user needs to provide
    a set of inputs.  This class is just a holding class for configuration
    data and the means of serializing it as XML.
    """
    
    @staticmethod
    def writeToXmlElement(filePaths, 
        strideConfig,
        grfSets, 
        trackedPositions,
        realLoadCellForceSets,
        syntheticLoadCellForceSets,
        syntheticDataConfig):
        
        root = ET.Element('Config')
        root.append(FilePaths.writeToXmlElement(filePaths, 'FilePaths'))
        root.append(StrideConfig.writeToXmlElement(strideConfig, 
            'StrideConfig'))
        root.append(GroundReactionForceSet.writeDictToXmlElement(grfSets, 
            'GroundReactionForceSets'))
        root.append(TrackedPosition.writeDictToXmlElement(trackedPositions, 
            'TrackedPositions'))
        root.append(LoadCellForceSet.writeDictToXmlElement(
            realLoadCellForceSets, 
            'RealLoadCellForceSets'))
        root.append(LoadCellForceSet.writeDictToXmlElement(
            syntheticLoadCellForceSets, 
            'SyntheticLoadCellForceSets'))
        root.append(SyntheticDataConfig.writeDictToXmlElement(
            syntheticDataConfig, 
            'SyntheticDataConfigItems'))
        
        return root
    
    @staticmethod
    def writeToXmlFile(writePath, 
        filePaths, 
        strideConfig,
        grfSets, 
        trackedPositions,
        realLoadCellForceSets,
        syntheticLoadCellForceSets,
        syntheticDataConfig):
        
        root = ET.Element('BESS_ConfigureForces', {'Version': '1'})
        root.append(Config.writeToXmlElement(filePaths, 
            strideConfig,
            grfSets, 
            trackedPositions,
            realLoadCellForceSets,
            syntheticLoadCellForceSets,
            syntheticDataConfig))
        
        TC.indentXml(root)
        tree = ET.ElementTree(root)
        tree.write(writePath, encoding='UTF-8')
    
    @staticmethod
    def readFromXmlElement(element):        
        for child in element.getchildren():
            if child.tag == 'FilePaths':
                filePaths = FilePaths.readFromXmlElement(child)
            if child.tag == 'StrideConfig':
                strideConfig = StrideConfig.readFromXmlElement(child)
            if child.tag == 'GroundReactionForceSets':
                grfSets = GroundReactionForceSet.readDictFromXmlElement(child)
            if child.tag == 'TrackedPositions':
                trackedPositions = TrackedPosition.readDictFromXmlElement(child)
            if child.tag == 'RealLoadCellForceSets':
                realLoadCellForceSets = (
                    LoadCellForceSet.readDictFromXmlElement(child))
            if child.tag == 'SyntheticLoadCellForceSets':
                syntheticLoadCellForceSets = (
                    LoadCellForceSet.readDictFromXmlElement(child))
            if child.tag == 'SyntheticDataConfigItems':
                syntheticDataConfig = (
                    SyntheticDataConfig.readDictFromXmlElement(child))
               
        return (filePaths, 
            strideConfig,
            grfSets, 
            trackedPositions, 
            realLoadCellForceSets, 
            syntheticLoadCellForceSets,
            syntheticDataConfig)
        
    @staticmethod
    def readFromXmlFile(readPath):
        tree = ET.ElementTree(file=readPath)
        root = tree.getroot()
        
        for child in root.getchildren():
            if child.tag == 'Config':
                return Config.readFromXmlElement(child)
                    
        raise ValueError('Error parsing XML')
        
    @staticmethod
    def validateModel(filePaths, 
        strideConfig,
        grfSets, 
        trackedPositions,
        realLoadCellForceSets,
        syntheticLoadCellForceSets,
        syntheticDataConfig):
        """
        Validates a complete set of model data.
        
        This function returns a list of strings, each one with an error
        description.  If there are no errors then this will be an empty list.
        Right now there is just a single classification:  error.  Nothing like
        warning, etc.
        
        The errors here are directed to the use of configure forces through
        the UI, which already performs a fair amount of validation when 
        objects (tracked positions, GRFs, etc.) are created.  This function
        mainly checks things that could break after the objects are created
        (like a dependent object being removed, etc.).
        
        I'm a little conflicted on how I'm doing this.  I should probably
        be a bit more OOP and just have each of the types do a validation on
        its little part.  Of course, a good chunk of the validation is across
        data types(a force set needs to know if a tracked position exists, 
        etc.)...
        """
        
        outList = []
        
        # Start with the file paths.  Verify the input files all exist though
        # make a bit of an exception with the model file.  When integrated into
        # BESS or another tool, the model file may not exist yet if it will
        # be created as part of the scaling tool (for example).
        if not filePaths.modelPath:
            outList.append('OpenSim model file path is not valid.')
        if not filePaths.ikPath or not os.path.exists(filePaths.ikPath):
            outList.append('Inverse kinematics motion file path is not valid.')
        if not filePaths.grfPath or not os.path.exists(filePaths.grfPath):
            outList.append('Ground reaction force data file path is not valid.')
        if not filePaths.lcPath or not os.path.exists(filePaths.lcPath):
            outList.append('Force/torque sensor data file path is not valid.')
        
        # Check the stride configuration and make sure they hysteresis tuple
        # is well-ordered.
        if (strideConfig.midpointHysteresis[0] > 
            strideConfig.midpointHysteresis[1]):
            outList.append('Stride midpoint hysteresis is not well-ordered.  '
                'Lower force is larger than higher force.')        
        if (strideConfig.rightGroundReactionForceSetName not in grfSets):
            outList.append('Right stride ground reaction force set does not exist.')
        if (strideConfig.leftGroundReactionForceSetName not in grfSets):
            outList.append('Left stride ground reaction force set does not exist.')
        
        # Ground reaction force sets.  Guard against things that could 
        # invalidate the GRF after it is created.
        for grfName, grfForceSet in grfSets.iteritems():
            if (grfForceSet.exosekeletonLoadCellName != '' and
                grfForceSet.exosekeletonLoadCellName not in itertools.chain(
                    realLoadCellForceSets, syntheticLoadCellForceSets)):
                outList.append('In ground reaction force set ' + grfName +
                    ' force/torque sensor ' + 
                    grfForceSet.exosekeletonLoadCellName + ' does not exist.')
                    
        # For load cells, just track if dependent tracked positions no longer
        # exist.
        for lcName, loadCell in itertools.chain(realLoadCellForceSets.items(),
            syntheticLoadCellForceSets.items()):
            if loadCell.sensorTrackedPositionName not in trackedPositions:
                outList.append('In force/torque sensor ' + lcName +
                    ' sensor tracked position ' + 
                    loadCell.sensorTrackedPositionName + ' does not exist.')
                    
            if loadCell.appliedToTrackedPositionName not in trackedPositions:
                outList.append('In force/torque sensor ' + lcName +
                    ' applied to tracked position ' + 
                    loadCell.appliedToTrackedPositionName + ' does not exist.')
        
        for lcName, dataConfig in syntheticDataConfig.iteritems():
            if dataConfig.realLoadCellName not in realLoadCellForceSets:
                outList.append('In synthetic force/torque sensor ' + lcName +
                    ' real force/torque sensor ' +
                    dataConfig.realLoadCellName + ' does not exist.')
        
        return outList
        
    # End validateModel.
    
    @staticmethod
    def validateXmlFile(xmlPath):
        """
        Takes an input XML, turns it into concrete objects, and validates them.
        """
        
        return Config.validateModel(*Config.readFromXmlFile(xmlPath))
        
    # End validateXmlFile.
    
# End Config.

class FilePaths:
    """
    File paths specifying inputs and outputs.
    """
    def __init__(self):
        """
        For explanation of different fields, see the XML function -- we embed
        comments for each item in the XML.
        """        
        self.modelPath = None
        self.ikPath = None
        self.lcPath = None
        self.grfPath = None
        self.forcesOutPath = None
        self.stridesXmlOutPath = None
        self.stridesStoOutPath = None
        self.externalLoadsOutPath = None
        
    @staticmethod
    def writeToXmlElement(filePaths, tag):
        root = ET.Element(tag)
        
        root.append(ET.Comment("File path to load the OpenSim model used "
            "for body segments and transform of forces to body and ground "
            "frames."))
        ET.SubElement(root, 'modelPath').text = filePaths.modelPath
        
        root.append(ET.Comment("File path to an inverse kinematics solution.  "
            "Used to get joint angles at points in time for transforming "
            "forces."))
        ET.SubElement(root, 'ikPath').text = filePaths.ikPath      

        root.append(ET.Comment("File path to a STO/MOT file containing "
            "exoskeleton load cell force and torque data."))
        ET.SubElement(root, 'lcPath').text = filePaths.lcPath
        
        root.append(ET.Comment("File path to a STO/MOT file containing ground "
            "reaction forces as measured by a treadmill or force plate."))
        ET.SubElement(root, 'grfPath').text = filePaths.grfPath
        
        root.append(ET.Comment("Output file path where we will write the "
            "resulting transformed forces and supporting time-indexed data."))
        ET.SubElement(root, 'forcesOutPath').text = filePaths.forcesOutPath
        
        root.append(ET.Comment("Output file where we will write the identified "
            "strides as XML."))
        ET.SubElement(root, 'stridesXmlOutPath').text = (
            filePaths.stridesXmlOutPath)
        
        root.append(ET.Comment("Output file where we will write the identified "
            "strides in a time format suitable for plotting."))
        ET.SubElement(root, 'stridesStoOutPath').text = (
            filePaths.stridesStoOutPath)
            
        root.append(ET.Comment("Output file where we will the external loads "
            "XML file used by OpenSim for forward simulation tools like "
            "ID, CMC, etc."))
        ET.SubElement(root, 'externalLoadsOutPath').text = (
            filePaths.externalLoadsOutPath)
            
        return root
        
    @staticmethod
    def readFromXmlElement(element):
        filePaths = FilePaths()
           
        filePaths.modelPath = element.findtext('modelPath')
        filePaths.ikPath = element.findtext('ikPath')
        filePaths.lcPath = element.findtext('lcPath')
        filePaths.grfPath = element.findtext('grfPath')
        filePaths.forcesOutPath = element.findtext('forcesOutPath')
        filePaths.stridesXmlOutPath = element.findtext('stridesXmlOutPath')
        filePaths.stridesStoOutPath = element.findtext('stridesStoOutPath')
        filePaths.externalLoadsOutPath = element.findtext(
            'externalLoadsOutPath')
            
        return filePaths
        
# End FilePaths.
    
class StrideConfig:
    """
    Holds configuration information for generating stride trackers.
    """
    def __init__(self):
        # Leave these with default arguments.  This allows the downstream GUI
        # to create an empty StrideConfig object and still be able to reference
        # the names for strides (which are used by the SyntheticDataConfig
        # object.
        self.rightStrideName = 'right_stride'
        self.rightGroundReactionForceSetName = 'right'
        self.leftStrideName = 'left_stride'
        self.leftGroundReactionForceSetName = 'left'
        self.midpointHysteresis = (10, 50)
        
    @staticmethod
    def writeToXmlElement(strideConfig, tag):
        root = ET.Element(tag)
        
        root.append(ET.Comment("Name to assign the right side stride.  Most "
            "likely 'right_stride'."))
        ET.SubElement(root, 'rightStrideName').text = (
            strideConfig.rightStrideName)
        
        root.append(ET.Comment("Name of the ground reaction force set to "
            "use when determining right side strides.  Most likely 'right'."))
        ET.SubElement(root, 'rightGroundReactionForceSetName').text = (
            strideConfig.rightGroundReactionForceSetName)
        
        root.append(ET.Comment("Name to assign the left side stride.  Most "
            "likely 'left_stride'."))
        ET.SubElement(root, 'leftStrideName').text = strideConfig.leftStrideName
        
        root.append(ET.Comment("Name of the ground reaction force set to "
            "use when determining left side strides.  Most likely 'left'."))
        ET.SubElement(root, 'leftGroundReactionForceSetName').text = (
            strideConfig.leftGroundReactionForceSetName)
        
        root.append(ET.Comment("Force hysteresis to use for determining "
            "strides.  Should be a tuple, like (10, 50)."))
        ET.SubElement(root, 'midpointHysteresis').text = str(
            strideConfig.midpointHysteresis)
        
        return root
             
    @staticmethod
    def readFromXmlElement(element):
        strideConfig = StrideConfig()
        
        strideConfig.rightStrideName = element.findtext('rightStrideName')
        strideConfig.rightGroundReactionForceSetName = element.findtext(
            'rightGroundReactionForceSetName')
        strideConfig.leftStrideName = element.findtext('leftStrideName')
        strideConfig.leftGroundReactionForceSetName = element.findtext(
            'leftGroundReactionForceSetName')
        strideConfig.midpointHysteresis = TC.xmlStringTupleToTuple(
            element.findtext('midpointHysteresis'))
        
        return strideConfig
            
# End StrideConfig.
            
class GroundReactionForceSet:
    """
    Bundles together a set of forces and torques being applied to the subject
    through the ground and measured by a force plate.
    """    
    def __init__(self, 
        forceName, 
        positionName, 
        torqueName,
        appliedToBodyName,
        exosekeletonLoadCellName):
        
        self.forces = modeling.Vec3()
        self.positions = modeling.Vec3()
        self.torques = modeling.Vec3()
        self.appliedToBodyName = appliedToBodyName
        self.exosekeletonLoadCellName = exosekeletonLoadCellName
        
        self.forceName = forceName
        self.positionName = positionName
        self.torqueName = torqueName
        
        self.fullForceNames = applyVectorSuffix(forceName)
        self.fullPositionNames = applyVectorSuffix(positionName)
        self.fullTorqueNames = applyVectorSuffix(torqueName)
                    
    def copy(self, source):
        """
        Define a copy operation.  If we were more pythonic we could just 
        override __deepcopy__, etc.
        """
        self.forces = modeling.Vec3(source.forces)
        self.positions = modeling.Vec3(source.positions)
        self.torques = modeling.Vec3(source.torques)
                   
    def writeAllToDictList(self, outDict):
        for idx, name in enumerate(self.fullForceNames):
            outDict[name].append(self.forces.get(idx))
        
        for idx, name in enumerate(self.fullPositionNames):
            outDict[name].append(self.positions.get(idx))
        
        for idx, name in enumerate(self.fullTorqueNames):
            outDict[name].append(self.torques.get(idx))
            
    def setValues(self, vector, x, y, z):
        for idx, value in enumerate((x, y, z)):
            vector.set(idx, value)
            
    def setForces(self, x, y, z):
        self.setValues(self.forces, x, y, z)
        
    def setPositions(self, x, y, z):
        self.setValues(self.positions, x, y, z)
    
    def setTorques(self, x, y, z):
        self.setValues(self.torques, x, y, z)
        
    def getForces(self):
        return self.forces.get(0), self.forces.get(1), self.forces.get(2)
        
    def getTorques(self):
        return self.torques.get(0), self.torques.get(1), self.torques.get(2)
        
    @staticmethod
    def writeToXmlElement(grfSet):
        root = ET.Element('GroundReactionForceSet')
        
        root.append(ET.Comment("Name prepended to forces when reading and "
            "writing ground reaction forces."))
        ET.SubElement(root, 'forceName').text = grfSet.forceName
        
        root.append(ET.Comment("Name prepended to positions when reading and "
            "writing ground reaction force positions."))
        ET.SubElement(root, 'positionName').text = grfSet.positionName
        
        root.append(ET.Comment("Name prepended to torques when reading and "
            "writing ground reaction torques."))
        ET.SubElement(root, 'torqueName').text = grfSet.torqueName
        
        root.append(ET.Comment("Body segment name this force applies to.  "
            "Used when writing out the external loads file."))
        ET.SubElement(root, 'appliedToBodyName').text = grfSet.appliedToBodyName
        
        root.append(ET.Comment("Name of a load cell that accounts for GRF "
            "forces transmitted to the exoskeleton and not the subject."))
        ET.SubElement(root, 'exosekeletonLoadCellName').text = (
            grfSet.exosekeletonLoadCellName)
        
        return root
        
    @staticmethod
    def writeDictToXmlElement(grfSetDict, tag):
        root = ET.Element(tag)
        
        for grfSetName, grfSet in grfSetDict.iteritems():
            grfsElement = GroundReactionForceSet.writeToXmlElement(grfSet)
            grfsElement.set('name', grfSetName)
            root.append(grfsElement)
            
        return root
        
    @staticmethod
    def readFromXmlElement(element):
        forceName = element.findtext('forceName')
        positionName = element.findtext('positionName')
        torqueName = element.findtext('torqueName')
        appliedToBodyName = element.findtext('appliedToBodyName')
        exosekeletonLoadCellName = element.findtext('exosekeletonLoadCellName')
        return GroundReactionForceSet(forceName, 
            positionName, 
            torqueName, 
            appliedToBodyName,
            exosekeletonLoadCellName)
        
    @staticmethod
    def readDictFromXmlElement(element):
        grfDict = dict()
        
        for child in element.getchildren():
            grfDict[child.get('name')] = (
                GroundReactionForceSet.readFromXmlElement(child))
                                
        return grfDict
        
# End GroundReactionForceSet.

class TrackedPosition:
    """
    Tracks a position on a body and transforms to ground frame.
    """        
    def __init__(self, 
        bodyPositionName, 
        groundPositionName, 
        bodySegmentName,
        trackBodyCOM=False,
        pBody=None):
        
        self.bodyPositionName = bodyPositionName
        self.groundPositionName = groundPositionName
        self.bodySegmentName = bodySegmentName
        self.trackBodyCOM = trackBodyCOM
        
        self.fullBodyNames = applyVectorSuffix(bodyPositionName)
        self.fullGroundNames = applyVectorSuffix(groundPositionName)
        
        # This is to be set after instantiation once we have the SimTK objects
        # loaded.
        self.sensorFrameBody = None
        
        if pBody:
            self.pBody = pBody
        else:
            # Just track the origin of the body initially.  This can be updated
            # after instantiation.
            self.pBody = modeling.Vec3(0.0)
        
        # The ground position is invalid until we call the transform function,
        # but this being Python, let's create the object to make sure the type
        # is correct before trying to use it.
        self.pGround = modeling.Vec3(0.0)
                    
    def setSensorFrameBody(self, simObjects):
        self.sensorFrameBody = simObjects.model.getBodySet().get(
            self.bodySegmentName)
            
    def setBodyPositionToCOM(self):
        
        # OpenSim 4.0 seems to return the vector instead of taking it as an
        # argument.  Try both.
        try:
            self.sensorFrameBody.getMassCenter(self.pBody)
        except TypeError,e:
            self.pBody = self.sensorFrameBody.getMassCenter()
         
    def writeAllToDictList(self, outDict, writeBodyFramePositions=True):
        for idx, name in enumerate(self.fullGroundNames):
            outDict[name].append(self.pGround.get(idx))        
        
        # Don't necessarily write out the body positions -- likely they are
        # (0,0,0) or don't change position (i.e. fixed to a body).  We may
        # choose to save some space by not writing them out.
        if writeBodyFramePositions:
            for idx, name in enumerate(self.fullBodyNames):
                outDict[name].append(self.pBody.get(idx))

    def transformToGround(self, simObjects):
        """
        Takes the tracked position in the body frame and transform to the ground
        frame.
        """
        simObjects.simEngine.transformPosition(simObjects.state, 
            self.sensorFrameBody, 
            self.pBody, 
            simObjects.groundBody, 
            self.pGround)
            
    def getGroundPositions(self):
        return self.pGround.get(0), self.pGround.get(1), self.pGround.get(2)

    @staticmethod
    def writeToXmlElement(trackedPosition):
        root = ET.Element('TrackedPosition')
        
        root.append(ET.Comment("Name prepended to body positions when writing "
            "to an output file."))
        ET.SubElement(root, 'bodyPositionName').text = (
            trackedPosition.bodyPositionName)
        
        root.append(ET.Comment("Name prepended to ground positions when "
            "writing to an output file."))
        ET.SubElement(root, 'groundPositionName').text = (
            trackedPosition.groundPositionName)
        
        root.append(ET.Comment("Name of the body segment this position "
            "tracks."))
        ET.SubElement(root, 'bodySegmentName').text = (
            trackedPosition.bodySegmentName)
        
        root.append(ET.Comment("Whether this tracked position should track "
            "the center of mass of the body segment."))
        ET.SubElement(root, 'trackBodyCOM').text = str(
            trackedPosition.trackBodyCOM)
        
        root.append(ET.Comment("Offset of this tracked position from the "
            "origin of the body segment."))
        ET.SubElement(root, 'pBody').text = str((trackedPosition.pBody.get(0),
            trackedPosition.pBody.get(1),
            trackedPosition.pBody.get(2)))
        
        return root
        
    @staticmethod
    def writeDictToXmlElement(trackedPositionDict, tag):
        root = ET.Element(tag)
        
        for trackedPositionName, trackedPosition in (
            trackedPositionDict.iteritems()):
            tpElement = TrackedPosition.writeToXmlElement(trackedPosition)
            tpElement.set('name', trackedPositionName)
            root.append(tpElement)
            
        return root
    
    @staticmethod
    def readFromXmlElement(element):
        bodyPositionName = element.findtext('bodyPositionName')
        groundPositionName = element.findtext('groundPositionName')
        bodySegmentName = element.findtext('bodySegmentName')
        trackBodyCOM = (element.findtext('trackBodyCOM') == 'True')
        pBody = modeling.Vec3(*TC.xmlStringTupleToTuple(
            element.findtext('pBody')))
        
        return TrackedPosition(bodyPositionName, 
            groundPositionName, 
            bodySegmentName, 
            trackBodyCOM, 
            pBody)
        
    @staticmethod
    def readDictFromXmlElement(element):
        trackedPositionDict = dict()
        
        for child in element.getchildren():
            trackedPositionDict[child.get('name')] = (
                TrackedPosition.readFromXmlElement(child))
                                
        return trackedPositionDict    
    
# End TrackedPosition.
            
class LoadCellForceSet:
    """
    Bundles together forces related to a load cell.
    """    
    def __init__(self,
        sensorForceName,
        sensorTorqueName,
        bodyForceName,
        bodyTorqueName,
        groundForceName,
        groundTorqueName,
        appliedForceName,
        appliedTorqueName,
        sensorTrackedPositionName,
        appliedToBodyName,
        appliedToTrackedPositionName,
        sensorToBodyRotationArray):
        
        # Sensor force name is the name we expect to find the force under in
        # the input dictionary when we read it in.
        self.sensorForceName = sensorForceName
        self.sensorTorqueName = sensorTorqueName
        self.bodyForceName = bodyForceName
        self.bodyTorqueName = bodyTorqueName
        self.groundForceName = groundForceName
        self.groundTorqueName = groundTorqueName
        self.appliedForceName = appliedForceName
        self.appliedTorqueName = appliedTorqueName
        self.sensorTrackedPositionName = sensorTrackedPositionName
        self.appliedToBodyName = appliedToBodyName
        self.appliedToTrackedPositionName = appliedToTrackedPositionName
        self.sensorToBodyRotationArray = sensorToBodyRotationArray
        
        # Get a 3x3 SimTK matrix for the rotation from the 1x9 list passed in.
        self.rMat = TC.listToMat33(sensorToBodyRotationArray)
        
        # [f,t]Body is the force/torque at the load cell in the sensor body
        # frame.
        self.fBody = modeling.Vec3(0.0)
        self.tBody = modeling.Vec3(0.0)
        
        # [f,t]Ground is the force/torque at the load cell in the ground frame.
        self.fGround = modeling.Vec3(0.0)
        self.tGround = modeling.Vec3(0.0)
        
        # [f,t]Applied is the force/torque applied to the human subject
        # in the ground frame.
        self.fApplied = modeling.Vec3(0.0)
        self.tApplied = modeling.Vec3(0.0)
        
        self.fullSensorForceNames = applyVectorSuffix(sensorForceName)
        self.fullSensorTorqueNames = applyVectorSuffix(sensorTorqueName)
        self.fullBodyForceNames = applyVectorSuffix(bodyForceName)
        self.fullBodyTorqueNames = applyVectorSuffix(bodyTorqueName)
        self.fullGroundForceNames = applyVectorSuffix(groundForceName)
        self.fullGroundTorqueNames = applyVectorSuffix(groundTorqueName)
        self.fullAppliedForceNames = applyVectorSuffix(
            appliedForceName)
        self.fullAppliedTorqueNames = applyVectorSuffix(
            appliedTorqueName)
        
        # This will be set after instantiation when we get the SimTK objects
        # necessary.  Sensor frame body is the OpenSim body that has a
        # frame that aligns to the load cell sensor frame -- not necessarily 
        # the body the load cell is measuring (i.e. the applied body).
        self.sensorFrameBody = None
        
    def setSensorFrameBody(self, sensorFrameBody):
        # We used to get this from looking at the model for a 
        # "sensorFrameBodyName" but we realized that the tracked position held
        # the body name as well.  So now we just set this value from the body
        # in the tracked position.
        self.sensorFrameBody = sensorFrameBody
            
    def writeBodyForcesToDictList(self, outDict):
        for idx, name in enumerate(self.fullBodyForceNames):
            outDict[name].append(self.fBody.get(idx))
            
    def writeBodyTorquesToDictList(self, outDict):
        for idx, name in enumerate(self.fullBodyTorqueNames):
            outDict[name].append(self.tBody.get(idx))
    
    def writeGroundForcesToDictList(self, outDict):
        for idx, name in enumerate(self.fullGroundForceNames):
            outDict[name].append(self.fGround.get(idx))
            
        for idx, name in enumerate(self.fullAppliedForceNames):
            outDict[name].append(self.fApplied.get(idx))
            
    def writeGroundTorquesToDictList(self, outDict):
        for idx, name in enumerate(self.fullGroundTorqueNames):
            outDict[name].append(self.tGround.get(idx))
            
        for idx, name in enumerate(self.fullAppliedTorqueNames):
            outDict[name].append(self.tApplied.get(idx))
                
    def writeAllToDictList(self, outDict):
        self.writeBodyForcesToDictList(outDict)
        self.writeBodyTorquesToDictList(outDict)
        self.writeGroundForcesToDictList(outDict)
        self.writeGroundTorquesToDictList(outDict)

    def transformToGround(self, objects):
        """
        Takes the current body force vector and transforms it to the ground
        frame.  Also the body torque vector.
        """
        objects.simObjects.simEngine.transform(objects.simObjects.state, 
            self.sensorFrameBody, 
            self.fBody, 
            objects.simObjects.groundBody, 
            self.fGround)
            
        objects.simObjects.simEngine.transform(objects.simObjects.state, 
            self.sensorFrameBody, 
            self.tBody, 
            objects.simObjects.groundBody, 
            self.tGround)
            
        # If we have the ground forces, we can also get them at the point of
        # external contact.
        self.calculateExternalForces(objects)
            
    def calculateExternalForces(self, objects):
        """
        While we measure force/torque at the load cell sensor, what we're 
        really interested in is the force/torque at the point of external
        application -- i.e. the actual contact point between the exoskeleton
        and human, or the exoskeleton and the force plate.
        """
        
        # For forces, just reverse the sign in an "equal and opposite"
        # perspective.
        for idx in range(3):
            self.fApplied.set(idx, -self.fGround.get(idx))
        
        # For torques, it is not as simple as saying the external torque is
        # equal and opposite to that measured at the load cell, since the
        # forces themselves are acting at moment arms and contribute to 
        # measured torque.
        #
        # Long story short, we need to calculate proper external torques
        # from the following moment balances.  Example notation:
        # * M_lc,x means Moment_loadcell,x-axis
        # * F_ex,z means Force_external,z-axis
        # * p_y means distance between external contact point and load cell
        #   in the y-axis direction
        # 
        # We are solving for external moments (torques):
        # M_ex,x = F_ex,y * p_z - F_ex,z * p_y - M_lc,x
        # M_ex,y = F_ex,z * p_x - F_ex,x * p_z - M_lc,y
        # M_ex,z = F_ex,x * p_y - F_ex,y * p_x - M_lc,z
        
        # Get the tracked positions in order to calculate the moment arms.
        appliedTrackedPosition = objects.trackedPositions[self.
            appliedToTrackedPositionName]
        sensorTrackedPosition = objects.trackedPositions[self.
            sensorTrackedPositionName]
            
        applied_px, applied_py, applied_pz = (appliedTrackedPosition.
            getGroundPositions())
        sensor_px, sensor_py, sensor_pz = (sensorTrackedPosition.
            getGroundPositions())
            
        # I'm sure we could be much slicker about all this but I prefer to be
        # explicit with notation so just fill in unique variables for all the
        # quantities.
        p_x = applied_px - sensor_px
        p_y = applied_py - sensor_py
        p_z = applied_pz - sensor_pz
        F_ex_x = self.fApplied.get(0)
        F_ex_y = self.fApplied.get(1)
        F_ex_z = self.fApplied.get(2)
        M_lc_x = self.tGround.get(0)
        M_lc_y = self.tGround.get(1)
        M_lc_z = self.tGround.get(2)
        
        M_ex_x = F_ex_y * p_z - F_ex_z * p_y - M_lc_x
        M_ex_y = F_ex_z * p_x - F_ex_x * p_z - M_lc_y
        M_ex_z = F_ex_x * p_y - F_ex_y * p_x - M_lc_z
        
        self.tApplied.set(0, M_ex_x)
        self.tApplied.set(1, M_ex_y)
        self.tApplied.set(2, M_ex_z)
        
    def setBodyForce(self, sensorFrame_fx, sensorFrame_fy, sensorFrame_fz):
        # Take the data in the sensor frame and apply the provided rotation
        # matrix to get it into a body frame.
        fSensor = modeling.Vec3(sensorFrame_fx, sensorFrame_fy, sensorFrame_fz)
        TC.rotateVector(fSensor, self.rMat, self.fBody)
        
    def setBodyTorque(self, sensorFrame_tx, sensorFrame_ty, sensorFrame_tz):
        # Take the data in the sensor frame and apply the provided rotation
        # matrix to get it into a body frame.
        tSensor = modeling.Vec3(sensorFrame_tx, sensorFrame_ty, sensorFrame_tz)
        TC.rotateVector(tSensor, self.rMat, self.tBody)        
    
    @staticmethod
    def writeToXmlElement(loadCellForceSet):
        root = ET.Element('LoadCellForceSet')
        
        root.append(ET.Comment("Name prepended to forces when reading sensor "
            "frame forces from an input file."))
        ET.SubElement(root, 'sensorForceName').text = (
            loadCellForceSet.sensorForceName)
            
        root.append(ET.Comment("Name prepended to torques when reading sensor "
            "frame torques from an input file."))
        ET.SubElement(root, 'sensorTorqueName').text = (
            loadCellForceSet.sensorTorqueName)
        
        root.append(ET.Comment("Name prepended to forces when writing sensor "
            "forces in a body frame to an output file."))
        ET.SubElement(root, 'bodyForceName').text = (
            loadCellForceSet.bodyForceName)
            
        root.append(ET.Comment("Name prepended to torques when writing sensor "
            "torques in a body frame to an output file."))
        ET.SubElement(root, 'bodyTorqueName').text = (
            loadCellForceSet.bodyTorqueName)
        
        root.append(ET.Comment("Name prepended to forces when writing sensor "
            "forces in a ground frame to an output file."))
        ET.SubElement(root, 'groundForceName').text = (
            loadCellForceSet.groundForceName)
            
        root.append(ET.Comment("Name prepended to torques when writing sensor "
            "torques in a ground frame to an output file."))
        ET.SubElement(root, 'groundTorqueName').text = (
            loadCellForceSet.groundTorqueName)
            
        root.append(ET.Comment("Name prepended to forces when writing external "
            "subject forces in a ground frame to an output file."))
        ET.SubElement(root, 'appliedForceName').text = (
            loadCellForceSet.appliedForceName)
            
        root.append(ET.Comment("Name prepended to torques when writing "
            "external subject torques in a ground frame to an output file."))
        ET.SubElement(root, 'appliedTorqueName').text = (
            loadCellForceSet.appliedTorqueName)
                   
        root.append(ET.Comment("Name of the tracked position representing "
            "the point where force is measured (i.e. the sensor position)."))
        ET.SubElement(root, 'sensorTrackedPositionName').text = (
            loadCellForceSet.sensorTrackedPositionName)
                    
        root.append(ET.Comment("Body segment name this force applies to.  "
            "Used when writing out the external loads file."))
        root.append(ET.Comment("This field can be 'ground' if it contacts "
            "the force plate instead of the subject."))
        ET.SubElement(root, 'appliedToBodyName').text = str(
            loadCellForceSet.appliedToBodyName)
            
        root.append(ET.Comment("Name of the tracked position representing "
            "the point where the force is applied on the subject or ground."))
        ET.SubElement(root, 'appliedToTrackedPositionName').text = str(
            loadCellForceSet.appliedToTrackedPositionName)
            
        root.append(ET.Comment("1-dimensional iterable of length 9 giving the "
            "3x3 rotation matrix to go from sensor coordinate frame to the "
            "body frame."))
        ET.SubElement(root, 'sensorToBodyRotationArray').text = str(
            loadCellForceSet.sensorToBodyRotationArray)        
        
        return root
        
    @staticmethod
    def writeDictToXmlElement(loadCellDict, tag):
        root = ET.Element(tag)
        
        for loadCellForceSetName, loadCellForceSet in loadCellDict.iteritems():
            lcElement = LoadCellForceSet.writeToXmlElement(loadCellForceSet)
            lcElement.set('name', loadCellForceSetName)
            root.append(lcElement)
            
        return root
    
    @staticmethod
    def readFromXmlElement(element):
        sensorForceName = element.findtext('sensorForceName')
        sensorTorqueName = element.findtext('sensorTorqueName')
        bodyForceName = element.findtext('bodyForceName')
        bodyTorqueName = element.findtext('bodyTorqueName')
        groundForceName = element.findtext('groundForceName')
        groundTorqueName = element.findtext('groundTorqueName')
        appliedForceName = element.findtext('appliedForceName')
        appliedTorqueName = element.findtext('appliedTorqueName')
        sensorTrackedPositionName = element.findtext(
            'sensorTrackedPositionName')
        appliedToBodyName = element.findtext('appliedToBodyName')
        appliedToTrackedPositionName = element.findtext(
            'appliedToTrackedPositionName')
                        
        sensorToBodyRotationArray = TC.xmlStringTupleToTuple(element.findtext(
            'sensorToBodyRotationArray'))
        
        return LoadCellForceSet(sensorForceName,
            sensorTorqueName,
            bodyForceName,
            bodyTorqueName,
            groundForceName,
            groundTorqueName,
            appliedForceName,
            appliedTorqueName,
            sensorTrackedPositionName,
            appliedToBodyName,
            appliedToTrackedPositionName,
            sensorToBodyRotationArray)
        
    @staticmethod
    def readDictFromXmlElement(element):
        loadCellDict = dict()
        
        for child in element.getchildren():
            loadCellDict[child.get('name')] = (
                LoadCellForceSet.readFromXmlElement(child))
                                
        return loadCellDict
        
# End LoadCellForceSet.

class SyntheticDataConfig:
    """
    Class to encode rules about how to convert from real load cell data to
    synthetic data.
    """
    def __init__(self, 
        syntheticLoadCellStrideName,
        realLoadCellName,
        realLoadCellStrideName,
        realToSyntheticForceRotationArray,
        realToSyntheticTorqueRotationArray):
        
        self.syntheticLoadCellStrideName = syntheticLoadCellStrideName
        self.realLoadCellName = realLoadCellName
        self.realLoadCellStrideName = realLoadCellStrideName
        self.realToSyntheticForceRotationArray = (
            realToSyntheticForceRotationArray)
        self.realToSyntheticTorqueRotationArray = (
            realToSyntheticTorqueRotationArray)
        
        self.forceRotationMatrix = TC.listToMat33(
            realToSyntheticForceRotationArray)
        self.torqueRotationMatrix = TC.listToMat33(
            realToSyntheticTorqueRotationArray)
            
    @staticmethod
    def writeToXmlElement(syntheticDataConfig):
        root = ET.Element('SyntheticDataConfig')
                
        root.append(ET.Comment("Name of the stride to use when calculating "
            "the synthetic load cell data."))
        ET.SubElement(root, 'syntheticLoadCellStrideName').text = (
            syntheticDataConfig.syntheticLoadCellStrideName)
        
        root.append(ET.Comment("Name of the existing load cell sensor to use "
            "when synthesizing data."))
        ET.SubElement(root, 'realLoadCellName').text = (
            syntheticDataConfig.realLoadCellName)
        
        root.append(ET.Comment("Name of the stride to use when matching data "
            "from the real load cell sensor."))
        ET.SubElement(root, 'realLoadCellStrideName').text = (
            syntheticDataConfig.realLoadCellStrideName)

        root.append(ET.Comment("1-dimensional iterable of length 9 giving a "
            "3x3 rotation matrix."))
        root.append(ET.Comment("This array is used to transform the real load "
            "cell force vector to the synthetic load cell frame."))
        ET.SubElement(root, 'realToSyntheticForceRotationArray').text = str(
            syntheticDataConfig.realToSyntheticForceRotationArray)     
            
        root.append(ET.Comment("1-dimensional iterable of length 9 giving a "
            "3x3 rotation matrix."))
        root.append(ET.Comment("This array is used to transform the real load "
            "cell torque vector to the synthetic load cell frame."))
        ET.SubElement(root, 'realToSyntheticTorqueRotationArray').text = str(
            syntheticDataConfig.realToSyntheticTorqueRotationArray)     
        
        return root
        
    @staticmethod
    def writeDictToXmlElement(syntheticDataDict, tag):
        root = ET.Element(tag)
        
        for syntheticDataName, syntheticDataConfig in (
            syntheticDataDict.iteritems()):
            sdElement = SyntheticDataConfig.writeToXmlElement(
                syntheticDataConfig)
            sdElement.set('name', syntheticDataName)
            root.append(sdElement)
            
        return root
    
    @staticmethod
    def readFromXmlElement(element):                
        syntheticLoadCellStrideName = element.findtext(
            'syntheticLoadCellStrideName')
        realLoadCellName = element.findtext('realLoadCellName')
        realLoadCellStrideName = element.findtext('realLoadCellStrideName')
        realToSyntheticForceRotationArray = TC.xmlStringTupleToTuple(
            element.findtext('realToSyntheticForceRotationArray'))
        realToSyntheticTorqueRotationArray = TC.xmlStringTupleToTuple(
            element.findtext('realToSyntheticTorqueRotationArray'))
        
        return SyntheticDataConfig(syntheticLoadCellStrideName,
            realLoadCellName,
            realLoadCellStrideName,
            realToSyntheticForceRotationArray,
            realToSyntheticTorqueRotationArray)
        
    @staticmethod
    def readDictFromXmlElement(element):
        syntheticDataDict = dict()
        
        for child in element.getchildren():
            syntheticDataDict[child.get('name')] = (
                SyntheticDataConfig.readFromXmlElement(child))
                                
        return syntheticDataDict
        
# End SyntheticDataConfig.

def applyVectorSuffix(inName):
    """
    Takes a root name and returns a list, with each of the components 
    directions appended to the root name.
    
    Example:
    * 'back_f' --> ['back_fx', 'back_fy', 'back_fz']
    """
    return [inName + suffix for suffix in ('x', 'y', 'z')]
        
# End applyVectorSuffix.
        
def mainWorker(configXmlFile, timeRange):
    """
    Main function.
    
    configXmlFile is a file path.
    timeRange is a TimeRange object.
    """
    
    print 'Starting configure forces script'
    
    # Create a single container object to add our input objects to.  This will
    # make it easier to break up our later code into functions and pass a 
    # single input object.
    objects = Objects()
    
    # Convert from the XML file to a concrete representation for configuration
    # objects.
    (objects.filePaths, 
        objects.strideConfig,
        objects.grfForceSets, 
        objects.trackedPositions,
        objects.realLoadCellForceSets,
        objects.syntheticLoadCellForceSets,
        objects.syntheticDataConfig) = Config.readFromXmlFile(configXmlFile)
        
    # Get SimTK objects, input dictionaries, and establish a common time base.
    setupInput(objects, timeRange)
        
    # We'll compile all our data into a dictionary and then write it out in
    # STO format.  Use the handly defaultdict so that new dict entries are 
    # automatically assumed to have a value of a list.
    collectedDataDict = defaultdict(list)
    
    # Use a dictionary to track strides for the right and left side.  New 
    # entries will by default be StrideTracker objects.
    strideTrackers = defaultdict(lambda: ST.StrideTracker(
        objects.strideConfig.midpointHysteresis))
        
    # First loop:
    # * Iterate over the IK solution and position the model.
    # * Transform real load cells into body frame and ground frame.
    # * Track individual body positions in the global frame.
    # * Feed force plate data into the stride trackers.
    firstLoop(objects, strideTrackers, collectedDataDict)
    
    # Finish up the data produced by the first loop (like completing strides),
    # and produce synthetic load cell data.
    produceSyntheticData(objects, strideTrackers, collectedDataDict)
    
    # Second loop:
    # * Iterate over the IK solution and position the model.
    # * Transform synthetic load cells from the body frame into the ground 
    #   frame.
    # * Read in ground reaction forces and reduce by amount going to
    #   the exoskeleton foot.
    secondLoop(objects, collectedDataDict)
                
    # For analysis, let's sum all the ground frame forces measured by the
    # exoskeleton.  They should (gulp) sum to a reasonable number, no?
    sumLoadCellGroundForces(objects, collectedDataDict)
                
    # We have a bunch of output files to write.
    writeOutputFiles(objects, strideTrackers, collectedDataDict)
                        
    print 'Configure forces script complete'
    
# End mainWorker.
    
def setupInput(objects, timeRange):
    """
    Does the prep work of corralling input quantities and defining the time
    range to operate in.
    """
        
    # Setup the model with OpenSim/SimTK.
    objects.simObjects = MM.getModelAndState(objects.filePaths.modelPath)
    
    # Access the IK motion file.  We're going to use this to get joint
    # coordinates at different points in time.  We're using our script for
    # reading storage files in preference to the modeling.Storage class since
    # I prefer to have the data as Python dictionaries as opposed to the rather
    # obtuse Storage interface.
    objects.ikDict = HSM.readStoMotAsDict(objects.filePaths.ikPath)
    
    # Now let's read in the load/cell force/torque data.
    objects.lcDict = HSM.readStoMotAsDict(objects.filePaths.lcPath)
    
    # And the ground reaction forces.
    objects.grfDict = HSM.readStoMotAsDict(objects.filePaths.grfPath)
        
    # Establish the time parameters for our data crunching.  We're getting
    # a list of times that span the union of the individual time ranges.    
    objects.mainTimeList = TC.createCommonTimeBase(
        [objects.ikDict['time'], 
            objects.lcDict['time'], 
            objects.grfDict['time']],
        allUnique=True,
        minTimeLimit=timeRange.minTime,
        maxTimeLimit=timeRange.maxTime)
        
    if timeRange.errorIfTimeNotInMinMaxRange:
        # Let's error if the common time base does not span the given min and 
        # max time.  An external tool (like BESS) may specify a min and max 
        # time and we need to notify that the input files together do not
        # span that amount of time.
        if timeRange.minTime and timeRange.minTime < objects.mainTimeList[0]:
            raise TimeRangeError('Common time base exceeds min time')
            
        if timeRange.maxTime and timeRange.maxTime > objects.mainTimeList[-1]:
            raise TimeRangeError('Common time base exceeds max time')
        
    # Mop up a few setup items:  after we've created objects from XML and then
    # got the SimTK objects we can do some model specific actions.
    for trackedPosition in objects.trackedPositions.values():
        trackedPosition.setSensorFrameBody(objects.simObjects)
        
        if trackedPosition.trackBodyCOM:
            trackedPosition.setBodyPositionToCOM()
           
    for forceSet in itertools.chain(objects.realLoadCellForceSets.values(), 
        objects.syntheticLoadCellForceSets.values()):
        
        # For load cell for sets, we specify what tracked position should 
        # identify the sensor frame.  It contains the body segment to use.        
        trackedPosition = objects.trackedPositions[
            forceSet.sensorTrackedPositionName]            
        forceSet.setSensorFrameBody(trackedPosition.sensorFrameBody)
            
# End setupInput.
    
def firstLoop(objects, 
    strideTrackers,
    collectedDataDict):
    """
    First loop iteration is going to be concerned with inverse kinematics
    (to get the body positioned properly) and transforming the force data from
    the real load cells.  Since we're positioning the body, we can also 
    calculate all the tracked body positions in the global frame.  We also feed
    the stride tracker with force plate data.
    
    This function uses our iteration method defined in toolsCommon.
    """
    
    def ikCallback(commonTimeIdx, ikIdx):
        # If the IK index changed then it is time to update the model 
        # coordinates.
        MM.setModelCoordinates(objects.simObjects, objects.ikDict, ikIdx)
            
        # If we have moved the model, we can update all the tracked
        # positions in the ground frame.
        updateTrackedPositions(objects)
    
    def lcCallback(commonTimeIdx, lcIdx):
        updateRealLoadCellForces(objects, lcIdx)
        
    def grfCallback(commonTimeIdx, grfIdx):
        # Get the force plate data to feed into the stride tracker.           
        updateGroundReactionForces(objects, grfIdx)
            
    def commonCallback(commonTimeIdx):
        time = objects.mainTimeList[commonTimeIdx]
        # Each time step record new data.  Since this is the first big loop,
        # also record time.
        collectedDataDict['time'].append(time)
        
        for lc in objects.realLoadCellForceSets.values():
            lc.writeAllToDictList(collectedDataDict)
            
        for tp in objects.trackedPositions.values():
            tp.writeAllToDictList(collectedDataDict)
            
        strideTrackers[objects.strideConfig.rightStrideName].addValue(time, 
            objects.grfForceSets[objects.strideConfig.
                rightGroundReactionForceSetName].getForces()[1])
        strideTrackers[objects.strideConfig.leftStrideName].addValue(time, 
            objects.grfForceSets[objects.strideConfig.
                leftGroundReactionForceSetName].getForces()[1])

    TC.iterateMultipleDataSources(objects.mainTimeList, 
        [(objects.ikDict['time'], ikCallback), 
        (objects.lcDict['time'], lcCallback),
        (objects.grfDict['time'], grfCallback)], 
        commonCallback)
            
# End firstLoop.
            
def produceSyntheticData(objects, strideTrackers, collectedDataDict):
    """
    Prepares data necessary to produce the synthetic load cell data and calls
    the worker function to do that.
    """    
    # The stride tracker needs to be told that we are finished and to record
    # the last strides.
    for strideTracker in strideTrackers.values():
        strideTracker.completeStrides()
        
    # Ok, we have crunched the data for real load cells.  Now, let's create 
    # synthetic data for contact points that we don't have instrumented.
    for syntheticLoadCellName, syntheticDataConfigItem in (
        objects.syntheticDataConfig.iteritems()):
        createSyntheticLoadCellData(objects, 
            strideTrackers, 
            collectedDataDict, 
            syntheticLoadCellName,
            syntheticDataConfigItem)
    
# End produceSyntheticData.
            
def secondLoop(objects, collectedDataDict):
    """
    We've created synthetic load cell data, but only
    in the body frame.  We also want that data in the ground frame and we
    shouldn't assume position/angle symmetry when we don't have to.  So,
    let's do another big loop over all the time, positioning the model as
    necessary.  Also, now that we have data for both the right and left side
    we can modify the ground reaction force data to reduce the amount being
    applied to the human.
    """
    
    def ikCallback(commonTimeIdx, ikIdx): 
        # If the IK index changed then it is time to update the model 
        # coordinates.
        MM.setModelCoordinates(objects.simObjects, objects.ikDict, ikIdx)
            
    def grfCallback(commonTimeIdx, grfIdx):
        # Now get the appropriate force plate ground reaction forces from the
        # input file.            
        updateGroundReactionForces(objects, grfIdx)
                    
    def commonCallback(commonTimeIdx):            
        # Ok, each time step we transform the body forces of the synthetic
        # load cells into the global frame.
        updateSyntheticLoadCellForces(objects, 
            commonTimeIdx,
            collectedDataDict)       
                
        # Ok, in the data dictionary we have ground frame forces for the right
        # and left exoskeleton feet.  In concrete force sets, we have the 
        # ground reaction forces.  We want to amend the GRF to account for the 
        # exoskeleton so that the forces applied to the human subject are 
        # smaller.  Note this callback can be called multiple times for each of
        # the grfCallbacks, so make sure we don't modify the stored object
        # here since we might end up doing it multiple times.
        reducedGRF_ForceSets = dict()
        for forceSetName, sourceGRF in objects.grfForceSets.iteritems():
            reducedGRF_ForceSets[forceSetName] = GroundReactionForceSet(
                sourceGRF.forceName,
                sourceGRF.positionName,
                sourceGRF.torqueName,
                sourceGRF.appliedToBodyName,
                sourceGRF.exosekeletonLoadCellName)
            reducedGRF_ForceSets[forceSetName].copy(sourceGRF)
            
        reduceGroundReactionForces(objects,
            commonTimeIdx, 
            collectedDataDict, 
            reducedGRF_ForceSets)
                        
        for grf in reducedGRF_ForceSets.values():
            grf.writeAllToDictList(collectedDataDict)            

    TC.iterateMultipleDataSources(objects.mainTimeList, 
        [(objects.ikDict['time'], ikCallback), 
        (objects.grfDict['time'], grfCallback)],
        commonCallback)
            
# End secondLoop.

def updateGroundReactionForces(objects, grfIdxNew):
    """
    Gets the set of ground reaction forces and torques for the given time
    index.    """
    
    # Pull data out of the ground reaction force dictionary for the current
    # time index and place it into the two objects (one for right and one for
    # left).  Once the data is in concrete objects we'll be able to manipulate
    # it easier downstream.
    for forceSet in objects.grfForceSets.values():
        forceNames = forceSet.fullForceNames
        forceSet.setForces(objects.grfDict[forceNames[0]][grfIdxNew],
            objects.grfDict[forceNames[1]][grfIdxNew],
            objects.grfDict[forceNames[2]][grfIdxNew])
            
        positionNames = forceSet.fullPositionNames
        forceSet.setPositions(objects.grfDict[positionNames[0]][grfIdxNew],
            objects.grfDict[positionNames[1]][grfIdxNew],
            objects.grfDict[positionNames[2]][grfIdxNew])
            
        torqueNames = forceSet.fullTorqueNames
        forceSet.setTorques(objects.grfDict[torqueNames[0]][grfIdxNew],
            objects.grfDict[torqueNames[1]][grfIdxNew],
            objects.grfDict[torqueNames[2]][grfIdxNew])
            
# End updateGroundReactionForces.

def updateRealLoadCellForces(objects, lcIdxNew):
    """
    Gets the load cell forces for the given time index.
    """            
    for lc in objects.realLoadCellForceSets.values():
        fx = objects.lcDict[lc.fullSensorForceNames[0]][lcIdxNew]
        fy = objects.lcDict[lc.fullSensorForceNames[1]][lcIdxNew]
        fz = objects.lcDict[lc.fullSensorForceNames[2]][lcIdxNew]
        lc.setBodyForce(fx, fy, fz)      
        
        tx = objects.lcDict[lc.fullSensorTorqueNames[0]][lcIdxNew]
        ty = objects.lcDict[lc.fullSensorTorqueNames[1]][lcIdxNew]
        tz = objects.lcDict[lc.fullSensorTorqueNames[2]][lcIdxNew]
        lc.setBodyTorque(tx, ty, tz)      
          
        lc.transformToGround(objects)
    
# End updateRealLoadCellForces.

def updateTrackedPositions(objects):
    """
    Transforms all tracked body positions to the ground frame.
    """    
    for tp in objects.trackedPositions.values():
        tp.transformToGround(objects.simObjects)
        
# End updateTrackedPositions.

def updateSyntheticLoadCellForces(objects, dataDictIdx, dataDict):
    """
    Gets the body frame synthetic load cell forces and transforms them to
    ground forces.
    """        
    # Iterate over each of the synthetic load cells.
    for lcName, lc in objects.syntheticLoadCellForceSets.iteritems():
        fx = dataDict[lc.fullBodyForceNames[0]][dataDictIdx]
        fy = dataDict[lc.fullBodyForceNames[1]][dataDictIdx]
        fz = dataDict[lc.fullBodyForceNames[2]][dataDictIdx]        
        lc.setBodyForce(fx, fy, fz)
        
        tx = dataDict[lc.fullBodyTorqueNames[0]][dataDictIdx]
        ty = dataDict[lc.fullBodyTorqueNames[1]][dataDictIdx]
        tz = dataDict[lc.fullBodyTorqueNames[2]][dataDictIdx]        
        lc.setBodyTorque(tx, ty, tz)
        
        lc.transformToGround(objects)
        
        # Only write the ground forces to the dictionary -- the 
        # body forces are already added when we created the synthetic data.
        lc.writeGroundForcesToDictList(dataDict)
        lc.writeGroundTorquesToDictList(dataDict)
                
# End updateSyntheticLoadCellForces.    

def createSyntheticLoadCellData(objects, 
    strideTrackers, 
    dataDict, 
    syntheticLoadCellName,
    syntheticDataConfigItem):
    """
    Creates load cell data for contact points that we don't have instrumented
    with a load cell, usually by assuming symmetry between the right side and
    left side of the model.
    """        
    # We are going to average real load cell data into a histogram.  The bins 
    # are going to be parameterized as a percent of stride.  We'll only look
    # at full strides (for the real load cell -- when we apply the average 
    # values to the synthetic load cell we will do partial strides as well).
    realHistogram = ST.StrideHistogram(range(101))
    
    # Setup the iteration.  We'll do a single forward pass over the
    # data dictionary for time and load cell data and enclose that in a loop
    # over the strides.  Strides should be unique in time and ordered in time,
    # so a single forward iteration through both should be fine.
    timeIndex = 0
    for stride in (
        strideTrackers[syntheticDataConfigItem.realLoadCellStrideName].strides):
        
        # Ignore partial strides -- they can't be used to get average load cell
        # data.
        if not stride.full:
            continue
          
        # Figure out the start time and end time for the stride.  
        startTime = stride.getStartTime()
        endTime = stride.getEndTime()
        
        # Ok, now we can start iterating through the data dictionary, picking
        # up where we left off with the previous stride.
        timeList = dataDict['time']
        for timeIndex in range(timeIndex, len(timeList)):
            # See how the time in this loop relates to the time interval of
            # the stride we're looking at.
            time = timeList[timeIndex]
            if time < startTime:
                # Haven't got to this stride yet -- probably we are at the
                # beginning of the data and the first full stride doesn't occur
                # for a second or two.  Advance time and try again.
                continue
                
            if time > endTime:
                # Finished with this stride -- move onto the next stride.
                break
                
            # Get percentage of the stride that this time relates to.
            stridePercent = ST.StrideTracker.getPercentCompleteByTime(stride,
                time,
                (strideTrackers[syntheticDataConfigItem.realLoadCellStrideName].
                    getFullStrideAverageDurations()),
                False,
                normalizeToFullGait=True)
                
            # If we know what percentage of the full stride the position is,
            # then we can get the histogram bin to put load cell data in.
            bin = realHistogram.getBin(stridePercent)

            # Compose the list of data to enter into the histogram bin.  We'll
            # pull this out of the data dictionary by name, so compose the names
            # that we want.  ex. "r_ankle_ground_fx".
            binList = []
            bodyNames = itertools.chain(
                objects.realLoadCellForceSets[syntheticDataConfigItem.
                    realLoadCellName].fullBodyForceNames,
                objects.realLoadCellForceSets[syntheticDataConfigItem.
                    realLoadCellName].fullBodyTorqueNames)

            for bodyName in bodyNames:                    
                binList.append((bodyName, dataDict[bodyName][timeIndex]))
            
            bin.addValues(binList)
            
    # Now, depending on how many strides we've accumulated and how big our bins
    # are, we may well have bins that don't have data.  So interpolate to fill
    # out empty bins.
    realHistogram.interpolateEmptyBins()
      
    timeIndex = 0
    for strideIdx, stride in enumerate(strideTrackers[syntheticDataConfigItem.
        syntheticLoadCellStrideName].strides):
        # Figure out the start time and end time for the stride.
        startTime = stride.getStartTime()
        endTime = stride.getEndTime()
        
        # Ok, now we can start iterating through the data dictionary, picking
        # up where we left off with the previous stride.
        timeList = dataDict['time']
        for timeIndex in range(timeIndex, len(timeList)):
            # See how the time in this loop relates to the time interval of
            # the stride we're looking at.
            time = timeList[timeIndex]
            if time < startTime:
                # Haven't got to this stride yet, though I don't expect this
                # condition to be struck with the sythetic load cell since 
                # we're also looking at partial strides I'd expect that the 
                # first time matches with the start time of the first 
                # (partial) stride.
                continue
                
            if time > endTime:
                # Finished with this stride -- move onto the next stride.
                break

            # Get percentage of the stride that this time relates to. 
            stridePercent = ST.StrideTracker.getPercentCompleteByTime(stride,
                time,
                (strideTrackers[syntheticDataConfigItem.syntheticLoadCellStrideName].
                    getFullStrideAverageDurations()),
                strideIdx == 0,
                normalizeToFullGait=True)
                        
            # If we know what percentage of the full stride the position is,
            # then we can get the real load cell histogram bin to get load cell 
            # data from.
            bin = realHistogram.getBin(stridePercent)
                
            applySyntheticLoadCellData(bin, 
                dataDict, 
                syntheticDataConfigItem.forceRotationMatrix,
                objects.realLoadCellForceSets[syntheticDataConfigItem.
                    realLoadCellName].fullBodyForceNames,
                objects.syntheticLoadCellForceSets[syntheticLoadCellName].
                    fullBodyForceNames)
                    
            applySyntheticLoadCellData(bin, 
                dataDict, 
                syntheticDataConfigItem.torqueRotationMatrix,
                objects.realLoadCellForceSets[syntheticDataConfigItem.
                    realLoadCellName].fullBodyTorqueNames,
                objects.syntheticLoadCellForceSets[syntheticLoadCellName].
                    fullBodyTorqueNames)
            
# End createSyntheticLoadCellData.    

def applySyntheticLoadCellData(bin, 
    dataDict, 
    rotationMatrix, 
    realNames, 
    syntheticNames):
    """
    This just gets the last mile of the synthetic data process.  Here we take
    data out of the real load cell histogram bin, transform it to a synthetic
    load cell frame and then write it out.
    """
    realVector = modeling.Vec3()
    for idx, realName in enumerate(realNames):
        realVector.set(idx, bin.statistics[realName].getAverage())
        
    syntheticVector = modeling.Vec3()
    TC.rotateVector(realVector, rotationMatrix, syntheticVector)
    
    for idx, syntheticName in enumerate(syntheticNames):
        dataDict[syntheticName].append(syntheticVector.get(idx))
        
# End applySyntheticLoadCellData.

def sumLoadCellGroundForces(objects, collectedDataDict):
    """
    Goes through the data dictionary and picks out all the load cell
    ground frame forces and sums on the x, y, and z dimensions.
    """    
    # Assemble the names of ground frame forces in each of the coordinate 
    # directions.
    xForceNames = []
    yForceNames = []
    zForceNames = []
    
    for loadCellForceSet in itertools.chain(
        objects.realLoadCellForceSets.values(), 
        objects.syntheticLoadCellForceSets.values()):
        
        xForceNames.append(loadCellForceSet.fullGroundForceNames[0])
        yForceNames.append(loadCellForceSet.fullGroundForceNames[1])
        zForceNames.append(loadCellForceSet.fullGroundForceNames[2])

    # Sanity check here that we're only summing load cells -- I'd hate to
    # accidentally pull in some data columns we're not supposed to.
    numLoadCells = (len(objects.realLoadCellForceSets) + 
        len(objects.syntheticLoadCellForceSets))
        
    if not all(numLoadCells == len(names) for names in 
        (xForceNames, yForceNames, zForceNames)):
        raise ValueError('Something wrong in summing load cell ground forces')
    
    # Bundle together a few different variables for each coordinate direction.
    forceDirections = ((xForceNames, 'sum_lc_fx', 'avg_lc_fx'),
        (yForceNames, 'sum_lc_fy', 'avg_lc_fy'),
        (zForceNames, 'sum_lc_fz', 'avg_lc_fz'))
        
    # Now let's loop over every row in the data dictionary and sum each of
    # the components.
    for idx in range(len(collectedDataDict['time'])):
        for fd in forceDirections:
            # Get a sum of all the forces in this one direction for this one
            # time step.
            sum = 0.0
            for forceName in fd[0]:
                sum += collectedDataDict[forceName][idx]
            
            collectedDataDict[fd[1]].append(sum)
            
            # Let's keep a running average of the sum.  I would expect each
            # component to level out to a steady state value.
            if idx == 0:
                avg = sum
            else:
                avg = (collectedDataDict[fd[2]][idx - 1] * idx + sum) / (idx + 1)
                
            collectedDataDict[fd[2]].append(avg)
            
# End sumLoadCellGroundForces.
    
def reduceGroundReactionForces(objects,
    dataDictIdx, 
    dataDict, 
    reducedGRF_ForceSets):
    """
    Reduce the ground reaction forces applied to the human subject by the amount
    passed to the exoskeleton.
    """    
    # The data dictionary has all the load cell info that we need to modify
    # the ground reaction forces for the human subject.  The full force plate
    # data is currently held in the reducedGRF_ForceSets object.
    for grfForceSet in reducedGRF_ForceSets.values():        
        # A exoskeleton load cell doesn't necessarily have to be included, 
        # though for the RE2 BESS exoskeleton it is.
        if not grfForceSet.exosekeletonLoadCellName:
            continue
    
        # Get some easy references so we don't have to use the Vec3 API.
        grf_fx, grf_fy, grf_fz = grfForceSet.getForces()
        grf_tx, grf_ty, grf_tz = grfForceSet.getTorques()
        
        # We only modify the force plate ground reaction forces when we deem
        # that we are in the stance period of the stride.  Do that by looking
        # at the y-component of the ground reaction force which runs from
        # about 0 to 1200 N (for a ~200 lb subject).  Let's just use a threshold
        # value which filters out most of the swing period.
        if grf_fy < 20:
            continue
            
        # Ok, if we're standing then we can subtract some amount from each of
        # the components.  Get the exoskeleton forces.
        if grfForceSet.exosekeletonLoadCellName in (objects.
            realLoadCellForceSets):
            # Use the load cell forces that are transformed to the point of
            # application.
            forceNames = (objects.realLoadCellForceSets[
                grfForceSet.exosekeletonLoadCellName].
                    fullAppliedForceNames)
            torqueNames = (objects.realLoadCellForceSets[
                grfForceSet.exosekeletonLoadCellName].
                    fullAppliedTorqueNames)
        elif grfForceSet.exosekeletonLoadCellName in (objects.
            syntheticLoadCellForceSets):
            forceNames = (objects.syntheticLoadCellForceSets[
                grfForceSet.exosekeletonLoadCellName].
                    fullAppliedForceNames)
            torqueNames = (objects.syntheticLoadCellForceSets[
                grfForceSet.exosekeletonLoadCellName].
                    fullAppliedTorqueNames)
        else:
            raise ValueError('Could not find load cell when reducing GRF')
                    
        es_fx = dataDict[forceNames[0]][dataDictIdx]
        es_fy = dataDict[forceNames[1]][dataDictIdx]
        es_fz = dataDict[forceNames[2]][dataDictIdx]
        
        # Todo: check the signs here after we confirm our experimental data
        # is correct (i.e. the foot sensor data really is the foot sensor).
        if abs(grf_fx) > 20:
            grf_fx += es_fx
            
        # Todo: figure out if we should account for the 1070 g of the foot 
        # bracket.  I think we should:  basically add +10.5 N to es_fy. 
        # We bound the contribution of the exoskeleton here based on the
        # observation 
        grf_fy += min(es_fy, 0)
        
        if abs(grf_fz) > 20:
            grf_fz += es_fz
        
        grfForceSet.setForces(grf_fx, grf_fy, grf_fz)
        
        es_tx = dataDict[torqueNames[0]][dataDictIdx]
        es_ty = dataDict[torqueNames[1]][dataDictIdx]
        es_tz = dataDict[torqueNames[2]][dataDictIdx]
        
        # Modify the torques in the same way.
        # Todo: check this data when we figure out which one is the foot sensor.
        grf_tx += es_tx
        grf_ty += es_ty
        grf_tz += es_tz
        
        grfForceSet.setTorques(grf_tx, grf_ty, grf_tz)
        
# End reduceGroundReactionForces.

def writeOutputFiles(objects, strideTrackers, collectedDataDict):
    """
    Handles the writing out of output files near the end of our script.
    """
    # We've calculated strides (and accounted for completing the last strides) 
    # -- serialize and output to an XML file for use by downstream tools.  
    ST.StrideTracker.writeDictToXmlFile(strideTrackers,
        objects.filePaths.stridesXmlOutPath)
        
    # Also create a time-based representation of strides and write it out 
    # to a STO file so strides can be visualized in the plot window.
    HSM.writeDictToStoMot(
        ST.StrideTracker.writeDictToStoDict(strideTrackers),
        objects.filePaths.stridesStoOutPath)    
        
    # We've been collecting everything into a big dictionary.  Write it out.
    HSM.writeDictToStoMot(collectedDataDict, 
        objects.filePaths.forcesOutPath)
        
    # Let's generate an external loads XML file that OpenSim can use for use
    # in all of its forward simulation tools (ID, CMC, etc.).
    generateExternalLoadsFile(objects)
        
# End writeOutputFiles.

def generateExternalLoadsFile(objects):
    """
    Write out an "external loads" config file.  This is the XML that OpenSim
    uses in all its forward simulation tools to identify which forces are
    applied to what part of the subject.
    """
    # We're going to co-opt the OpenSim objects to make it easy to generate
    # the XML -- this should make it easier to deal with future changes to
    # OpenSim since we're using the OpenSim concrete objects rather than trying
    # to reproduce the XML schema ourselves.
    
    # Start by creating an external loads object.  We'll seed it (possibly)
    # with a pre-existing XML file in order to pick up any settings already
    # set by the user on a previous run of the OpenSim tools.  (For example,
    # IK filtering frequency.)
    try:
        externalLoads = modeling.ExternalLoads(objects.simObjects.model,
            objects.filePaths.externalLoadsOutPath)
    except IOException, e:
        # In this case the XML file does not exist, so fall back to just 
        # creating a new object.  When we print to XML we will be creating
        # the file.
        externalLoads = modeling.ExternalLoads(objects.simObjects.model)
                
    # Clear out any previous force objects that were already listed there.
    externalLoads.clearAndDestroy()
    
    # I don't think the "name" attribute in the XML is important but fill it
    # in anyways if it doesn't exist, just as a placeholder if nothing else.
    if externalLoads.getName() == '':
        externalLoads.setName('BESS_external_loads_generated_file')
    
    # Set data file that contains all the raw force data..
    externalLoads.setDataFileName(objects.filePaths.forcesOutPath)
    
    # Set the IK file.  I don't see any reason why this would be different from
    # the IK file we've been using to generate the external forces in this
    # script.
    externalLoads.setExternalLoadsModelKinematicsFileName(
        objects.filePaths.ikPath)
    
    # Ok, let's now start filling in the list of force objects.  Each one of
    # these also has a concrete representation in OpenSim Java that we can use.
    # We'll add an object for each ground reaction force set as well as real
    # and synthetic load cells.
    for grfsName, grfs in objects.grfForceSets.iteritems():
        externalForce = modeling.ExternalForce()
        externalForce.setName(grfsName)
        externalForce.setAppliedToBodyName(grfs.appliedToBodyName)
        externalForce.setPointExpressedInBodyName('ground')
        externalForce.setForceExpressedInBodyName('ground')
        externalForce.setForceIdentifier(grfs.forceName)
        externalForce.setPointIdentifier(grfs.positionName)
        externalForce.setTorqueIdentifier(grfs.torqueName)
        externalLoads.cloneAndAppend(externalForce)
    
    for lcfsName, lcfs in itertools.chain(
        objects.realLoadCellForceSets.iteritems(), 
        objects.syntheticLoadCellForceSets.iteritems()):
        
        # Check if the load cell doesn't measure a contact point with the
        # human subject.  In this case that load cell force shouldn't be
        # included in the external loads file.
        if lcfs.appliedToBodyName == 'ground':
            continue
            
        externalForce = modeling.ExternalForce()
        externalForce.setName(lcfsName)
        externalForce.setAppliedToBodyName(lcfs.appliedToBodyName)
        externalForce.setPointExpressedInBodyName('ground')
        externalForce.setForceExpressedInBodyName('ground')
        externalForce.setForceIdentifier(lcfs.appliedForceName)
        externalForce.setTorqueIdentifier(lcfs.appliedTorqueName)
        
        # The application point of the force is given by a tracked position.
        if lcfs.appliedToTrackedPositionName:
            trackedPosition = objects.trackedPositions[
                lcfs.appliedToTrackedPositionName]
            externalForce.setPointIdentifier(trackedPosition.groundPositionName)
            
        externalLoads.cloneAndAppend(externalForce)
        
    # Write out the XML.
    externalLoads.print(objects.filePaths.externalLoadsOutPath)
    
# End generateExternalLoadsFile.

