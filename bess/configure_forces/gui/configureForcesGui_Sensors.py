

################################################################################
#
# This script handles the "Sensors" tab of the configure forces GUI.
#
################################################################################

import itertools
import os
import sys

from javax.swing import DefaultListModel

# Add the parent "bess" directory to the Python module search path so that we
# can import modules from different directories.  Subsequent imports of BESS
# scripts may use reload() to ensure that any changes during development get
# reloaded by the interpreter.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
if bessDir not in sys.path:
    sys.path.append(bessDir)

import utilities.handleStoMot as HSM; reload(HSM)
import utilities.toolsCommon as TC; reload(TC)
import configure_forces.gui.configureForcesGui_EditSensor as EditSensor; reload(EditSensor)
import configure_forces.gui.configureForcesGui_TabBase as TB; reload(TB)
    
class SensorsTab(TB.TabBase):

    def __init__(self, main):
        """
        By this point, we can expect that the GUI panels have been created.
        So this function can act as a GUI setup as well.
        """
        
        # Get a backpointer to the main object -- this is where we will get
        # the GUI elements and underlying data.
        self.main = main

        self.main.mainPanel.sensorsDataFileTextField.focusLost = (
            self.handleDataFileTextFieldFocusLost)
            
        # Add in a click handler for the various buttons on this tab.
        self.main.mainPanel.sensorsDataFileBrowseButton.actionPerformed = (
            self.handleDataFileBrowseButtonClick)
        self.main.mainPanel.sensorsAddSensorButton.actionPerformed = (
            self.handleAddSensorButtonClick)
        self.main.mainPanel.sensorsRemoveSensorButton.actionPerformed = (
            self.handleRemoveSensorButtonClick)
        self.main.mainPanel.sensorsEditSensorButton.actionPerformed = (
            self.handleEditSensorButtonClick)
    
        # Initialize the list to have an empty list model, but one that is
        # subsequently mutable.
        self.main.mainPanel.sensorsSensorList.setModel(DefaultListModel())
                
    # End __init__.
    
    def setViewFromModel(self):
        """
        Completely repopulates GUI from member data.
        
        Effectively this is a "virtual" override -- the main class expects all
        the tab classes to have this function.
        """
        
        self.onChangeSensorsDataFile()
        self.populateSensorsList()
        
    # End setViewFromModel.
    
    def handleDataFileTextFieldFocusLost(self, event):
        """
        When losing focus we update the model.
        """
        
        self.onChangeSensorsDataFile(self.main.mainPanel.
            sensorsDataFileTextField.text, False)
        
    # End handleDataFileTextFieldFocusLost.            
    
    def handleDataFileBrowseButtonClick(self, event):
        """
        Event handler for browse button to select data file.
        """
        
        try:
            forcesPath = TC.getSingleFilePath(
                'Select force/torque sensor data file',
                TC.FileFilter('Sensor data files (.sto,.mot)', 'sto,mot'))
        except:
            # If nothing is selected, an exception is thrown.
            return
        
        # Anytime we set/change the file path, call the on-change function.
        self.onChangeSensorsDataFile(forcesPath)
        
    # End handleDataFileBrowseButtonClick.
        
    def handleAddSensorButtonClick(self, event):
        
        if not self.main.checkModelLoadedAndShowError():
            return
            
        editDialog = EditSensor.EditRealSensorDialog(self.main, 
            initialSensorName=None)
        editDialog.show()
        
        if editDialog.nameAndLoadCell:
            self.onAddSensor(editDialog.nameAndLoadCell)
        
    # End handleAddSensorButtonClick.
        
    def handleRemoveSensorButtonClick(self, event):
        # Figure out what item has been selected.
        idx, name = self.getSensorsListSelection()
        if idx == -1:
            return
            
        self.onRemoveSensor(name)
        
    # End handleRemoveSensorButtonClick.
        
    def handleEditSensorButtonClick(self, event):
        
        if not self.main.checkModelLoadedAndShowError():
            return
            
        # Figure out what item has been selected.
        idx, name = self.getSensorsListSelection()
        if idx == -1:
            return
        
        editDialog = EditSensor.EditRealSensorDialog(self.main, 
            initialSensorName=name)
        editDialog.show()
        
        if editDialog.nameAndLoadCell:
            # Ok, successful completion of the dialog.  We need to handle the
            # case where the name changed though.
            if name != editDialog.nameAndLoadCell[0]:
                # Don't do a redraw since we will do that on the subsequently
                # add operation.
                self.onRemoveSensor(name, False)                
                self.onAddSensor(editDialog.nameAndLoadCell)
            else:
                self.onModifiedSensor(editDialog.nameAndLoadCell)
        
    # End handleEditSensorButtonClick.
        
    def onChangeSensorsDataFile(self, forcesPath=None, validateFile=False):
        """
        Called anytime we set or change the force/torque sensor data file.
        """
        
        # Attempt to read in the headers from this file.  I was planning on
        # using these to limit the set of names for load cell input sensor
        # force names from the column headers in the file, but that is probably
        # overkill for now.
        try:
            if forcesPath and validateFile:
                rawColumnNames = HSM.readStoMotColumnNamesAsList(forcesPath)
            elif validateFile:
                rawColumnNames = HSM.readStoMotColumnNamesAsList(
                    self.main.model.filePaths.lcPath)
        except:
            print 'Unable to load force/torque sensor data file'
            return 
                
        # Update the model data.
        if forcesPath:
            self.main.model.filePaths.lcPath = forcesPath
        
        # Change the view to correspond to the new model data.
        self.populateSensorsDataFileTextField()
        
    # End onChangeSensorsDataFile.
        
    def populateSensorsDataFileTextField(self):
        """
        Fills in the forces data file text field from model data.
        """
        
        self.main.mainPanel.sensorsDataFileTextField.text = (
            self.main.model.filePaths.lcPath)
            
    # End populateSensorsDataFileTextField.
        
    def populateSensorsList(self):
        """
        Adds the name of each load cell to the list.
        """
                
        # Iterate through the names of the load cells.
        listModel = self.main.mainPanel.sensorsSensorList.model
        listModel.clear()
        
        for name in sorted(self.main.model.realLoadCellForceSets):
            listModel.addElement(name)
            
    # End populateSensorsList.
    
    def getSensorsListSelection(self):
        list = self.main.mainPanel.sensorsSensorList
        
        # If there is no selection, will return -1, None.
        return list.getSelectedIndex(), list.getSelectedValue()        
    
    # End getSensorsListSelection.
    
    def onAddSensor(self, nameAndLoadCell, redraw=True):
        # Add the entry to the model data.
        self.main.model.realLoadCellForceSets[nameAndLoadCell[0]] = (
            nameAndLoadCell[1])
    
        # Simply redraw the list box from model data.
        if redraw:
            self.populateSensorsList()
            
    # End onAddSensor.
            
    def onRemoveSensor(self, name, redraw=True):
            
        del(self.main.model.realLoadCellForceSets[name])
            
        # Simply redraw the list box from model data.
        if redraw:
            self.populateSensorsList()
        
    # End onRemoveSensor.
    
    def onModifiedSensor(self, nameAndLoadCell):
        # We could probably get away with using the "onAddSensor" function,
        # but it might be helpful to keep these separate.
        self.main.model.realLoadCellForceSets[nameAndLoadCell[0]] = (
            nameAndLoadCell[1])
            
    # End onModifiedSensor.
    
# End SensorsTab.


