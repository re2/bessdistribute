

################################################################################
#
# This script handles the add or editing of a tracked position in the 
# Configure Forces UI.
#
################################################################################

import os
import sys

from org.opensim import modeling

from javax.swing import DefaultComboBoxModel, JCheckBox, JComboBox
from javax.swing import JFrame, JOptionPane

# Add the parent "bess" directory to the Python module search path so that we
# can import modules from different directories.  Subsequent imports of BESS
# scripts may use reload() to ensure that any changes during development get
# reloaded by the interpreter.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
if bessDir not in sys.path:
    sys.path.append(bessDir)
    
import utilities.toolsCommon as TC; reload(TC)
import configure_forces.configureForces_Support as CF; reload(CF)

TC.importJar(os.path.join(bessDir, 'lib', 'ConfigureForcesGuiPanels.jar'))
from bess.gui.configureforces import EditPositionDialog as ViewEditPositionDialog

class EditPositionPanel:
    """
    This is the presenter for the "Edit Position" panel.  This is used by a
    wrapper dialog, and ultimately by the "Positions" tab of the Configure 
    Forces UI.
    """
    
    def __init__(self, main, initialPositionName, view):
                
        # Get a backpointer to the main object -- this is where we will get
        # the GUI elements and underlying data.
        self.main = main
        
        # Whether or not we are creating a new position or editing an existing
        # one.
        self.initialPositionName = initialPositionName
                
        # This is the actual Java view object.
        self.view = view
        
        # Ultimately this is going to be the record of the new or modified
        # tracked position object on successful completion of this dialog.
        self.nameAndTrackedPosition = None
        
        # Setup a button handler for setting defaults for frame names.
        self.view.setDefaultNamesButton.actionPerformed = (
            self.setDefaultNamesHandler)
            
        # Setup a checkbox handler for selecting tracking of a body COM.
        self.view.positionTracksBodyCOM_Check.actionPerformed = (
            self.bodyCOM_CheckHandler)
                    
        # Call a worker to fill-in the initial values of the GUI.
        self.populateFields()
        
    # End __init__.
    
    def populateFields(self):
        """
        Called at the beginning to populate the fields in the GUI.
        """
        
        # Get the set of all body segments to populate the combo box.
        bodyNames = []
        bodySet = self.main.simObjects.model.getBodySet()     
        for idx in range(bodySet.getSize()):
            bodyNames.append(bodySet.get(idx).getName())
            
        # Don't sort the body names here -- list them in the same order as
        # OpenSim gives them to help with look/feel consistency between this
        # tool and OpenSim.
        self.view.bodySegmentCombo.setModel(DefaultComboBoxModel(bodyNames))
        
        # At this point, if we are adding a new position we don't have anything
        # more to populate.
        if not self.initialPositionName:
            return
            
        # If this is an editing job, then we can fill in the data as 
        # appropriate.  Check that the model actually contains the tracked
        # position.
        if self.initialPositionName not in self.main.model.trackedPositions:
            print 'Could not find named tracked position'
            return
            
        # Get a reference to the tracked position.
        position = self.main.model.trackedPositions[self.initialPositionName]
        
        self.view.positionNameTextField.text = self.initialPositionName
        self.view.bodyNameTextField.text = position.bodyPositionName
        self.view.groundNameTextField.text = position.groundPositionName
        
        # Attempt to set the combo box to the appropriate value.
        self.view.bodySegmentCombo.setSelectedItem(position.bodySegmentName)
        
        # Set the "track COM" check box and call the handler to act upon
        # its value.
        self.view.positionTracksBodyCOM_Check.selected = position.trackBodyCOM
        self.onChangePositionTracksBodyCOM_Check()
        
        # Set the values of position offset.
        self.view.xBodyOffsetTextField.text = str(position.pBody.get(0))
        self.view.yBodyOffsetTextField.text = str(position.pBody.get(1))
        self.view.zBodyOffsetTextField.text = str(position.pBody.get(2))
        
    # End populateFields.
    
    def validateStateAndLatch(self):
        """
        Checks the data is valid and saves it to member data.
        
        Presumably the dialog presenter will call this when the user hits
        the 'OK' button.
        """
        
        # Good practice to clear out the result before using it for indicating
        # success.
        self.nameAndTrackedPosition = None
        
        try:
            # Go through each of the fields and make sure they have data.
            if self.view.positionNameTextField.text == '':
                raise ValueError('Position name is empty.')
            
            if self.view.bodyNameTextField.text == '':
                raise ValueError('Body frame name is empty.')
            
            if self.view.groundNameTextField.text == '':
                raise ValueError('Ground frame name is empty.')
            
            # Only validate offsets if we are using them.
            if not self.view.positionTracksBodyCOM_Check.selected:
                if not TC.isTextValidFloat(self.view.xBodyOffsetTextField.text):
                    raise ValueError('X-offset is not a valid number.')
                
                if not TC.isTextValidFloat(self.view.yBodyOffsetTextField.text):
                    raise ValueError('Y-offset is not a valid number.')
                
                if not TC.isTextValidFloat(self.view.zBodyOffsetTextField.text):
                    raise ValueError('Z-offset is not a valid number.')
                    
            # Make sure the name of the position doesn't clash with an existing
            # name.  Of course don't worry if we are editing a position and the
            # name stays the same...
            if (self.view.positionNameTextField.text != self.initialPositionName
                and (self.view.positionNameTextField.text in 
                    self.main.model.trackedPositions)):
                raise ValueError('Position name already exists.')
                
        except ValueError, error:
            JOptionPane.showMessageDialog(self.main.frame,
                str(error),
                "Error",
                JOptionPane.ERROR_MESSAGE);
                
            return False
                
        # If we're here then all data is valid.  Latch and return True.
        # Prepare the vector.
        pBody = modeling.Vec3(0.0)
        if not self.view.positionTracksBodyCOM_Check.selected:
            pBody.set(0, float(self.view.xBodyOffsetTextField.text))
            pBody.set(1, float(self.view.yBodyOffsetTextField.text))
            pBody.set(2, float(self.view.zBodyOffsetTextField.text))
            
        trackedPosition = CF.TrackedPosition(self.view.bodyNameTextField.text, 
            self.view.groundNameTextField.text,
            self.view.bodySegmentCombo.getSelectedItem(),
            self.view.positionTracksBodyCOM_Check.selected,
            pBody)
            
        # The latched value is actually a tuple, combining the position name
        # with the Tracked Position object (since the TP class doesn't hold
        # the name).
        self.nameAndTrackedPosition = (self.view.positionNameTextField.text,
            trackedPosition)
            
        return True
        
    # End validateStateAndLatch.
        
    def setDefaultNamesHandler(self, event):
    
        # Take the position name and append standard suffixes for each.
        positionName = self.view.positionNameTextField.text
        
        self.view.bodyNameTextField.text = positionName + '_body_p'
        self.view.groundNameTextField.text = positionName + '_ground_p'
    
    # End setDefaultNamesHandler.
    
    def bodyCOM_CheckHandler(self, event):
        self.onChangePositionTracksBodyCOM_Check()
        
    # End bodyCOM_CheckHandler.
    
    def onChangePositionTracksBodyCOM_Check(self):
        enableOffsetFields = (
            not self.view.positionTracksBodyCOM_Check.selected)
           
        self.view.xBodyOffsetTextField.enabled = enableOffsetFields
        self.view.yBodyOffsetTextField.enabled = enableOffsetFields
        self.view.zBodyOffsetTextField.enabled = enableOffsetFields
        
    # End onChangePositionTracksBodyCOM_Check.
        
# End EditPositionPanel.

class EditPositionDialog:
    """
    This is the wrapper for the edit tracked position frame.
    """
    
    def __init__(self, main, initialPositionName=None):
    
        # Get a backpointer to the main object -- this is where we will get
        # the GUI elements and underlying data.
        self.main = main
        
        # Whether or not we are creating a new position or editing an existing
        # one.
        self.initialPositionName = initialPositionName
        
        # Ultimately this is going to be the record of the new or modified
        # tracked position object on successful completion of this dialog.
        self.nameAndTrackedPosition = None
                
    # End __init__.
    
    def show(self):
        """
        Show the modal dialog.
        """
        
        dlg = ViewEditPositionDialog(self.main.frame, True)
        dlg.title = 'Add/Edit Tracked Position'
        dlg.defaultCloseOperation = JFrame.DISPOSE_ON_CLOSE
        dlg.windowClosing = self.closingHandler
        
        # The large guts of this frame are in the contained edit position panel.
        # Our presenters mimic the structure of the Java view objects.
        editPanel = EditPositionPanel(self.main, 
            self.initialPositionName,
            dlg.editPositionPanelView)  
        
        # Add in callbacks for the Ok and Cancel buttons.
        dlg.okButton.actionPerformed = lambda event: self.okHandler(event, dlg, editPanel)
        dlg.cancelButton.actionPerformed = lambda event: self.cancelHandler(event, dlg)
                
        # Show the modal dialog.
        dlg.visible = True
        
    # End show.
    
    def okHandler(self, event, dlg, panel):
        
        # See if the data is valid.  If not, the panel will throw up an
        # error dialog and we want to give them a chance to fix, so we don'tell
        # close the dialog in that case.
        if not panel.validateStateAndLatch():
            return
            
        self.nameAndTrackedPosition = panel.nameAndTrackedPosition
        dlg.visible = False
    
    # End okHandler.
    
    def cancelHandler(self, event, dlg):
    
        # For now, this is a no-op (other than closing the window!).        
        dlg.visible = False
    
    # End cancelHandler.
    
    def closingHandler(self, event):
    
        # For now, this is a no-op.
        pass
        
    # End closingHandler.
    
# End EditPositionDialog.

