

################################################################################
#
# This script handles the "General" tab of the configure forces GUI.
#
################################################################################

import os
import sys

from javax.swing import JOptionPane

# Add the parent "bess" directory to the Python module search path so that we
# can import modules from different directories.  Subsequent imports of BESS
# scripts may use reload() to ensure that any changes during development get
# reloaded by the interpreter.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
if bessDir not in sys.path:
    sys.path.append(bessDir)

import utilities.modelManipulation as MM; reload(MM)
import utilities.toolsCommon as TC; reload(TC)
import configure_forces.gui.configureForcesGui_TabBase as TB; reload(TB)
    
class GeneralTab(TB.TabBase):

    def __init__(self, main):
        """
        By this point, we can expect that the GUI panels have been created.
        So this function can act as a GUI setup as well.
        """
        
        # Get a backpointer to the main object -- this is where we will get
        # the GUI elements and underlying data.
        self.main = main

        self.main.mainPanel.generalModelFileTextField.focusLost = (
            self.handleModelFileTextFieldFocusLost)
        self.main.mainPanel.generalIkFileTextField.focusLost = (
            self.handleIkFileTextFieldFocusLost)
        (self.main.mainPanel.generalExternalLoadsSpecificationTextField.
            focusLost) = self.handleExternalLoadsSpecificationTextFieldFocusLost
        self.main.mainPanel.generalExternalLoadsDataTextField.focusLost = (
            self.handleExternalLoadsDataTextFieldFocusLost)
        
        # Add in a click handler for the various buttons on this tab.
        self.main.mainPanel.generalModelFileBrowseButton.actionPerformed = (
            self.handleModelFileBrowseButtonClick)
        self.main.mainPanel.generalIkFileBrowseButton.actionPerformed = (
            self.handleIkFileBrowseButtonClick)
        (self.main.mainPanel.generalExternalLoadsSpecificationBrowseButton.
            actionPerformed) = (
                self.handleExternalLoadsSpecificationBrowseButtonClick)
        (self.main.mainPanel.generalExternalLoadsDataBrowseButton.
            actionPerformed) = self.handleExternalLoadsDataBrowseButtonClick
        
        self.updateFileSpecifications()
        
    # End __init__.
    
    def setViewFromModel(self):
        """
        Completely repopulates GUI from member data.
        
        Effectively this is a "virtual" override -- the main class expects all
        the tab classes to have this function.
        """
        
        self.updateFileSpecifications()
        self.onChangeModelFile()
        self.onChangeIkFile()
        self.onChangeExternalLoadsSpecificationFile()
        self.onChangeExternalLoadsDataFile()
        
    # End setViewFromModel.
    
    def updateFileSpecifications(self):
        """
        If the user has locked down or specified an input file we need to change
        some component properties.
        """
        
        TB.updateWidgetsToFileSpecification(
            self.main.specifiedModelFile,
            self.main.mainPanel.generalModelFileLabel,
            self.main.mainPanel.generalModelFileTextField,
            self.main.mainPanel.generalModelFileBrowseButton)

        TB.updateWidgetsToFileSpecification(
            self.main.specifiedIkFile,
            self.main.mainPanel.generalIkFileLabel,
            self.main.mainPanel.generalIkFileTextField,
            self.main.mainPanel.generalIkFileBrowseButton)
            
        TB.updateWidgetsToFileSpecification(
            self.main.specifiedExternalLoadSpecificationFile,
            self.main.mainPanel.generalExternalLoadsSpecificationLabel,
            self.main.mainPanel.generalExternalLoadsSpecificationTextField,
            self.main.mainPanel.generalExternalLoadsSpecificationBrowseButton)
                    
        # If the model file and ik file are both meant to be invisible, just
        # hide the whole panel they're on.  This ensures we don't have
        # any extra vertical padding on this tab.
        if (self.main.specifiedModelFile and not self.main.specifiedModelFile.visible) and (
            self.main.specifiedIkFile and not self.main.specifiedIkFile.visible):
            self.main.mainPanel.generalInputPanel.visible = False
        else:
            self.main.mainPanel.generalInputPanel.visible = True
            
        # If the external loads specification file is meant to be invisible,
        # also hide the enclosing panel as well.  This ensures we don't have
        # any extra vertical padding on this tab.
        if (self.main.specifiedExternalLoadSpecificationFile and not 
            self.main.specifiedExternalLoadSpecificationFile.visible):
            self.main.mainPanel.generalExternalLoadsSpecificationPanel.visible = False
        else:
            self.main.mainPanel.generalExternalLoadsSpecificationPanel.visible = True
        
    # End updateFileSpecifications.
    
    def handleModelFileTextFieldFocusLost(self, event):
    
        self.onChangeModelFile(
            self.main.mainPanel.generalModelFileTextField.text)
    
    # End handleModelFileTextFieldFocusLost.
    
    def handleIkFileTextFieldFocusLost(self, event):
    
        self.onChangeIkFile(
            self.main.mainPanel.generalIkFileTextField.text)
    
    # End handleIkFileTextFieldFocusLost.
    
    def handleExternalLoadsSpecificationTextFieldFocusLost(self, event):
    
        self.onChangeExternalLoadsSpecificationFile(
            self.main.mainPanel.generalExternalLoadsSpecificationTextField.text)
    
    # End handleExternalLoadsSpecificationTextFieldFocusLost.
    
    def handleExternalLoadsDataTextFieldFocusLost(self, event):
    
        self.onChangeExternalLoadsDataFile(
            self.main.mainPanel.generalExternalLoadsDataTextField.text)
    
    # End handleExternalLoadsDataTextFieldFocusLost.
    
    def handleModelFileBrowseButtonClick(self, event):
        """
        Event handler for browse button to select a model file.
        """
        
        try:
            modelPath = TC.getSingleFilePath(
                'Select OpenSim model file', 
                TC.FileFilter('OpenSim model (.osim)', 'osim'))
        except:
            # If nothing is selected, an exception is thrown.
            return
        
        # Anytime we set/change the file path, call the on-change function.
        self.onChangeModelFile(modelPath)
        
    # End handleModelFileBrowseButtonClick.
    
    def handleIkFileBrowseButtonClick(self, event):
        """
        Event handler for browse button to select an IK file.
        """
        
        try:
            ikPath = TC.getSingleFilePath(
                'Select inverse kinematics file',
                TC.FileFilter('IK MOT files (.mot)', 'mot'))
        except:
            # If nothing is selected, an exception is thrown.
            return
            
        # Anytime we set/change the file path, call the on-change function.
        self.onChangeIkFile(ikPath)
        
    # End handleIkFileBrowseButtonClick.
                
    def handleExternalLoadsSpecificationBrowseButtonClick(self, event):
        """
        Event handler for browse button to select an external loads
        specification file.
        """
        
        try:
            specificationPath = TC.getSingleFilePath(
                'Select external loads XML specification file',
                TC.FileFilter('External loads config file (.xml)', 'xml'))
        except:
            # If nothing is selected, an exception is thrown.
            return
            
        # Anytime we set/change the file path, call the on-change function.
        self.onChangeExternalLoadsSpecificationFile(specificationPath)
        
    # End handleExternalLoadsSpecificationBrowseButtonClick.
             
    def handleExternalLoadsDataBrowseButtonClick(self, event):
        """
        Event handler for browse button to select an external loads
        data file.
        """
        
        try:
            dataPath = TC.getSingleFilePath(
                'Select external loads data file',
                TC.FileFilter('External loads data file (.sto)', 'sto'))
        except:
            # If nothing is selected, an exception is thrown.
            return
            
        # Anytime we set/change the file path, call the on-change function.
        self.onChangeExternalLoadsDataFile(dataPath)
        
    # End handleExternalLoadsDataBrowseButtonClick.
    
    def onChangeModelFile(self, modelPath=None):
        """
        Called anytime we set or change the model file.
        """
        
        # Attempt to load the new model.
        try:
            if modelPath:
                simObjects = MM.getModelAndState(modelPath)
            else:
                simObjects = MM.getModelAndState(
                    self.main.model.filePaths.modelPath)
        except:
            # We can fail if the model path is invalid.
            JOptionPane.showMessageDialog(self.main.frame,
                'Unable to load model file.  Reverting to previous value.',
                "Error",
                JOptionPane.ERROR_MESSAGE)
                
            # Change the view to correspond to the old model data.
            self.populateModelDataFileTextField()
            return
        
        self.main.simObjects = simObjects
        
        # Update the model data with the new path now that it is validated.
        if modelPath:
            self.main.model.filePaths.modelPath = modelPath
        
        # Change the view to correspond to the new model data.
        self.populateModelDataFileTextField()
        
    # End onChangeModelFile.
    
    def onChangeIkFile(self, ikPath=None):
        """
        Called anytime we set or change the IK file.
        """
        
        # Update the model data.
        if ikPath:
            self.main.model.filePaths.ikPath = ikPath
             
        # Change the view to correspond to the new IK data.
        self.populateIkDataFileTextField()
        
    # End onChangeIkFile.
    
    def onChangeExternalLoadsSpecificationFile(self, specificationPath=None):
        """
        Called anytime we set or change the external loads specification file.
        """
        
        # Update the model data.
        if specificationPath:
            self.main.model.filePaths.externalLoadsOutPath = specificationPath
             
        # Change the view to correspond to the new model data.
        self.populateExternalLoadsSpecificationFileTextField()
        
    # End onChangeExternalLoadsSpecificationFile.
    
    def onChangeExternalLoadsDataFile(self, dataPath=None):
        """
        Called anytime we set or change the external loads data file.
        """
        
        # Update the model data.
        if dataPath:
            self.main.model.filePaths.forcesOutPath = dataPath
             
        # Change the view to correspond to the new model data.
        self.populateExternalLoadsDataFileTextField()
        
    # End onChangeExternalLoadsDataFile.
        
    def populateModelDataFileTextField(self):
        """
        Fills in the model file text field from model data.
        """
        
        self.main.mainPanel.generalModelFileTextField.text = (
            self.main.model.filePaths.modelPath)
            
    # End populateModelDataFileTextField.
    
    def populateIkDataFileTextField(self):
        """
        Fills in the IK file text field from model data.
        """
        
        self.main.mainPanel.generalIkFileTextField.text = (
            self.main.model.filePaths.ikPath)
            
    # End populateIkDataFileTextField.
    
    def populateExternalLoadsSpecificationFileTextField(self):
        """
        Fills in the external loads specification file text field from model 
        data.
        """
        
        self.main.mainPanel.generalExternalLoadsSpecificationTextField.text = (
            self.main.model.filePaths.externalLoadsOutPath)
            
    # End populateExternalLoadsSpecificationFileTextField.
            
    def populateExternalLoadsDataFileTextField(self):
        """
        Fills in the external loads data file text field from model data.
        """
        
        self.main.mainPanel.generalExternalLoadsDataTextField.text = (
            self.main.model.filePaths.forcesOutPath)
            
    # End populateExternalLoadsDataFileTextField.
            
# End GeneralTab.


