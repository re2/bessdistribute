

################################################################################
#
# This script handles the "Ground Reaction" tab of the configure forces GUI.
#
################################################################################

import itertools
import os
import sys

from javax.swing import DefaultListModel

# Add the parent "bess" directory to the Python module search path so that we
# can import modules from different directories.  Subsequent imports of BESS
# scripts may use reload() to ensure that any changes during development get
# reloaded by the interpreter.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
if bessDir not in sys.path:
    sys.path.append(bessDir)

import utilities.handleStoMot as HSM; reload(HSM)
import utilities.toolsCommon as TC; reload(TC)
import configure_forces.gui.configureForcesGui_EditGRF as EditGRF; reload(EditGRF)
import configure_forces.gui.configureForcesGui_TabBase as TB; reload(TB)
    
class GroundReactionTab(TB.TabBase):

    def __init__(self, main):
        """
        By this point, we can expect that the GUI panels have been created.
        So this function can act as a GUI setup as well.
        """
        
        # Get a backpointer to the main object -- this is where we will get
        # the GUI elements and underlying data.
        self.main = main

        self.main.mainPanel.groundReactionDataFileTextField.focusLost = (
            self.handleDataFileTextFieldFocusLost)
            
        (self.main.mainPanel.groundReactionDataFileBrowseButton.
            actionPerformed) = self.handleDataFileBrowseButtonClick
        
        self.main.mainPanel.groundReactionAddForceSetButton.actionPerformed = (
            self.handleAddForceSetButtonClick)
        (self.main.mainPanel.groundReactionRemoveForceSetButton.
            actionPerformed) = self.handleRemoveForceSetButtonClick
        self.main.mainPanel.groundReactionEditForceSetButton.actionPerformed = (
            self.handleEditForceSetButtonClick)
            
        # Initialize the list to have an empty list model, but one that is
        # subsequently mutable.
        self.main.mainPanel.groundReactionForceSetList.setModel(
            DefaultListModel())
                
        self.updateFileSpecifications()
        
    # End __init__.
        
    def setViewFromModel(self):
        """
        Completely repopulates GUI from member data.
        
        Effectively this is a "virtual" override -- the main class expects all
        the tab classes to have this function.
        """
        
        self.updateFileSpecifications()
        self.onChangeGroundReactionDataFile()
        self.populateForceSetList()
        
    # End setViewFromModel.
    
    def updateFileSpecifications(self):
        """
        If the user has locked down or specified an input file we need to change
        some component properties.
        """
        
        TB.updateWidgetsToFileSpecification(
            self.main.specifiedGroundReactionFile,
            self.main.mainPanel.groundReactionDataFileLabel,
            self.main.mainPanel.groundReactionDataFileTextField,
            self.main.mainPanel.groundReactionDataFileBrowseButton)
            
        # If the ground reaction force data file is meant to be invisible,
        # also hide the enclosing panel as well.  This ensures we don't have
        # any extra vertical padding on this tab.
        if (self.main.specifiedGroundReactionFile and not 
            self.main.specifiedGroundReactionFile.visible):
            self.main.mainPanel.groundReactionDataFilePanel.visible = False
        else:
            self.main.mainPanel.groundReactionDataFilePanel.visible = True
                    
    # End updateFileSpecifications.
    
    def handleDataFileTextFieldFocusLost(self, event):
        """
        When losing focus we update the model.
        """
        
        self.onChangeGroundReactionDataFile(self.main.mainPanel.
            groundReactionDataFileTextField.text, False)
        
    # End handleDataFileTextFieldFocusLost.            
    
    def handleDataFileBrowseButtonClick(self, event):
        """
        Event handler for browse button to select data file.
        """
        
        try:
            groundReactionPath = TC.getSingleFilePath(
                'Select ground reaction force file',
                TC.FileFilter('GRF STO/MOT files (.sto,.mot)', 'sto,mot'))
        except:
            # If nothing is selected, an exception is thrown.
            return
        
        # Anytime we set/change the file path, call the on-change function.
        self.onChangeGroundReactionDataFile(groundReactionPath, False)
        
    # End handleDataFileBrowseButtonClick.
            
    def handleAddForceSetButtonClick(self, event):
        
        if not self.main.checkModelLoadedAndShowError():
            return
            
        editDialog = EditGRF.EditGRF_Dialog(self.main, initialGRF_Name=None)
        editDialog.show()
        
        if editDialog.nameAndForceSet:
            self.onAddForceSet(editDialog.nameAndForceSet)
        
    # End handleAddForceSetButtonClick.
        
    def handleRemoveForceSetButtonClick(self, event):
        # Figure out what item has been selected.
        idx, name = self.getForceSetListSelection()
        if idx == -1:
            return
            
        self.onRemoveForceSet(name)
        
    # End handleRemoveForceSetButtonClick.
        
    def handleEditForceSetButtonClick(self, event):
        
        if not self.main.checkModelLoadedAndShowError():
            return
            
        # Figure out what item has been selected.
        idx, name = self.getForceSetListSelection()
        if idx == -1:
            return
        
        editDialog = EditGRF.EditGRF_Dialog(self.main, initialGRF_Name=name)
        editDialog.show()
        
        if editDialog.nameAndForceSet:
            # Ok, successful completion of the dialog.  We need to handle the
            # case where the name changed though.
            if name != editDialog.nameAndForceSet[0]:
                # Don't do a redraw since we will do that on the subsequently
                # add operation.
                self.onRemoveForceSet(name, False)                
                self.onAddForceSet(editDialog.nameAndForceSet)
            else:
                self.onModifiedForceSet(editDialog.nameAndForceSet)
        
    # End handleEditForceSetButtonClick.
    
    def onChangeGroundReactionDataFile(self, 
        groundReactionPath=None, 
        validateFile=False):
        """
        Called anytime we set or change the ground reaction force data file.
        """
        
        # Attempt to read in the headers from this file.  I was planning on
        # using these to limit the set of names for GRF fields to the column 
        # headers in the file, but that is probably overkill for now.
        try:
            if groundReactionPath and validateFile:
                rawColumnNames = HSM.readStoMotColumnNamesAsList(
                    groundReactionPath)
            elif validateFile:
                rawColumnNames = HSM.readStoMotColumnNamesAsList(
                    self.main.model.filePaths.grfPath)
        except:
            print 'Unable to load ground reaction force data file'
            return 
                
        # Update the model data.
        if groundReactionPath:
            self.main.model.filePaths.grfPath = groundReactionPath
        
        # Change the view to correspond to the new model data.
        self.populateGroundReactionDataFileTextField()
        
    # End onChangeGroundReactionDataFile.
        
    def populateGroundReactionDataFileTextField(self):
        """
        Fills in the forces data file text field from model data.
        """
        
        self.main.mainPanel.groundReactionDataFileTextField.text = (
            self.main.model.filePaths.grfPath)
            
    # End populateGroundReactionDataFileTextField.
        
    def populateForceSetList(self):
        """
        Adds the name of each force set to the list.
        """
                
        # Iterate through the names of the force sets.
        listModel = self.main.mainPanel.groundReactionForceSetList.model
        listModel.clear()
        
        for name in sorted(self.main.model.grfForceSets):
            listModel.addElement(name)
            
    # End populateForceSetList.
    
    def getForceSetListSelection(self):
        list = self.main.mainPanel.groundReactionForceSetList
        
        # If there is no selection, will return -1, None.
        return list.getSelectedIndex(), list.getSelectedValue()        
    
    # End getForceSetListSelection.
    
    def onAddForceSet(self, nameAndForceSet, redraw=True):
        # Add the entry to the model data.
        self.main.model.grfForceSets[nameAndForceSet[0]] = (
            nameAndForceSet[1])
    
        # Simply redraw the list box from model data.
        if redraw:
            self.populateForceSetList()
            
    # End onAddForceSet.
            
    def onRemoveForceSet(self, name, redraw=True):
            
        del(self.main.model.grfForceSets[name])
            
        # Simply redraw the list box from model data.
        if redraw:
            self.populateForceSetList()
        
    # End onRemoveForceSet.
    
    def onModifiedForceSet(self, nameAndForceSet):
        # We could probably get away with using the "onAddForceSet" function,
        # but it might be helpful to keep these separate.
        self.main.model.grfForceSets[nameAndForceSet[0]] = (
            nameAndForceSet[1])
            
    # End onModifiedForceSet.
    
# End GroundReactionTab.


