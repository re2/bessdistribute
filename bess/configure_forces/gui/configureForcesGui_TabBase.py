

################################################################################
#
# This script gives a base class for our tabbed pane presenter classes.
#
################################################################################

import os
import sys

# Add the parent "bess" directory to the Python module search path so that we
# can import modules from different directories.  Subsequent imports of BESS
# scripts may use reload() to ensure that any changes during development get
# reloaded by the interpreter.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
if bessDir not in sys.path:
    sys.path.append(bessDir)
    
class TabBase:

    def setViewFromModel(self):
        """
        Called when new XML is loaded and we want to do a complete repopulation
        of the UI.
        """
        pass
        
    # End setViewFromModel.
    
    def tabShown(self):
        """
        Called when the tab is "shown" -- i.e. when we click on the tab the UI
        switches to it.
        """
        pass
        
    # End tabShown.
                
# End TabBase.

def updateWidgetsToFileSpecification(specifiedFile,
    label,
    textField,
    browseButton):
    """
    If the user has locked down or specified an input file we need to change
    some component properties.
    
    We put this function in the tab base because multiple tabs need this
    functionality and they are all import this anyway.
    """
        
    if not specifiedFile:
        return
            
    # Mutability.
    textField.editable = specifiedFile.mutable
        
    # Visibility.        
    label.visible = specifiedFile.visible
    textField.visible = specifiedFile.visible
    browseButton.visible = (specifiedFile.visible and specifiedFile.mutable)
        
    # Locked filename.
    if (not specifiedFile.mutable and (specifiedFile.immutableFilename or 
        specifiedFile.immutableFilename == '')):
        textField.text = specifiedFile.immutableFilename
                    
# End updateWidgetsToFileSpecification.

