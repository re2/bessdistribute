

################################################################################
#
# This script launches the configureForcesGui panel in standalone fashion,
# with the ability to load or save a configuration XML file.
#
################################################################################

import os
import sys
import tempfile
from threading import Thread
import traceback

from java.awt import BorderLayout, Dimension
from java.lang import Thread as JThread
from javax.swing import BorderFactory, Box, BoxLayout
from javax.swing import JButton, JFrame, JOptionPane, JPanel

# Add the parent "bess" directory to the Python module search path so that we
# can import modules from different directories.  Subsequent imports of BESS
# scripts may use reload() to ensure that any changes during development get
# reloaded by the interpreter.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
if bessDir not in sys.path:
    sys.path.append(bessDir)

import configure_forces.configureForces_Support as CF; reload(CF)
import configure_forces.gui.configureForcesGui_Main as CFGM; reload(CFGM)
import utilities.toolsCommon as TC; reload(TC)

# Get the main GUI components from our NetBeans/Java project.
TC.importJar(os.path.join(bessDir, 'lib', 'ConfigureForcesGuiPanels.jar'))
from bess.gui.configureforces import ConfigureForcesStandaloneFrame

def mainWorker():
    """
    The main worker function called by the script entry point.
    """
    
    mediumBorder = 20
        
    # The guts of the GUI is a JPanel -- we need to provide a JFrame to hold
    # the panel.  When closing, you can't EXIT_ON_CLOSE since that is a
    # sys.exit() which would shut down OpenSim -- it is a security exception.
    frame = ConfigureForcesStandaloneFrame()
    frame.title = 'Configure Forces Standalone'
    frame.defaultCloseOperation = JFrame.DISPOSE_ON_CLOSE
        
    # Install an uncaught exception handler in the GUI thread so that any
    # exceptions thrown from any of our UI callbacks (button clicks, etc.)
    # get handled.  If we don't do this they go to OpenSim's global handler
    # why is very cryptically displayed to the user (i.e. a small red circle-X
    # in the bottom-right tray).
    JThread.currentThread().uncaughtExceptionHandler = (
        lambda thread, throwable: guiExceptionHandler(thread, throwable, frame))
    
    # Load the main panel from the JAR file and the support class.  Add it to
    # the placeholder panel which is waiting for it.
    main = CFGM.Main(frame)    
    frame.mainPanelPlaceholder.add(main.mainPanel)
    
    frame.loadButton.actionPerformed = lambda event: loadHandler(event, main)
    frame.saveButton.actionPerformed = lambda event: saveHandler(event, main)
    frame.runButton.actionPerformed = lambda event: runHandler(event, main)
    
    # Don't let the window get shrunk down past the minimum of the main
    # panel.  Really we should also account for the size of the bottom row
    # of buttons too...
    minHeight = (main.mainPanel.getMinimumSize().height + 
        frame.loadButton.getMinimumSize().height + 3 * mediumBorder)    
    minWidth = main.mainPanel.getMinimumSize().width + 2 * mediumBorder
    frame.setMinimumSize(Dimension(minWidth, minHeight))
        
    frame.pack()
    frame.visible = True
        
    # The caller may want a handle to the window.
    return frame

# End mainWorker.

def loadHandler(event, main):
    """
    Allows this standalone version to get an XML config file to populate the
    GUI.
    """
    
    # Popup a file selection dialog.
    try:
        xmlPath = TC.getSingleFilePath(
            'Select configure forces setup file to load from', 
            TC.FileFilter('Configure forces file (.xml)', 'xml'))
    except:
        # If nothing is selected, an exception is thrown.
        print 'No configure forces setup file selected'
        return
        
    main.loadXmlToMainPanel(xmlPath)

# End loadHandler.

def saveHandler(event, main):
    """
    Allows this standalone version to save an XML config file from the
    GUI contents.
    """
    
    if not validateAndWarn(main):
        return    
    
    # Popup a file selection dialog.
    try:
        xmlPath = TC.getSingleFilePath(
            'Select configure forces setup file to save to', 
            TC.FileFilter('Configure forces file (.xml)', 'xml'))
    except:
        # If nothing is selected, an exception is thrown.
        print 'No configure forces setup file selected'
        return
        
    main.saveXmlFromMainPanel(xmlPath)
    
# End saveHandler.
    
def runHandler(event, main):
    """
    Allows this standalone version to run the Configure Forces tool.
    
    It still does this via an XML file however, so we first save the current
    contents of the UI to a temporary XML file.
    """
    
    if not validateAndWarn(main):
        return   
        
    # Creat a temporary directory.
    dirPath = tempfile.mkdtemp()
    xmlPath = os.path.join(dirPath, 'configure_forces_temp_setup.xml')
        
    if not main.saveXmlFromMainPanel(xmlPath):
        # Error.  Bail.  The function will have displayed an error to the
        # user.
        return
    
    # Run in a worker thread.  This means we don't block the GUI thread when
    # we are actually running the CF tool.  It's not sufficient to run this
    # whole script in a thread -- we are inside the GUI thread here since
    # this is a button callback.
    Thread(target=lambda: runThread(xmlPath, main.frame)).start()        
    
# End runHandler.

def runThread(xmlPath, frame):
    """
    Thread which runs the Configure Forces tool.
    
    Break this out as a function in case we want to do anything fancy when
    we catch an exception.
    """
            
    try:
        CF.mainWorker(xmlPath, CF.TimeRange())
    except Exception, e:
        stackTrace = traceback.format_exc(limit=10)
        
        JOptionPane.showMessageDialog(frame,
            makeErrorMessage(
                'Unexpected error when running Configure Forces tool.', 
                    stackTrace),
            "Error",
            JOptionPane.ERROR_MESSAGE);
        
# End runThread.

def validateAndWarn(main):
    """
    Before saving and/or running, check if the data is valid or not.
    """
    
    # First check to see if the data is valid or not.
    errorList = main.validateModel()
    
    if not errorList:
        return True
        
    # Warn the user, and see if we should proceed.
    msg = ('There are errors in the data and are listed below.  Click Yes '
        'to proceed and No to cancel.\n\n')
    for error in errorList:
        msg += '- ' + error + '\n'
        
    result = JOptionPane.showConfirmDialog(
        main.frame,
        msg,
        "Error in Data Validation",
        JOptionPane.YES_NO_OPTION,
        JOptionPane.WARNING_MESSAGE)
        
    if result == JOptionPane.NO_OPTION:
        return False
    else:
        return True
        
# End validateAndWarn.

def guiExceptionHandler(thread, throwable, frame):
    """
    Handles any unhandled exception.
    
    Ostensibly this is installed in the main GUI thread (or what Java calls
    the Event Dispatch Loop).
    """
    
    # You don't really need to specify the parent frame here -- you likely 
    # can get away with None.  But by specifying the parent frame, the dialog
    # we throw up will be positioned over our GUI which looks better than
    # a parent frame of None, which puts the dialog at the center of the 
    # screen.  (And who knows what happens in multiple monitor cases.)
    # Also note that this code persists after we have closed down the 
    # Configure Forces UI -- this function is likely still installed as ability
    # GUI exception handler.  In that case no worries about "frame" not being
    # valid.  This is handled gracefully -- frame is likely "None" at that
    # point.
    JOptionPane.showMessageDialog(frame,
        makeErrorMessage('Unexpected error.', throwable.toString()),
        "Error",
        JOptionPane.ERROR_MESSAGE);
    
# End guiExceptionHandler.

def makeErrorMessage(preamble, stackTrace):
    msg = preamble + '\n\nError information:\n\n' + stackTrace
    return msg
    
# End makeErrorMessage.

if __name__ == '__main__':
    
    mainWorker()
        
# End of main.


