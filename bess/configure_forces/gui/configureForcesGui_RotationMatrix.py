

################################################################################
#
# This script handles the display and editing of a 3x3 rotation matrix.
#
################################################################################

import os
import sys

from javax.swing import JFrame, JOptionPane

# Add the parent "bess" directory to the Python module search path so that we
# can import modules from different directories.  Subsequent imports of BESS
# scripts may use reload() to ensure that any changes during development get
# reloaded by the interpreter.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
if bessDir not in sys.path:
    sys.path.append(bessDir)
    
import utilities.toolsCommon as TC; reload(TC)
    
TC.importJar(os.path.join(bessDir, 'lib', 'ConfigureForcesGuiPanels.jar'))
from bess.gui.configureforces import RotationMatrixDialog as ViewRotationMatrixDialog

class RotationMatrixPanel:
    """
    This is the presenter for the "Rotation Matrix" panel.  This is used 
    anywhere we need to specify a 3x3 rotation matrix.
    """
    
    def __init__(self, 
        main, 
        sourceLabel, 
        destLabel, 
        initialRotationMatrixArray, 
        view):
                        
        self.main = main
                        
        # Initial rotation.  This should come to us as a string that looks like:
        # '(1.0, 0.0 ..... 1.0)'.
        self.initialRotationMatrixArray = initialRotationMatrixArray
                                                
        # This is the actual Java view object.
        self.view = view
        
        # Ultimately this is going to be the record of the new or modified
        # matrix on successful completion of this dialog.
        self.rotationMatrixArray = None
        
        # Setup a button handler for setting defaults for force/torque names.
        self.view.identityButton.actionPerformed = self.setIdentityHandler
            
        # We customize the names of the source and destination vectors to
        # help the user figure out what is being transformed to what.
        self.view.sourceLabel.text = sourceLabel
        self.view.destLabel.text = destLabel
        
        # For convenience, let's put all the text fields into a list so 
        # we can iterate over them.  This is a one-time brute force hit.
        self.textFields = []
        self.addTextFieldsToList(self.textFields)
        
        self.populateFields()
        
    # End __init__.
    
    def addTextFieldsToList(self, textFields):
        textFields.append(self.view.r1C1)
        textFields.append(self.view.r1C2)
        textFields.append(self.view.r1C3)
        textFields.append(self.view.r2C1)
        textFields.append(self.view.r2C2)
        textFields.append(self.view.r2C3)
        textFields.append(self.view.r3C1)
        textFields.append(self.view.r3C2)
        textFields.append(self.view.r3C3)
    
    # End addTextFieldsToDict.
    
    def populateFields(self):
        """
        Called at the beginning to populate the fields in the GUI.
        """
                
        if not self.initialRotationMatrixArray:
            return
            
        # We need to get the rotation matrix array from it's original format
        # which is a string representation of a tuple, to individual elements. 
        self.setIndicesToTuple(
            TC.xmlStringTupleToTuple(self.initialRotationMatrixArray))       
                
    # End populateFields.
    
    def validateStateAndLatch(self):
        """
        Checks the data is valid and saves it to member data.
        
        Presumably the dialog presenter will call this when the user hits
        the 'OK' button.
        """
        
        # Good practice to clear out the result before using it for indicating
        # success.
        self.rotationMatrixArray = None
        
        try:
            # Check that each text field has valid data.
            for idx in range(len(self.textFields)):
                text = self.textFields[idx].text
                fieldName = self.getFieldName(idx)
                
                msg = None
                if not TC.isTextValidFloat(text):
                    raise ValueError('Value in ' + fieldName + ' is not a '
                        'valid number.')
                    
            # Do a determinant check on the matrix.  As a rotation matrix, the
            # determinant should equal 1 or -1.  I think -1 is valid in cases
            # where we have a flipping of a single axis, which can be the case
            # when mirroring forces from the right side of the body to the left
            # side.
            arrayList = [float(x.text) for x in self.textFields]
            matrix = TC.listToMat33(arrayList)
            determinant = TC.getMat33Determinant(matrix)
            
            # So, we could be stricter on the error bound here -- I'm just
            # worried that the user rounds off some cosine/sine values and then
            # gets a strange error they don't understand.  So be a bit more
            # tolerant than we would with engineers.
            if abs(1.0 - abs(determinant)) > 0.01:
                raise ValueError('Matrix determinant does not equal +/- 1 and '
                    'is not a valid rotation matrix.')
            
        except ValueError, error:
            JOptionPane.showMessageDialog(self.main.frame,
                str(error),
                "Error",
                JOptionPane.ERROR_MESSAGE);
                
            return False
        
        # If we're here then all data is valid.  Latch and return True.
        # This may be a bit convoluted (text to float to list to tuple) 
        # but I want to make sure the data follow the same progression
        # that it does in other parts of the code.        
        self.rotationMatrixArray = tuple(arrayList)    
    
        return True
        
    # End validateStateAndLatch.
    
    def getFieldName(self, idx):
        """
        Gets a string ID for a given text field based on its 1-d list index.
        
        For example, index 0 should return "Row 1, Col 1".
        """
        
        if idx < 0 or idx > len(self.textFields):
            raise ValueError('Invalid index for text field')
            
        r = idx / 3 + 1
        c = idx % 3 + 1
        return 'Row %d, Col %d' % (r, c)
        
    # End getFieldName.
    
    def setIndicesToTuple(self, values):
        if len(values) != 9:
            print 'Incorrect formatting for rotation matrix'
            return
            
        for idx in range(len(values)):
            self.textFields[idx].text = str(values[idx])
        
    # End setIndicesToTuple.
    
    def setIdentityHandler(self, event):
    
        self.setIndicesToTuple((1.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,1.0))        
    
    # End setIdentityHandler.
        
# End RotationMatrixPanel.

class RotationMatrixDialog:
    """
    This is the presenter for the Rotation Matrix dialog.
    """
    
    def __init__(self, main, sourceLabel, destLabel, initialRotationMatrixArray=None):
            
        self.main = main
        self.sourceLabel = sourceLabel
        self.destLabel = destLabel
        self.initialRotationMatrixArray = initialRotationMatrixArray
        
        # Ultimately this is going to be the record of the new or modified
        # matrix on successful completion of this dialog.
        self.rotationMatrixArray = None
                
    # End __init__.
    
    def show(self):
        """
        Show the modal dialog.
        """
        
        dlg = ViewRotationMatrixDialog(self.main.frame, True)
        dlg.title = 'Set Rotation Matrix'
        dlg.defaultCloseOperation = JFrame.DISPOSE_ON_CLOSE
        dlg.windowClosing = self.closingHandler
        
        # The large guts of this frame are in the contained panel.
        # Our presenters mimic the structure of the Java view objects.
        panel = RotationMatrixPanel(self.main,
            self.sourceLabel,
            self.destLabel,
            self.initialRotationMatrixArray,
            dlg.rotationMatrixPanelView)
        
        # Add in callbacks for the Ok and Cancel buttons.
        dlg.okButton.actionPerformed = lambda event: self.okHandler(event, dlg, panel)
        dlg.cancelButton.actionPerformed = lambda event: self.cancelHandler(event, dlg)
                
        # Show the modal dialog.
        dlg.visible = True
        
    # End show.
    
    def okHandler(self, event, dlg, panel):
        
        # See if the data is valid.  If not, the panel will throw up an
        # error dialog and we want to give them a chance to fix, so we don'tell
        # close the dialog in that case.
        if not panel.validateStateAndLatch():
            return
        
        # Setting a value for the rotation array is how we will indicate 
        # success to the caller.
        self.rotationMatrixArray = panel.rotationMatrixArray
        dlg.visible = False
    
    # End okHandler.
    
    def cancelHandler(self, event, dlg):
        # For now, this is a no-op (other than closing the window!).
        
        dlg.visible = False
    
    # End cancelHandler.
    
    def closingHandler(self, event):
        # For now, this is a no-op.
        pass
        
    # End closingHandler.
    
# End RotationMatrixDialog.

