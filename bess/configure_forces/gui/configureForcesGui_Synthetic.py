

################################################################################
#
# This script handles the "Synthetic" tab of the configure forces GUI.
#
################################################################################

import itertools
import os
import sys

from javax.swing import DefaultListModel

# Add the parent "bess" directory to the Python module search path so that we
# can import modules from different directories.  Subsequent imports of BESS
# scripts may use reload() to ensure that any changes during development get
# reloaded by the interpreter.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
if bessDir not in sys.path:
    sys.path.append(bessDir)

import utilities.toolsCommon as TC; reload(TC)
import configure_forces.gui.configureForcesGui_EditSynthetic as EditSynthetic; reload(EditSynthetic)
import configure_forces.gui.configureForcesGui_TabBase as TB; reload(TB)
    
class SyntheticTab(TB.TabBase):

    def __init__(self, main):
        """
        By this point, we can expect that the GUI panels have been created.
        So this function can act as a GUI setup as well.
        """
        
        # Get a backpointer to the main object -- this is where we will get
        # the GUI elements and underlying data.
        self.main = main

        # Add in a click handler for the various buttons on this tab.
        self.main.mainPanel.syntheticAddSensorButton.actionPerformed = (
            self.handleAddSensorButtonClick)
        self.main.mainPanel.syntheticRemoveSensorButton.actionPerformed = (
            self.handleRemoveSensorButtonClick)
        self.main.mainPanel.syntheticEditSensorButton.actionPerformed = (
            self.handleEditSensorButtonClick)
    
        # Initialize the list to have an empty list model, but one that is
        # subsequently mutable.
        self.main.mainPanel.syntheticSensorList.setModel(DefaultListModel())
        
    # End __init__.
    
    def setViewFromModel(self):
        """
        Completely repopulates GUI from member data.
        
        Effectively this is a "virtual" override -- the main class expects all
        the tab classes to have this function.
        """
        
        self.populateSensorsList()
        
    # End setViewFromModel.
            
    def handleAddSensorButtonClick(self, event):
        
        if not self.main.checkModelLoadedAndShowError():
            return
           
        editDialog = EditSynthetic.EditSyntheticSensorDialog(self.main, 
            initialSensorName=None)
        editDialog.show()
        
        if editDialog.syntheticSensorDialogData:
            self.onAddSensor(editDialog.syntheticSensorDialogData)
                
    # End handleAddSensorButtonClick.
        
    def handleRemoveSensorButtonClick(self, event):
        # Figure out what item has been selected.
        idx, name = self.getSensorsListSelection()
        if idx == -1:
            return
            
        self.onRemoveSensor(name)
        
    # End handleRemoveSensorButtonClick.
        
    def handleEditSensorButtonClick(self, event):
        
        if not self.main.checkModelLoadedAndShowError():
            return
            
        # Figure out what item has been selected.
        idx, name = self.getSensorsListSelection()
        if idx == -1:
            return
        
        editDialog = EditSynthetic.EditSyntheticSensorDialog(self.main, 
            initialSensorName=name)
        editDialog.show()
        
        if editDialog.syntheticSensorDialogData:
            # Ok, successful completion of the dialog.  We need to handle the
            # case where the name changed though.
            if name != editDialog.syntheticSensorDialogData.name:
                # Don't do a redraw since we will do that on the subsequently
                # add operation.
                self.onRemoveSensor(name, False)                
                self.onAddSensor(editDialog.syntheticSensorDialogData)
            else:
                self.onModifiedSensor(editDialog.syntheticSensorDialogData)
        
    # End handleEditSensorButtonClick.
                        
    def populateSensorsList(self):
        """
        Adds the name of each load cell to the list.
        """
                
        # Iterate through the names of the load cells.
        listModel = self.main.mainPanel.syntheticSensorList.model
        listModel.clear()
        
        for name in sorted(self.main.model.syntheticLoadCellForceSets):
            listModel.addElement(name)
            
    # End populateSensorsList.
    
    def getSensorsListSelection(self):
        list = self.main.mainPanel.syntheticSensorList
        
        # If there is no selection, will return -1, None.
        return list.getSelectedIndex(), list.getSelectedValue()        
    
    # End getSensorsListSelection.
    
    def onAddSensor(self, syntheticSensorDialogData, redraw=True):
        # Add the entry to the model data.
        self.main.model.syntheticLoadCellForceSets[
            syntheticSensorDialogData.name] = (
                syntheticSensorDialogData.loadCell)
        
        self.main.model.syntheticDataConfig[syntheticSensorDialogData.name] = (
            syntheticSensorDialogData.syntheticDataConfig)
    
        # Simply redraw the list box from model data.
        if redraw:
            self.populateSensorsList()
            
    # End onAddSensor.
            
    def onRemoveSensor(self, name, redraw=True):
            
        del(self.main.model.syntheticLoadCellForceSets[name])
        del(self.main.model.syntheticDataConfig[name])
            
        # Simply redraw the list box from model data.
        if redraw:
            self.populateSensorsList()
        
    # End onRemoveSensor.
    
    def onModifiedSensor(self, syntheticSensorDialogData):
        # We could probably get away with using the "onAddSensor" function,
        # but it might be helpful to keep these separate.
        self.main.model.syntheticLoadCellForceSets[
            syntheticSensorDialogData.name] = (
                syntheticSensorDialogData.loadCell)
        
        self.main.model.syntheticDataConfig[syntheticSensorDialogData.name] = (
            syntheticSensorDialogData.syntheticDataConfig)
            
    # End onModifiedSensor.
    
# End SyntheticTab.


