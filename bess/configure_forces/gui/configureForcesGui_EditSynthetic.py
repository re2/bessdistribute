

################################################################################
#
# This script handles the add or editing of a synthetic force/torque sensor 
# in the Configure Forces UI.
#
################################################################################

import os
import sys

from org.opensim import modeling

from javax.swing import DefaultComboBoxModel, JComboBox, JFrame, JOptionPane

# Add the parent "bess" directory to the Python module search path so that we
# can import modules from different directories.  Subsequent imports of BESS
# scripts may use reload() to ensure that any changes during development get
# reloaded by the interpreter.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
if bessDir not in sys.path:
    sys.path.append(bessDir)
    
import utilities.toolsCommon as TC; reload(TC)
import configure_forces.gui.configureForcesGui_RotationMatrix as RotationMatrix; reload(RotationMatrix)
import configure_forces.gui.configureForcesGui_EditSensor as EditSensor; reload(EditSensor)
import configure_forces.configureForces_Support as CF; reload(CF)

TC.importJar(os.path.join(bessDir, 'lib', 'ConfigureForcesGuiPanels.jar'))
from bess.gui.configureforces import EditSyntheticSensorDialog as ViewEditSyntheticSensorDialog

class SyntheticSensorDialogData:
    """
    Encapsulates data to be returned to the caller after displaying the
    EditSyntheticSensorDialog.
    
    When the user hits the "OK" button, the caller wants to retrieve the data
    filled in by the user.  Elsewhere we just use a 2-tuple for this.  In this
    case the data is grown to atleast 3 elements and warrants a type.
    """
    def __init__(self, name, loadCell, syntheticDataConfig):
        self.name = name
        self.loadCell = loadCell
        self.syntheticDataConfig = syntheticDataConfig
        
    # End __init__.
    
# End SyntheticSensorDialogData.

class SyntheticDataConfigPanel:
    """
    This is the presenter for the "Synthetic Data Config" panel.  This is 
    used by the synthetic force/torque sensor dialog.
    """
    
    def __init__(self, main, initialSensorName, view):
                
        # Get a backpointer to the main object -- this is where we will get
        # the GUI elements and underlying data.
        self.main = main
        
        # Whether or not we are creating a new sensor or editing an existing
        # one.
        self.initialSensorName = initialSensorName
                
        # This is the actual Java view object.
        self.view = view
        
        # Ultimately this is going to be the record of the new or modified
        # data on successful completion of this dialog.
        self.syntheticDataConfig = None
                    
        # Setup a button handler for launching the force rotation matrix dialog.
        self.view.setForceRotationButton.actionPerformed = (
            self.setForceRotationHandler)
            
        # And setup a button handler for launching the torque rotation matrix 
        # dialog.
        self.view.setTorqueRotationButton.actionPerformed = (
            self.setTorqueRotationHandler)
            
        # Disable any non-editable fields.
        self.view.forceRotationTextField.enabled = False
        self.view.torqueRotationTextField.enabled = False
        
        # Call a worker to fill-in the initial values of the GUI.
        self.populateFields()
        
    # End __init__.
    
    def populateFields(self):
        """
        Called at the beginning to populate the fields in the GUI.
        """
        
        # Populate the combo boxes for strides.
        strideNames = [self.main.model.strideConfig.rightStrideName,
            self.main.model.strideConfig.leftStrideName]
                    
        self.view.syntheticStrideCombo.setModel(
            DefaultComboBoxModel(strideNames))            
        self.view.realStrideCombo.setModel(DefaultComboBoxModel(strideNames))
        
        # For new synthetic cells, assume the right stride is associated with
        # the real load cell and the left for the synthetic.  This is a bit of
        # an RE2-specific optimization, but doesn't harm anyone else.
        self.view.syntheticStrideCombo.setSelectedItem(
            self.main.model.strideConfig.leftStrideName)
        self.view.realStrideCombo.setSelectedItem(
            self.main.model.strideConfig.rightStrideName)
            
        # Populate the combo box for the real load cell that the synthetic
        # data is derived from.
        self.view.realSensorNameCombo.setModel(
            DefaultComboBoxModel(
                sorted(self.main.model.realLoadCellForceSets.keys())))
                
        # At this point, if we are adding a new load cell we don't have anything
        # more to populate.
        if not self.initialSensorName:
            return
            
        # If this is an editing job, then we can fill in the data as 
        # appropriate.  Check that the model actually contains the load cell.
        if self.initialSensorName not in self.main.model.syntheticDataConfig:
            print 'Could not find synthetic data configuration'
            return
            
        # Get a reference to the data.
        syntheticDataConfig = self.main.model.syntheticDataConfig[
            self.initialSensorName]
                
        # Attempt to set the combo boxes to the appropriate values.
        self.view.syntheticStrideCombo.setSelectedItem(
            syntheticDataConfig.syntheticLoadCellStrideName)
        self.view.realSensorNameCombo.setSelectedItem(
            syntheticDataConfig.realLoadCellName)
        self.view.realStrideCombo.setSelectedItem(
            syntheticDataConfig.realLoadCellStrideName)
                    
        # Set the rotation matrix arrays as strings.
        self.view.forceRotationTextField.text = str(
            syntheticDataConfig.realToSyntheticForceRotationArray)
        self.view.torqueRotationTextField.text = str(
            syntheticDataConfig.realToSyntheticTorqueRotationArray)
        
    # End populateFields.
        
    def validateStateAndLatch(self):
        """
        Checks the data is valid and saves it to member data.
        
        Presumably the dialog presenter will call this when the user hits
        the 'OK' button.
        """
        
        # This panel can be used in conjunction with others.  When the user
        # hits the "OK" button, this panel may validate and latch but another
        # panel may fail.  If they change a bunch of data and try to hit "OK"
        # again, we want to make sure we clear our result before validating
        # again.
        self.syntheticDataConfig = None
        
        try:
            if self.view.forceRotationTextField.text == '':
                raise ValueError('Real to synthetic force rotation is empty.')
                
            if self.view.torqueRotationTextField.text == '':
                raise ValueError('Real to synthetic torque rotation is empty.')
                            
            # Todo: error on load cell combo not being filled in.
                            
        except ValueError, error:
            JOptionPane.showMessageDialog(self.main.frame,
                str(error),
                "Error",
                JOptionPane.ERROR_MESSAGE);
                
            return False
                
        # If we're here then all data is valid.  Latch and return True.
        self.syntheticDataConfig = CF.SyntheticDataConfig(
            self.view.syntheticStrideCombo.getSelectedItem(),
            self.view.realSensorNameCombo.getSelectedItem(),
            self.view.realStrideCombo.getSelectedItem(),
            TC.xmlStringTupleToTuple(
                self.view.forceRotationTextField.text),
            TC.xmlStringTupleToTuple(
                self.view.torqueRotationTextField.text))
            
        return True
        
    # End validateStateAndLatch.
            
    def setForceRotationHandler(self, event):
    
        # Ok, we need to instantiate and launch a modal dialog ourselves.
        dlg = RotationMatrix.RotationMatrixDialog(self.main, 
            'Real Force Frame',
            'Synthetic Force Frame',
            self.view.forceRotationTextField.text)
        dlg.show()
        
        # The interface for determining success or failure isn't the clearest.
        if dlg.rotationMatrixArray:
            self.view.forceRotationTextField.text = str(
                dlg.rotationMatrixArray)
            
    # End setForceRotationHandler.
    
    def setTorqueRotationHandler(self, event):
    
        # Ok, we need to instantiate and launch a modal dialog ourselves.
        dlg = RotationMatrix.RotationMatrixDialog(self.main, 
            'Real Torque Frame',
            'Synthetic Torque Frame',
            self.view.torqueRotationTextField.text)
        dlg.show()
        
        # The interface for determining success or failure isn't the clearest.
        if dlg.rotationMatrixArray:
            self.view.torqueRotationTextField.text = str(
                dlg.rotationMatrixArray)
            
    # End setTorqueRotationHandler.
        
# End SyntheticDataConfigPanel.

class EditSyntheticSensorDialog:
    """
    This is the presenter for the editing a synthetic load cell.
    """
    
    def __init__(self, main, initialSensorName=None):
    
        # Get a backpointer to the main object -- this is where we will get
        # the GUI elements and underlying data.
        self.main = main
        
        # Whether or not we are creating a new sensor or editing an existing
        # one.
        self.initialSensorName = initialSensorName
        
        # Ultimately this is going to be the record of the new or modified
        # load cell object on successful completion of this dialog.
        self.syntheticSensorDialogData = None
                
    # End __init__.
    
    def show(self):
        """
        Show the modal dialog.
        """
        
        dlg = ViewEditSyntheticSensorDialog(self.main.frame, True)
        dlg.title = 'Add/Edit Synthetic Force/Torque Sensor'
        dlg.defaultCloseOperation = JFrame.DISPOSE_ON_CLOSE
        dlg.windowClosing = self.closingHandler
        
        # The large guts of this frame are in two contained panels.
        # Our presenters mimic the structure of the Java view objects.
        sensorPanel = EditSensor.EditSensorPanel(self.main, 
            self.initialSensorName, 
            self.main.model.syntheticLoadCellForceSets, 
            dlg.editSensorPanelView)  
            
        syntheticConfigPanel = SyntheticDataConfigPanel(self.main,
            self.initialSensorName,
            dlg.syntheticDataConfigPanelView)
        
        # Add in callbacks for the Ok and Cancel buttons.
        dlg.okButton.actionPerformed = lambda event: self.okHandler(event, 
            dlg, 
            sensorPanel, 
            syntheticConfigPanel)
        dlg.cancelButton.actionPerformed = lambda event: self.cancelHandler(
            event, dlg)
                
        # Show the modal dialog.
        dlg.visible = True
        
    # End show.
    
    def okHandler(self, event, dlg, sensorPanel, syntheticConfigPanel):
        
        # See if the data is valid.  If not, the panel will throw up an
        # error dialog and we want to give them a chance to fix, so we don'tell
        # close the dialog in that case.
        if not sensorPanel.validateStateAndLatch():
            return
            
        if not syntheticConfigPanel.validateStateAndLatch():
            return
            
        self.syntheticSensorDialogData = SyntheticSensorDialogData(
            sensorPanel.nameAndLoadCell[0],
            sensorPanel.nameAndLoadCell[1],
            syntheticConfigPanel.syntheticDataConfig)
            
        dlg.visible = False
    
    # End okHandler.
    
    def cancelHandler(self, event, dlg):
    
        # For now, this is a no-op (other than closing the window!).        
        dlg.visible = False
    
    # End cancelHandler.
    
    def closingHandler(self, event):
    
        # For now, this is a no-op.
        pass
        
    # End closingHandler.
    
# End EditSyntheticSensorDialog.

