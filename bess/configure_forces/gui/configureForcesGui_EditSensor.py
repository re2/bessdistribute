

################################################################################
#
# This script handles the add or editing of a force/torque sensor in the 
# Configure Forces UI.
#
################################################################################

import os
import sys

from org.opensim import modeling

from javax.swing import DefaultComboBoxModel, JComboBox, JFrame, JOptionPane

# Add the parent "bess" directory to the Python module search path so that we
# can import modules from different directories.  Subsequent imports of BESS
# scripts may use reload() to ensure that any changes during development get
# reloaded by the interpreter.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
if bessDir not in sys.path:
    sys.path.append(bessDir)
    
import utilities.toolsCommon as TC; reload(TC)
import configure_forces.gui.configureForcesGui_RotationMatrix as RotationMatrix; reload(RotationMatrix)
import configure_forces.configureForces_Support as CF; reload(CF)

TC.importJar(os.path.join(bessDir, 'lib', 'ConfigureForcesGuiPanels.jar'))
from bess.gui.configureforces import EditRealSensorDialog as ViewEditRealSensorDialog

class EditSensorPanel:
    """
    This is the presenter for the "Edit Sensor" panel.  This is used by the
    real force/torque sensor dialog as well as the synthetic force/torque sensor
    dialog.
    """
    
    def __init__(self, main, initialSensorName, loadCellDict, view):
                
        # Get a backpointer to the main object -- this is where we will get
        # the GUI elements and underlying data.
        self.main = main
        
        # Whether or not we are creating a new sensor or editing an existing
        # one.
        self.initialSensorName = initialSensorName
        
        # This type is used both for dialogs covering real load cells as
        # well as synthetic load cells.
        self.loadCellDict = loadCellDict
        
        # This is the actual Java view object.
        self.view = view
        
        # Ultimately this is going to be the record of the new or modified
        # load cell object on successful completion of this dialog.
        self.nameAndLoadCell = None
        
        # Setup a button handler for setting defaults for force/torque names.
        self.view.setDefaultNamesButton.actionPerformed = (
            self.setDefaultNamesHandler)
            
        # Setup a button handler for launching the rotation matrix dialog.
        self.view.setSensorBodyRotationButton.actionPerformed = (
            self.setSensorBodyRotationHandler)
            
        # Disable any non-editable fields.
        self.view.sensorBodyRotationTextField.enabled = False
        
        # Call a worker to fill-in the initial values of the GUI.
        self.populateFields()
        
    # End __init__.
    
    def populateFields(self):
        """
        Called at the beginning to populate the fields in the GUI.
        """
        
        # Get the set of all body segments to populate the combo boxes.
        # In order for the two combox boxes to operate independently they have
        # to have different data models.  
        bodyNames = []
        bodySet = self.main.simObjects.model.getBodySet()     
        for idx in range(bodySet.getSize()):
            bodyNames.append(bodySet.get(idx).getName())
            
        # Don't sort the body names here -- list them in the same order as
        # OpenSim gives them to help with look/feel consistency between this
        # tool and OpenSim.
        self.view.appliedToBodyCombo.setModel(DefaultComboBoxModel(bodyNames))
        
        # Set the possible tracked positions in their combo boxes.
        self.view.sensorTrackedPositionCombo.setModel(
            DefaultComboBoxModel(sorted(
                self.main.model.trackedPositions.keys())))
        self.view.appliedToTrackedPositionCombo.setModel(
            DefaultComboBoxModel(sorted(
                self.main.model.trackedPositions.keys())))
        
        # At this point, if we are adding a new load cell we don't have anything
        # more to populate.
        if not self.initialSensorName:
            return
            
        # If this is an editing job, then we can fill in the data as 
        # appropriate.  Check that the model actually contains the load cell.
        if self.initialSensorName not in self.loadCellDict:
            print 'Could not find named force/torque sensor'
            return
            
        # Get a reference to the load cell.
        loadCell = self.loadCellDict[self.initialSensorName]
        
        self.view.sensorNameTextField.text = self.initialSensorName
        self.view.sensorForceTextField.text = loadCell.sensorForceName
        self.view.bodyForceTextField.text = loadCell.bodyForceName
        self.view.groundForceTextField.text = loadCell.groundForceName
        self.view.appliedForceTextField.text = loadCell.appliedForceName
        self.view.sensorTorqueTextField.text = loadCell.sensorTorqueName
        self.view.bodyTorqueTextField.text = loadCell.bodyTorqueName
        self.view.groundTorqueTextField.text = loadCell.groundTorqueName
        self.view.appliedTorqueTextField.text = (
            loadCell.appliedTorqueName)
        
        # Attempt to set the combo boxes to the appropriate values.
        self.view.appliedToBodyCombo.setSelectedItem(loadCell.appliedToBodyName)
        self.view.sensorTrackedPositionCombo.setSelectedItem(
            loadCell.sensorTrackedPositionName)
        self.view.appliedToTrackedPositionCombo.setSelectedItem(
            loadCell.appliedToTrackedPositionName)        
        
        # Set the rotation matrix array as a string.
        self.view.sensorBodyRotationTextField.text = str(
            loadCell.sensorToBodyRotationArray)
        
    # End populateFields.
        
    def validateStateAndLatch(self):
        """
        Checks the data is valid and saves it to member data.
        
        Presumably the dialog presenter will call this when the user hits
        the 'OK' button.
        """
        
        # This panel can be used in conjunction with others.  When the user
        # hits the "OK" button, this panel may validate and latch but another
        # panel may fail.  If they change a bunch of data and try to hit "OK"
        # again, we want to make sure we clear our result before validating
        # again.
        self.nameAndLoadCell = None
        
        try:
            # Go through each of the fields and make sure they have data.
            if self.view.sensorNameTextField.text == '':
                raise ValueError('Sensor name is empty.')
            
            if self.view.sensorForceTextField.text == '':
                raise ValueError('Sensor force name is empty.')
            
            if self.view.bodyForceTextField.text == '':
                raise ValueError('Body force name is empty.')
                
            if self.view.groundForceTextField.text == '':
                raise ValueError('Ground force name is empty.')
                
            if self.view.appliedForceTextField.text == '':
                raise ValueError('Applied force name is empty.')
            
            if self.view.sensorTorqueTextField.text == '':
                raise ValueError('Sensor torque name is empty.')
                
            if self.view.bodyTorqueTextField.text == '':
                raise ValueError('Body torque name is empty.')
                
            if self.view.groundTorqueTextField.text == '':
                raise ValueError('Ground torque name is empty.')
                
            if self.view.appliedTorqueTextField.text == '':
                raise ValueError('Applied torque name is empty.')
                
            if self.view.sensorBodyRotationTextField.text == '':
                raise ValueError('Sensor to body rotation is empty.')
                    
            # Todo: error on tracked positions not being filled in.
                    
            # Make sure the name of the sensor doesn't clash with an existing
            # name.  Of course don't worry if we are editing a sensor and the
            # name stays the same...
            if self.view.sensorNameTextField.text != self.initialSensorName:
                if (self.view.sensorNameTextField.text in 
                    self.main.model.realLoadCellForceSets):                    
                    raise ValueError('Sensor name already exists.')
                
                # I'm not sure it's a real problem but I'm going to disallow
                # anyway:  make sure this sensor name does match with a
                # synthetic sensor name either.
                if (self.view.sensorNameTextField.text in 
                    self.main.model.syntheticLoadCellForceSets):                    
                    raise ValueError('Sensor name already exists as a synthetic '
                        'sensor name.') 
                
        except ValueError, error:
            JOptionPane.showMessageDialog(self.main.frame,
                str(error),
                "Error",
                JOptionPane.ERROR_MESSAGE);
                
            return False
                
        # If we're here then all data is valid.  Latch and return True.
        loadCell = CF.LoadCellForceSet(
            self.view.sensorForceTextField.text,
            self.view.sensorTorqueTextField.text,
            self.view.bodyForceTextField.text,
            self.view.bodyTorqueTextField.text,
            self.view.groundForceTextField.text,
            self.view.groundTorqueTextField.text,
            self.view.appliedForceTextField.text,
            self.view.appliedTorqueTextField.text,        
            self.view.sensorTrackedPositionCombo.getSelectedItem(),
            self.view.appliedToBodyCombo.getSelectedItem(),
            self.view.appliedToTrackedPositionCombo.getSelectedItem(),
            TC.xmlStringTupleToTuple(
                self.view.sensorBodyRotationTextField.text))
                        
        # The latched value is actually a tuple, combining the position name
        # with the load cell object (since the LC class doesn't hold
        # the name).
        self.nameAndLoadCell = (self.view.sensorNameTextField.text, loadCell)
            
        return True
        
    # End validateStateAndLatch.
        
    def setDefaultNamesHandler(self, event):
    
        # Take the sensor name and append standard suffixes for each.
        sensorName = self.view.sensorNameTextField.text
        
        self.view.sensorForceTextField.text = sensorName + '_sensor_f'
        self.view.bodyForceTextField.text = sensorName + '_body_f'
        self.view.groundForceTextField.text = sensorName + '_ground_f'
        self.view.appliedForceTextField.text = sensorName + '_applied_f'
        self.view.sensorTorqueTextField.text = sensorName + '_sensor_t'
        self.view.bodyTorqueTextField.text = sensorName + '_body_t'
        self.view.groundTorqueTextField.text = sensorName + '_ground_t'
        self.view.appliedTorqueTextField.text = sensorName + '_applied_t'
    
    # End setDefaultNamesHandler.
    
    def setSensorBodyRotationHandler(self, event):
    
        # Ok, we need to instantiate and launch a modal dialog ourselves.
        dlg = RotationMatrix.RotationMatrixDialog(self.main, 
            'Sensor Frame',
            'Body Frame', 
            self.view.sensorBodyRotationTextField.text)
        dlg.show()
        
        # The interface for determining success or failure isn't the clearest.
        if dlg.rotationMatrixArray:
            self.view.sensorBodyRotationTextField.text = str(
                dlg.rotationMatrixArray)
            
    # End setSensorBodyRotationHandler.
        
# End EditSensorPanel.

class EditRealSensorDialog:
    """
    This is the presenter for the "Real Force/Torque Sensor" frame.
    """
    
    def __init__(self, main, initialSensorName=None):
    
        # Get a backpointer to the main object -- this is where we will get
        # the GUI elements and underlying data.
        self.main = main
        
        # Whether or not we are creating a new sensor or editing an existing
        # one.
        self.initialSensorName = initialSensorName
        
        # Ultimately this is going to be the record of the new or modified
        # load cell object on successful completion of this dialog.
        self.nameAndLoadCell = None
                
    # End __init__.
    
    def show(self):
        """
        Show the modal dialog.
        """
        
        dlg = ViewEditRealSensorDialog(self.main.frame, True)
        dlg.title = 'Add/Edit Real Force/Torque Sensor'
        dlg.defaultCloseOperation = JFrame.DISPOSE_ON_CLOSE
        dlg.windowClosing = self.closingHandler
        
        # The large guts of this frame are in the contained edit sensor panel.
        # Our presenters mimic the structure of the Java view objects.
        editPanel = EditSensorPanel(self.main, 
            self.initialSensorName, 
            self.main.model.realLoadCellForceSets, 
            dlg.editSensorPanelView)  
        
        # Add in callbacks for the Ok and Cancel buttons.
        dlg.okButton.actionPerformed = lambda event: self.okHandler(event, dlg, editPanel)
        dlg.cancelButton.actionPerformed = lambda event: self.cancelHandler(event, dlg)
                
        # Show the modal dialog.
        dlg.visible = True
        
    # End show.
    
    def okHandler(self, event, dlg, panel):
        
        # See if the data is valid.  If not, the panel will throw up an
        # error dialog and we want to give them a chance to fix, so we don'tell
        # close the dialog in that case.
        if not panel.validateStateAndLatch():
            return
            
        self.nameAndLoadCell = panel.nameAndLoadCell
        dlg.visible = False
    
    # End okHandler.
    
    def cancelHandler(self, event, dlg):
    
        # For now, this is a no-op (other than closing the window!).        
        dlg.visible = False
    
    # End cancelHandler.
    
    def closingHandler(self, event):
    
        # For now, this is a no-op.
        pass
        
    # End closingHandler.
    
# End EditRealSensorDialog.

