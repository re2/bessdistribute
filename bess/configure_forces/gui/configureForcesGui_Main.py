

################################################################################
#
# This script contains the guts of code for dealing with the configureForcesGui.
# Mainly it provides all the functionality of intelligently dealing with the
# Java GUI -- we're trying to keep all logic on the Jython side and just use
# NetBeans/Java for it's nice layout abilities.
#
################################################################################

import os
import sys

from javax.swing import JOptionPane

# Add the parent "bess" directory to the Python module search path so that we
# can import modules from different directories.  Subsequent imports of BESS
# scripts may use reload() to ensure that any changes during development get
# reloaded by the interpreter.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
if bessDir not in sys.path:
    sys.path.append(bessDir)

import utilities.toolsCommon as TC; reload(TC)
import configure_forces.configureForces_Support as CF; reload(CF)
import configure_forces.gui.configureForcesGui_General as General; reload(General)
import configure_forces.gui.configureForcesGui_Positions as Positions; reload(Positions)
import configure_forces.gui.configureForcesGui_Sensors as Sensors; reload(Sensors)
import configure_forces.gui.configureForcesGui_Synthetic as Synthetic; reload(Synthetic)
import configure_forces.gui.configureForcesGui_GroundReaction as GroundReaction; reload(GroundReaction)
import configure_forces.gui.configureForcesGui_Strides as Strides; reload(Strides)

# Get the main GUI components from our NetBeans/Java project.
TC.importJar(os.path.join(bessDir, 'lib', 'ConfigureForcesGuiPanels.jar'))
from bess.gui.configureforces import MainPanel

class Model:
    """
    Just a large holding class for all of the underlying data that reflects
    the state of the configure forces tool.
    
    This is the "model" in the model-view-presenter pattern.
    """
    def __init__(self):
        # These are the data objects that map to the configure forces XML file.
        self.filePaths = CF.FilePaths()
        self.strideConfig = CF.StrideConfig()
        self.grfForceSets = dict()
        self.trackedPositions = dict()
        self.realLoadCellForceSets = dict()
        self.syntheticLoadCellForceSets = dict()
        self.syntheticDataConfig = dict()
        
# End Model.
    
class SpecifiedFile:
    """
    This is a bundle of information when the user wants to set behavior of some
    of the files.
    
    In standalone, the user needs to be able to set any and all of the 8 files
    exposed by the Configure Forces UI.  When integrating in BESS (or another
    tool?) so files may be specified by BESS so we don't want the user to be
    able to set them.
    """
    def __init__(self, mutable, visible, immutableFilename):
        # Whether or not the file may be changed.  If false, the text field 
        # will be read-only and the browse button will be hidden.
        self.mutable = mutable
        
        # Whether or not to even display the text field and browse button.
        self.visible = visible
        
        # When not mutable, this is what we will display in the text field.
        # Can be None which means the user doesn't want to force a file and
        # we should just keep on using the filename from XML.  An empty string
        # is different than None -- it will force an empty string in the model
        # data.
        self.immutableFilename = immutableFilename
        
# End SpecifiedFile.
    
class Main:
    """
    Put all the code here in an instantiable class for encapsulation.
    
    We could probably get away with globals for this, but using a class
    is better practice.  This class is the main interface class used by
    a client for interacting with the Configure Forces UI elements.
    """
    
    def __init__(self, frame):
        
        # When we display child dialogs we need to specify a parent frame.
        self.frame = frame
        
        # Instantiate the Java class object.  This contains all the tabs and
        # GUI controls.  This is the view.
        self.mainPanel = MainPanel()
        
        # Model contains the underyling concrete data.
        self.model = Model()
        
        # In order to list body segments we need a model (state, etc.).
        self.simObjects = None        
        
        # The user may want to lock down some of the files.
        self.specifiedModelFile = None
        self.specifiedIkFile = None
        self.specifiedGroundReactionFile = None
        self.specifiedExternalLoadSpecificationFile = None
        
        # Farm out the presenter logic for each of the tabs to a separate class
        # object.  We need to be careful that the order that the key/values
        # here match the layout of the UI:  i.e. the "General" tab really is
        # at 0 index, the "Positions" tab is at 1, etc.  We do this so that
        # we can deduce which tab is being displayed and call appropriate
        # event handlers.
        self.tabs = dict()
        self.tabs[0] = General.GeneralTab(self)
        self.tabs[1] = Positions.PositionsTab(self)
        self.tabs[2] = Sensors.SensorsTab(self)
        self.tabs[3] = Synthetic.SyntheticTab(self)
        self.tabs[4] = GroundReaction.GroundReactionTab(self)
        self.tabs[5] = Strides.StridesTab(self)
        
        # Set up a state change listener on the tabbed pane so we can detect
        # when the currently viewed tab changes.
        self.mainPanel.mainTabbedPanel.stateChanged = self.tabChangedHandler
        
    # End __init__.
    
    def tabChangedHandler(self, event):
        """
        Should be called anytime the user changes tabs on the main panel.
        
        Though not called when the UI is first displayed.
        """
        
        tab = self.tabs[self.mainPanel.mainTabbedPanel.getSelectedIndex()]
        tab.tabShown()
        
    # End tabChangedHandler.

    def loadXmlToMainPanel(self, xmlPath):
        """
        Responsible for taking XML and pushing it into the GUI.
        """
        
        # The ConfigureForces support file has the ability to turn an XML
        # configuration file into a set of discrete objects, all of which
        # are defined in the support file.
        (self.model.filePaths, 
            self.model.strideConfig,
            self.model.grfForceSets, 
            self.model.trackedPositions,
            self.model.realLoadCellForceSets,
            self.model.syntheticLoadCellForceSets,
            self.model.syntheticDataConfig) = CF.Config.readFromXmlFile(xmlPath)
                
        # We may have previously specified some of the files, which should
        # override whatever may be in the XML.
        self.updateModelFromSpecifiedFiles()
        
        # Loading a new XML triggers a total reset of the UI to reflect our
        # (new) member data.
        self.setViewFromModel()
                
    # End loadXmlToMainPanel.

    def saveXmlFromMainPanel(self, xmlPath):
        """
        Responsible for pulling data from the GUI and writing to XML.
        """
                        
        CF.Config.writeToXmlFile(xmlPath, 
            self.model.filePaths, 
            self.model.strideConfig,
            self.model.grfForceSets,
            self.model.trackedPositions,
            self.model.realLoadCellForceSets,
            self.model.syntheticLoadCellForceSets,
            self.model.syntheticDataConfig)
        
        return True
        
    # End saveHandler.
    
    def validateModel(self):
        """
        Validates the model and returns an error list.
        """
        
        return CF.Config.validateModel(self.model.filePaths, 
            self.model.strideConfig,
            self.model.grfForceSets,
            self.model.trackedPositions,
            self.model.realLoadCellForceSets,
            self.model.syntheticLoadCellForceSets,
            self.model.syntheticDataConfig)
            
    # End validateModel.
            
    def setViewFromModel(self):
        """
        Tells all the tabs to reset the view from model data.
        """
        
        for tab in self.tabs.values():
            tab.setViewFromModel()
            
    # End setViewFromModel.
            
    def updateModelFromSpecifiedFiles(self):
        """
        Looks to see if a non-mutable file has been specified and updates the
        model with it.
        """
        
        self.model.filePaths.modelPath = (
            self.checkAndUpdateModelFromSpecifiedFile(
                self.model.filePaths.modelPath, 
                self.specifiedModelFile))
        self.model.filePaths.ikPath = (
            self.checkAndUpdateModelFromSpecifiedFile(
                self.model.filePaths.ikPath, 
                self.specifiedIkFile))
        self.model.filePaths.grfPath = (
            self.checkAndUpdateModelFromSpecifiedFile(
                self.model.filePaths.grfPath, 
                self.specifiedGroundReactionFile))
        self.model.filePaths.externalLoadsOutPath = (
            self.checkAndUpdateModelFromSpecifiedFile(
                self.model.filePaths.externalLoadsOutPath, 
                self.specifiedExternalLoadSpecificationFile))
        
    # End updateModelFromSpecifiedFiles.
    
    def checkAndUpdateModelFromSpecifiedFile(self, modelFile, specifiedFile):
        
        if not specifiedFile:
            return modelFile
            
        if specifiedFile.mutable:
            # The immutable file is only applicable if this file is considered
            # immutable.
            return modelFile
            
        if specifiedFile.immutableFilename == None:
            return modelFile
            
        return specifiedFile.immutableFilename
        
    # End checkAndUpdateModelFromSpecifiedFile.
            
    def checkModelLoadedAndShowError(self):
        """
        Convenience function used throughout the GUI to determine if an OpenSim
        model has been loaded.
        
        An OpenSim model is necessary to populate body segments.
        """
        
        if self.simObjects:
            return True
        
        JOptionPane.showMessageDialog(self.frame,
            "OpenSim model file must be specified.  Set this on the 'General' tab.",
            "Error",
            JOptionPane.ERROR_MESSAGE);
    
        return False
        
    # End checkModelLoadedAndShowError.
    
    def specifyModelFile(self, specifiedModelFile):
    
        self.specifiedModelFile = specifiedModelFile
        self.updateModelFromSpecifiedFiles()
        self.setViewFromModel()
        
    # End specifyModelFile.
    
    def specifyIkFile(self, specifiedIkFile):
    
        self.specifiedIkFile = specifiedIkFile
        self.updateModelFromSpecifiedFiles()
        self.setViewFromModel()
        
    # End specifyIkFile.
    
    def specifyGroundReactionForceFile(self, specifiedGroundReactionFile):
    
        self.specifiedGroundReactionFile = specifiedGroundReactionFile
        self.updateModelFromSpecifiedFiles()
        self.setViewFromModel()
        
    # End specifiedGroundReactionFile.
    
    def specifyExternalLoadSpecificationFile(self, 
        specifiedExternalLoadSpecificationFile):
    
        self.specifiedExternalLoadSpecificationFile = (
            specifiedExternalLoadSpecificationFile)
        self.updateModelFromSpecifiedFiles()
        self.setViewFromModel()
        
    # End specifyExternalLoadSpecificationFile.
        
        
# End Main.



