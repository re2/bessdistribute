

################################################################################
#
# This script handles the "Positions" tab of the configure forces GUI.
#
################################################################################

import itertools
import os
import sys

from javax.swing import DefaultListModel

# Add the parent "bess" directory to the Python module search path so that we
# can import modules from different directories.  Subsequent imports of BESS
# scripts may use reload() to ensure that any changes during development get
# reloaded by the interpreter.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
if bessDir not in sys.path:
    sys.path.append(bessDir)

import utilities.toolsCommon as TC; reload(TC)
import configure_forces.gui.configureForcesGui_EditPosition as EditPosition; reload(EditPosition)
import configure_forces.gui.configureForcesGui_TabBase as TB; reload(TB)
    
class PositionsTab(TB.TabBase):

    def __init__(self, main):
        """
        By this point, we can expect that the GUI panels have been created.
        So this function can act as a GUI setup as well.
        """
        
        # Get a backpointer to the main object -- this is where we will get
        # the GUI elements and underlying data.
        self.main = main

        # Add in a click handler for the various buttons on this tab.
        self.main.mainPanel.positionsAddPositionButton.actionPerformed = (
            self.handleAddPositionButtonClick)
        self.main.mainPanel.positionsRemovePositionButton.actionPerformed = (
            self.handleRemovePositionButtonClick)
        self.main.mainPanel.positionEditPositionButton.actionPerformed = (
            self.handleEditPositionButtonClick)
    
        # Initialize the list to have an empty list model, but one that is
        # subsequently mutable.
        self.main.mainPanel.positionsPositionList.setModel(DefaultListModel())
                
    # End __init__.
    
    def setViewFromModel(self):
        """
        Completely repopulates GUI from member data.
        
        Effectively this is a "virtual" override -- the main class expects all
        the tab classes to have this function.
        """
        
        self.populatePositionsList()
        
    # End setViewFromModel.
            
    def handleAddPositionButtonClick(self, event):
        
        if not self.main.checkModelLoadedAndShowError():
            return
                    
        editDialog = EditPosition.EditPositionDialog(self.main, 
            initialPositionName=None)
        editDialog.show()
               
        if editDialog.nameAndTrackedPosition:
            self.onAddPosition(editDialog.nameAndTrackedPosition)
            
    # End handleAddPositionButtonClick.
        
    def handleRemovePositionButtonClick(self, event):
        
        # Figure out what item has been selected.
        idx, name = self.getPositionsListSelection()
        if idx == -1:
            return
            
        self.onRemovePosition(name)
        
    # End handleRemovePositionButtonClick.
        
    def handleEditPositionButtonClick(self, event):
        
        if not self.main.checkModelLoadedAndShowError():
            return
            
        # Figure out what item has been selected.
        idx, name = self.getPositionsListSelection()
        if idx == -1:
            return
                
        editDialog = EditPosition.EditPositionDialog(self.main, 
            initialPositionName=name)
        editDialog.show()
        
        if editDialog.nameAndTrackedPosition:
            # Ok, successful completion of the dialog.  We need to handle the
            # case where the name changed though.
            if name != editDialog.nameAndTrackedPosition[0]:
                # Don't do a redraw since we will do that on the subsequently
                # add operation.
                self.onRemovePosition(name, False)                
                self.onAddPosition(editDialog.nameAndTrackedPosition)
            else:
                self.onModifiedPosition(editDialog.nameAndTrackedPosition)
                
    # End handleEditPositionButtonClick.
        
    def populatePositionsList(self):
        """
        Adds the name of each tracked position to the list.
        """
                
        # Iterate through the names of the tracked positions.
        listModel = self.main.mainPanel.positionsPositionList.model
        listModel.clear()
        
        for name in sorted(self.main.model.trackedPositions):                        
            listModel.addElement(name)
            
    # End populatePositionsList.
    
    def getPositionsListSelection(self):
        list = self.main.mainPanel.positionsPositionList
        
        # If there is no selection, will return -1, None.
        return list.getSelectedIndex(), list.getSelectedValue()        
    
    # End getPositionsListSelection.
    
    def onAddPosition(self, nameAndTrackedPosition, redraw=True):
        # Add the entry to the model data.
        self.main.model.trackedPositions[nameAndTrackedPosition[0]] = (
            nameAndTrackedPosition[1])
    
        # Simply redraw the list box from model data.
        if redraw:
            self.populatePositionsList()
            
    # End onAddPosition.
            
    def onRemovePosition(self, name, redraw=True):
            
        del(self.main.model.trackedPositions[name])
            
        # Simply redraw the list box from model data.
        if redraw:
            self.populatePositionsList()
        
    # End onRemovePosition.
    
    def onModifiedPosition(self, nameAndTrackedPosition):
        # We could probably get away with using the "onAddPosition" function,
        # but it might be helpful to keep these separate.
        self.main.model.trackedPositions[nameAndTrackedPosition[0]] = (
            nameAndTrackedPosition[1])
            
    # End onModifiedPosition.
    
# End PositionsTab.


