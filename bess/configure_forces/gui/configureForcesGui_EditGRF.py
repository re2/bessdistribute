

################################################################################
#
# This script handles the add or editing of a ground reaction force in the 
# Configure Forces UI.
#
################################################################################

import itertools
import os
import sys

from org.opensim import modeling

from javax.swing import DefaultComboBoxModel, JCheckBox, JComboBox
from javax.swing import JFrame, JOptionPane

# Add the parent "bess" directory to the Python module search path so that we
# can import modules from different directories.  Subsequent imports of BESS
# scripts may use reload() to ensure that any changes during development get
# reloaded by the interpreter.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
if bessDir not in sys.path:
    sys.path.append(bessDir)
    
import utilities.toolsCommon as TC; reload(TC)
import configure_forces.configureForces_Support as CF; reload(CF)

TC.importJar(os.path.join(bessDir, 'lib', 'ConfigureForcesGuiPanels.jar'))
from bess.gui.configureforces import EditGRF_Dialog as ViewEditGRF_Dialog

class EditGRF_Panel:
    """
    This is the presenter for the "Edit GRF" panel.  This is used by a
    wrapper dialog, and ultimately by the "Ground Reactions" tab of the 
    Configure Forces UI.
    """
    
    def __init__(self, main, initialGRF_Name, view):
                
        # Get a backpointer to the main object -- this is where we will get
        # the GUI elements and underlying data.
        self.main = main
        
        # Whether or not we are creating a new GRF or editing an existing
        # one.
        self.initialGRF_Name = initialGRF_Name
                
        # This is the actual Java view object.
        self.view = view
        
        # Ultimately this is going to be the record of the new or modified
        # ground reaction forceset object on successful completion of this 
        # dialog.
        self.nameAndForceSet = None
        
        # Setup a button handler for setting default names for force, positions,
        # and torques.
        self.view.setCommonRightNamesButton.actionPerformed = (
            self.setCommonRightNamesHandler)
        self.view.setCommonLeftNamesButton.actionPerformed = (
            self.setCommonLeftNamesHandler)
            
        # Setup a checkbox handler for selecting whether to select a 
        # force/torque sensor.
        self.view.sensorCheck.actionPerformed = self.sensorCheckHandler
                    
        # Call a worker to fill-in the initial values of the GUI.
        self.populateFields()
        
    # End __init__.
    
    def populateFields(self):
        """
        Called at the beginning to populate the fields in the GUI.
        """
        
        # Get the set of all body segments to populate the combo box.
        bodyNames = []
        bodySet = self.main.simObjects.model.getBodySet()     
        for idx in range(bodySet.getSize()):
            bodyNames.append(bodySet.get(idx).getName())
            
        # Don't sort the body names here -- list them in the same order as
        # OpenSim gives them to help with look/feel consistency between this
        # tool and OpenSim.
        self.view.appliedToBodyCombo.setModel(DefaultComboBoxModel(bodyNames))
        
        # Populate the force/torque sensor combo with all sensors -- both
        # real and synthetic.
        self.view.sensorNameCombo.setModel(DefaultComboBoxModel(
            list(sorted(itertools.chain(self.main.model.realLoadCellForceSets,
                self.main.model.syntheticLoadCellForceSets)))))
        
        # Make sure we initially handle the state of the checkbox.
        self.onChangeSensorCheck()
        
        # At this point, if we are adding a new GRF we don't have anything
        # more to populate.
        if not self.initialGRF_Name:
            return
            
        # If this is an editing job, then we can fill in the data as 
        # appropriate.  Check that the model actually contains the force set.
        if self.initialGRF_Name not in self.main.model.grfForceSets:
            print 'Could not find named ground reaction force set.'
            return
            
        # Get a reference to the GRF.
        grf = self.main.model.grfForceSets[self.initialGRF_Name]
        
        self.view.forceSetNameTextField.text = self.initialGRF_Name
        self.view.forceNameTextField.text = grf.forceName
        self.view.positionNameTextField.text = grf.positionName
        self.view.torqueNameTextField.text = grf.torqueName
                
        self.view.appliedToBodyCombo.setSelectedItem(grf.appliedToBodyName)
        self.view.sensorNameCombo.setSelectedItem(grf.exosekeletonLoadCellName)
        
        # Set the sensor check box and call the handler to act upon
        # its value.  We don't actually track this as a boolean, but rather
        # look to see if the sensor name is blank or not.
        self.view.sensorCheck.selected = (grf.exosekeletonLoadCellName != '')
        self.onChangeSensorCheck()
                
    # End populateFields.
    
    def validateStateAndLatch(self):
        """
        Checks the data is valid and saves it to member data.
        
        Presumably the dialog presenter will call this when the user hits
        the 'OK' button.
        """
        
        # Good practice to clear out the result before using it for indicating
        # success.
        self.nameAndForceSet = None
        
        try:
            # Go through each of the fields and make sure they have data.
            if self.view.forceSetNameTextField.text == '':
                raise ValueError('Ground reaction force set name is empty.')
            
            if self.view.forceNameTextField.text == '':
                raise ValueError('Force name is empty.')
            
            if self.view.positionNameTextField.text == '':
                raise ValueError('Position name is empty.')
                
            if self.view.torqueNameTextField.text == '':
                raise ValueError('Torque name is empty.')
                
            # Todo: error if force/torque sensor not filled in.
                                
            # Make sure the name of the GRF doesn't clash with an existing
            # name.  Of course don't worry if we are editing a GRF and the
            # name stays the same...
            if (self.view.forceSetNameTextField.text != self.initialGRF_Name
                and (self.view.forceSetNameTextField.text in 
                    self.main.model.grfForceSets)):
                raise ValueError('Ground reaction force set name already '
                    'exists.')
                
        except ValueError, error:
            JOptionPane.showMessageDialog(self.main.frame,
                str(error),
                "Error",
                JOptionPane.ERROR_MESSAGE);
                
            return False
                
        # If we're here then all data is valid.  Latch and return True.
        if self.view.sensorCheck.selected:
            sensorName = self.view.sensorNameCombo.getSelectedItem()
        else:
            sensorName = ''
            
        forceSet = CF.GroundReactionForceSet(self.view.forceNameTextField.text, 
            self.view.positionNameTextField.text,
            self.view.torqueNameTextField.text,
            self.view.appliedToBodyCombo.getSelectedItem(),
            sensorName)
            
        # The latched value is actually a tuple, combining the GRF name
        # with the concrete object (since the GRF class doesn't hold
        # the name).
        self.nameAndForceSet = (self.view.forceSetNameTextField.text, forceSet)
            
        return True
        
    # End validateStateAndLatch.
        
    def setCommonRightNamesHandler(self, event):
        """
        Uses the common OpenSim convention for GRF names.
        """
        
        self.view.forceNameTextField.text = 'ground_force_v'
        self.view.positionNameTextField.text = 'ground_force_p'
        self.view.torqueNameTextField.text = 'ground_torque_'        
    
    # End setCommonRightNamesHandler.
    
    def setCommonLeftNamesHandler(self, event):
        """
        Uses the common OpenSim convention for GRF names.
        """
        
        self.view.forceNameTextField.text = '1_ground_force_v'
        self.view.positionNameTextField.text = '1_ground_force_p'
        self.view.torqueNameTextField.text = '1_ground_torque_'        
    
    # End setCommonLeftNamesHandler.
    
    def sensorCheckHandler(self, event):
        self.onChangeSensorCheck()
        
    # End sensorCheckHandler.
    
    def onChangeSensorCheck(self):
        self.view.sensorNameCombo.enabled = self.view.sensorCheck.selected
        
    # End onChangeSensorCheck.
        
# End EditGRF_Panel.

class EditGRF_Dialog:
    """
    This is the wrapper for the edit ground reaction force set panel.
    """
    
    def __init__(self, main, initialGRF_Name=None):
    
        # Get a backpointer to the main object -- this is where we will get
        # the GUI elements and underlying data.
        self.main = main
        
        # Whether or not we are creating a new GRF or editing an existing
        # one.
        self.initialGRF_Name = initialGRF_Name
        
        # Ultimately this is going to be the record of the new or modified
        # GRF object on successful completion of this dialog.
        self.nameAndForceSet = None
                
    # End __init__.
    
    def show(self):
        """
        Show the modal dialog.
        """
        
        dlg = ViewEditGRF_Dialog(self.main.frame, True)
        dlg.title = 'Add/Edit Ground Reaction Force Set'
        dlg.defaultCloseOperation = JFrame.DISPOSE_ON_CLOSE
        dlg.windowClosing = self.closingHandler
        
        # The large guts of this frame are in the contained edit GRF panel.
        # Our presenters mimic the structure of the Java view objects.
        editPanel = EditGRF_Panel(self.main, 
            self.initialGRF_Name,
            dlg.editGRF_PanelView)  
        
        # Add in callbacks for the Ok and Cancel buttons.
        dlg.okButton.actionPerformed = lambda event: self.okHandler(event, 
            dlg, 
            editPanel)
        dlg.cancelButton.actionPerformed = lambda event: self.cancelHandler(
            event, 
            dlg)
                
        # Show the modal dialog.
        dlg.visible = True
        
    # End show.
    
    def okHandler(self, event, dlg, panel):
        
        # See if the data is valid.  If not, the panel will throw up an
        # error dialog and we want to give them a chance to fix, so we don'tell
        # close the dialog in that case.
        if not panel.validateStateAndLatch():
            return
            
        self.nameAndForceSet = panel.nameAndForceSet
        dlg.visible = False
    
    # End okHandler.
    
    def cancelHandler(self, event, dlg):
    
        # For now, this is a no-op (other than closing the window!).        
        dlg.visible = False
    
    # End cancelHandler.
    
    def closingHandler(self, event):
    
        # For now, this is a no-op.
        pass
        
    # End closingHandler.
    
# End EditGRF_Dialog.

