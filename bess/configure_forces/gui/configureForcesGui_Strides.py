

################################################################################
#
# This script handles the "Strides" tab of the configure forces GUI.
#
################################################################################

import os
import sys

from java.awt.event import ItemEvent
from javax.swing import DefaultComboBoxModel, JComboBox, JOptionPane

# Add the parent "bess" directory to the Python module search path so that we
# can import modules from different directories.  Subsequent imports of BESS
# scripts may use reload() to ensure that any changes during development get
# reloaded by the interpreter.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
if bessDir not in sys.path:
    sys.path.append(bessDir)

import utilities.toolsCommon as TC; reload(TC)
import configure_forces.gui.configureForcesGui_TabBase as TB; reload(TB)
    
class StridesTab(TB.TabBase):

    def __init__(self, main):
        """
        By this point, we can expect that the GUI panels have been created.
        So this function can act as a GUI setup as well.
        """
        
        # Get a backpointer to the main object -- this is where we will get
        # the GUI elements and underlying data.
        self.main = main

        self.main.mainPanel.stridesLowerForceThresholdTextField.focusLost = (
            self.handleLowerHysteresisTextFieldFocusLost)
        self.main.mainPanel.stridesHigherForceThresholdTextField.focusLost = (
            self.handleHigherHysteresisTextFieldFocusLost)
            
        self.main.mainPanel.stridesXmlOutputTextField.focusLost = (
            self.handleXmlOutputTextFieldFocusLost)
        self.main.mainPanel.stridesStoOutputTextField.focusLost = (
            self.handleStoOutputTextFieldFocusLost)
        
        # Add in a click handler for the various buttons on this tab.
        self.main.mainPanel.stridesXmlOutputBrowseButton.actionPerformed = (
            self.handleXmlOutputFileBrowseButtonClick)
        self.main.mainPanel.stridesStoOutputBrowseButton.actionPerformed = (
            self.handleStoOutputFileBrowseButtonClick)
                    
        # Handle changes in the combo boxes.
        self.main.mainPanel.stridesRightGRF_NameCombo.itemStateChanged = (
            self.rightGRF_ComboHandler)
        self.main.mainPanel.stridesLeftGRF_NameCombo.itemStateChanged = (
            self.leftGRF_ComboHandler)
        
    # End __init__.
    
    def tabShown(self):
        """
        Override of base class.
        
        Our combo boxes for what GRF to use for stride identification aren't
        necessarily static -- they can change with user input.
        """
        self.resetGRF_Combos()
        
    # End tabShown.
    
    def resetGRF_Combos(self):
        """
        Called at the beginning to populate the fields in the GUI.
        """
        
        grfNames = sorted(self.main.model.grfForceSets.keys())
        self.main.mainPanel.stridesRightGRF_NameCombo.setModel(
            DefaultComboBoxModel(grfNames))
        self.main.mainPanel.stridesLeftGRF_NameCombo.setModel(
            DefaultComboBoxModel(grfNames))
            
        # Attempt to make selections consistent with model data.
        self.main.mainPanel.stridesRightGRF_NameCombo.setSelectedItem(
            self.main.model.strideConfig.rightGroundReactionForceSetName)
        self.main.mainPanel.stridesLeftGRF_NameCombo.setSelectedItem(
            self.main.model.strideConfig.leftGroundReactionForceSetName)
            
        # It's possible that a change has been made here (for example a user
        # deleted a GRF on another tab and has switched to this one and we've
        # automatically selected another from the combox.  So call the handler
        # to make sure the model reflects the current view.
        self.onChangeGRF_Combos()
        
    # End resetGRF_Combos.
    
    def setViewFromModel(self):
        """
        Completely repopulates GUI from member data.
        
        Effectively this is a "virtual" override -- the main class expects all
        the tab classes to have this function.
        """
        
        self.onChangeHysteresis()
        self.resetGRF_Combos()
        self.onChangeXmlOutputFile()
        self.onChangeStoOutputFile()
        
    # End setViewFromModel.
        
    def populateHysteresis(self):
    
        self.main.mainPanel.stridesLowerForceThresholdTextField.text = str(
            self.main.model.strideConfig.midpointHysteresis[0])
        self.main.mainPanel.stridesHigherForceThresholdTextField.text = str(
            self.main.model.strideConfig.midpointHysteresis[1])
    
    # End populateHysteresis.
    
    def handleLowerHysteresisTextFieldFocusLost(self, event):
    
        lowValueText = (self.main.mainPanel.
            stridesLowerForceThresholdTextField.text)
        highValueText = (self.main.mainPanel.
            stridesHigherForceThresholdTextField.text)
            
        if not TC.isTextValidFloat(lowValueText):
            JOptionPane.showMessageDialog(self.main.frame,
                'Lower force threshold is not a valid number.  Reverting '
                    'to previous value.',
                "Error",
                JOptionPane.ERROR_MESSAGE)
            
            self.onChangeHysteresis()
            return
            
        # Handle weird case where user puts an invalid value into the higher
        # hysteresis text field and then clicks on this text field.  Throwing
        # the error from the higher hysteresis will also cause this text field
        # to lose focus.
        if not TC.isTextValidFloat(highValueText):
            return
            
        midpointHysteresis = (float(lowValueText), float(highValueText))
        self.onChangeHysteresis(midpointHysteresis)
    
    # End handleLowerHysteresisTextFieldFocusLost.
    
    def handleHigherHysteresisTextFieldFocusLost(self, event):
    
        lowValueText = (self.main.mainPanel.
            stridesLowerForceThresholdTextField.text)
        highValueText = (self.main.mainPanel.
            stridesHigherForceThresholdTextField.text)
            
        if not TC.isTextValidFloat(highValueText):
            JOptionPane.showMessageDialog(self.main.frame,
                'Higher force threshold is not a valid number.  Reverting '
                    'to previous value.',
                "Error",
                JOptionPane.ERROR_MESSAGE)
            
            self.onChangeHysteresis()
            return
            
        # Handle weird case where user puts an invalid value into the lower
        # hysteresis text field and then clicks on this text field.  Throwing
        # the error from the lower hysteresis will also cause this text field
        # to lose focus.
        if not TC.isTextValidFloat(lowValueText):
            return
            
        midpointHysteresis = (float(lowValueText), float(highValueText))
        self.onChangeHysteresis(midpointHysteresis)
    
    # End handleHigherHysteresisTextFieldFocusLost.
    
    def handleXmlOutputTextFieldFocusLost(self, event):
        """
        When losing focus we update the model.
        """
        
        self.onChangeXmlOutputFile(self.main.mainPanel.
            stridesXmlOutputTextField.text)
        
    # End handleXmlOutputTextFieldFocusLost.        

    def handleStoOutputTextFieldFocusLost(self, event):
        """
        When losing focus we update the model.
        """
        
        self.onChangeStoOutputFile(self.main.mainPanel.
            stridesStoOutputTextField.text)
        
    # End handleStoOutputTextFieldFocusLost.      
    
    def handleXmlOutputFileBrowseButtonClick(self, event):
        """
        Event handler for browse button to select an XML output file.
        """
        
        try:
            path = TC.getSingleFilePath(
                'Select strides XML output file',
                TC.FileFilter('Strides file (.xml)', 'xml'))
        except:
            # If nothing is selected, an exception is thrown.
            return
        
        # Anytime we set/change the file path, call the on-change function.
        self.onChangeXmlOutputFile(path)
        
    # End handleXmlOutputFileBrowseButtonClick.
    
    def handleStoOutputFileBrowseButtonClick(self, event):
        """
        Event handler for browse button to select a STO output file.
        """
        
        try:
            path = TC.getSingleFilePath(
                'Select strides STO output file',
                TC.FileFilter('Strides file (.sto)', 'sto'))
        except:
            # If nothing is selected, an exception is thrown.
            return
            
        # Anytime we set/change the file path, call the on-change function.
        self.onChangeStoOutputFile(path)
        
    # End handleStoOutputFileBrowseButtonClick.
                    
    def onChangeHysteresis(self, hysteresis=None):
        
        if hysteresis:
            self.main.model.strideConfig.midpointHysteresis = hysteresis
            
        self.populateHysteresis()
        
    # End onChangeHysteresis.
                    
    def onChangeXmlOutputFile(self, xmlPath=None):
        """
        Called anytime we set or change the XML output file.
        """
        
        # Update the model data with the new path now that it is validated.
        if xmlPath:
            self.main.model.filePaths.stridesXmlOutPath = xmlPath
        
        # Change the view to correspond to the new model data.
        self.populateXmlOutputFileTextField()
        
    # End onChangeXmlOutputFile.
    
    def onChangeStoOutputFile(self, stoPath=None):
        """
        Called anytime we set or change the STO output file.
        """
        
        # Update the model data.
        if stoPath:
            self.main.model.filePaths.stridesStoOutPath = stoPath
             
        # Change the view to correspond to the new data.
        self.populateStoOutputFileTextField()
        
    # End onChangeStoOutputFile.
            
    def populateXmlOutputFileTextField(self):
        """
        Fills in the XML output file text field from model data.
        """
        
        self.main.mainPanel.stridesXmlOutputTextField.text = (
            self.main.model.filePaths.stridesXmlOutPath)
            
    # End populateXmlOutputFileTextField.
    
    def populateStoOutputFileTextField(self):
        """
        Fills in the STO output file text field from model data.
        """
        
        self.main.mainPanel.stridesStoOutputTextField.text = (
            self.main.model.filePaths.stridesStoOutPath)
            
    # End populateStoOutputFileTextField.
    
    def rightGRF_ComboHandler(self, itemEvent):
        
        if itemEvent.getStateChange() == ItemEvent.SELECTED:
            self.onChangeGRF_Combos()
        
    # End rightGRF_ComboHandler.
    
    def leftGRF_ComboHandler(self, itemEvent):
        
        if itemEvent.getStateChange() == ItemEvent.SELECTED:
            self.onChangeGRF_Combos()
        
    # End leftGRF_ComboHandler.
    
    def onChangeGRF_Combos(self):
    
        self.main.model.strideConfig.rightGroundReactionForceSetName = (
            self.main.mainPanel.stridesRightGRF_NameCombo.getSelectedItem())
        self.main.model.strideConfig.leftGroundReactionForceSetName = (
            self.main.mainPanel.stridesLeftGRF_NameCombo.getSelectedItem())
     
    # End onChangeGRF_Combos.
                
# End StridesTab.


