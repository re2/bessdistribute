

################################################################################
#
# This script analyzes marker error of an IK solution.
#
################################################################################

import os
import sys
from threading import Thread

# Add the parent "bess" directory to the Python module search path so that we
# can import modules from different directories.  Subsequent imports of BESS
# scripts may use reload() to ensure that any changes during development get
# reloaded by the interpreter.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
if bessDir not in sys.path:
    sys.path.append(bessDir)

import utilities.toolsCommon as TC; reload(TC)
import re2.analysis.analyzeMarkerError_Support as AME; reload(AME)

if __name__ == '__main__':
    """
    Main logic.
    """
    
    # This version of the scripts requires that we populate user inputs from
    # a series of file browser dialogs.
    userInputs = AME.UserInputs()
    
    userInputs.modelPath = TC.getSingleFilePath(
        'Select OpenSim model file', 
        TC.FileFilter('OpenSim model (.osim)', 'osim'))
    userInputs.trcPath = TC.getSingleFilePath(
        'Select TRC marker data file',
        TC.FileFilter('Marker data files (.trc)', 'trc'))
    userInputs.ikPath = TC.getSingleFilePath(
        'Select inverse kinematics file',
        TC.FileFilter('IK MOT files (.mot)', 'mot'))
    userInputs.rawOutPath = TC.getSingleFilePath(
        'Select raw output file',
        TC.FileFilter('Output file (.sto)', 'sto'))
    userInputs.summaryOutPath = TC.getSingleFilePath(
        'Select summary output file',
        TC.FileFilter('CSV output file (.csv)', 'csv'))
        
    # Run in a worker thread.  This means we don't block the GUI thread when
    # doing a lot of data processing.
    Thread(target=lambda: AME.mainWorker(userInputs)).start()

# End main.


