

################################################################################
#
# The guts of the code to analyze inverse dynamics data.
#
################################################################################

import os
import sys

# Add the parent "bess" directory to the Python module search path so that we
# can import modules from different directories.  Subsequent imports of BESS
# scripts may use reload() to ensure that any changes during development get
# reloaded by the interpreter.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
if bessDir not in sys.path:
    sys.path.append(bessDir)

import utilities.handleStoMot; reload(utilities.handleStoMot)
import utilities.strideAnalysis as SA; reload(SA)
import utilities.strideTracker as ST; reload(ST)

class UserInputs:
    """
    Whether from BESS or from a standalone script, the user needs to provide
    a set of inputs.
    """    
    def __init__(self):        
        # File path to an XML file that contains the stride trackers.
        self.stridePath = None
                
        # File path to a STO/MOT file containing ID force/moment data.
        self.idPath = None
                
        # Output file path where we will write the results that are normalized
        # to the time of an average stride.
        self.averageStrideOutPath = None
        
# End UserInputs.

def mainWorker(userInputs):
    """
    Main function.
    """    
    print 'Starting ID analysis'
    
    # Read in the input dictionaries.
    idDict = getInputDatasets(userInputs)
    
    # Read in the strides from XML to a dictionary.
    strideTrackers = ST.StrideTracker.readDictFromXmlFile(userInputs.stridePath)
    
    # Have a utility function do the tough work of putting data on a gait
    # cycle basis.
    inputData = SA.StridifyInputData('right_stride', 
        handleStoMot.readStoMotAsDict(userInputs.idPath), 
        ['pelvis_tx_force', 'pelvis_ty_force', 'pelvis_tz_force'])
        
    # Get a dictionary of dictionaries of the input data.  The top-level
    # dictionary has keys for the different strides we're tracking (probably
    # "right" and "left".
    outDict = SA.stridifyData(strideTrackers, [inputData])
    
    # We're really only interested in looking at the data normalized to the
    # gait cycle of the right leg.
    handleStoMot.writeDictToStoMot(outDict['right_stride'], 
        userInputs.averageStrideOutPath)
    
    print 'ID analysis script finished'
    
# End mainWorker.

