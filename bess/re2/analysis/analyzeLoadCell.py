

################################################################################
#
# The guts of the code to analyze transformed load cell data.
#
################################################################################

import os
import sys
from threading import Thread

# Add the parent "bess" directory to the Python module search path so that we
# can import modules from different directories.  Subsequent imports of BESS
# scripts may use reload() to ensure that any changes during development get
# reloaded by the interpreter.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
if bessDir not in sys.path:
    sys.path.append(bessDir)

import utilities.toolsCommon as TC; reload(TC)
import re2.analysis.analyzeLoadCell_Support as ALC; reload(ALC)

if __name__ == '__main__':
    """
    Main logic.
    """
    
    # This version of the scripts requires that we populate user inputs from
    # a series of file browser dialogs.
    userInputs = ALC.UserInputs()
    
    userInputs.stridePath = TC.getSingleFilePath(
        'Select strides XML configuration file', 
        TC.FileFilter('XML files (.xml)', 'xml'))
    userInputs.forcesPath = TC.getSingleFilePath(
        'Select external loads data file',
        TC.FileFilter('STO files (.sto)', 'sto'))
    userInputs.averageStrideOutPath = TC.getSingleFilePath(
        'Select output data file',
        TC.FileFilter('STO files (.sto)', 'sto'))
        
    # Run in a worker thread.  This means we don't block the GUI thread when
    # doing a lot of data processing.
    Thread(target=lambda: ALC.mainWorker(userInputs)).start()

# End main.

