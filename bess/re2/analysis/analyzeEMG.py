

################################################################################
#
# This script analyzes EMG and CMC data.  It will bin and average EMG data as a
# percentage of gait cycle and also do the same for CMC activations.
#
################################################################################

import os
import sys
from threading import Thread

# Add the parent "bess" directory to the Python module search path so that we
# can import modules from different directories.  Subsequent imports of BESS
# scripts may use reload() to ensure that any changes during development get
# reloaded by the interpreter.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
if bessDir not in sys.path:
    sys.path.append(bessDir)

import utilities.toolsCommon as TC; reload(TC)
import re2.analysis.analyzeEMG_Support as EMG; reload(EMG)

"""
Main logic
"""
if __name__ == '__main__':
    
    userInputs = EMG.UserInputs()
    
    userInputs.stridePath = TC.getSingleFilePath(
        'Select file containing strides', 
        TC.FileFilter('Stride data file (.xml)', 'xml'))
    userInputs.emgPath = TC.getSingleFilePath(
        'Select EMG data file',
        TC.FileFilter('EMG data files (.sto)', 'sto'))
    userInputs.activationPath = TC.getSingleFilePath(
        'Select CMC states file (*_states.sto)',
        TC.FileFilter('CMC states files (*_states.sto)', 'sto'))
    userInputs.averageStrideOutPath = TC.getSingleFilePath(
        'Select output file',
        TC.FileFilter('Ouput files (.sto)', 'sto'))
            
    # Run in a worker thread.  This means we don't block the GUI thread when
    # doing a lot of data processing.
    Thread(target=lambda: EMG.mainWorker(userInputs)).start()

# End of main.


