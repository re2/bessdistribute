

################################################################################
#
# The guts of the code to analyze marker error in an IK solution.
#
################################################################################

from collections import defaultdict
import csv
import math
import os
import sys

import org.opensim.modeling as modeling

# Add the parent "bess" directory to the Python module search path so that we
# can import modules from different directories.  Subsequent imports of BESS
# scripts may use reload() to ensure that any changes during development get
# reloaded by the interpreter.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
if bessDir not in sys.path:
    sys.path.append(bessDir)

import utilities.handleStoMot as HSM; reload(HSM)
import utilities.handleTRC as HTRC; reload(HTRC)
import utilities.modelManipulation as MM; reload(MM)
import utilities.toolsCommon as TC; reload(TC)

class UserInputs:
    """
    Whether from BESS or from a standalone script, the user needs to provide
    a set of inputs.
    """
    
    def __init__(self):        
        # File path to an OpenSim model file.
        self.modelPath = None
        
        # File path to a TRC file that contains marker positions.
        self.trcPath = None
        
        # File path to an IK solution.
        self.ikPath = None
                
        # Output file path where we will write the raw error results.
        # This should be a storage file (STO) and not a MOT since we can't
        # guarantee the time will be periodic.
        self.rawOutPath = None
        
        # Output file path where we will write the summary error results.
        # This should be a CSV file.
        self.summaryOutPath = None
                
# End UserInputs.

def mainWorker(userInputs):
    """
    Main function.
    """
    
    print 'Starting marker error analysis'
    
    # Read in the input dictionaries.
    trcDict, ikDict = getInputDatasets(userInputs)
    
    # Get OpenSim/SimTK object.
    simObjects = MM.getModelAndState(userInputs.modelPath)
    
    # Come up with a common time base for all of our input datasets.
    commonTimeList = TC.createCommonTimeBase([trcDict['time'], 
        ikDict['time']], allUnique=True)
    
    rawDict, summaryDict = calculateMarkerError(commonTimeList,
        trcDict,
        ikDict,
        simObjects)
        
    # Write out the raw results to a STO/MOT file and then write a CSV
    # of the summarized results.
    HSM.writeDictToStoMot(rawDict, userInputs.rawOutPath)
    writeSummary(summaryDict, userInputs.summaryOutPath)
        
    print 'Marker error analysis script finished'
    
# End mainWorker.
    
def getInputDatasets(userInputs):
    """
    Reads the input datasets into data dictionaries.
    """
    
    # Access the file containing experimental marker data.
    trcDict = HTRC.readTrcAsDict(userInputs.trcPath)
    
    # Read in the IK solution data.
    ikDict = HSM.readStoMotAsDict(userInputs.ikPath)
    
    return trcDict, ikDict
    
# End getInputDatasets.

def calculateMarkerError(commonTimeList, trcDict, ikDict, simObjects):
    """
    Does the hard work of iterating through the data dictionaries and 
    calculating errors.
    """
    
    # We have a nice set of utility functions to handle iteration over multiple
    # data sources.  
    
    # Setup cache storage accessible to the callbacks.
    trcPositions = defaultdict(lambda: modeling.Vec3(0))
    ikPositions = defaultdict(lambda: modeling.Vec3(0))
    rawOut = defaultdict(list)    
    # We'll summarize statistics in a dictionary of dictionaries.  The first
    # dict will have keys of marker names and value of a statistics dictionary.
    # The statistics dictionary will have keys of stat names ("L2 avg. err.")
    # and primitive values.
    summaryOut = defaultdict(dict)
    
    def trcCallback(commonTimeIdx, trcIdx):
        getTRC_MarkerPositions(trcDict, trcIdx, trcPositions)
            
    def ikCallback(commonTimeIdx, ikIdx):
        # When we get to a new line in the IK solution, position the model.
        MM.setModelCoordinates(simObjects, ikDict, ikIdx)    
    
        # Now get the markers.  We have to do old C++03 style iteration.
        markerSet = simObjects.model.getMarkerSet()
        for markerIdx in range(markerSet.getSize()):
            marker = markerSet.get(markerIdx)
            
            # Each marker has a body it is attached to and an offset.
            simObjects.simEngine.transformPosition(simObjects.state, 
                marker.getBody(),
                marker.getOffset(),
                simObjects.groundBody, 
                ikPositions[marker.getName()])
    
    def commonCallback(commonTimeIdx):
        # Use a helper function to keep this local function clean.
        calculateMarkerErrorForTimeStep(commonTimeList[commonTimeIdx], 
            trcDict, 
            ikDict,
            trcPositions,
            ikPositions,
            rawOut)
        
    # Do the iteration.
    TC.iterateMultipleDataSources(commonTimeList,
        [(trcDict['time'], trcCallback),
        (ikDict['time'], ikCallback)],
        commonCallback)
    
    # So, at this point we have got all the raw data.  Please note that if one
    # of the markers didn't have ANY data, then we won't have any raw data
    # (since we only have aggregated raw data for timestamps that we have data
    # for all markers).  So we should protect against that.
    if not rawOut:
        raise ValueError("No data aggregated:  likely one of the markers has "
            "no data")
    
    # Compile some summary statistics.
    composeErrorSummary(ikPositions.keys(), rawOut, summaryOut)
            
    return rawOut, summaryOut
    
# End calculateMarkerError.

def getTRC_MarkerPositions(trcDict, trcIdx, trcPositions):
    """
    Helper function to get the set of marker positions for the given index into
    the TRC data dictionary.
    """
    # When we come up against a new line in the TRC dictionary, just cache
    # the data to a dictionary.
    for name, dataList in trcDict.iteritems():
        # Ignore the time column.
        if name == 'time':
            continue
            
        if not dataList[trcIdx]:
            # Handle missing experimental marker data which can happen when
            # the marker goes out of sight of the cameras.  In the 
            # dictionary the entry will be None.  We need to delete it from
            # the local dictionary to prevent a previous entry from
            # persisting to this time step.  Also not that if data is 
            # missing for one component of the vector, it is missing for
            # all 3 components.
            key = name.rstrip('_xyz')
            if key in trcPositions:
                del(trcPositions[key])
                
            continue
            
        # Attempt to strip the "_x" from the right side and if we're able 
        # to do that then we can fill in the component in the dictionary.
        # Remember that TRC files are ostensibly in millimeters so get it
        # into meters.
        if name != name.rstrip('_x'):
            trcPositions[name.rstrip('_x')].set(0, dataList[trcIdx] / 1000)
            
        if name != name.rstrip('_y'):
            trcPositions[name.rstrip('_y')].set(1, dataList[trcIdx] / 1000)
            
        if name != name.rstrip('_z'):
            trcPositions[name.rstrip('_z')].set(2, dataList[trcIdx] / 1000)

# End getTRC_MarkerPositions.

def calculateMarkerErrorForTimeStep(time,
    trcDict, 
    ikDict,
    trcPositions,
    ikPositions,
    rawOut):
    """
    Helper function which calculates and writes marker error for this time
    in the common iteration over our data dictionaries.
    """
    # Create a local dictionary.  We first write results to this.  After
    # we verify that there is no missing data then we can write it to the
    # main dictionary.
    localRawOut = defaultdict(float)        
    localRawOut['time'] = time
    
    # Iterate over the IK marker positions.  Those are all guaranteed
    # to be there.
    missingData = False
    L2ErrSumSquares = 0
    L2Max = None
    
    for ikName, ikVec in ikPositions.iteritems():
        # If we are missing TRC data for this marker, then go on to the 
        # next one.
        if ikName not in trcPositions:
            missingData = True
            continue
            
        trcVec = trcPositions[ikName]
        # Wow, we could really use numpy here.  :(
        ikPos = (ikVec.get(0), ikVec.get(1), ikVec.get(2))
        trcPos = (trcVec.get(0), trcVec.get(1), trcVec.get(2))
            
        localRawOut[ikName + '_ik_pos_x'] = ikPos[0]
        localRawOut[ikName + '_ik_pos_y'] = ikPos[1]
        localRawOut[ikName + '_ik_pos_z'] = ikPos[2]
        localRawOut[ikName + '_trc_pos_x'] = trcPos[0]
        localRawOut[ikName + '_trc_pos_y'] = trcPos[1]
        localRawOut[ikName + '_trc_pos_z'] = trcPos[2]
        
        err = tuple(x - y for x, y in zip(ikPos, trcPos))
        localRawOut[ikName + '_err_x'] = err[0]
        localRawOut[ikName + '_err_y'] = err[1]
        localRawOut[ikName + '_err_z'] = err[2]
        
        L1Err = sum(abs(x) for x in err)
        L2Err = math.sqrt(sum(x * x for x in err))
        localRawOut[ikName + '_err_L1'] = L1Err
        localRawOut[ikName + '_err_L2'] = L2Err
        
        L2ErrSumSquares += L2Err * L2Err
        L2Max = max(L2Max, L2Err)
        
    # Update the main output dictionary with the local contents from this
    # timestep.  Only do that if we didn't have any missing data though
    # since we can't have any missing data in the main data dictionary
    # (all columns must be the same length).
    if not missingData:
        for key, value in localRawOut.iteritems():
            rawOut[key].append(value)
            
        rawOut['ALL_rms_L2'].append(math.sqrt
            (L2ErrSumSquares / len(ikPositions)))
        rawOut['ALL_max_L2'].append(L2Max)
        
# End calculateMarkerErrorForTimeStep.

def composeErrorSummary(markerNames, rawOut, summaryOut):
    """
    Helper function for when we've gone through all our data and collected
    all the raw data.  We can then revisit the data and create a summary.
    """
    for markerName in markerNames:
        # Create a stats dictionary for the marker.
        markerStats = summaryOut[markerName]
        markerStats['Name'] = markerName
        
        # Get stats on the L2 error.
        mean, stdDev, minValue, maxValue, rms = (
            TC.calculateStatsDataColumn(
                rawOut[markerName + '_err_L2']))
            
        markerStats['L2 Mean'] = mean
        markerStats['L2 StdDev'] = stdDev
        markerStats['L2 Min'] = minValue
        markerStats['L2 Max'] = maxValue
        markerStats['L2 RMS'] = rms
        
        # Get stats on each component of the error.  We want to see average
        # error since this can help to tell us if we have a persistent error
        # in a specific direction.  It's not perfect since these are in the
        # global coordinate and different body segments are rotating, but it's
        # still useful.
        for component in ('x', 'y', 'z'):
            mean, stdDev, minValue, maxValue, rms = (
                TC.calculateStatsDataColumn(
                    rawOut[markerName + '_err_' + component]))
        
            # Report the error mean as an absolute value -- it graphs better
            # this way (everything above the X-axis labels, and we really don't
            # care about direction -- users can dive into graphing the 
            # individual error components on their own -- this is just about
            # alerting to potential problems.
            markerStats[component.upper() + ' Err Abs Mean'] = abs(mean)
            markerStats[component.upper() + ' Err StdDev'] = stdDev

# End composeErrorSummary.

def writeSummary(summaryDict, summaryOutPath):

    # Write out the summary results to a CSV file.  Need the 'b' mode flag to
    # prevent blank lines after each row.
    summaryFile = open(summaryOutPath, 'wb')
    
    # Just easier to write it out ourselves -- no need to use DictWriter
    # (especially in Python 2.5).    
    writer = csv.writer(summaryFile)
    fieldNames = sorted(summaryDict.values()[0].keys())
    fieldNames.remove('Name')
    fieldNames.insert(0, 'Name')
    writer.writerow(fieldNames)
    for markerName in sorted(summaryDict.keys()):
        statsDict = summaryDict[markerName]
        writer.writerow([statsDict[name] for name in fieldNames])
    
    summaryFile.close()
    
# End writeSummary.

