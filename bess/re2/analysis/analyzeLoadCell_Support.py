

################################################################################
#
# The guts of the code to analyze transformed load cell data.
#
################################################################################

from collections import defaultdict
import math
import os
import re
import sys

# Add the parent "bess" directory to the Python module search path so that we
# can import modules from different directories.  Subsequent imports of BESS
# scripts may use reload() to ensure that any changes during development get
# reloaded by the interpreter.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
if bessDir not in sys.path:
    sys.path.append(bessDir)

import utilities.handleStoMot as HSM; reload(HSM)
import utilities.strideAnalysis as SA; reload(SA)
import utilities.strideTracker as ST; reload(ST)

class UserInputs:
    """
    Whether from BESS or from a standalone script, the user needs to provide
    a set of inputs.
    """
    
    def __init__(self):        
        # File path to an XML file that contains the stride trackers.
        self.stridePath = None
                
        # File path to a STO file containing transformed right-side load cell
        # data.
        self.forcesPath = None
                
        # Output file path where we will write the results that are normalized
        # to the time of an average stride.
        self.averageStrideOutPath = None
        
# End UserInputs.

def mainWorker(userInputs):
    """
    Main function.
    """
    
    print 'Starting load cell analysis'
        
    # Read in the strides from XML to a dictionary.
    strideTrackers = ST.StrideTracker.readDictFromXmlFile(userInputs.stridePath)
        
    # Have a utility function do the tough work of putting data on a gait
    # cycle basis.  For now we'll only look at right side data but this can
    # easily be expanded to include left side as well.
    forceDict = HSM.readStoMotAsDict(userInputs.forcesPath)
    forceInputData = SA.StridifyInputData('right_stride', 
        forceDict,
        [name for name in forceDict if re.search('ground_f[xyz]', name)])
                
    # Get a dictionary of dictionaries of the input data.  The top-level
    # dictionary has keys for the different strides we're tracking (probably
    # "right" and "left".
    outDict = SA.stridifyData(strideTrackers, [forceInputData])
            
    HSM.writeDictToStoMot(outDict['right_stride'], 
        userInputs.averageStrideOutPath)
    
    print 'Load cell analysis script finished'
    
# End mainWorker.

