

################################################################################
#
# Closes all plots in the OpenSim GUI.
#
################################################################################

import time
from threading import Thread
 
def closePlots():
    # Errrr, I'm making this up as I go -- there's not very much documentation
    # on this.  However, the following seems to be reasonable.
    while True:
        lastPlot = getLastPlotterPanel()
        
        # If there are no more open plot windows this object should be None.
        if not lastPlot:
            break
        
        # We have to dispose of the enclosing frame I think -- setVisible(False)
        # is not enough for OpenSim to move onto the next plot.
        frame = lastPlot.getFrame()
        frame.dispose()
        
        # Now, wait here until it actually goes away.  Use a timeout, though
        # I've never seen it needed.
        startTime = time.clock()
        while time.clock() - startTime < 2.0:
            # A short sleep seems to help here, both after the initial call 
            # to dispose() and to slow down the loop here from checking too 
            # often.            
            time.sleep(0.1)
            
            # Protect against the frame going out of scope and not being
            # a legitimate object.  This is not an error condition.
            try:
                if not frame.isValid():
                    # Success.  Go get the next plot.
                    break
                else:
                    continue
            except AttributeError:
                # Success.  Go get the next plot.
                break
        else:
            print 'Timeout waiting for plot window to close'
            # This breaks out of the main loop.  We're done due to the failure.
            break
    
        # We've close one window, time to move onto the next one.  Give it a 
        # bit of time to slow the loop down.  Probably not necessary.
        time.sleep(0.05)
        
    print 'Finished trying to close plot windows.'
    
"""
Main logic
"""
if __name__ == '__main__':
    
    # Don't try to close windows from the main GUI thread (which is running
    # __main__).  We need to give the GUI thread back and do our work in a
    # worker thread.
    Thread(target=lambda: closePlots()).start()
    
# End of main.

