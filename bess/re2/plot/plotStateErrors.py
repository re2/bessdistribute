

################################################################################
#
# Plots state errors produced when running CMC or RRA (which uses CMC).  These
# are held in a file with a name pattern like "*_pErr.sto".
#
################################################################################

import os
from threading import Thread

# Add the parent "bess" directory to the Python module search path so that we
# can import modules from different directories.  Subsequent imports of BESS
# scripts may use reload() to ensure that any changes during development get
# reloaded by the interpreter.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
if bessDir not in sys.path:
    sys.path.append(bessDir)

import utilities.toolsCommon as TC; reload(TC)

"""
Creates plots of state errors (like the translational error in the pelvis in
the Z-axis direction).
"""
def mainWorker():
    # Useful for drawing degree bounds on a graph of radians.
    pos1_Deg =  modeling.Constant(0.01745)
    pos1_Deg.setName('+1 deg.')
    neg1_Deg =  modeling.Constant(-0.01745)
    neg1_Deg.setName('-1 deg.')
    
    # Useful for drawing centimeter bounds on a graph of meters.
    pos2_CM =  modeling.Constant(0.02)
    pos2_CM.setName('+2 cm')
    neg2_CM =  modeling.Constant(-0.02)
    neg2_CM.setName('-2 cm')
    
    # Let the user select the file to load.
    sourcePromptString = 'Select state error file (*_pErr.sto)'
    sourceFileFullPath = TC.getSingleFilePath(sourcePromptString, 
        TC.FileFilter('STO files', 'sto'))
    
    # First plot will be pelvis translation errors.
    panel = createPlotterPanel("Pelvis Translation Errors")
    
    # This has the effect of opening the data source for use.  If we're smart
    # this will be a one-time hit.
    print 'Loading data source, this could take a bit of time for large files...'
    dataSource = addDataSource(panel, sourceFileFullPath)    
    
    curve = addCurve(panel, dataSource, 'time', 'pelvis_tx')
    setCurveLegend(curve, 'X')
    curve = addCurve(panel, dataSource, 'time', 'pelvis_ty')
    setCurveLegend(curve, 'Y')
    curve = addCurve(panel, dataSource, 'time', 'pelvis_tz')
    setCurveLegend(curve, 'Z')
    curve = addFunctionCurve(panel, pos2_CM)
    curve = addFunctionCurve(panel, neg2_CM)    
    panel.setXAxisLabel("Time (s)")
    panel.setYAxisLabel("Error (m)")
    
    # Next will be pelvis angle errors.
    panel = createPlotterPanel("Pelvis Angle Errors")
    panel.getPlotterModel().addSource(dataSource);
    curve = addCurve(panel, dataSource, 'time', 'pelvis_tilt')
    setCurveLegend(curve, 'Tilt')
    curve = addCurve(panel, dataSource, 'time', 'pelvis_list')
    setCurveLegend(curve, 'List')
    curve = addCurve(panel, dataSource, 'time', 'pelvis_rotation')
    setCurveLegend(curve, 'Rotation')    
    curve = addFunctionCurve(panel, pos1_Deg)
    curve = addFunctionCurve(panel, neg1_Deg)    
    panel.setXAxisLabel("Time (s)")
    panel.setYAxisLabel("Error (rad)")
    
    # Next will be right hip angle errors.
    panel = createPlotterPanel("Right Hip Angle Errors")
    panel.getPlotterModel().addSource(dataSource);
    curve = addCurve(panel, dataSource, 'time', 'hip_flexion_r')
    setCurveLegend(curve, 'Flexion')
    curve = addCurve(panel, dataSource, 'time', 'hip_adduction_r')
    setCurveLegend(curve, 'Adduction')
    curve = addCurve(panel, dataSource, 'time', 'hip_rotation_r')
    setCurveLegend(curve, 'Rotation')    
    curve = addFunctionCurve(panel, pos1_Deg)
    curve = addFunctionCurve(panel, neg1_Deg)    
    panel.setXAxisLabel("Time (s)")
    panel.setYAxisLabel("Error (rad)")
    
    # Next will be leg angle errors.
    panel = createPlotterPanel("Leg Angle Errors")
    panel.getPlotterModel().addSource(dataSource);
    curve = addCurve(panel, dataSource, 'time', 'knee_angle_r')
    setCurveLegend(curve, 'Right Knee')
    curve = addCurve(panel, dataSource, 'time', 'ankle_angle_r')
    setCurveLegend(curve, 'Right Ankle')
    curve = addCurve(panel, dataSource, 'time', 'knee_angle_l')
    setCurveLegend(curve, 'Left Knee')
    curve = addCurve(panel, dataSource, 'time', 'ankle_angle_l')
    setCurveLegend(curve, 'Left Ankle')
    curve = addFunctionCurve(panel, pos1_Deg)
    curve = addFunctionCurve(panel, neg1_Deg)    
    panel.setXAxisLabel("Time (s)")
    panel.setYAxisLabel("Error (rad)")
    
    print 'Finished generating plots'
    
# End mainWorker.

"""
Main logic
"""
# Let this script be run on its own.
if __name__ == '__main__':
    
    # Run in a worker thread.  This makes the GUI look a little snappier when
    # loading data and displaying the panels.
    Thread(target=lambda: mainWorker()).start()
    
# End of main.

