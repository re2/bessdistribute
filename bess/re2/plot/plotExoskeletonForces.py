

################################################################################
#
# Plots (some of) the exoskeleton forces as measured by the load cells, after
# transforming into correct frames and accounting for inverse kinematics (i.e.
# after we've done all of our conversion from the raw load cell data into the
# finished product).
#
################################################################################

import os
from threading import Thread

import org.opensim.plotter.PlotterSourceFile as PlotterSourceFile

# Add the parent "bess" directory to the Python module search path so that we
# can import modules from different directories.  Subsequent imports of BESS
# scripts may use reload() to ensure that any changes during development get
# reloaded by the interpreter.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
if bessDir not in sys.path:
    sys.path.append(bessDir)

import utilities.toolsCommon as TC; reload(TC)

def mainWorker():
    # Let the user select the file to load.
    sourcePromptString = 'Select force file (*_forces.sto)'
    sourceFileFullPath = TC.getSingleFilePath(sourcePromptString, 
        TC.FileFilter('STO files', 'sto'))
    
    # This has the effect of opening the data source for use.  If we're smart
    # this will be a one-time hit.
    print 'Loading data source, this could take a bit of time for large files...'
    dataSource = PlotterSourceFile(sourceFileFullPath)
    
    # First plot will be the left ankle -- if we get valid forces on the left
    # side that is a good indication that our whole pipeline is working.
    panel = createPlotterPanel("Left Ankle Forces in Ground Frame")
    panel.getPlotterModel().addSource(dataSource);        
    curve = addCurve(panel, dataSource, 'time', 'l_ankle_lc_ground_fx')
    setCurveLegend(curve, 'FX')
    curve = addCurve(panel, dataSource, 'time', 'l_ankle_lc_ground_fy')
    setCurveLegend(curve, 'FY')
    curve = addCurve(panel, dataSource, 'time', 'l_ankle_lc_ground_fz')
    setCurveLegend(curve, 'FZ')
    panel.setXAxisLabel("Time (s)")
    panel.setYAxisLabel("Force (N)")
    
    # Plot the average sum of forces in the ground frame.
    panel = createPlotterPanel("Average Sum of all Load Cell Forces in Ground Frame")
    panel.getPlotterModel().addSource(dataSource);
    curve = addCurve(panel, dataSource, 'time', 'avg_lc_fx')
    setCurveLegend(curve, 'Avg FX')
    curve = addCurve(panel, dataSource, 'time', 'avg_lc_fy')
    setCurveLegend(curve, 'Avg FY')
    curve = addCurve(panel, dataSource, 'time', 'avg_lc_fz')
    setCurveLegend(curve, 'Avg FZ')
    panel.setXAxisLabel("Time (s)")
    panel.setYAxisLabel("Force (N)")
        
    # Plot the right foot.
    panel = createPlotterPanel("Right ES Foot Forces Ground Frame")
    panel.getPlotterModel().addSource(dataSource);
    curve = addCurve(panel, dataSource, 'time', 'r_foot_lc_ground_fx')
    setCurveLegend(curve, 'FX')
    curve = addCurve(panel, dataSource, 'time', 'r_foot_lc_ground_fy')
    setCurveLegend(curve, 'FY')
    curve = addCurve(panel, dataSource, 'time', 'r_foot_lc_ground_fz')
    setCurveLegend(curve, 'FZ')
    panel.setXAxisLabel("Time (s)")
    panel.setYAxisLabel("Force (N)")
    
    panel = createPlotterPanel("Right Ankle Forces Ground Frame")
    panel.getPlotterModel().addSource(dataSource);
    curve = addCurve(panel, dataSource, 'time', 'r_ankle_lc_ground_fx')
    setCurveLegend(curve, 'FX')
    curve = addCurve(panel, dataSource, 'time', 'r_ankle_lc_ground_fy')
    setCurveLegend(curve, 'FY')
    curve = addCurve(panel, dataSource, 'time', 'r_ankle_lc_ground_fz')
    setCurveLegend(curve, 'FZ')
    panel.setXAxisLabel("Time (s)")
    panel.setYAxisLabel("Force (N)")
    
    panel = createPlotterPanel("Right Hip Forces Ground Frame")
    panel.getPlotterModel().addSource(dataSource);
    curve = addCurve(panel, dataSource, 'time', 'r_hip_lc_ground_fx')
    setCurveLegend(curve, 'FX')
    curve = addCurve(panel, dataSource, 'time', 'r_hip_lc_ground_fy')
    setCurveLegend(curve, 'FY')
    curve = addCurve(panel, dataSource, 'time', 'r_hip_lc_ground_fz')
    setCurveLegend(curve, 'FZ')
    panel.setXAxisLabel("Time (s)")
    panel.setYAxisLabel("Force (N)")
    
    panel = createPlotterPanel("Back Forces Ground Frame")
    panel.getPlotterModel().addSource(dataSource);
    curve = addCurve(panel, dataSource, 'time', 'back_lc_ground_fx')
    setCurveLegend(curve, 'FX')
    curve = addCurve(panel, dataSource, 'time', 'back_lc_ground_fy')
    setCurveLegend(curve, 'FY')
    curve = addCurve(panel, dataSource, 'time', 'back_lc_ground_fz')
    setCurveLegend(curve, 'FZ')
    panel.setXAxisLabel("Time (s)")
    panel.setYAxisLabel("Force (N)")
        
    print 'Finished generating plots'
        
# End mainWorker.

# Let this script be run on its own.
if __name__ == '__main__':
    """
    Main logic
    """

    # Run in a worker thread.  This makes the GUI look a little snappier when
    # loading data and displaying the panels.
    Thread(target=lambda: mainWorker()).start()
    
# End of main.

