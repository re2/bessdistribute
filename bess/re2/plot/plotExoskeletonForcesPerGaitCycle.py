

################################################################################
#
# Plots EMG voltages and CMC muscle activations.
#
################################################################################

import os
from threading import Thread

import org.opensim.plotter.PlotterSourceFile as PlotterSourceFile

# Add the parent "bess" directory to the Python module search path so that we
# can import modules from different directories.  Subsequent imports of BESS
# scripts may use reload() to ensure that any changes during development get
# reloaded by the interpreter.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
if bessDir not in sys.path:
    sys.path.append(bessDir)

import utilities.toolsCommon as TC; reload(TC)

def mainWorker():
       
    # Let the user select the file to load.
    sourcePromptString = 'Select load cell analysis file (*.sto)'
    sourceFileFullPath = TC.getSingleFilePath(sourcePromptString, 
        TC.FileFilter('STO files', 'sto'))
    
    # This has the effect of opening the data source for use.  If we're smart
    # this will be a one-time hit.
    print 'Loading data source, this could take a bit of time for large files...'
    dataSource = PlotterSourceFile(sourceFileFullPath)
    
    plotForceComponents(dataSource, 'Right Ankle Forces', 'r_ankle_lc_ground_f')
    plotForceComponents(dataSource, 'Left Ankle Forces', 'l_ankle_lc_ground_f')
    plotForceComponents(dataSource, 'Right Calf Forces', 'r_calf_lc_ground_f')
    plotForceComponents(dataSource, 'Left Calf Forces', 'l_calf_lc_ground_f')
    plotForceComponents(dataSource, 'Right Foot Forces', 'r_foot_lc_ground_f')
    plotForceComponents(dataSource, 'Left Foot Forces', 'l_foot_lc_ground_f')
    plotForceComponents(dataSource, 'Right Hip Forces', 'r_hip_lc_ground_f')
    plotForceComponents(dataSource, 'Left Hip Forces', 'l_hip_lc_ground_f')
    plotForceComponents(dataSource, 'Back Forces', 'back_lc_ground_f')

    print 'Finished generating plots'
    
# End mainWorker.

def plotForceComponents(dataSource, title, forceName):

    panel = createPlotterPanel(title)
    panel.getPlotterModel().addSource(dataSource)   
    curve = addCurve(panel, dataSource, 'percent', forceName + 'x' + '_avg')
    setCurveLegend(curve, 'FX')
    curve = addCurve(panel, dataSource, 'percent', forceName + 'y' + '_avg')
    setCurveLegend(curve, 'FY')
    curve = addCurve(panel, dataSource, 'percent', forceName + 'z' + '_avg')
    setCurveLegend(curve, 'FZ')
    panel.setXAxisLabel('Percent')
    panel.setYAxisLabel('Force (N)')

# End plotForceComponents.

"""
Main logic
"""
# Let this script be run on its own.
if __name__ == '__main__':
    
    # Run in a worker thread.  This makes the GUI look a little snappier when
    # loading data and displaying the panels.
    Thread(target=lambda: mainWorker()).start()
    
# End of main.

