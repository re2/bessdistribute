

################################################################################
#
# Plots marker errors produced when we compare TRC and IK files.
#
################################################################################

import os
from threading import Thread

import org.opensim.plotter.PlotterSourceFile as PlotterSourceFile

# Add the parent "bess" directory to the Python module search path so that we
# can import modules from different directories.  Subsequent imports of BESS
# scripts may use reload() to ensure that any changes during development get
# reloaded by the interpreter.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
if bessDir not in sys.path:
    sys.path.append(bessDir)

import utilities.toolsCommon as TC; reload(TC)

def mainWorker():
       
    # Useful for drawing position bounds on a graph of position errors. 
    dis2cm =  modeling.Constant(0.02)
    dis2cm.setName('+2 cm')
    dis4cm =  modeling.Constant(0.04)
    dis4cm.setName('+4 cm')
    
    # Let the user select the file to load.
    sourcePromptString = 'Select marker error analysis file (*.sto)'
    sourceFileFullPath = TC.getSingleFilePath(sourcePromptString, 
        TC.FileFilter('STO files', 'sto'))
    
    # This has the effect of opening the data source for use.  If we're smart
    # this will be a one-time hit.
    print 'Loading data source, this could take a bit of time for large files...'
    dataSource = PlotterSourceFile(sourceFileFullPath)
    
    panel = createPlotterPanel('Right Foot L2 Marker Errors')
    panel.getPlotterModel().addSource(dataSource)   
    curve = addCurve(panel, dataSource, 'time', 'RTOE_err_L2')
    setCurveLegend(curve, 'Toe')
    curve = addCurve(panel, dataSource, 'time', 'RHEE_err_L2')
    setCurveLegend(curve, 'Heel')
    curve = addCurve(panel, dataSource, 'time', 'RMM_err_L2')
    setCurveLegend(curve, 'Med.Mal.')
    curve = addFunctionCurve(panel, dis2cm)
    panel.setXAxisLabel("Time (s)")
    panel.setYAxisLabel("Distance (m)")
    
    panel = createPlotterPanel('Left Foot L2 Marker Errors')
    panel.getPlotterModel().addSource(dataSource)   
    curve = addCurve(panel, dataSource, 'time', 'LTOE_err_L2')
    setCurveLegend(curve, 'Toe')
    curve = addCurve(panel, dataSource, 'time', 'LHEE_err_L2')
    setCurveLegend(curve, 'Heel')
    curve = addCurve(panel, dataSource, 'time', 'LMM_err_L2')
    setCurveLegend(curve, 'Med.Mal.')
    curve = addFunctionCurve(panel, dis2cm)
    panel.setXAxisLabel("Time (s)")
    panel.setYAxisLabel("Distance (m)")
        
    panel = createPlotterPanel('Knee L2 Marker Errors')
    panel.getPlotterModel().addSource(dataSource)   
    curve = addCurve(panel, dataSource, 'time', 'RLEK_err_L2')
    setCurveLegend(curve, 'Rt. Knee, Lat.')
    curve = addCurve(panel, dataSource, 'time', 'RMEK_err_L2')
    setCurveLegend(curve, 'Rt. Knee, Med.')
    curve = addCurve(panel, dataSource, 'time', 'LLEK_err_L2')
    setCurveLegend(curve, 'Lt. Knee, Lat.')
    curve = addCurve(panel, dataSource, 'time', 'LMEK_err_L2')
    setCurveLegend(curve, 'Lt. Knee, Med.')
    curve = addFunctionCurve(panel, dis2cm)
    panel.setXAxisLabel("Time (s)")
    panel.setYAxisLabel("Distance (m)")
    
    panel = createPlotterPanel('Pelvis L2 Marker Errors')
    panel.getPlotterModel().addSource(dataSource)   
    curve = addCurve(panel, dataSource, 'time', 'RASIS_err_L2')
    setCurveLegend(curve, 'Rt. Hip')
    curve = addCurve(panel, dataSource, 'time', 'LASIS_err_L2')
    setCurveLegend(curve, 'Lt. Hip')
    curve = addFunctionCurve(panel, dis2cm)
    panel.setXAxisLabel("Time (s)")
    panel.setYAxisLabel("Distance (m)")
    
    panel = createPlotterPanel('Shoulder L2 Marker Errors')
    panel.getPlotterModel().addSource(dataSource)   
    curve = addCurve(panel, dataSource, 'time', 'RSHO_err_L2')
    setCurveLegend(curve, 'Rt. Shoulder')
    curve = addCurve(panel, dataSource, 'time', 'LSHO_err_L2')
    setCurveLegend(curve, 'Lt. Shoulder')
    curve = addFunctionCurve(panel, dis2cm)
    panel.setXAxisLabel("Time (s)")
    panel.setYAxisLabel("Distance (m)")
    
    panel = createPlotterPanel('All Marker L2 Marker Errors')
    panel.getPlotterModel().addSource(dataSource)   
    curve = addCurve(panel, dataSource, 'time', 'ALL_rms_L2')
    setCurveLegend(curve, 'RMS')
    curve = addCurve(panel, dataSource, 'time', 'ALL_max_L2')
    setCurveLegend(curve, 'Max')
    curve = addFunctionCurve(panel, dis2cm)
    curve = addFunctionCurve(panel, dis4cm)
    panel.setXAxisLabel("Time (s)")
    panel.setYAxisLabel("Distance (m)")
        
    print 'Finished generating plots'
    
# End mainWorker.
    
"""
Main logic
"""
# Let this script be run on its own.
if __name__ == '__main__':
    
    # Run in a worker thread.  This makes the GUI look a little snappier when
    # loading data and displaying the panels.
    Thread(target=lambda: mainWorker()).start()
    
# End of main.

