

################################################################################
#
# Plots EMG voltages and CMC muscle activations.
#
################################################################################

import os
from threading import Thread

import org.opensim.plotter.PlotterSourceFile as PlotterSourceFile

# Add the parent "bess" directory to the Python module search path so that we
# can import modules from different directories.  Subsequent imports of BESS
# scripts may use reload() to ensure that any changes during development get
# reloaded by the interpreter.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
if bessDir not in sys.path:
    sys.path.append(bessDir)

import utilities.toolsCommon as TC; reload(TC)

"""
Creates plots of state errors (like the translational error in the pelvis in
the Z-axis direction).
"""
def mainWorker():
       
    # Let the user select the file to load.
    sourcePromptString = 'Select EMG analysis file (*.sto)'
    sourceFileFullPath = TC.getSingleFilePath(sourcePromptString, 
        TC.FileFilter('STO files', 'sto'))
    
    # This has the effect of opening the data source for use.  If we're smart
    # this will be a one-time hit.
    print 'Loading data source, this could take a bit of time for large files...'
    dataSource = PlotterSourceFile(sourceFileFullPath)
    
    plotAvgAndStdDev(dataSource, 'Rt. Biceps Fem. Muscle EMG Voltage', 'rt_biceps_fem')
    plotAvgAndStdDev(dataSource, 'Rt. Glut. Max. Muscle EMG Voltage', 'rt_glut_max')
    plotAvgAndStdDev(dataSource, 'Rt. Lumbar Es. Muscle EMG Voltage', 'rt_lumbar_es')
    plotAvgAndStdDev(dataSource, 'Rt. Med. Gastro. Muscle EMG Voltage', 'rt_med_gastro')
    plotAvgAndStdDev(dataSource, 'Rt. Rectus Fem. Muscle EMG Voltage', 'rt_rectus_fem')
    plotAvgAndStdDev(dataSource, 'Rt. Soleus Muscle EMG Voltage', 'rt_soleus')
    plotAvgAndStdDev(dataSource, 'Rt. Ant. Tib. Muscle EMG Voltage', 'rt_tib_ant')
    
    plotEMG_AndCMC(dataSource, 'Rt. Biceps Fem. Muscle Output', 'rt_biceps_fem_scaled', 'bifemlh_r')
    plotEMG_AndCMC(dataSource, 'Rt. Glut. Max. Muscle Output', 'rt_glut_max_scaled', 'glut_max2_r')
    plotEMG_AndCMC(dataSource, 'Rt. Lumbar Es. Muscle Output', 'rt_lumbar_es_scaled', 'ercspn_r')
    plotEMG_AndCMC(dataSource, 'Rt. Med. Gastro. Muscle Output', 'rt_med_gastro_scaled', 'med_gas_r')
    plotEMG_AndCMC(dataSource, 'Rt. Rectus Fem. Muscle Output', 'rt_rectus_fem_scaled', 'rect_fem_r')
    plotEMG_AndCMC(dataSource, 'Rt. Soleus Muscle Output', 'rt_soleus_scaled', 'soleus_r')
    plotEMG_AndCMC(dataSource, 'Rt. Ant. Tib. Muscle Output', 'rt_tib_ant_scaled', 'tib_ant_r')
    
    print 'Finished generating plots'
    
# End mainWorker.

def plotAvgAndStdDev(dataSource, title, muscleName):

    panel = createPlotterPanel(title)
    panel.getPlotterModel().addSource(dataSource)   
    curve = addCurve(panel, dataSource, 'percent', muscleName + '_avg')
    setCurveLegend(curve, 'Avg')
    curve = addCurve(panel, dataSource, 'percent', muscleName + '_avg_psigma')
    setCurveLegend(curve, '+1 Sigma')
    curve = addCurve(panel, dataSource, 'percent', muscleName + '_avg_nsigma')
    setCurveLegend(curve, '-1 Sigma')
    panel.setXAxisLabel("Percent of Gait Cycle")
    panel.setYAxisLabel("Normalized RMS Voltage (uV)")

# End plotAvgAndStdDev.

def plotEMG_AndCMC(dataSource, title, emgName, cmcName):

    panel = createPlotterPanel(title)
    panel.getPlotterModel().addSource(dataSource)   
    curve = addCurve(panel, dataSource, 'percent', emgName + '_avg')
    setCurveLegend(curve, 'EMG')
    curve = addCurve(panel, dataSource, 'percent', cmcName + '.activation_avg')
    setCurveLegend(curve, 'CMC')
    panel.setXAxisLabel("Percent of Gait Cycle")
    panel.setYAxisLabel("Normalized Muscle Output")

# End plotEMG_AndCMC.
    
"""
Main logic
"""
# Let this script be run on its own.
if __name__ == '__main__':
    
    # Run in a worker thread.  This makes the GUI look a little snappier when
    # loading data and displaying the panels.
    Thread(target=lambda: mainWorker()).start()
    
# End of main.

