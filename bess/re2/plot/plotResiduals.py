

################################################################################
#
# Plots residual forces produced when running CMC or RRA (which uses CMC).
# These are held in a file with a name pattern like "*_Actuation_force.sto",
# though any file that holds the named columns should be fine.
#
################################################################################

import os
from threading import Thread

# Add the parent "bess" directory to the Python module search path so that we
# can import modules from different directories.  Subsequent imports of BESS
# scripts may use reload() to ensure that any changes during development get
# reloaded by the interpreter.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
if bessDir not in sys.path:
    sys.path.append(bessDir)

import utilities.toolsCommon as TC; reload(TC)

"""
Creates plots of state errors (like the translational error in the pelvis in
the Z-axis direction).
"""
def mainWorker():
    # Useful for drawing force bounds on a graph of residual forces.  I think
    # that residual forces of 60 N is the upper limit we would want, which
    # corresponds to 5% of body mass force for our subject (which produces
    # 1200 N of downward force).
    pos60_N =  modeling.Constant(60)
    pos60_N.setName('+60 N')
    neg60_N =  modeling.Constant(-60)
    neg60_N.setName('-60 N')
    
    # Useful for drawing moment bounds on a graph of residual moments.  I think
    # that residual forces of 120 N-m is the upper limit we would want, which
    # corresponds to 10% of body mass force for our subject (which produces
    # 1200 N of downward force).
    pos120_N_m =  modeling.Constant(120)
    pos120_N_m.setName('+120 N')
    neg120_N_m =  modeling.Constant(-120)
    neg120_N_m.setName('-120 N')
    
    # Let the user select the file to load.
    sourcePromptString = 'Select force file (*_Actuation_force.sto)'
    sourceFileFullPath = TC.getSingleFilePath(sourcePromptString, 
        TC.FileFilter('STO files', 'sto'))
    
    # First plot will be residual forces.
    panel = createPlotterPanel("Residual Forces")
    
    # This has the effect of opening the data source for use.  If we're smart
    # this will be a one-time hit.
    print 'Loading data source, this could take a bit of time for large files...'
    dataSource = addDataSource(panel, sourceFileFullPath)    
    
    curve = addCurve(panel, dataSource, 'time', 'FX')
    setCurveLegend(curve, 'FX (Front/Back)')
    curve = addCurve(panel, dataSource, 'time', 'FY')
    setCurveLegend(curve, 'FY (Up/Down)')
    curve = addCurve(panel, dataSource, 'time', 'FZ')
    setCurveLegend(curve, 'FZ (Right/Left)')
    curve = addFunctionCurve(panel, pos60_N)
    curve = addFunctionCurve(panel, neg60_N)    
    panel.setXAxisLabel("Time (s)")
    panel.setYAxisLabel("Force (N)")
    
    # Next will be residual moments.
    panel = createPlotterPanel("Residual Moments")
    panel.getPlotterModel().addSource(dataSource); 
    curve = addCurve(panel, dataSource, 'time', 'MX')
    setCurveLegend(curve, 'MX (Right/Left)')
    curve = addCurve(panel, dataSource, 'time', 'MY')
    setCurveLegend(curve, 'MY (Spin)')
    curve = addCurve(panel, dataSource, 'time', 'MZ')
    setCurveLegend(curve, 'MZ (Front/Back)')    
    curve = addFunctionCurve(panel, pos120_N_m)
    curve = addFunctionCurve(panel, neg120_N_m)    
    panel.setXAxisLabel("Time (s)")
    panel.setYAxisLabel("Moment (N-m)")
    
    print 'Finished generating plots'
        
# End mainWorker.

"""
Main logic
"""
# Let this script be run on its own.
if __name__ == '__main__':
    
    # Run in a worker thread.  This makes the GUI look a little snappier when
    # loading data and displaying the panels.
    Thread(target=lambda: mainWorker()).start()
    
# End of main.

