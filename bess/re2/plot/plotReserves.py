

################################################################################
#
# Plots reserve forces produced when running CMC or RRA (which uses CMC).
# These are held in a file with a name pattern like "*_Actuation_force.sto",
# though any file that holds the named columns should be fine.
#
################################################################################

import os
from threading import Thread

# Add the parent "bess" directory to the Python module search path so that we
# can import modules from different directories.  Subsequent imports of BESS
# scripts may use reload() to ensure that any changes during development get
# reloaded by the interpreter.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
if bessDir not in sys.path:
    sys.path.append(bessDir)

import utilities.toolsCommon as TC; reload(TC)

"""
Creates plots of state errors (like the translational error in the pelvis in
the Z-axis direction).
"""
def mainWorker():
    # Useful for drawing moment bounds on a graph of reserve moments.  I think
    # that residual forces of 40 N-m is the upper limit we would want, which
    # if "Good" according to OpenSim, and corresponds to 3-4% of body mass 
    # force for our subject (which produces 1200 N of downward force).
    pos40_N_m =  modeling.Constant(40)
    pos40_N_m.setName('+40 N')
    neg40_N_m =  modeling.Constant(-40)
    neg40_N_m.setName('-40 N')
    
    # Let the user select the file to load.
    sourcePromptString = 'Select force file (*_Actuation_force.sto)'
    sourceFileFullPath = TC.getSingleFilePath(sourcePromptString, 
        TC.FileFilter('STO files', 'sto'))
    
    # First plot will be reserve moments about the right hip.
    panel = createPlotterPanel("Right Hip Reserve Moments")
    
    # This has the effect of opening the data source for use.  If we're smart
    # this will be a one-time hit.
    print 'Loading data source, this could take a bit of time for large files...'
    dataSource = addDataSource(panel, sourceFileFullPath)    
    
    curve = addCurve(panel, dataSource, 'time', 'hip_flexion_r_reserve')
    setCurveLegend(curve, 'Flexion')
    curve = addCurve(panel, dataSource, 'time', 'hip_adduction_r_reserve')
    setCurveLegend(curve, 'Adduction')
    curve = addCurve(panel, dataSource, 'time', 'hip_rotation_r_reserve')
    setCurveLegend(curve, 'Rotation')
    curve = addFunctionCurve(panel, pos40_N_m)
    curve = addFunctionCurve(panel, neg40_N_m)    
    panel.setXAxisLabel("Time (s)")
    panel.setYAxisLabel("Moment (N-m)")
    
    panel = createPlotterPanel("Left Hip Reserve Moments")
    panel.getPlotterModel().addSource(dataSource);
    curve = addCurve(panel, dataSource, 'time', 'hip_flexion_l_reserve')
    setCurveLegend(curve, 'Flexion')
    curve = addCurve(panel, dataSource, 'time', 'hip_adduction_l_reserve')
    setCurveLegend(curve, 'Adduction')
    curve = addCurve(panel, dataSource, 'time', 'hip_rotation_l_reserve')
    setCurveLegend(curve, 'Rotation')
    curve = addFunctionCurve(panel, pos40_N_m)
    curve = addFunctionCurve(panel, neg40_N_m)    
    panel.setXAxisLabel("Time (s)")
    panel.setYAxisLabel("Moment (N-m)")
    
    # Next plot will be reserve moments for the legs.
    panel = createPlotterPanel("Right Leg Reserve Moments")
    panel.getPlotterModel().addSource(dataSource);
    curve = addCurve(panel, dataSource, 'time', 'knee_angle_r_reserve')
    setCurveLegend(curve, 'Right Knee')
    curve = addCurve(panel, dataSource, 'time', 'ankle_angle_r_reserve')
    setCurveLegend(curve, 'Right Ankle')
    curve = addCurve(panel, dataSource, 'time', 'subtalar_angle_r_reserve')
    setCurveLegend(curve, 'Right Subtalar')
    curve = addFunctionCurve(panel, pos40_N_m)
    curve = addFunctionCurve(panel, neg40_N_m)    
    panel.setXAxisLabel("Time (s)")
    panel.setYAxisLabel("Moment (N-m)")
    
    panel = createPlotterPanel("Left Leg Reserve Moments")
    panel.getPlotterModel().addSource(dataSource);
    curve = addCurve(panel, dataSource, 'time', 'knee_angle_l_reserve')
    setCurveLegend(curve, 'Left Knee')
    curve = addCurve(panel, dataSource, 'time', 'ankle_angle_l_reserve')
    setCurveLegend(curve, 'Left Ankle')
    curve = addCurve(panel, dataSource, 'time', 'subtalar_angle_l_reserve')
    setCurveLegend(curve, 'Left Subtalar')      
    curve = addFunctionCurve(panel, pos40_N_m)
    curve = addFunctionCurve(panel, neg40_N_m)    
    panel.setXAxisLabel("Time (s)")
    panel.setYAxisLabel("Moment (N-m)")
        
    # Next plot will be reserve moments for the lumbar.
    panel = createPlotterPanel("Lumbar Reserve Moments")
    panel.getPlotterModel().addSource(dataSource);  
    curve = addCurve(panel, dataSource, 'time', 'lumbar_extension_reserve')
    setCurveLegend(curve, 'Extension')
    curve = addCurve(panel, dataSource, 'time', 'lumbar_bending_reserve')
    setCurveLegend(curve, 'Bending')
    curve = addCurve(panel, dataSource, 'time', 'lumbar_rotation_reserve')
    setCurveLegend(curve, 'Rotation')
    curve = addFunctionCurve(panel, pos40_N_m)
    curve = addFunctionCurve(panel, neg40_N_m)    
    panel.setXAxisLabel("Time (s)")
    panel.setYAxisLabel("Moment (N-m)")
    
    print 'Finished generating plots'
        
# End mainWorker.

"""
Main logic
"""
# Let this script be run on its own.
if __name__ == '__main__':
    
    # Run in a worker thread.  This makes the GUI look a little snappier when
    # loading data and displaying the panels.
    Thread(target=lambda: mainWorker()).start()
    
# End of main.

