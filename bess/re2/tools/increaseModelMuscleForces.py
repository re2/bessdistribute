

################################################################################
#
# This script is used to increase the muscle "optimal forces" of a model by
# a percentage factor.
#
################################################################################

import os
import sys
from threading import Thread

import org.opensim.modeling as modeling

# Add the parent "bess" directory to the Python module search path so that we
# can import modules from different directories.  Subsequent imports of BESS
# scripts may use reload() to ensure that any changes during development get
# reloaded by the interpreter.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
if bessDir not in sys.path:
    sys.path.append(bessDir)

import utilities.modelManipulation as MM; reload(MM)
import utilities.toolsCommon as TC; reload(TC)

"""
Main worker function.
"""
def mainWorker(inputFile, outputFile, increaseFactor):
    
    # Get the model from the input file.
    simObjects = MM.getModelAndState(inputFile)
    model = simObjects.model
    
    MM.modifyModelMuscleForces(model, increaseFactor)
    
    # Save the model out again.
    model.print(outputFile)

# End mainWorker.

"""
Main logic
"""
if __name__ == '__main__':
        
    inputFile = TC.getSingleFilePath('Select input OpenSim model file',
        TC.FileFilter('OpenSim model (.osim)', 'osim'))
        
    outputFile = TC.getSingleFilePath('Select output OpenSim model file',
        TC.FileFilter('OpenSim model (.osim)', 'osim'))
            
    # Run in a worker thread.  This means we don't block the GUI thread when
    # doing a lot of data processing.  This script will increase all the muscle
    # max isometric forces by 10%.  If you want more than that you could call
    # this script multiple times (to get a 10% compounding increase: 10%, 21%,
    # 33%, etc.) or just hack the script to whatever number you want.
    #
    # I tried to use raw_input() to let the user type in a value in the 
    # scripting window but we don't seem to be able to do that in the OpenSim
    # scripting window.  It throws an IOError.
    Thread(target=lambda: mainWorker(inputFile, outputFile, 0.1)).start()

    print 'Increase of model muscle forces complete'
    
# End of main.


