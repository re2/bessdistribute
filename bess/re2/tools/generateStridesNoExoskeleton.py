

################################################################################
#
# This script is used to identify and write-out strides when no exoskeleton is
# used.  We use strides for downstream analysis when we put things on a
# "per stride" basis.  We don't need this when we have an exoskeleton since
# there we use "configure forces" to generate the strides.
#
################################################################################

from collections import defaultdict
import os
import sys
from threading import Thread

# Add the parent "bess" directory to the Python module search path so that we
# can import modules from different directories.  Subsequent imports of BESS
# scripts may use reload() to ensure that any changes during development get
# reloaded by the interpreter.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
if bessDir not in sys.path:
    sys.path.append(bessDir)

import configure_forces.configureForces_Support as CF; reload(CF)
import utilities.handleStoMot as HSM; reload(HSM)
import utilities.strideTracker as ST; reload(ST)
import utilities.toolsCommon as TC; reload(TC)

def mainWorker(grfPath, stridesXmlOutPath, stridesStoOutPath):
    
    # Get the ground reaction forces as a dictionary.
    grfDict = HSM.readStoMotAsDict(grfPath)
    
    # In the configure forces pipeline we read in stride configuration from
    # XML (presumably created by the CF GUI).  For our purposes, let's just
    # use the default values for stride configuration (stride names and force
    # hysteresis.
    strideConfig = CF.StrideConfig()

    # Create a dictionary of stride trackers (one for left and one for right).
    strideTrackers = defaultdict(lambda: ST.StrideTracker(
        strideConfig.midpointHysteresis))
        
    # Get the lists of data we're going to iterate over.  We should have a
    # column for time, and one for the left vertical force and one for the
    # right vertical force.  We assume the convention that GRF files should
    # use the "ground_force_vy" and "1_ground_force_vy" labels for right and
    # left vertical forces respectively.
    time = grfDict['time']
    rightForce = grfDict['ground_force_vy']
    leftForce = grfDict['1_ground_force_vy']
    
    # Assume these are the same length -- this should have been checked 
    # upstream.
    for ii in range(len(time)):
        strideTrackers[strideConfig.rightStrideName].addValue(time[ii], 
            rightForce[ii])
        strideTrackers[strideConfig.leftStrideName].addValue(time[ii], 
            leftForce[ii])
    
    # We need to tell the stride trackers that we are at the end of the data
    # for it to finish up the last strides.
    for strideTracker in strideTrackers.values():
        strideTracker.completeStrides()
        
    # We've calculated strides (and accounted for completing the last strides) 
    # -- serialize and output to an XML file for use by downstream tools.  
    ST.StrideTracker.writeDictToXmlFile(strideTrackers, stridesXmlOutPath)
        
    # Also create a time-based representation of strides and write it out 
    # to a STO file so strides can be visualized in the plot window.
    HSM.writeDictToStoMot(ST.StrideTracker.writeDictToStoDict(strideTrackers),
        stridesStoOutPath)    
    
# End mainWorker.

"""
Main logic
"""
if __name__ == '__main__':
        
    grfPath = TC.getSingleFilePath(
        'Select ground reaction force file',
        TC.FileFilter('GRF STO/MOT files (.sto,.mot)', 'sto,mot'))
    stridesXmlOutPath = TC.getSingleFilePath(
        'Select strides XML output file',
        TC.FileFilter('Strides file (.xml)', 'xml'))
    stridesStoOutPath = TC.getSingleFilePath(
        'Select strides STO output file',
        TC.FileFilter('Strides file (.sto)', 'sto'))
        
    # Run in a worker thread.  This means we don't block the GUI thread when
    # doing a lot of data processing.
    Thread(target=lambda: mainWorker(grfPath, 
        stridesXmlOutPath, 
        stridesStoOutPath)).start()

    print 'Finished generating strides.'
    
# End of main.


