

################################################################################
#
# This script is used to create a usable XML config file for the configure
# forces tool for the BESS exoskeleton.
#
################################################################################

import math
import os
import sys
from threading import Thread
from xml.etree import cElementTree as ET

import org.opensim.modeling as modeling

# Add the parent "bess" directory to the Python module search path so that we
# can import modules from different directories.  Subsequent imports of BESS
# scripts may use reload() to ensure that any changes during development get
# reloaded by the interpreter.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
if bessDir not in sys.path:
    sys.path.append(bessDir)

import configure_forces.configureForces_Support as CF; reload(CF)
import re2.tools.createConfigureForcesXmlFile_Support as CCF; reload(CCF)
import utilities.toolsCommon as TC; reload(TC)

"""
Main logic
"""
if __name__ == '__main__':
    
    print 'Creating configure forces XML file'
    
    filePaths = CF.FilePaths()
    filePaths.modelPath = TC.getSingleFilePath(
        'Select OpenSim model file', 
        TC.FileFilter('OpenSim model (.osim)', 'osim'))
    filePaths.ikPath = TC.getSingleFilePath(
        'Select inverse kinematics file',
        TC.FileFilter('IK MOT files (.mot)', 'mot'))
    filePaths.lcPath = TC.getSingleFilePath(
        'Select load cell force file',
        TC.FileFilter('Load cell files (.sto,.mot)', 'sto,mot'))
    filePaths.grfPath = TC.getSingleFilePath(
        'Select ground reaction force file',
        TC.FileFilter('GRF STO/MOT files (.sto,.mot)', 'sto,mot'))
    filePaths.forcesOutPath = TC.getSingleFilePath(
        'Select forces output file',
        TC.FileFilter('Output file (.sto)', 'sto'))
    filePaths.stridesXmlOutPath = TC.getSingleFilePath(
        'Select strides XML output file',
        TC.FileFilter('Strides file (.xml)', 'xml'))
    filePaths.stridesStoOutPath = TC.getSingleFilePath(
        'Select strides STO output file',
        TC.FileFilter('Strides file (.sto)', 'sto'))
    filePaths.externalLoadsOutPath = TC.getSingleFilePath(
        'Select external loads XML output file',
        TC.FileFilter('External loads file (.xml)', 'xml'))
    
    outPath = TC.getSingleFilePath(
        'Select configure forces XML output file',
        TC.FileFilter('Configure forces file (.xml)', 'xml'))
            
    subjectMeasurementsPath = TC.getSingleFilePath(
        'Select subject measurement file', 
        TC.FileFilter('Measurement files (.xml)', 'xml'))
    tree = ET.ElementTree(file=subjectMeasurementsPath)
    root = tree.getroot()
    
    # Run in a worker thread.  This means we don't block the GUI thread when
    # doing a lot of data processing.
    Thread(target=lambda: CF.Config.writeToXmlFile(outPath, 
        filePaths, 
        CCF.getStrideConfig(root),
        CCF.getGRF_ForceSets(root),
        CCF.getTrackedPositions(root),
        CCF.getRealLoadCellForceSets(root),
        CCF.getSyntheticLoadCellForceSets(root),
        CCF.getSyntheticDataConfig(root))).start()

    print 'Configure Forces XML file complete'
    
# End of main.


