

################################################################################
#
# This script is used to create a usable XML config file for the configure
# forces tool for the BESS exoskeleton.
#
################################################################################

import math
import os
import sys
from threading import Thread
from xml.etree import cElementTree as ET

import org.opensim.modeling as modeling

# Add the parent "bess" directory to the Python module search path so that we
# can import modules from different directories.  Subsequent imports of BESS
# scripts may use reload() to ensure that any changes during development get
# reloaded by the interpreter.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
if bessDir not in sys.path:
    sys.path.append(bessDir)

import configure_forces.configureForces_Support as CF; reload(CF)
import utilities.toolsCommon as TC; reload(TC)

def getVectorFromXml(measurementXmlRootElement, tag):
    strVector = measurementXmlRootElement.find('Vectors').findtext(tag)
    return TC.xmlStringTupleToTuple(strVector)
    
def getStrideConfig(measurementXmlRootElement):    
    strideConfig = CF.StrideConfig()
    strideConfig.rightStrideName = 'right_stride'
    strideConfig.rightGroundReactionForceSetName = 'right_foot_grf'
    strideConfig.leftStrideName = 'left_stride'
    strideConfig.leftGroundReactionForceSetName = 'left_foot_grf'
    strideConfig.midpointHysteresis = (10, 50)
    
    return strideConfig
    
def getGRF_ForceSets(measurementXmlRootElement):    
    grfForceSets = dict()
    grfForceSets['right_foot_grf'] = CF.GroundReactionForceSet( 
        'ground_force_v',
        'ground_force_p',
        'ground_torque_',
        'calcn_r',
        'r_foot_lc')
    grfForceSets['left_foot_grf'] = CF.GroundReactionForceSet(
        '1_ground_force_v',
        '1_ground_force_p',
        '1_ground_torque_',
        'calcn_l',
        'l_foot_lc')
        
    return grfForceSets
    
def getTrackedPositions(measurementXmlRootElement):        
    trackedPositions = dict()
    trackedPositions['back_lc_tp'] = CF.TrackedPosition(
        'back_lc_tp_body_p',
        'back_lc_tp_ground_p',
        'pelvis',
        False,
        modeling.Vec3(*getVectorFromXml(measurementXmlRootElement, 'back_lc_tp')))
        
    trackedPositions['r_hip_lc_tp'] = CF.TrackedPosition(
        'r_hip_lc_tp_body_p',
        'r_hip_lc_tp_ground_p',
        'pelvis', 
        False,
        modeling.Vec3(*getVectorFromXml(measurementXmlRootElement, 'r_hip_lc_tp')))
        
    # Load cell position of the calf sensor.  These measurements assume
    # the tibia frame (at the top of the knee) lines up in the vertical
    # dimension with the knee joint of the exoskeleton.  (So the y-dimension
    # comes from CAD.)  The z-dimension is eyeballed guessing the thickness
    # of the subject's leg.
    trackedPositions['r_calf_lc_tp'] = CF.TrackedPosition(
        'r_calf_lc_tp_body_p',
        'r_calf_lc_tp_ground_p',
        'tibia_r',
        False,
        modeling.Vec3(*getVectorFromXml(measurementXmlRootElement, 'r_calf_lc_tp')))
        
    # Point where the calf load cell force is applied.
    trackedPositions['r_calf_ap_tp'] = CF.TrackedPosition(
        'r_calf_ap_tp_body_p',
        'r_calf_ap_tp_ground_p',
        'tibia_r',
        True,
        None)
        
    # Incidentally, I estimated the y-axis value here by looking at the global
    # y-axis position of the ankle load cell vs the foot center-of-mass, and
    # adjusting until the load cell was slightly higher than the foot.  Best
    # we can do right now with no experimental measurements.
    trackedPositions['r_ankle_lc_tp'] = CF.TrackedPosition(
        'r_ankle_lc_tp_body_p',
        'r_ankle_lc_tp_ground_p',
        'tibia_r',
        False,
        modeling.Vec3(*getVectorFromXml(measurementXmlRootElement, 'r_ankle_lc_tp')))
        
    trackedPositions['r_ankle_ap_tp'] = CF.TrackedPosition(
        'r_ankle_ap_tp_body_p',
        'r_ankle_ap_tp_ground_p',
        'calcn_r',
        True,
        None)
        
    trackedPositions['r_foot_lc_tp'] = CF.TrackedPosition(
        'r_foot_lc_tp_body_p',
        'r_foot_lc_tp_ground_p',
        'tibia_r',
        False,
        modeling.Vec3(*getVectorFromXml(measurementXmlRootElement, 'r_foot_lc_tp')))
        
    trackedPositions['r_foot_ap_tp'] = CF.TrackedPosition(
        'r_foot_lc_ap_body_p',
        'r_foot_lc_ap_ground_p',
        'tibia_r',
        False,
        modeling.Vec3(*getVectorFromXml(measurementXmlRootElement, 'r_foot_ap_tp')))
        
    trackedPositions['l_hip_lc_tp'] = CF.TrackedPosition(
        'l_hip_lc_tp_body_p',
        'l_hip_lc_tp_ground_p',
        'pelvis',
        False,
        modeling.Vec3(*getVectorFromXml(measurementXmlRootElement, 'l_hip_lc_tp')))
    
    trackedPositions['l_calf_lc_tp'] = CF.TrackedPosition(
        'l_calf_lc_tp_body_p',
        'l_calf_lc_tp_ground_p',
        'tibia_l',
        False,
        modeling.Vec3(*getVectorFromXml(measurementXmlRootElement, 'l_calf_lc_tp')))
    
    trackedPositions['l_calf_ap_tp'] = CF.TrackedPosition(
        'l_calf_ap_tp_body_p',
        'l_calf_ap_tp_ground_p',
        'tibia_l',
        True,
        None)
        
    trackedPositions['l_ankle_lc_tp'] = CF.TrackedPosition(
        'l_ankle_lc_tp_body_p',
        'l_ankle_lc_tp_ground_p',
        'tibia_l',
        False,
        modeling.Vec3(*getVectorFromXml(measurementXmlRootElement, 'l_ankle_lc_tp')))
        
    trackedPositions['l_ankle_ap_tp'] = CF.TrackedPosition(
        'l_ankle_ap_tp_body_p',
        'l_ankle_ap_tp_ground_p',
        'calcn_l',
        True,
        None)
        
    trackedPositions['l_foot_lc_tp'] = CF.TrackedPosition(
        'l_foot_lc_tp_body_p',
        'l_foot_lc_tp_ground_p',
        'tibia_l',
        False,
        modeling.Vec3(*getVectorFromXml(measurementXmlRootElement, 'l_foot_lc_tp')))
        
    trackedPositions['l_foot_ap_tp'] = CF.TrackedPosition(
        'l_foot_lc_ap_body_p',
        'l_foot_lc_ap_ground_p',
        'tibia_l',
        False,
        modeling.Vec3(*getVectorFromXml(measurementXmlRootElement, 'l_foot_ap_tp')))
    
    return trackedPositions
    
def getLoadCellArgs(rootName, 
        sensorTrackedPositionName,
        appliedToBodyName,
        appliedToTrackedPositionName,
        rotationMatrixArray):
        """
        Turns the rootName into 6 different names and then adds on the other
        function arguments.
        
        Returns a tuple that can be expanded into a LoadCellForceSet 
        constructor argument list using the * operation.
        """
        return (rootName + '_sensor_f',
            rootName + '_sensor_t',
            rootName + '_body_f',
            rootName + '_body_t',
            rootName + '_ground_f',
            rootName + '_ground_t',
            rootName + '_applied_f',
            rootName + '_applied_t',
            sensorTrackedPositionName,
            appliedToBodyName,
            appliedToTrackedPositionName,
            rotationMatrixArray)
    
def getRealLoadCellForceSets(measurementXmlRootElement):
    realLoadCellForceSets = dict()
    # Name the load cells and assign to a body part -- note that this assignment
    # is just for frame and transform purposes:  for example the ankle load
    # cell might be applying forces to the subject's foot but the sensor is
    # mounted to a plate that tracks the tibia.
    
    # We need the following mappings to go from the back sensor frame to 
    # the OpenSim pelvis frame:
    # OS    LC
    # +X    -Z
    # +Y    +X
    # +Z    -Y
    realLoadCellForceSets['back_lc'] = CF.LoadCellForceSet(
        *getLoadCellArgs('back_lc',
            'back_lc_tp',
            'pelvis',
            'back_lc_tp',
            (0,0,-1,1,0,0,0,-1,0)))

    # The hip sensor is at an odd angle.  The X-Y sensor plane aligns with
    # the X-Y pelvis plane in OpenSim but at a rotation and as a left
    # hand.
    # OS    LC
    # +X    +Y * c(34d) - +X * s(34d)
    # +Y    +X * c(34d) + +Y * s(34d)
    # +Z    -Z
    r34 = math.radians(34.0)
    c34 = math.cos(r34)
    s34 = math.sin(r34)   
    realLoadCellForceSets['r_hip_lc'] = CF.LoadCellForceSet(
        *getLoadCellArgs('r_hip_lc',
            'r_hip_lc_tp',
            'pelvis',
            'r_hip_lc_tp',
            (-s34,c34,0,c34,s34,0,0,0,-1)))
        
    # We need the following mappings to go from the right calf sensor frame to 
    # the OpenSim tibia frame:
    # OS    LC
    # +X    -X
    # +Y    +Y
    # +Z    -Z
    realLoadCellForceSets['r_calf_lc'] = CF.LoadCellForceSet(
        *getLoadCellArgs('r_calf_lc',
            'r_calf_lc_tp',
            'tibia_r',
            'r_calf_ap_tp',
            (-1,0,0,0,1,0,0,0,-1)))
        
    # The right ankle load cell has a sensor frame already aligned with the
    # body frame, so just make this a passthrough.
    realLoadCellForceSets['r_ankle_lc'] = CF.LoadCellForceSet(
        *getLoadCellArgs('r_ankle_lc',
            'r_ankle_lc_tp',
            'calcn_r',
            'r_ankle_ap_tp',
            (1,0,0,0,1,0,0,0,1)))
        
    # We need the following mappings to go from the right foot sensor frame to 
    # the OpenSim tibia frame:
    # OS    LC
    # +X    +X
    # +Y    -Z
    # +Z    +Y
    realLoadCellForceSets['r_foot_lc'] = CF.LoadCellForceSet(
        *getLoadCellArgs('r_foot_lc',
            'r_foot_lc_tp',
            'ground',
            'r_foot_ap_tp',
            (1,0,0,0,0,-1,0,1,0)))
        
    return realLoadCellForceSets
    
def getSyntheticLoadCellForceSets(measurementXmlRootElement):
    # Track synthetic load cells in a separate dictionary since we update these
    # at a different point in the code as the real ones.
    syntheticLoadCellForceSets = dict()
    # Synthetic load cells are assumed to already have a coordinate frame 
    # aligned with the particular OpenSim body frame.
    syntheticLoadCellForceSets['l_hip_lc'] = CF.LoadCellForceSet(
        *getLoadCellArgs('l_hip_lc',
            'l_hip_lc_tp',
            'pelvis', 
            'l_hip_lc_tp',
            (1,0,0,0,1,0,0,0,1)))
        
    syntheticLoadCellForceSets['l_calf_lc'] = CF.LoadCellForceSet(
        *getLoadCellArgs('l_calf_lc',
            'l_calf_lc_tp',
            'tibia_l',
            'l_calf_ap_tp',
            (1,0,0,0,1,0,0,0,1)))
        
    syntheticLoadCellForceSets['l_ankle_lc'] = CF.LoadCellForceSet(
        *getLoadCellArgs('l_ankle_lc',
            'l_ankle_lc_tp',
            'calcn_l',
            'l_ankle_ap_tp',
            (1,0,0,0,1,0,0,0,1)))
        
    syntheticLoadCellForceSets['l_foot_lc'] = CF.LoadCellForceSet(
        *getLoadCellArgs('l_foot_lc',
            'l_foot_lc_tp',
            'ground',
            'l_foot_ap_tp',
            (1,0,0,0,1,0,0,0,1)))
    
    return syntheticLoadCellForceSets
    
def getSyntheticDataConfig(measurementXmlRootElement):
    syntheticDataConfig = dict()
    syntheticDataConfig['l_hip_lc'] = CF.SyntheticDataConfig('left_stride',
        'r_hip_lc',
        'right_stride',
        (1,0,0,0,1,0,0,0,-1),
        (-1,0,0,0,-1,0,0,0,1))
    
    syntheticDataConfig['l_calf_lc'] = CF.SyntheticDataConfig('left_stride',
        'r_calf_lc',
        'right_stride',
        (1,0,0,0,1,0,0,0,-1),
        (-1,0,0,0,-1,0,0,0,1))
        
    syntheticDataConfig['l_ankle_lc'] = CF.SyntheticDataConfig('left_stride',
        'r_ankle_lc',
        'right_stride',
        (1,0,0,0,1,0,0,0,-1),
        (-1,0,0,0,-1,0,0,0,1))
        
    syntheticDataConfig['l_foot_lc'] = CF.SyntheticDataConfig('left_stride',
        'r_foot_lc',
        'right_stride',
        (1,0,0,0,1,0,0,0,-1),
        (-1,0,0,0,-1,0,0,0,1))
        
    return syntheticDataConfig


