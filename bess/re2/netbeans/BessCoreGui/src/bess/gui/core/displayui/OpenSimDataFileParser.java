/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bess.gui.core.displayui;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * This class parses sto data files in search of column headers
 * @author david.rusbarsky
 */
public class OpenSimDataFileParser {
    private String stoFileFullFilePath = "";
    private String[] headers = {};
    
    /**
     * Constructor that takes the full file path of the file to be parsed
     * @param filePath 
     */
    public OpenSimDataFileParser(String filePath)
    {
        this.stoFileFullFilePath = filePath;
    }
    
    /**
     * Get the column headers for the specified file path
     * 
     * @return array of Strings representing the column headers
     */
    public String[] getColHeaders()
    {
        // Only parse the headers out of the file if we haven't done so yet.
        // Also make sure a file is specified.
        if( (this.headers.length == 0) && ( !this.stoFileFullFilePath.equals("") ) )
        {
            BufferedReader reader;
            try
            {
                reader = new BufferedReader(new FileReader(this.stoFileFullFilePath));
                String line = reader.readLine();
                while ( !line.equals("endheader") ) 
                {
                    // read next line
                    line = reader.readLine();
                }
                line = reader.readLine(); // This should be the headers
                
                // Columns are tab delimited
                this.headers = line.split("\t");
                
                // We are done with the file
                reader.close();
            }
            catch (IOException e) 
            {
                e.printStackTrace();
            }
        }
        
        return this.headers;
    }
}

