/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bess.gui.core;

/**
 *
 * @author david.rusbarsky
 */
public class ScaleToolScaleFactors extends javax.swing.JPanel {
    
    public boolean[] useManual;
    private boolean adjustingValues;
    
    /**
     * Creates new form ScaleToolScaleFactors
     */
    public ScaleToolScaleFactors() {
        initComponents();
        
        useManual = new boolean[this.scaleFactorsTable.getRowCount()];
        for(int i=0; i<this.scaleFactorsTable.getRowCount(); ++i){
            useManual[i] = false;
        }
        
        adjustingValues = false;
    }

    /**
     * Check the current state of the scale factors table and set the appropriate values in the editing panel
     */
    public void checkScaleFactorsTable() {
        int selectedRow = this.scaleFactorsTable.getSelectedRow();

        Object measurementUsedAsObject;
        String measurementUsedAsString = "";
        String[] measurementUsed;
        Object appliedScaleFactorsAsObject;
        String appliedScaleFactorsAsString = "";
        String[] appliedScaleFactors;
        
        int measurementAIndex = 0;
        int measurementBIndex = 0;
        int measurementCIndex = 0;
        
        try {
            measurementUsedAsObject = this.scaleFactorsTable.getValueAt(selectedRow, 1);
            appliedScaleFactorsAsObject = this.scaleFactorsTable.getValueAt(selectedRow, 2);
            
            measurementUsedAsString = measurementUsedAsObject.toString();
            appliedScaleFactorsAsString = appliedScaleFactorsAsObject.toString();
            
            
            measurementUsedAsString = measurementUsedAsString.trim();
            measurementUsed = measurementUsedAsString.split(" ");
            appliedScaleFactorsAsString = appliedScaleFactorsAsString.trim();
            appliedScaleFactors = appliedScaleFactorsAsString.split(" ");
                    
        } catch(Exception e) {
            // Make these empty so we don't cause errors later
            measurementUsed = new String[0];
            appliedScaleFactors = new String[0];
            
            System.err.println("Error setting Scale Tool Scale Factors reading values from table: " + e.toString());
        }
        
        // Set the radio buttons
        this.useManualScalesRadioButton.setSelected(useManual[selectedRow]);
        this.useMeasurementsRadioButton.setSelected(!(useManual[selectedRow]));
        
        if(this.useMeasurementsRadioButton.isSelected()){ // Adjust the combo boxes
            // Set the Uniform check box (True if there is only one item in our array)
            this.useMeasurementsUniformCheckBox.setSelected((1 == measurementUsed.length));
                
            for(int i=0; i<this.measurementComboBoxA.getItemCount(); ++i){
                try{
                    Object currentMeasurementA = this.measurementComboBoxA.getItemAt(i);
                    String currentMeasurementAAsString = currentMeasurementA.toString();
                
                    if(this.useMeasurementsUniformCheckBox.isSelected()){
                        if(currentMeasurementAAsString.equals(measurementUsed[0])){
                            measurementAIndex = i;
                            measurementBIndex = i;
                            measurementCIndex = i;
                        }
                    } else {
                        Object currentMeasurementB = this.measurementComboBoxB.getItemAt(i);
                        String currentMeasurementBAsString = currentMeasurementB.toString();
                        Object currentMeasurementC = this.measurementComboBoxC.getItemAt(i);
                        String currentMeasurementCAsString = currentMeasurementC.toString();
                    
                        if(currentMeasurementAAsString.equals(measurementUsed[0])){
                            measurementAIndex = i;
                        }
                        if(currentMeasurementBAsString.equals(measurementUsed[1])){
                            measurementBIndex = i;
                     }
                        if(currentMeasurementCAsString.equals(measurementUsed[2])){
                            measurementCIndex = i;
                        }
                    }
                } catch(Exception e){
                    System.err.println("Error setting Scale Tool Scale Factors measurement values: " + e.toString());
                }
            }
            this.measurementComboBoxA.setSelectedIndex(measurementAIndex);
            this.measurementComboBoxB.setSelectedIndex(measurementBIndex);
            this.measurementComboBoxC.setSelectedIndex(measurementCIndex);
        } else { // Adjust the text fields
            // Set the Uniform check box (True if there is only one item in our array)
            this.useManualScalesUniformCheckBox.setSelected((1 == appliedScaleFactors.length));
                
            try{
                if(this.useManualScalesUniformCheckBox.isSelected()){
                    this.manualScalesTextFieldA.setText(appliedScaleFactors[0]);
                    this.manualScalesTextFieldB.setText(appliedScaleFactors[0]);
                    this.manualScalesTextFieldC.setText(appliedScaleFactors[0]);
                } else {
                    this.manualScalesTextFieldA.setText(appliedScaleFactors[0]);
                    this.manualScalesTextFieldB.setText(appliedScaleFactors[1]);
                    this.manualScalesTextFieldC.setText(appliedScaleFactors[2]);
                }
            } catch(Exception e){
                System.err.println("Error setting Scale Tool Scale Factors manual settings values: " + e.toString());
            }
        }
        
        updateTable();
    }
    
    /**
     * Check the state of the editing panel and update the table accordingly
     */
    private void updateTable(){
        int selectedRow = this.scaleFactorsTable.getSelectedRow();
        String measurementsUsedValueAsString = " ";
        String appliedScaleFactorValueAsString = " ";
        
        try{
            if (this.useManualScalesRadioButton.isSelected()) {
            //if (this.useManual[selectedRow]) {
                measurementsUsedValueAsString = "MANUAL SCALES";
                //appliedScaleFactorValueAsString = "TESTING";
                if (this.useManualScalesUniformCheckBox.isSelected()) {
                    //appliedScaleFactorValueAsString = this.manualScalesTextFieldA.getText();
                    appliedScaleFactorValueAsString = this.manualScalesTextFieldA.getText() + " " + this.manualScalesTextFieldB.getText() + " " + this.manualScalesTextFieldC.getText();
                } else {
                    appliedScaleFactorValueAsString = this.manualScalesTextFieldA.getText() + " " + this.manualScalesTextFieldB.getText() + " " + this.manualScalesTextFieldC.getText();
                }
            } else {
                if (this.useMeasurementsUniformCheckBox.isSelected()) {
                    Object valueA = this.measurementComboBoxA.getSelectedItem();
                    String valueAAsString = valueA.toString();
                    measurementsUsedValueAsString = valueAAsString;
                } else {
                    Object valueA = this.measurementComboBoxA.getSelectedItem();
                    Object valueB = this.measurementComboBoxB.getSelectedItem();
                    Object valueC = this.measurementComboBoxC.getSelectedItem();

                    String valueAAsString = valueA.toString();
                    String valueBAsString = valueB.toString();
                    String valueCAsString = valueC.toString();

                    measurementsUsedValueAsString = valueAAsString + " " + valueBAsString + " " + valueCAsString;
                }

                // Instead of calculating the value and displaying it, we will just display "CALCULATED"
                // This value is not stored in the config file unless it is manually entered.
                appliedScaleFactorValueAsString = "CALCULATED";
            }

            //this.scaleFactorsTable.setValueAt(measurementsUsedValueAsString, selectedRow, 2);
            //this.scaleFactorsTable.setValueAt(measurementsUsedValueAsString, selectedRow, 1);
        } catch(Exception e){
            // Do nothing...
            //this.scaleFactorsTable.setValueAt("ERR", selectedRow, 2);
            //this.manualScalesTextFieldA.setText(e.toString());
        }
        
        
        try{
            this.scaleFactorsTable.setValueAt(measurementsUsedValueAsString, selectedRow, 1);
        } catch(Exception e){
            // Do nothing...
            //this.manualScalesTextFieldB.setText(e.toString());
        }
        
        try{
            this.scaleFactorsTable.setValueAt(appliedScaleFactorValueAsString, selectedRow, 2);
        } catch(Exception e){
            // Do nothing...
            //this.manualScalesTextFieldC.setText(e.toString());
        }
        
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        measurementEditorPanel = new javax.swing.JPanel();
        useMeasurementsRadioButton = new javax.swing.JRadioButton();
        useManualScalesRadioButton = new javax.swing.JRadioButton();
        measurementComboBoxA = new javax.swing.JComboBox<>();
        equalsLabelA = new javax.swing.JLabel();
        measurementComboBoxB = new javax.swing.JComboBox<>();
        equalsLabelB = new javax.swing.JLabel();
        measurementComboBoxC = new javax.swing.JComboBox<>();
        useMeasurementsUniformCheckBox = new javax.swing.JCheckBox();
        manualScalesTextFieldA = new javax.swing.JTextField();
        equalsLabelC = new javax.swing.JLabel();
        manualScalesTextFieldB = new javax.swing.JTextField();
        equalsLabelD = new javax.swing.JLabel();
        manualScalesTextFieldC = new javax.swing.JTextField();
        useManualScalesUniformCheckBox = new javax.swing.JCheckBox();
        tablePanel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        scaleFactorsTable = new javax.swing.JTable();

        setMinimumSize(new java.awt.Dimension(500, 534));
        setPreferredSize(new java.awt.Dimension(650, 534));
        setLayout(new java.awt.GridBagLayout());

        measurementEditorPanel.setMinimumSize(new java.awt.Dimension(216, 84));
        measurementEditorPanel.setPreferredSize(new java.awt.Dimension(216, 84));
        measurementEditorPanel.setLayout(new java.awt.GridBagLayout());

        useMeasurementsRadioButton.setSelected(true);
        useMeasurementsRadioButton.setText("Use measurements");
        useMeasurementsRadioButton.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                useMeasurementsRadioButtonStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.gridheight = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(10, 6, 0, 0);
        measurementEditorPanel.add(useMeasurementsRadioButton, gridBagConstraints);

        useManualScalesRadioButton.setText("Use manual scales");
        useManualScalesRadioButton.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                useManualScalesRadioButtonStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridheight = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(8, 6, 9, 0);
        measurementEditorPanel.add(useManualScalesRadioButton, gridBagConstraints);

        measurementComboBoxA.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        measurementComboBoxA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                measurementComboBoxAActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.ipadx = 9;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(11, 2, 0, 0);
        measurementEditorPanel.add(measurementComboBoxA, gridBagConstraints);

        equalsLabelA.setText("=");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(14, 4, 0, 0);
        measurementEditorPanel.add(equalsLabelA, gridBagConstraints);

        measurementComboBoxB.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        measurementComboBoxB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                measurementComboBoxBActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(11, 4, 0, 0);
        measurementEditorPanel.add(measurementComboBoxB, gridBagConstraints);

        equalsLabelB.setText("=");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(14, 4, 0, 0);
        measurementEditorPanel.add(equalsLabelB, gridBagConstraints);

        measurementComboBoxC.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        measurementComboBoxC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                measurementComboBoxCActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(11, 4, 0, 0);
        measurementEditorPanel.add(measurementComboBoxC, gridBagConstraints);

        useMeasurementsUniformCheckBox.setText("Uniform");
        useMeasurementsUniformCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                useMeasurementsUniformCheckBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.gridheight = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(10, 2, 0, 0);
        measurementEditorPanel.add(useMeasurementsUniformCheckBox, gridBagConstraints);

        manualScalesTextFieldA.setMinimumSize(new java.awt.Dimension(30, 26));
        manualScalesTextFieldA.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                manualScalesTextFieldAKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.ipadx = 63;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(9, 2, 0, 0);
        measurementEditorPanel.add(manualScalesTextFieldA, gridBagConstraints);

        equalsLabelC.setText("=");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(12, 4, 0, 0);
        measurementEditorPanel.add(equalsLabelC, gridBagConstraints);

        manualScalesTextFieldB.setMinimumSize(new java.awt.Dimension(30, 26));
        manualScalesTextFieldB.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                manualScalesTextFieldBKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.ipadx = 50;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(9, 4, 0, 0);
        measurementEditorPanel.add(manualScalesTextFieldB, gridBagConstraints);

        equalsLabelD.setText("=");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(12, 4, 0, 0);
        measurementEditorPanel.add(equalsLabelD, gridBagConstraints);

        manualScalesTextFieldC.setMinimumSize(new java.awt.Dimension(30, 26));
        manualScalesTextFieldC.setPreferredSize(new java.awt.Dimension(30, 26));
        manualScalesTextFieldC.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                manualScalesTextFieldCKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.ipadx = 50;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(9, 4, 0, 0);
        measurementEditorPanel.add(manualScalesTextFieldC, gridBagConstraints);

        useManualScalesUniformCheckBox.setText("Uniform");
        useManualScalesUniformCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                useManualScalesUniformCheckBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.gridheight = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(8, 2, 9, 92);
        measurementEditorPanel.add(useManualScalesUniformCheckBox, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = java.awt.GridBagConstraints.RELATIVE;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weighty = 0.2;
        add(measurementEditorPanel, gridBagConstraints);

        tablePanel.setMinimumSize(new java.awt.Dimension(500, 450));
        tablePanel.setPreferredSize(new java.awt.Dimension(650, 450));
        tablePanel.setLayout(new java.awt.GridLayout(1, 0));

        jScrollPane1.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        jScrollPane1.setMinimumSize(new java.awt.Dimension(500, 450));
        jScrollPane1.setPreferredSize(new java.awt.Dimension(650, 450));

        scaleFactorsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Body Name", "Measurement(s) Used", "Applied Scale Factor(s)"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                true, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        scaleFactorsTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        scaleFactorsTable.setFillsViewportHeight(true);
        scaleFactorsTable.setMinimumSize(new java.awt.Dimension(800, 800));
        scaleFactorsTable.setPreferredSize(new java.awt.Dimension(625, 800));
        scaleFactorsTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                scaleFactorsTableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(scaleFactorsTable);

        tablePanel.add(jScrollPane1);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridheight = java.awt.GridBagConstraints.RELATIVE;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weighty = 0.8;
        add(tablePanel, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void useMeasurementsUniformCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_useMeasurementsUniformCheckBoxActionPerformed
        if(!adjustingValues){
            adjustingValues = true;
            
            selectUseMeasurementsRadioButton();
            
            if(this.useMeasurementsUniformCheckBox.isSelected()){
                int selectedIndex = this.measurementComboBoxA.getSelectedIndex();
                this.measurementComboBoxB.setSelectedIndex(selectedIndex);
                this.measurementComboBoxC.setSelectedIndex(selectedIndex);
            }
            
            updateTable();
            
            adjustingValues = false;
        }
    }//GEN-LAST:event_useMeasurementsUniformCheckBoxActionPerformed

    private void measurementComboBoxAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_measurementComboBoxAActionPerformed
        if(!adjustingValues){
            adjustingValues = true;
            
            selectUseMeasurementsRadioButton();
        
            if(this.useMeasurementsUniformCheckBox.isSelected()){
                int selectedIndex = this.measurementComboBoxA.getSelectedIndex();
                this.measurementComboBoxB.setSelectedIndex(selectedIndex);
                this.measurementComboBoxC.setSelectedIndex(selectedIndex);
            }
            
            updateTable();
            
            adjustingValues = false;
        }
    }//GEN-LAST:event_measurementComboBoxAActionPerformed

    private void measurementComboBoxBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_measurementComboBoxBActionPerformed
        if(!adjustingValues){
            adjustingValues = true;
            
            selectUseMeasurementsRadioButton();
        
            if(this.useMeasurementsUniformCheckBox.isSelected()){
                int selectedIndex = this.measurementComboBoxB.getSelectedIndex();
                this.measurementComboBoxA.setSelectedIndex(selectedIndex);
                this.measurementComboBoxC.setSelectedIndex(selectedIndex);
            }
            
            updateTable();
            
            adjustingValues = false;
        }
    }//GEN-LAST:event_measurementComboBoxBActionPerformed

    private void measurementComboBoxCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_measurementComboBoxCActionPerformed
        if(!adjustingValues){
            adjustingValues = true;
            
            selectUseMeasurementsRadioButton();
        
            if(this.useMeasurementsUniformCheckBox.isSelected()){
                int selectedIndex = this.measurementComboBoxC.getSelectedIndex();
                this.measurementComboBoxB.setSelectedIndex(selectedIndex);
                this.measurementComboBoxA.setSelectedIndex(selectedIndex);
            }
            
            updateTable();
            
            adjustingValues = false;
        }
    }//GEN-LAST:event_measurementComboBoxCActionPerformed

    private void scaleFactorsTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_scaleFactorsTableMouseClicked
        checkScaleFactorsTable();
    }//GEN-LAST:event_scaleFactorsTableMouseClicked

    private void selectUseMeasurementsRadioButton() {
        this.useMeasurementsRadioButton.setSelected(true);
        this.useManualScalesRadioButton.setSelected(false);
        
        try{
            int selectedRow = this.scaleFactorsTable.getSelectedRow();
            this.useManual[selectedRow] = false;
        } catch (Exception e) {
            // Do nothing...
        }
    }
    
    private void selectUseManualScalesRadioButton() {
        this.useMeasurementsRadioButton.setSelected(false);
        this.useManualScalesRadioButton.setSelected(true);
        
        try{
            int selectedRow = this.scaleFactorsTable.getSelectedRow();
            this.useManual[selectedRow] = true;
        } catch (Exception e){
            // Do nothing...
        }
    }
    
    private void useMeasurementsRadioButtonStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_useMeasurementsRadioButtonStateChanged
        this.useManualScalesRadioButton.setSelected(!(this.useMeasurementsRadioButton.isSelected()));
        
        int selectedRow = this.scaleFactorsTable.getSelectedRow();
        this.useManual[selectedRow] = !(this.useMeasurementsRadioButton.isSelected());
        
        updateTable();
    }//GEN-LAST:event_useMeasurementsRadioButtonStateChanged

    private void useManualScalesRadioButtonStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_useManualScalesRadioButtonStateChanged
        this.useMeasurementsRadioButton.setSelected(!(this.useManualScalesRadioButton.isSelected()));
        
        int selectedRow = this.scaleFactorsTable.getSelectedRow();
        this.useManual[selectedRow] = this.useManualScalesRadioButton.isSelected();
        
        updateTable();
    }//GEN-LAST:event_useManualScalesRadioButtonStateChanged

    private void useManualScalesUniformCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_useManualScalesUniformCheckBoxActionPerformed
        selectUseManualScalesRadioButton();

        if(this.useManualScalesUniformCheckBox.isSelected()){
            String value = this.manualScalesTextFieldA.getText();
            
            this.manualScalesTextFieldB.setText(value);
            this.manualScalesTextFieldC.setText(value);
        }
        
        updateTable();
    }//GEN-LAST:event_useManualScalesUniformCheckBoxActionPerformed

    private void manualScalesTextFieldAKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_manualScalesTextFieldAKeyReleased
        selectUseManualScalesRadioButton();
        
        if(this.useManualScalesUniformCheckBox.isSelected()){
            String value = this.manualScalesTextFieldA.getText();
            
            this.manualScalesTextFieldB.setText(value);
            this.manualScalesTextFieldC.setText(value);
            
            //int selectedRow = this.scaleFactorsTable.getSelectedRow();
            //this.scaleFactorsTable.setValueAt(value, selectedRow, 2);
        }
        
        updateTable();
    }//GEN-LAST:event_manualScalesTextFieldAKeyReleased

    private void manualScalesTextFieldBKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_manualScalesTextFieldBKeyReleased
        selectUseManualScalesRadioButton();
        
        if(this.useManualScalesUniformCheckBox.isSelected()){
            String value = this.manualScalesTextFieldB.getText();
            
            this.manualScalesTextFieldA.setText(value);
            this.manualScalesTextFieldC.setText(value);
            
            //int selectedRow = this.scaleFactorsTable.getSelectedRow();
            //this.scaleFactorsTable.setValueAt(value, selectedRow, 2);
        }
        
        updateTable();
    }//GEN-LAST:event_manualScalesTextFieldBKeyReleased

    private void manualScalesTextFieldCKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_manualScalesTextFieldCKeyReleased
        selectUseManualScalesRadioButton();
        
        if(this.useManualScalesUniformCheckBox.isSelected()){
            String value = this.manualScalesTextFieldC.getText();
            
            this.manualScalesTextFieldB.setText(value);
            this.manualScalesTextFieldA.setText(value);
            
            //int selectedRow = this.scaleFactorsTable.getSelectedRow();
            //this.scaleFactorsTable.setValueAt(value, selectedRow, 2);
        }
        
        updateTable();
    }//GEN-LAST:event_manualScalesTextFieldCKeyReleased


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel equalsLabelA;
    private javax.swing.JLabel equalsLabelB;
    private javax.swing.JLabel equalsLabelC;
    private javax.swing.JLabel equalsLabelD;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField manualScalesTextFieldA;
    private javax.swing.JTextField manualScalesTextFieldB;
    private javax.swing.JTextField manualScalesTextFieldC;
    public javax.swing.JComboBox<String> measurementComboBoxA;
    public javax.swing.JComboBox<String> measurementComboBoxB;
    public javax.swing.JComboBox<String> measurementComboBoxC;
    private javax.swing.JPanel measurementEditorPanel;
    public javax.swing.JTable scaleFactorsTable;
    private javax.swing.JPanel tablePanel;
    private javax.swing.JRadioButton useManualScalesRadioButton;
    private javax.swing.JCheckBox useManualScalesUniformCheckBox;
    private javax.swing.JRadioButton useMeasurementsRadioButton;
    private javax.swing.JCheckBox useMeasurementsUniformCheckBox;
    // End of variables declaration//GEN-END:variables
}
