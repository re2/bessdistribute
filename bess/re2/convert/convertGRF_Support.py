

################################################################################
#
# This script provides the main routines used by the GRF conversion scripts.
#
################################################################################

import codecs
import csv # For CSV reader.
import os
import sys

# Add the parent "bess" directory to the Python module search path so that we
# can import modules from different directories.  Subsequent imports of BESS
# scripts may use reload() to ensure that any changes during development get
# reloaded by the interpreter.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
if bessDir not in sys.path:
    sys.path.append(bessDir)

import utilities.toolsCommon as TC; reload(TC)

"""
Represents the data pertaining to a single force plate.  Note that the X/Y/Z
coordinates should be the OpenSim convention.  i.e. the data should be
transformed before entering into this type.
"""
class SingleForcePlate:
    def __init__(self):
        # Just initialize all the lists of data.  We are using a list for each
        # variable, though we could also use a tuple for each 3-DOF component
        # and keep lists of tuples.  Oh, we will be storing the elements as floats.
        self.data = dict()
        self.data['copX'] = []
        self.data['copY'] = []
        self.data['copZ'] = []
        self.data['fX'] = []
        self.data['fY'] = []
        self.data['fZ'] = []
        self.data['mX'] = []
        self.data['mY'] = []
        self.data['mZ'] = []
                
    def copyData(self, source):
        for key in self.data:
            self.data[key] = list(source.data[key])
    
# End SingleForcePlate.

"""
Represents all the force plate data.  Note that all data lists (time, forces, etc.)
will be the same length since we are reading in data from a uniform CSV.
"""
class ForcePlateDataset:
    # Join time along with the two force plates.
    def __init__(self):
        self.time = []
        self.leftPlate = SingleForcePlate()
        self.rightPlate = SingleForcePlate()

    def copyData(self, source):
        self.time = list(source.time)
        self.leftPlate.copyData(source.leftPlate)
        self.rightPlate.copyData(source.rightPlate)
    
# End ForcePlateDataset.

"""
Different iterations and forms of the force plate data.
"""
class TotalForcePlate:
    def __init__(self):
        # This is the data after being read in and transformed to the OpenSim
        # coordinate frame.
        self.initial = ForcePlateDataset()
        
        # This is the data after being sent through a low pass filter to get
        # rid of transients and to smooth the data slightly.
        self.corrected = ForcePlateDataset()
        
# End TotalForcePlate.

"""
Main worker function called by different forms of the convertGRF script.
Since we invoke scripts using OpenSim and don't pass arguments we need 
different parent scripts to handle different cases.
"""
def mainWorker(correctData=True):
    # Start by getting the file paths to use.  Worker function is guaranteed to
    # return non-empty strings.
    sourceFileFullPath, destinationFileFullPath = TC.getFilePaths(
        TC.FileFilter('CSV files', 'csv'), TC.FileFilter('MOT files', 'mot'))
              
    # Call another function to do the heavy lifting of reading in the ground 
    # data.
    totalForcePlate = readSourceFile(sourceFileFullPath)
    
    # Use a reference to track what operations we've performed on the data.
    workingDataset = totalForcePlate.initial
    
    if correctData:
        # Start the corrected dataset off as a copy of the initial data.  That
        # way we are guaranteed that all the lists are allocated and that we
        # can perform in-place operations on the data.
        totalForcePlate.corrected.copyData(workingDataset)
        
        zeroTime(totalForcePlate.corrected.time)
        filterData(totalForcePlate.corrected)
        
        workingDataset = totalForcePlate.corrected
    
    # Write out the force plate data in a MOT format, selecting the form
    # of the dataset to write out.
    writeDestinationFile(destinationFileFullPath, workingDataset)
    
    print "Conversion completed"
       
# End mainWorker.

"""
Reads the source CSV file and populates a TotalForcePlate data object, being
careful to transform from the Pitt lab coordinate frame to the OpenSim frame.
"""
def readSourceFile(sourceFileFullPath):
    print 'Reading SOURCE data file: %s' % (sourceFileFullPath)
    
    # Attempt to open the source file.  Python will raise an exception
    # if this fails.
    infile = open(sourceFileFullPath, 'r')
    
    # When converting the lab's XLSX files to a CSV, Excel gives a bunch of
    # choices all with .csv extensions.  I made the mistake of using a 
    # "UTF-8 CSV" choice which puts three bytes at the start of the file.
    # You later can't read in the "TimeStamp" column because of those three
    # bytes.
    startText = infile.read(32)
    if startText.startswith(codecs.BOM_UTF8):
        print "Source CSV file is a UTF-8 BOM type."
        print "Please use a normal CSV type -- chances are you converted it " \
                "in Excel as an UTF-8 CSV."                
        raise ValueError("Source CSV file has incorrect encoding.")
    
    # Make up for the previous read of 32 and reset to the beginning of the 
    # file.
    infile.seek(0)
    reader = csv.DictReader(infile)
    
    totalForcePlate = TotalForcePlate()
    
    for index, row in enumerate(reader):
        
        if 'TimeStamp' in row:
            totalForcePlate.initial.time.append(float(row['TimeStamp']))
        elif 'Time' in row:
            totalForcePlate.initial.time.append(float(row['Time']))
        else:
            raise ValueError("Source CSV file doesn't have a Time(Stamp) column.")
            
        # We need to apply a coordinate transform, basically 90 degree rotation
        # around the vertical Y-axis.  So OpenSim+X is Pitt-Z, and OpenSim+Z is Pitt+X.
        # Convert forces, points, and moments accordingly.  Also, FP1 is the
        # left plate and FP2 is the right plate.
        try:
            # totalForcePlate.initial.rightPlate.data['copX'].append(-float(row['FP2.CopZ']))
            # totalForcePlate.initial.rightPlate.data['copY'].append(float(row['FP2.CopY']))
            # totalForcePlate.initial.rightPlate.data['copZ'].append(float(row['FP2.CopX']))
            # totalForcePlate.initial.rightPlate.data['fX'].append(-float(row['FP2.ForZ']))
            # totalForcePlate.initial.rightPlate.data['fY'].append(float(row['FP2.ForY']))
            # totalForcePlate.initial.rightPlate.data['fZ'].append(float(row['FP2.ForX']))
            # totalForcePlate.initial.rightPlate.data['mX'].append(-float(row['FP2.MomZ']))
            # totalForcePlate.initial.rightPlate.data['mY'].append(float(row['FP2.MomY']))
            # totalForcePlate.initial.rightPlate.data['mZ'].append(float(row['FP2.MomX']))
            
            # totalForcePlate.initial.leftPlate.data['copX'].append(-float(row['FP1.CopZ']))
            # totalForcePlate.initial.leftPlate.data['copY'].append(float(row['FP1.CopY']))
            # totalForcePlate.initial.leftPlate.data['copZ'].append(float(row['FP1.CopX']))
            # totalForcePlate.initial.leftPlate.data['fX'].append(-float(row['FP1.ForZ']))
            # totalForcePlate.initial.leftPlate.data['fY'].append(float(row['FP1.ForY']))
            # totalForcePlate.initial.leftPlate.data['fZ'].append(float(row['FP1.ForX']))
            # totalForcePlate.initial.leftPlate.data['mX'].append(-float(row['FP1.MomZ']))
            # totalForcePlate.initial.leftPlate.data['mY'].append(float(row['FP1.MomY']))
            # totalForcePlate.initial.leftPlate.data['mZ'].append(float(row['FP1.MomX']))

            totalForcePlate.initial.rightPlate.data['copX'].append(-float(row['Channel164']))
            totalForcePlate.initial.rightPlate.data['copY'].append(float(row['Channel163']))
            totalForcePlate.initial.rightPlate.data['copZ'].append(float(row['Channel162']))
            totalForcePlate.initial.rightPlate.data['fX'].append(-float(row['Channel167']))
            totalForcePlate.initial.rightPlate.data['fY'].append(float(row['Channel166']))
            totalForcePlate.initial.rightPlate.data['fZ'].append(float(row['Channel165']))
            totalForcePlate.initial.rightPlate.data['mX'].append(-float(row['Channel170']))
            totalForcePlate.initial.rightPlate.data['mY'].append(float(row['Channel169']))
            totalForcePlate.initial.rightPlate.data['mZ'].append(float(row['Channel168']))
            
            totalForcePlate.initial.leftPlate.data['copX'].append(-float(row['Channel155']))
            totalForcePlate.initial.leftPlate.data['copY'].append(float(row['Channel154']))
            totalForcePlate.initial.leftPlate.data['copZ'].append(float(row['Channel153']))
            totalForcePlate.initial.leftPlate.data['fX'].append(-float(row['Channel158']))
            totalForcePlate.initial.leftPlate.data['fY'].append(float(row['Channel157']))
            totalForcePlate.initial.leftPlate.data['fZ'].append(float(row['Channel156']))
            totalForcePlate.initial.leftPlate.data['mX'].append(-float(row['Channel161']))
            totalForcePlate.initial.leftPlate.data['mY'].append(float(row['Channel160']))
            totalForcePlate.initial.leftPlate.data['mZ'].append(float(row['Channel159']))
        except Exception, e:
            # For line number, add 1 for the column headers and add 1 for the
            # zero-based index.
            print "Error reading CSV file around line", index + 2
            
            # Print a blank line so there's a bit of space for the user.
            print
            raise
            
    infile.close()
    
    return totalForcePlate
    
# End readSourceFile.

"""
Writes out a dataset to a MOT file.
"""
def writeDestinationFile(destinationFileFullPath, forcePlateDataset):
    # Save to the destination file.
    print 'Writing DESTINATION data file: %s' % (destinationFileFullPath)
    outfile = open(destinationFileFullPath, 'w')
    
    outfile.write(os.path.basename(destinationFileFullPath) + '\n')    
    outfile.write('version=1\n')
    outfile.write('nRows=%d\n' % len(forcePlateDataset.time))
    # There is time, and then 9 columns for each of the two force plates.
    outfile.write('nColumns=19\n')
    # There are no angular measurements, so the next line can be whatever.
    outfile.write('inDegrees=yes\n')
    outfile.write('endheader\n')

    # Write out the header.  From here on in, all delimiters are tabs.
    headerList = ['time',
        'ground_force_vx', 'ground_force_vy', 'ground_force_vz',
        'ground_force_px', 'ground_force_py', 'ground_force_pz',
        '1_ground_force_vx', '1_ground_force_vy', '1_ground_force_vz',
        '1_ground_force_px', '1_ground_force_py', '1_ground_force_pz',
        'ground_torque_x', 'ground_torque_y', 'ground_torque_z',
        '1_ground_torque_x', '1_ground_torque_y', '1_ground_torque_z']
        
    outfile.write('\t'.join(headerList))
    outfile.write('\n')
    
    # Write out the data.  We've already transformed to OpenSim coordinates
    # so the only thing to specify here is that the right force plate will be
    # ground_force_* and the left force plate will be 1_ground_force_*, per
    # the convention in OpenSim.  Oh, also note that we can assume that the
    # data length of the time column and all the data columns is the same.
    for ii, time in enumerate(forcePlateDataset.time):
        # Convenient reference.
        rp = forcePlateDataset.rightPlate
        lp = forcePlateDataset.leftPlate
        
        floatDataList = [time,
            rp.data['fX'][ii], rp.data['fY'][ii], rp.data['fZ'][ii],
            rp.data['copX'][ii], rp.data['copY'][ii], rp.data['copZ'][ii],
            lp.data['fX'][ii], lp.data['fY'][ii], lp.data['fZ'][ii],
            lp.data['copX'][ii], lp.data['copY'][ii], lp.data['copZ'][ii],
            rp.data['mX'][ii], rp.data['mY'][ii], rp.data['mZ'][ii],
            lp.data['mX'][ii], lp.data['mY'][ii], lp.data['mZ'][ii]]
        
        # Go from a list of floats to a list of strings.
        stringDataList = ['%f' % value for value in floatDataList]
        
        outfile.write('\t'.join(stringDataList))
        outfile.write('\n')
    
    outfile.close()
    
# End writeDestinationFile.

"""
Zeros out the time column.

The source GRF file may not start at 0.0 seconds.  Rather it is some time
after logging has started with the first timestamp being the time that the
trigger was set.  But downstream tools expect time to begin at 0.0, so deduct
the first time from every entry.
"""
def zeroTime(timeList):

    if (len(timeList) == 0):
        return
        
    # Get the first entry.
    initialTime = timeList[0]
    
    for index, time in enumerate(timeList):
        timeList[index] = time - initialTime
        
# End zeroTime.

"""
Performs smoothing operations on the input data.
"""
def filterData(dataset):
    
    # First, saturate the center of pressure values.  Do this to protect from
    # transient spike when the opposite foot strikes the other force plate.
    # We're picking up large COP spikes (10 meters?) that need to be thrown
    # out.
    for dataDict in (dataset.leftPlate.data, dataset.rightPlate.data):
        for copName in ('copX', 'copY', 'copZ'):
            dataDict[copName] = TC.saturateDataColumn(dataDict[copName], -0.5, 0.5)
    
    # Calculate the frequency of the data.  One set of PITT data was 300 Hz,
    # while the second was at 100 Hz.  This affects what filter constant we
    # use when smoothing the data.
    if len(dataset.time) < 2:
        raise ValueError("Not enough time entries to compute a sampling frequency.")
        
    period = dataset.time[1] - dataset.time[0]
    
    # We were happy with a filter contstant of 0.1 when we were sampling with a
    # period of 0.0033.  A period of 0.01 gives a filter frequency of 0.3 which
    # looks to have the same smoothing properties -- gets rid of actual noise
    # without adding significant lag or limiting real moves in the data.
    filterTau = period * 30
    
    # For each of the data columns in our force data, run them through a 
    # low-pass filter.  Note that the time column doesn't get filtered.
    for dataDict in (dataset.leftPlate.data, dataset.rightPlate.data):
        for key, dataList in dataDict.iteritems():
            # See function doc for explanation on time constant.  A lower value
            # means more filtering.  Note that we are now using the vertical
            # force to help identify strides, so we want this to be a bit 
            # smoother than we otherwise might want.
            dataDict[key] = TC.filterDataColumn(dataList, tau=filterTau)
        
    # Ok, we need to account for the difference in how Pitt reports moments
    # and what OpenSim means when it talks about ground_torque_*.  They're not
    # the same!  Pitt is reporting the force plate moment as seen by the origin
    # which for the moments around the X and Z axis (in OpenSim notation) are
    # 99.9% just the vertical force at a moment arm.  For example, a standing
    # subject might have a vertical force on the right leg (fY) of about 600 N
    # and contacting the right treadmill 0.2 m from the origin (copZ).  So,
    # this is why mX is approx. -120 Nm.  Applying -120 Nm to the foot of the
    # model is effectively double counting the 600 N vertical force.  Really,
    # mX/mZ (or ground_torque_x, ground_torque_z in OpenSim wording) should 
    # be approximately 0, which matches with OpenSim data and physical
    # interpretation of whether a foot can actually receive a reaction moment
    # in those directions.  Now, mY can be non-zero as can be seen by standing
    # on one leg and trying to spin around a flat foot.
    #
    # Long story short, we need to calculate proper reaction ground torques
    # from the following moment balances.  Example notation:
    # - M_m,x means Moment_measured,x-axis
    # - F_r,z means Force_reaction,z-axis
    # - p_y means center-of-pressure distance in the y-axis direction
    # 
    # We are solving for reaction moments:
    # M_r,x = F_m,z * p_y - F_m,y * p_z - M_m,x
    # M_r,y = F_m,x * p_z - F_m,z * p_x - M_m,y
    # M_r,z = F_m,y * p_x - F_m,x * p_y - M_m,z
    for dataDict in (dataset.leftPlate.data, dataset.rightPlate.data):
        for ii in range(0, len(dataset.time)):
            dataDict['mX'][ii] = dataDict['fZ'][ii] * dataDict['copY'][ii] \
                - dataDict['fY'][ii] * dataDict['copZ'][ii] \
                - dataDict['mX'][ii]
                
            dataDict['mY'][ii] = dataDict['fX'][ii] * dataDict['copZ'][ii] \
                - dataDict['fZ'][ii] * dataDict['copX'][ii] \
                - dataDict['mY'][ii]
                
            dataDict['mZ'][ii] = dataDict['fY'][ii] * dataDict['copX'][ii] \
                - dataDict['fX'][ii] * dataDict['copY'][ii] \
                - dataDict['mZ'][ii]
            
# End filterData.

