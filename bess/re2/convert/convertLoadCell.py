

################################################################################
#
# This script converts the load cell forces and torques recorded by the
# National Instruments/Labview I/O pipeline.
#
################################################################################

import os
import sys
from threading import Thread

# Add the parent "bess" directory to the Python module search path so that we
# can import modules from different directories.  Subsequent imports of BESS
# scripts may use reload() to ensure that any changes during development get
# reloaded by the interpreter.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
if bessDir not in sys.path:
    sys.path.append(bessDir)

import re2.convert.convertLoadCell_Support as LC; reload(LC)

"""
Main logic
"""
if __name__ == '__main__':
    
    # Run in a worker thread.  This means we don't block the GUI thread when
    # doing a lot of data processing.  Decimate the output data from 1000 Hz
    # down to 200 Hz, which is more inline with the VICON data (100 Hz), and
    # force plate (output at 300 Hz but actually underlying 100 Hz data).  Less
    # data is quicker to load and plot in OpenSim.
    Thread(target=lambda: LC.mainWorker(outputDecimation=5)).start()

# end of main    


