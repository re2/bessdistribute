

################################################################################
#
# This script converts the VICON motion capture data (in CSV format) to a 
# TRC format for use in OpenSim.
#
################################################################################

import csv # For CSV reader.
import os
import sys
from threading import Thread

# Add the parent "bess" directory to the Python module search path so that we
# can import modules from different directories.  Subsequent imports of BESS
# scripts may use reload() to ensure that any changes during development get
# reloaded by the interpreter.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
if bessDir not in sys.path:
    sys.path.append(bessDir)

import utilities.toolsCommon as TC; reload(TC)

"""
Data type to represent all the 3-DOF pose data for a single marker.
"""
class MarkerPose:
    # No need for any accessor methods -- we'll just allow the owner to modify
    # data as need be since this is an internal-use script.
    def __init__(self, markerName):
        self.name = markerName
        
        # Each coordinate is a list of values.  Not clear here but we're
        # currently storing them as floats even though we read them as strings
        # and write them as strings.  This is useful since we have to transform
        # between coordinate systems and it is easier to negate a value as
        # a float then as a string.
        self.xPose = []
        self.yPose = []
        self.zPose = []

# End MarkerPose.

"""
Data type to represent the whole of input VICON data.
"""
class ViconData:
    def __init__(self):
        # Header fields.
        self.datasetName = ''
        self.date = ''
        self.time = ''
        self.type = ''
        # Not clear if this is ever a floating point number in the VICON data
        # but in the output TRC file it is foating point (look at it in ASCII
        # and not in Excel).  I don't think it hurts us to hold it as a float.
        self.frequency = 0.0
        
        # The first column of the data is a frame number, which just seems to
        # be an incrementing positive integer.  So it's likely redundant info
        # to the CSV row.  However, maybe there can be jumps in the data and
        # this is how we would catch it?  Store the frame numbers for the heck
        # of it and use them for sanity checking.
        self.frameNumbers = []
        
        # Use a list to hold the columns of marker data.  Each entry will be a
        # MarkerPose object (name plus columns of pose data).
        self.markerPoses = []
    
# End ViconData.

"""
Main worker function which handles all operation including selection of input
and output files (i.e. GUI elements).
"""
def mainWorker():
    # Start by getting the file paths to use.  Worker function is guaranteed to
    # return non-empty strings.
    sourceFileFullPath, destinationFileFullPath = TC.getFilePaths(
            TC.FileFilter('CSV files', 'csv'), TC.FileFilter('TRC files', 'trc'))
    
    # Call another function to do the heavy lifting of reading in the VICON 
    # data.
    viconData = readSourceFile(sourceFileFullPath)
    
    # Now write out data to a TRC file.
    writeDestinationFile(destinationFileFullPath, viconData)
    
    print "Conversion completed"
    
# End mainWorker.
        
"""
Opens and reads source file into data object.
"""
def readSourceFile(sourceFileFullPath):    
    # Try to printout status, though my experience is that subsequent code
    # chokes up the interpreter and this doesn't appear until after we've
    # processed the data.  I've tried to flush stdout but that didn't have
    # any effect.
    # Todo: figure out output delay.
    print 'Reading SOURCE data file: %s' % (sourceFileFullPath)
    
    # We're going to read the file into a class object.
    viconData = ViconData()
    
    # Attempt to open the source file.  Python will raise an exception
    # if this fails.
    infile = open(sourceFileFullPath, 'r')
    reader = csv.reader(infile)
        
    # We want to make sure that header and data was present in expected
    # fashion and a minimum number of rows is part of that.  For a CSV file
    # you have to count them as you go -- it's not a known property at load
    # time.
    numRows = 0
    for data in reader:        
        numRows += 1
        if (len(data) >= 1 and data[0] == 'ANALOG'):
            break

        # Use a function to handle the tedious data extraction.
        handleSourceRow(reader.line_num, data, viconData)
        
    # 12 rows is the minimum for the header rows and a single frame of data.
    if numRows < 12:
        raise ValueError('Less than 12 rows were read from source CSV file')
    
    infile.close()
    
    return viconData
    
# End of readSourceFile.

"""
Handles a single row of VICON data from the source CSV file.
"""
def handleSourceRow(rowNumber, rowData, viconData):    
    # This isn't a clean CSV -- it has a bunch of rows devoted to one-off
    # header info.  So, key off of rowNumber and fail hard if the data is
    # not formatted as expected.  Use an if/elif chain to brute force it.
    
    if rowNumber <= 9:
        # This is just metadata.
        handleSourceMetadataRow(rowNumber, rowData, viconData)
    elif rowNumber == 10:
        # This is the row that has target marker names.
        handleSourceMarkerNameRow(rowData, viconData)
    elif rowNumber == 11:
        # This is the row that has the column names for all the pose data.
        handleSourceColumnHeaderRow(rowData)
    else:
        # This is a full blown data row.
        handleSourceDataRow(rowNumber, rowData, viconData)

# End handleSourceRow.

"""
Handles a header row from the source CSV file.
"""
def handleSourceMetadataRow(rowNumber, rowData, viconData):    
    # The caller will ensure the rowNumber is between 1 and 9.
    
    if rowNumber == 1:
        # First line is the name of the data -- probably the same as the filename
        # but could possibly be different.
        if len(rowData) < 1:
            raise ValueError('Dataset name row is not formatted properly')
        
        viconData.datasetName = rowData[0]
        
    elif rowNumber == 2:
        # Should be "Date: 03/04/05".
        if len(rowData) < 2 or rowData[0] != 'Date:':
            raise ValueError('Source date row is not formatted properly')
            
        viconData.date = rowData[1]
        
    elif rowNumber == 3:
        # Should be "Time: 4:41:40 PM".
        if len(rowData) < 2 or rowData[0] != 'Time:':
            raise ValueError('Source time row is not formatted properly')
            
        viconData.time = rowData[1]
        
    elif rowNumber == 4:
        # Should be "Type: DFlowTrigger", though I really don't care what
        # the type is (or even know what it is.  I just want the data to look
        # regular.
        if len(rowData) < 2 or rowData[0] != 'Type:':
            raise ValueError('Source type row is not formatted properly')
            
        viconData.type = rowData[1]
        
    elif rowNumber == 5:
        # Should be "Description:", though I don't care about any data here.
        if len(rowData) < 1 or rowData[0] != 'Description:':
            raise ValueError('Source description row is not formatted properly')
        
    elif rowNumber == 6:
        # Should be "Notes:", though I don't care about any data here.
        if len(rowData) < 1 or rowData[0] != 'Notes:':
            raise ValueError('Source notes row is not formatted properly')
            
    elif rowNumber == 7:
        # Should be blank.  Careful here, it could be a truly blank line, or
        # it could be something like ",,,,,,,,,,,".  So, don't sweat this one
        # too hard.
        if len(rowData) >= 1 and rowData[0] != '':
            raise ValueError('Line 7 of source CSV is not blank')
            
    elif rowNumber == 8:
        # Should be the string "TRAJECTORIES".
        if len(rowData) < 1 or rowData[0] != 'TRAJECTORIES':
            raise ValueError('Line 8 of source CSV is not TRAJECTORIES')
            
    elif rowNumber == 9:
        # Should be the frequency, encoded like "100,Hz".
        if len(rowData) < 2 or rowData[1] != 'Hz':
            raise ValueError('Source frequency row is not formatted properly')
            
        # Sample TRC files show frequency as a floating point number, so just
        # hold the VICON frequency as a float.
        viconData.frequency = float(rowData[0])

# End handleSourceMetadataRow.

"""
Parses the VICON CSV row that contains marker names.
"""
def handleSourceMarkerNameRow(rowData, viconData):
    # Expect a CSV format of ",Marker 1 Name,,,Marker 2 Name,,".  Assume that
    # there is at least one marker defined for this data capture.
    if len(rowData) < 4:
        raise ValueError('Source marker name row is not formatted properly')
        
    for index, value in enumerate(rowData):        
        if (index == 0 or (index - 1) % 3 != 0) and value != '':
            # Check that an entry that should be blank is blank.  
            # Entries 1, 4, 7, etc. should be non-blank.
            # Todo: better debug info.
            raise ValueError('Source marker name row is not formatted properly')
        elif value != '':
            # This is the case where we have a marker name.  Create a new
            # marker object.
            viconData.markerPoses.append(MarkerPose(value))
        else:
            # This is the case where we have an empty field, either the
            # first field, or any of the pair of fields following a marker name.
            pass

# End handleSourceMarkerNameRow.

def handleSourceColumnHeaderRow(rowData):
    # Expect a CSV format of "Field #,X,Y,Z,X,Y,Z".  Assume that
    # there is at least one marker defined for this data capture.
    if len(rowData) < 4:
        raise ValueError('Source column header row is not formatted properly')
        
    for index, value in enumerate(rowData):        
        if index == 0:
            if value != 'Field #':
                # Check that the first entry is the field number.
                # Todo: better debug info.
                raise ValueError('Source column header row is not formatted properly')
        elif (index - 1) % 3 == 0:
            if value != 'X':
                # Todo: better debug info.
                raise ValueError('Source column header row is not formatted properly')
        elif (index - 2) % 3 == 0:
            if value != 'Y':
                # Todo: better debug info.
                raise ValueError('Source column header row is not formatted properly')
        elif (index - 3) % 3 == 0:
            if value != 'Z':
                # Todo: better debug info.
                raise ValueError('Source column header row is not formatted properly')

# End handleSourceColumnHeaderRow.

def handleSourceDataRow(rowNumber, rowData, viconData):
    # Skip blank lines.  I've seen extra blank lines at the bottom of a file.
    if len(rowData) == 0:
        return

    # A data row should be the frame number (as integer) followed by floating
    # point numbers, with 3 numbers for each marker.
    
    # Verify that we have enough data for all the markers we expect.  After
    # accounting for the frame field, we should have 3 fields for each marker.
    if len(rowData) - 1 != len(viconData.markerPoses) * 3:
        print 'Error in row number: %d' % (rowNumber)
        raise ValueError('Mismatch in number of pose values and number of markers')
        
    for index, value in enumerate(rowData):
        if index == 0:
            viconData.frameNumbers.append(int(value))
            
            # Todo: check frame number to row number and detect mismatch.
            continue
        
        # Figure out which marker we are dealing with.  We can use integer 
        # division to throw away the remainder to find the right marker.
        markerIndex = int((index - 1) / 3)
        markerPose = viconData.markerPoses[markerIndex]
        
        # So, fields can be empty.  For example, look at the LLEK marker
        # at the beginning of the Static02.csv for the 20180322 data collect.
        # I wonder if that happens if the marker gets obscured from the
        # VICON cameras.  Use the wonderful Python 'None' to mark these
        # instances and deal with it when we write out the TRC file.
        floatValue = None
        if value != '':
            floatValue = float(value)
            
        # Now figure out whether to apply the data as an X, Y, or Z component.
        if (index - 1) % 3 == 0:
            markerPose.xPose.append(floatValue)
        elif (index - 2) % 3 == 0:
            markerPose.yPose.append(floatValue)
        elif (index - 3) % 3 == 0:
            markerPose.zPose.append(floatValue)
    
# End handleSourceDataRow.

"""
Writes the VICON data to an output TRC file.
"""
def writeDestinationFile(destinationFileFullPath, viconData):
    # Save to the destination file.
    print 'Writing DESTINATION data file: %s' % (destinationFileFullPath)
    # Todo: figure out if we should prompt here (or earlier) on overwrite of
    # an existing file.
    destinationFile = open(destinationFileFullPath, 'w')
    
    # Note on output formatting:  TRC files are tab delimited.  A brief format
    # guide is here:  https://simtk-confluence.stanford.edu/display/OpenSim/Marker+%28.trc%29+Files
    # Line endings are '\n' which will get converted to \r\n on Windows, per
    # Python docs:  always use \n and don't use os.linesep.
    # String formatting is limited in Python 2.5, so do field substitution using
    # the "% tuple([])" construct per https://stackoverflow.com/questions/10006144/string-formatting-for-python-2-5
    
    # First line sample:
    # "PathFileType<TAB>4<TAB>(X/Y/Z)<TAB>subject01_static.trc"
    outString = 'PathFileType\t4\t(X/Y/Z)\t' + os.path.basename(destinationFileFullPath) + '\n'
    destinationFile.write(outString)
    
    # Second line sample:
    # DataRate<TAB>CameraRate<TAB>NumFrames<TAB>NumMarkers<TAB>Units<TAB>OrigDataRate<TAB>OrigDataStartFrame<TAB>OrigNumFrames
    outString = 'DataRate\tCameraRate\tNumFrames\tNumMarkers\tUnits\tOrigDataRate\tOrigDataStartFrame\tOrigNumFrames\n'
    destinationFile.write(outString)
    
    # Third line sample:
    # 60.00<TAB>60.00<TAB>300<TAB>49<TAB>mm<TAB>60.00<TAB>1<TAB>300
    outList = [
            viconData.frequency,
            viconData.frequency,
            len(viconData.frameNumbers),
            len(viconData.markerPoses),
            'mm',
            viconData.frequency,
            viconData.frameNumbers[0],
            len(viconData.frameNumbers)
            ]
            
    outString = '%f\t%f\t%d\t%d\t%s\t%d\t%d\t%d\n' % tuple(outList)
    destinationFile.write(outString)
    
    # 4th line sample:
    # Frame#<TAB>Time<TAB>R.ASIS<TAB><TAB><TAB>L.ASIS<TAB><TAB><TAB>
    outList = ['Frame#', 'Time']
    for index, value in enumerate(viconData.markerPoses):
        outList.extend([value.name, '', ''])
        
    outList.append('\n')
    outString = '\t'.join(outList)    
    destinationFile.write(outString)
    
    # 5th line sample:
    # <TAB><TAB>X1<TAB>Y1<TAB>Z1<TAB>X2<TAB>Y2<TAB>Z2<TAB>
    outList = ['', '']
    for index, value in enumerate(viconData.markerPoses):
        markerNum = index + 1
        outList.extend(['X%d' % markerNum, 'Y%d' % markerNum, 'Z%d' % markerNum])
        
    outList.append('\n')
    outString = '\t'.join(outList)    
    destinationFile.write(outString)
    
    # 6th line is blank.
    destinationFile.write('\n')
    
    # All remaining lines are data lines.  Sample:
    # 1<TAB>0.000000<TAB>608.664310<TAB>1072.713130<TAB>157.442150<TAB>
    # Do an iteration over the frame numbers list to get the index of marker
    # data to reference for each marker.  Based on how we read data in we
    # should be guaranteed that the number of elements in the frame numbers
    # list is the same as each of the marker pose coordinate lists.
    firstFrameNumber = viconData.frameNumbers[0]
    for frameIndex, frameNumber in enumerate(viconData.frameNumbers):
        outList = []
        outList.append('%d' % frameNumber)
        
        # Calculate a time based on frame number and time.  Zero-index the
        # frame number so the first time is always 0.
        time = (frameNumber - firstFrameNumber) / viconData.frequency
        outList.append('%f' % time)
        
        # Now go through the markers and for each, get it's x,y,z marker poses.
        for markerPose in viconData.markerPoses:
            viconTuple = markerPose.xPose[frameIndex], markerPose.yPose[frameIndex], markerPose.zPose[frameIndex]
            openSimTuple = transformPose(viconTuple);
            
            appendPoseToList(outList, openSimTuple[0])
            appendPoseToList(outList, openSimTuple[1])
            appendPoseToList(outList, openSimTuple[2])
        
        outList.append('\n')
        outString = '\t'.join(outList)    
        destinationFile.write(outString)
    
    destinationFile.close()
    
# End writeDestinationFile.

"""
Appends a pose coordinate to a list of poses.  This function exists in order
to uniformly handle the case where a pose was empty in the source VICON data
and we need to figure out what to write in the output TRC file:  (blank, 0.0,
infinity, NaN, etc.)
"""
def appendPoseToList(outList, pose):
    if pose is None:
        # OpenSim seems to be able to handle empty fields gracefully when it
        # loads and visualizes a TRC file.  This is probably preferable to
        # putting a phony number in there that could throw off IK.
        outList.append('')
    else:
        outList.append('%f' % pose)

# End appendPoseToList.

"""
Transforms a 3DOF pose from the VICON reference frame to the OpenSim frame.
"""
def transformPose(viconTuple):
    """
    Description of frames:  both VICON and OpenSim are right-handed.
    
    VICON frame:
        HUMAN FRONT
    +X <-----
            |
            |
            V
            +Y
            
    OpenSim frame:
        HUMAN FRONT
            +X
            ^
            |
            |
            -----> +Z
    """
    
    # Protect against poses for which no data existed (i.e. gaps in data for 
    # a marker) which we encoded as "None" type.
    x = y = z = None
    
    if viconTuple[1]:
        x = -viconTuple[1]
        
    if viconTuple[2]:
        y = viconTuple[2]
        
    if viconTuple[0]:
        z = -viconTuple[0]
        
    return x, y, z

# End transformPose.

"""
Main function.
"""
if __name__ == '__main__':
    
    # Run in a worker thread.  This means we don't block the GUI thread when
    # doing a lot of data processing.
    Thread(target=lambda: mainWorker()).start()
    
# End main.


