

################################################################################
#
# This script converts the EMG data recorded by Pitt into an OpenSim STO file.
#
################################################################################

import os
import sys
from threading import Thread

# Add the parent "bess" directory to the Python module search path so that we
# can import modules from different directories.  Subsequent imports of BESS
# scripts may use reload() to ensure that any changes during development get
# reloaded by the interpreter.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
if bessDir not in sys.path:
    sys.path.append(bessDir)

import re2.convert.convertEMG_Support as EMG; reload(EMG)

"""
Main logic
"""
if __name__ == '__main__':
    
    # Run in a worker thread.  This means we don't block the GUI thread when
    # doing a lot of data processing.  Optionally decimate the output data to
    # reduce the amount of data.  Source data is at 1500 Hz.
    Thread(target=lambda: EMG.mainWorker(outputDecimation=3)).start()

# End of main.


