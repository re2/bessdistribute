

################################################################################
#
# This script provides the main routines used by the EMG conversion 
# scripts.
#
################################################################################

from collections import defaultdict
import csv # For CSV reader.
import os
import sys

# Add the parent "bess" directory to the Python module search path so that we
# can import modules from different directories.  Subsequent imports of BESS
# scripts may use reload() to ensure that any changes during development get
# reloaded by the interpreter.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
if bessDir not in sys.path:
    sys.path.append(bessDir)

import utilities.toolsCommon as TC; reload(TC)

class EMG_Dataset:
    """
    Represents all the EMG data.
    """

    # Join time along with each of the named muscle sensors.
    def __init__(self):
        self.time = []
        self.muscles = defaultdict(list)

    def copyData(self, source):
        self.time = list(source.time)
        self.muscles = defaultdict(list)
        for key in source.muscles:
            self.muscles[key] = list(source.muscles[key])
            
    # Cleans the names of the muscles in the dictionary to something more
    # palatable to OpenSim.
    def cleanMuscleNames(self):
        newDict = dict()
        for key, value in self.muscles.iteritems():
            newDict[self.cleanMuscleString(key)] = value
            
        self.muscles = newDict
        
    # Routine to clean a single muscle name.
    def cleanMuscleString(self, name):
        # Get rid of unit suffix if uV.  "RT TIB.ANT.,uV"
        if name.endswith(',uV'):
            name = name[:-len(',uV')]
                        
        # Get rid of unit suffix if V.  "RT TIB.ANT.,V"
        if name.endswith(',V'):
            name = name[:-len(',V')]
                        
        # Get rid of a trailing period.  "RT TIB.ANT."
        name = name.rstrip('.')
        
        # Underscore a period with a trailing space.  "LT MED. GASTRO"
        name = name.replace('. ', '_')
        
        # Underscore a period in the middle of a name.  "RT TIB.ANT"
        name = name.replace('.', '_')
        
        # Underscore a space in the middle of a name.  "RT TIB_ANT"
        name = name.replace(' ', '_')
        
        # Lowercase everything.
        name = name.lower()
        
        return name
    
# End EMG_Dataset.

class TotalEMG:
    """
    Different iterations and forms of the EMG data.
    """
    
    def __init__(self):
        # This is the data after being read in.
        self.initial = EMG_Dataset()
        
        # This is the data after being normalized (rectified, filtered, etc.).
        self.normalized = EMG_Dataset()
        
# End TotalEMG.

def mainWorker(outputDecimation=1, normalizeData=True):
    """
    Main worker function called by different forms of the convertEMG script.
    
    Since we invoke scripts using OpenSim and don't pass arguments we need 
    different parent scripts to handle different cases.

    outputDecimation: input data is 1500 Hz across 15 different components, which is
    a lot of data for OpenSim to open.  So let's give the option to decimate the
    output data.  For example, a decimation of 3 means only write out every third
    value.
    """
    # Start by getting the file paths to use.  Worker function is guaranteed to
    # return non-empty strings.  Oh, we want the output to be a STO file (and 
    # not a MOT file) since the time in the source file seems to be regularly
    # irregular, with a .007 second increment followed by a .006 increment.
    # I think a MOT needs to be regular time increment but a STO can be
    # variable.
    sourceFileFullPath, destinationFileFullPath = TC.getFilePaths(
        TC.FileFilter('CSV files', 'csv'), TC.FileFilter('STO files', 'sto'))
              
    # Call another function to do the heavy lifting of reading in the EMG data.
    totalEMG = readSourceFile(sourceFileFullPath)
    
    # Use a reference to track what operations we've performed on the data.
    workingDataset = totalEMG.initial
    
    if normalizeData:
        # Start the corrected dataset off as a copy of the initial data.
        totalEMG.normalized.copyData(workingDataset)
        normalize(totalEMG.normalized)
        workingDataset = totalEMG.normalized
    
    # Write out the EMG data in a STO format, selecting the form
    # of the dataset to write out.
    writeDestinationFile(destinationFileFullPath, workingDataset, outputDecimation)
    
    print "Conversion completed"
       
# End mainWorker.

def readSourceFile(sourceFileFullPath):
    """
    Reads the source CSV file and populates a TotalEMG data object.
    """

    print 'Reading SOURCE data file: %s' % (sourceFileFullPath)
    
    # Attempt to open the source file.  Python will raise an exception if the 
    # file open fails.
    infile = open(sourceFileFullPath, 'r')
    
    # Will receive the data we read in.
    totalEMG = TotalEMG()
    
    # Let's read a few header lines before we get to the proper CSV data.  The
    # header really doesn't provide any interesting information so throw it
    # away -- we could easily add it to the dataset object later.
    # 
    # Example header:
    # MR32CSV
    # Name,Gait02
    # Frequency,1500
    # Date,22-03-2018 17:17:27
    
    # Skip the first line because I'm not sure what it should be.
    infile.readline()
    
    # Make sure the next line starts with "Name,".
    if 'Name,' not in infile.readline():
        raise ValueError('2nd line is not a name line')
        
    if 'Frequency,' not in infile.readline():
        raise ValueError('3rd line is not a frequency line')
        
    if 'Date,' not in infile.readline():
        raise ValueError('4th line is not a date line')
    
    # Now we can put the remainder of the file into a dictionary reader.
    reader = csv.DictReader(infile)

    for index,row in enumerate(reader):
        # Each row is a dictionary of key/value pairs.  Iterate through them.
        index=index+1
        for key, value in row.iteritems():
            try:
                # if (index%50 == 0):
                    # print "Reading CSV file around line", index + 5
                # Special case the "Time,s" column since we want to track this
                # separately from the individual muscle data.
                # separately from the individual muscle data.
                if 'Time,s' in key:
                    totalEMG.initial.time.append(float(value))
                    continue
                    
                # Ok, deal with a muscle voltage.  A defaultdict takes care of
                # list creation.
                totalEMG.initial.muscles[key].append(float(value))
            except Exception, e:
                # For line number, add 1 for the column headers and add 1 for the
                # zero-based index.
                print "Error reading CSV file around line", index + 5
                
                # Print a blank line so there's a bit of space for the user.
                print
                raise
    infile.close()
    
    # Ok, let's clean the muscle names.  They came in looking like
    # "RT LUMBAR ES,uV" which OpenSim doesn't handle well.  So, get rid of the
    # spaces and unit suffix.
    totalEMG.initial.cleanMuscleNames()
    
    return totalEMG
    
# End readSourceFile.

def writeDestinationFile(destinationFileFullPath, EMG_Dataset, outputDecimation):
    """
    Writes out a dataset to a MOT file.
    """

    # Save to the destination file.
    print 'Writing DESTINATION data file: %s' % (destinationFileFullPath)
    outfile = open(destinationFileFullPath, 'w')
    
    outfile.write(os.path.basename(destinationFileFullPath) + '\n')    
    outfile.write('version=1\n')
    outfile.write('nRows=%d\n' % len(EMG_Dataset.time))
    # There is time, and then columns for each muscle.
    outfile.write('nColumns=%d\n' % (1 + len(EMG_Dataset.muscles)))
    # There are no angular measurements, so the next line can be whatever.
    outfile.write('inDegrees=yes\n')
    outfile.write('endheader\n')

    # Write out the header.  From here on in, all delimiters are tabs.
    headerList = ['time']
    for key in sorted(EMG_Dataset.muscles):
        headerList.append(key)
        
    outfile.write('\t'.join(headerList))
    outfile.write('\n')
    
    # Write out the data.  Oh, also note that we can assume that the
    # data length of the time column and all the data columns is the same.
    for ii, time in enumerate(EMG_Dataset.time):
        # Potentially decimate the output by some factor.
        if ii % outputDecimation != 0:
            continue
            
        floatDataList = [time]
        for key in sorted(EMG_Dataset.muscles):
            floatDataList.append(EMG_Dataset.muscles[key][ii])
            
        stringDataList = ['%f' % value for value in floatDataList]
        
        outfile.write('\t'.join(stringDataList))
        outfile.write('\n')
        
    outfile.close()
    
# End writeDestinationFile.

def normalize(dataset):
    """
    Rectifies the EMG data and then filter it.
    
    This is based on the advice of Jen Hicks, referencing this document:
    https://www.noraxon.com/wp-content/uploads/2014/12/ABC-EMG-ISBN.pdf
    """
    
    scaledMuscles = defaultdict(list)
    
    for name, dataList in dataset.muscles.iteritems():
        # Do a full wave rectification.        
        rectifiedList = TC.rectifyDataColumn(dataList, fullRectify=True)
        
        # Do a low pass filter.  The paper recommends an RMS central moving
        # average with a time window of 50 to 100 ms.  We'll use the higher
        # window since our data looks really noisy.
        if len(dataset.time) < 2:
            raise ValueError('Not enough samples in EMG file')
            
        timeDiff = dataset.time[1] - dataset.time[0]
        numSamples = int(0.1 / timeDiff)
        # Moving window needs to be an odd number.
        if numSamples % 2 == 0:
            numSamples += 1
        
        filteredList = TC.rmsCentralMovingAverageDataColumn(rectifiedList, 
            numSamples)
        
        # Make sure to set the value back in the dictionary.  i.e. replace the
        # original data.
        dataset.muscles[name] = filteredList
    
        # Also produce a list of data normalized to the near-maximum RMS value.  
        # So most everything in this new column should be in the range of [0 1].
        # Start by getting stats on the data column since we're going to use
        # standard deviation to determine an effective maximium in order to
        # remove any possible outliers that would ruin our scaling (i.e. using a
        # one-time data spike as the max value would scale the rest of the
        # data down close to 0).
        mean, stdDev, minValue, maxValue, rms = TC.calculateStatsDataColumn(
            filteredList)
        
        # Use the 99.7% mark as an effective maximum.  This puts most of the 
        # data in the range of [0 1] though there will spikes up to 1.5 or so.
        # We could bound the data but I think it better to leave the data in.
        # The reason we are normalizing is to compare to CMC activations which
        # are between 0 and 1, so this is mainly for visualization and I don't
        # want to distort data just for visualization.        
        effectiveMax = mean + 3.0 * stdDev     
        scaledList = [x / effectiveMax for x in filteredList]
        scaledMuscles[name + '_scaled'] = scaledList
    
    # Add in the scaled muscles to our main data dictionary.  We can't do
    # this in the loop because you can't modify a dictionary you're iterating
    # over.
    dataset.muscles.update(scaledMuscles)
    
# End normalize.

