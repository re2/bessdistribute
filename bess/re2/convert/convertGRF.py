

################################################################################
#
# This script converts the ground reaction forces recorded by the Pitt treadmill
# and converts the data and file format to something that OpenSim accepts.
# Input is a CSV file, output is a MOT file.  This is the variant that corrects
# errors in the data (filtering, saturating, etc.)
#
################################################################################

import os
import sys
from threading import Thread

# Add the parent "bess" directory to the Python module search path so that we
# can import modules from different directories.  Subsequent imports of BESS
# scripts may use reload() to ensure that any changes during development get
# reloaded by the interpreter.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
if bessDir not in sys.path:
    sys.path.append(bessDir)

import re2.convert.convertGRF_Support as GRF; reload(GRF)

"""
Main logic
"""
if __name__ == '__main__':
    
    # Run in a worker thread.  This means we don't block the GUI thread when
    # doing a lot of data processing.
    Thread(target=lambda: GRF.mainWorker(correctData=True)).start()

# End of main.


