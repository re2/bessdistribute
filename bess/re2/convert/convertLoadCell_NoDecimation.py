

################################################################################
#
# This script converts the load cell forces and torques recorded by the
# National Instruments/Labview I/O pipeline.
#
################################################################################

import os
import sys
from threading import Thread

# Add the parent "bess" directory to the Python module search path so that we
# can import modules from different directories.  Subsequent imports of BESS
# scripts may use reload() to ensure that any changes during development get
# reloaded by the interpreter.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
if bessDir not in sys.path:
    sys.path.append(bessDir)

import re2.convert.convertLoadCell_Support as LC; reload(LC)

"""
Main logic
"""
if __name__ == '__main__':
    
    # Run in a worker thread.  This means we don't block the GUI thread when
    # doing a lot of data processing.  Don't decimate the output -- this is
    # useful (for example) if you were to identify the load cells by tapping
    # each one with a mallet -- in that case the force response could really
    # be a single data value that you could possibly miss if you decimated the
    # output.
    Thread(target=lambda: LC.mainWorker(outputDecimation=1)).start()

# end of main    


