

################################################################################
#
# This script provides the main routines used by the load cell conversion 
# scripts.
#
################################################################################

import csv # For CSV reader.
import os
import sys

# Add the parent "bess" directory to the Python module search path so that we
# can import modules from different directories.  Subsequent imports of BESS
# scripts may use reload() to ensure that any changes during development get
# reloaded by the interpreter.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
if bessDir not in sys.path:
    sys.path.append(bessDir)

import utilities.toolsCommon as TC; reload(TC)

"""
Represents the data pertaining to a single load cell.
"""
class LoadCell:
    # This is just the order the data is recorded in and what order we
    # write it out.  Using a capitalization that we will want to see when
    # we load these strings into OpenSim:  i.e. 'back_fx', 'ankle_tz'.
    orderedComponents = ('fx', 'fy', 'fz', 'tx', 'ty', 'tz')
        
    def __init__(self):
        # Just initialize all the lists of data.  We are using a list for each
        # variable, though we could also use a tuple for each 6-DOF component
        # and keep lists of tuples.  Oh, we will be storing the elements as floats.
        self.data = dict()
        for component in LoadCell.orderedComponents:
            self.data[component] = []
                
    def copyData(self, source):
        for key in self.data:
            self.data[key] = list(source.data[key])
    
# End LoadCell.

"""
Represents all the load cell data.  Note that all data lists (time, forces, etc.)
will be the same length since we are reading in data from a uniform CSV.
"""
class LoadCellDataset:
    # This is the order the data is recorded in.  Identify these as right
    # side to help downstream:  we will be creating synthetic load cells for
    # the left side and having these strings denote right side will be helpful.
    # Also denote as "sensor"-frame since later on we will also have
    # body-frame and ground-frame.
    orderedInputs = ('back_lc_sensor', 
        'r_foot_lc_sensor', 
        'r_ankle_lc_sensor', 
        'r_calf_lc_sensor', 
        'r_hip_lc_sensor')

    # Join time along with each of the named load cells.
    def __init__(self):
        self.time = []
        self.cells = dict()        
        for key in LoadCellDataset.orderedInputs:
            self.cells[key] = LoadCell()

    def copyData(self, source):
        self.time = list(source.time)
        for key in self.cells:
            self.cells[key] = dict(source.cells[key])
    
# End LoadCellDataset.

"""
Different iterations and forms of the load cell data.
"""
class TotalLoadCell:
    def __init__(self):
        # This is the data after being read in.
        self.initial = LoadCellDataset()
        
# End TotalLoadCell.

"""
Main worker function called by different forms of the convertLoadCell script.
Since we invoke scripts using OpenSim and don't pass arguments we need 
different parent scripts to handle different cases.

outputDecimation: input data is 1000 Hz across 30 different components, which is
a lot of data for OpenSim to open.  So let's give the option to decimate the
output data.  For example, a decimation of 3 means only write out every third
value.
"""
def mainWorker(outputDecimation=1):
    # Start by getting the file paths to use.  Worker function is guaranteed to
    # return non-empty strings.
    sourceFileFullPath, destinationFileFullPath = TC.getFilePaths(
        TC.FileFilter('TXT files', 'txt'), TC.FileFilter('STO files', 'sto'))
    
    offsetFileFullPath = TC.getSingleFilePath("Select file containing the OFFSET values", TC.FileFilter('TXT files', 'txt'))
    
    # Call another function to do the heavy lifting of reading in the load cell 
    # data. 
    totalLoadCell = readSourceFile(sourceFileFullPath, offsetFileFullPath)
    
    # Use a reference to track what operations we've performed on the data.
    workingDataset = totalLoadCell.initial
    
    # Write out the load cell data in a STO format, selecting the form
    # of the dataset to write out.
    writeDestinationFile(destinationFileFullPath, workingDataset, outputDecimation)
    
    print "Conversion completed"
       
# End mainWorker.

"""
Reads the source CSV file and populates a TotalLoadCell data object.
"""
def readSourceFile(sourceFileFullPath, offsetFileFullPath):
    print 'Reading SOURCE data file: %s' % (sourceFileFullPath)
    print 'Reading OFFSET data file: %s' % (offsetFileFullPath)
    
    infile = open(offsetFileFullPath, 'r')
    reader = csv.reader(infile, delimiter='\t')
    offsetValues = []
    
    # Should only be one line, but doing a copy/paste to speed up the coding process for this feature...
    for rowIndex, row in enumerate(reader):
        columnIndex = 0
        for input in LoadCellDataset.orderedInputs:
            for index, component in enumerate(LoadCell.orderedComponents):                
                offsetValues.append( float(row[columnIndex + index]) )
                #totalLoadCell.initial.cells[input].data[component].append(      float(row[columnIndex + index])      )
                
            columnIndex += len(LoadCell.orderedComponents)
    infile.close()
    
    
    # Attempt to open the source file and read it into a CSV object.  Python 
    # will raise an exception if the file open fails.  Note that the source
    # data is tab delimited CSV.
    infile = open(sourceFileFullPath, 'r')
    reader = csv.reader(infile, delimiter='\t')
    
    totalLoadCell = TotalLoadCell()
    
    for rowIndex, row in enumerate(reader):
        # Fill in a time based on the output data rate.  Right now this number
        # is not documented and I've calculated by lining up the data with 
        # ground reaction forces to line up the "footsteps".  Time doesn't 
        # appear in the source data set as a column.
        # Update:  1000 Hz is confirmed by the lab.
        dataRate = 1000.0
        totalLoadCell.initial.time.append(rowIndex / dataRate)
        
        # Let's go through each of the load cells and read in each of the
        # 6 degrees of freedom.
        columnIndex = 0
        for input in LoadCellDataset.orderedInputs:
            for index, component in enumerate(LoadCell.orderedComponents):                
                totalLoadCell.initial.cells[input].data[component].append( (float(row[columnIndex + index]) + offsetValues[columnIndex + index]) )
                
            columnIndex += len(LoadCell.orderedComponents)
            
    infile.close()
    
    return totalLoadCell
    
# End readSourceFile.

"""
Writes out a dataset to a MOT file.
"""
def writeDestinationFile(destinationFileFullPath, loadCellDataset, outputDecimation):
    # Save to the destination file.
    print 'Writing DESTINATION data file: %s' % (destinationFileFullPath)
    outfile = open(destinationFileFullPath, 'w')
    
    outfile.write(os.path.basename(destinationFileFullPath) + '\n')    
    outfile.write('version=1\n')
    outfile.write('nRows=%d\n' % len(loadCellDataset.time))
    # There is time, and then 6 columns for each of the load cells.
    outfile.write('nColumns=31\n')
    # There are no angular measurements, so the next line can be whatever.
    outfile.write('inDegrees=yes\n')
    outfile.write('endheader\n')

    # Write out the header.  From here on in, all delimiters are tabs.
    headerList = ['time']
    for input in LoadCellDataset.orderedInputs:
        for component in LoadCell.orderedComponents:
            headerList.append(input + '_' + component)
    
    outfile.write('\t'.join(headerList))
    # Add a newline after joining with a tab delimeter.
    outfile.write('\n')
    
    # Write out the data.  Oh, also note that we can assume that the
    # data length of the time column and all the data columns is the same.
    for ii, time in enumerate(loadCellDataset.time):
        # Potentially decimate the output by some factor.
        if ii % outputDecimation != 0:
            continue
            
        floatDataList = [time]
        for input in LoadCellDataset.orderedInputs:
            for component in LoadCell.orderedComponents:
                floatDataList.append(loadCellDataset.cells[input].data[component][ii])
        
        stringDataList = ['%f' % value for value in floatDataList]
            
        outfile.write('\t'.join(stringDataList))
        outfile.write('\n')
        
    outfile.close()
    
# End writeDestinationFile.

