

################################################################################
#
#
################################################################################

import os
import os.path

from xml.dom.minidom import parse, parseString, getDOMImplementation, Document
import xml.etree.cElementTree as ET

class bessXmlInvDynIO:
    def __init__(self, myConfig):
        print "Initializing bessXmlInvDyn"
        self.myConfig = myConfig
        print "Done initializing bessXmlInvDyn"
        
        
    #os.path.relpath() is not working, so attempting to make a local one (below).
    def relpath(self, path, start):
        """Return a relative version of a path"""
        
        if not path:
            raise ValueError("no path specified")
        
        start_list = [x for x in os.path.abspath(start).split(os.path.sep) if x]
        path_list = [x for x in os.path.abspath(path).split(os.path.sep) if x]
        
        # Work out how much of the filepath is shared by start and path.
        i = len(os.path.commonprefix([start_list, path_list]))
        
        rel_list = [os.path.pardir] * (len(start_list)-i) + path_list[i:]
        if not rel_list:
            return os.path.curdir
        return os.path.join(*rel_list)    
        
        
        
    # http://stackoverflow.com/questions/3605680/creating-a-simple-xml-file-using-python
    def writeConfigFile(self):
        print "Writing Inverse Dynamics xml file"
        #<?xml version="1.0" encoding="UTF-8" ?>
        #<OpenSimDocument Version="30000">
        self.xmlTag_OpenSimDocument = ET.Element("OpenSimDocument", Version="30000")
        
        #   <InverseDynamicsTool name="3DGaitModel2392"> ----------> TODO: This is probably not the value we want here... Duplicate of the next value
        xmlTag_InvDynTool = ET.SubElement(self.xmlTag_OpenSimDocument, "InverseDynamicsTool", name=self.myConfig.common_modelFile)
        
        #       <!--Directory used for writing results.-->
        #       <results_directory>C:\OpenSim 3.3\Models\Gait2392_Simbody</results_directory>
        xmlTag_InvDynTool_resultsDirectory = ET.SubElement(xmlTag_InvDynTool, "results_directory")
        xmlTag_InvDynTool_resultsDirectory.text = self.myConfig.inverseDynTool_resultsDir
        
        #       <!--Directory for input files-->
        #       <input_directory />
        xmlTag_InvDynTool_inputDirectory = ET.SubElement(xmlTag_InvDynTool, "input_directory")
        xmlTag_InvDynTool_inputDirectory.text = self.myConfig.inverseDynTool_inputDir
        
        #       <!--Name of the .osim file used to construct a model.-->
        #       <model_file>Unassigned</model_file>
        xmlTag_InvDynTool_modelFile = ET.SubElement(xmlTag_InvDynTool, "model_file")
        xmlTag_InvDynTool_modelFile.text = self.myConfig.getPostScalingModelFilename()
        
        #       <!--Time range over which the inverse dynamics problem is solved.-->
        #       <time_range> -1.#INF 1.#INF</time_range>
        xmlTag_InvDynTool_timeRange = ET.SubElement(xmlTag_InvDynTool, "time_range")
        xmlTag_InvDynTool_timeRange.text = str(self.myConfig.common_timeRangeFrom) + ' ' + str(self.myConfig.common_timeRangeTo)
        
        #       <!--List of forces by individual or grouping name (e.g. All, actuators, muscles, ...) to be excluded when computing model dynamics.-->
        #       <forces_to_exclude> Muscles</forces_to_exclude>
        xmlTag_InvDynTool_forcesToExclude = ET.SubElement(xmlTag_InvDynTool, "forces_to_exclude")
        xmlTag_InvDynTool_forcesToExclude.text = self.myConfig.inverseDynTool_forcesToExclude
        
        #       <!--XML file (.xml) containing the external loads applied to the model as a set of ExternalForce(s).-->
        #       <external_loads_file>Unassigned</external_loads_file>
        xmlTag_InvDynTool_externalLoadsFile = ET.SubElement(xmlTag_InvDynTool, "external_loads_file")
        xmlTag_InvDynTool_externalLoadsFile.text = self.myConfig.common_externalLoadsSpecFile
        
        #       <!--The name of the file containing coordinate data. Can be a motion (.mot) or a states (.sto) file.-->
        #       <coordinates_file>Unassigned</coordinates_file>
        xmlTag_InvDynTool_coordFile = ET.SubElement(xmlTag_InvDynTool, "coordinates_file")
        if('true' == self.myConfig.useIkTool):
            cwd = self.myConfig.common_rootDir
            print '[debug XMLInvDyn] --------------------------> cwd: %s' % (cwd)
            path = self.myConfig.ikTool_outputMotionFile
            print '[debug XMLInvDyn] --------------------------> path: %s' % (path)
            relativePath = self.relpath(path, cwd)
            print '[debug XMLInvDyn] --------------------------> relativePath: %s' % (relativePath)
            #xmlTag_InvDynTool_coordFile.text = self.myConfig.ikTool_outputMotionFile
            #xmlTag_InvDynTool_coordFile.text = relativePath
            absPath = cwd + "\\" + path
            print '[debug XMLInvDyn] --------------------------> absPath: %s' % (absPath)
            xmlTag_InvDynTool_coordFile.text = absPath
        else:
            xmlTag_InvDynTool_coordFile.text = self.myConfig.common_desiredKinematicsFile
        print '[debug XMLInvDyn] --------------------------> InvDynTool_coordFile: %s' % (xmlTag_InvDynTool_coordFile.text)
        
        #       <!--Low-pass cut-off frequency for filtering the coordinates_file data (currently does not apply to states_file or speeds_file). A negative value results in no filtering. The default value is -1.0, so no filtering.-->
        #       <lowpass_cutoff_frequency_for_coordinates>-1</lowpass_cutoff_frequency_for_coordinates>
        xmlTag_InvDynTool_lowpassCutoffFreqForCoords = ET.SubElement(xmlTag_InvDynTool, "lowpass_cutoff_frequency_for_coordinates")
        xmlTag_InvDynTool_lowpassCutoffFreqForCoords.text = self.myConfig.common_filterKinematics
        
        #       <!--Name of the storage file (.sto) to which the generalized forces are written.-->
        #       <output_gen_force_file>inverse_dynamics.sto</output_gen_force_file>
        xmlTag_InvDynTool_outputGenForceFile = ET.SubElement(xmlTag_InvDynTool, "output_gen_force_file")
        xmlTag_InvDynTool_outputGenForceFile.text = self.myConfig.inverseDynTool_outputGenForceFile
        
        #       <!--List of joints (keyword All, for all joints) to report body forces acting at the joint frame expressed in ground.-->
        #       <joints_to_report_body_forces />
        xmlTag_InvDynTool_jointsToReportBodyForces = ET.SubElement(xmlTag_InvDynTool, "joints_to_report_body_forces")
        xmlTag_InvDynTool_jointsToReportBodyForces.text = self.myConfig.inverseDynTool_jointsToReportBodyForces
        
        #       <!--Name of the storage file (.sto) to which the body forces at specified joints are written.-->
        #       <output_body_forces_file>body_forces_at_joints.sto</output_body_forces_file>
        xmlTag_InvDynTool_outputBodyForcesFile = ET.SubElement(xmlTag_InvDynTool, "output_body_forces_file")
        xmlTag_InvDynTool_outputBodyForcesFile.text = self.myConfig.inverseDynTool_outputBodyForcesFile
        
        #   </InverseDynamicsTool>
        #</OpenSimDocument>
        
        
        # Fix the indentation
        self.indent(self.xmlTag_OpenSimDocument)
        tree = ET.ElementTree(self.xmlTag_OpenSimDocument)
        
        # Write out the xml file
        xmlFileName = self.myConfig.invDynamicsConfigFile
        tree.write(xmlFileName, encoding='utf-8')
        print "Wrote Inverse Dynamics xml file"
        
    def indent(self, elem, level=0):
        i = "\n" + level*"  "
        j = "\n" + (level-1)*"  "
        if len(elem):
            if not elem.text or not elem.text.strip():
                elem.text = i + "  "
            if not elem.tail or not elem.tail.strip():
                elem.tail = i
            for subelem in elem:
                self.indent(subelem, level+1)
            if not elem.tail or not elem.tail.strip():
                elem.tail = j
        else:
            if level and (not elem.tail or not elem.tail.strip()):
                elem.tail = j
        return elem        

