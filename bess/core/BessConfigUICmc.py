

################################################################################
#
# This file creates the Configuration UI components and handles user input as
# it relates to BESS configuration.
#
################################################################################
from javax.swing import JFrame, JPanel, JButton, JFileChooser, JTextField, JLabel, JCheckBox, SwingConstants
from javax.swing.border import TitledBorder
from java.awt import GridLayout, FlowLayout, Color, GridBagLayout, GridBagConstraints, CardLayout, BorderLayout

from xml.dom.minidom import parse, parseString, getDOMImplementation, Document
import xml.etree.cElementTree as ET

import os.path

from core.BessConfigUICardBase import bessCardBase
from core.BessUIFileChooser import bessFileChooser

'''
The bessConfigUICmcCard extends JPanel and is used as the CMC Card UI component

Attempted to subclass as explained here: http://www.jython.org/archive/21/docs/subclassing.html
Got errors when calling add() near the end of __init__
Decided to have this class act as a container
'''
class bessConfigUICmcCard(bessCardBase):
    def __init__(self, myConfig):
        self.myConfig = myConfig
        
        # Create the UI components
        self.cmcToolCard = JPanel(BorderLayout())
        
        self.cmcToolPanel = JPanel(GridBagLayout())
        self.cmcToolPanel_GBLC = GridBagConstraints()
        self.cmcToolPanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        # Use This Tool settings
        useThisToolGroup = JPanel(GridBagLayout())
        useThisToolGroup_GBLC = GridBagConstraints()
        useThisToolGroup_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        self.useThisToolGroup_useItCheckBox = JCheckBox("Perform CMC", True, actionPerformed=self.onUseItCheckBoxSelect)
        useThisToolGroup_loadSettingsButton = JButton("Load settings", actionPerformed = self.loadSettings)
        
        useThisToolGroup_GBLC.weightx = 0.25
        useThisToolGroup_GBLC.gridx = 0
        useThisToolGroup_GBLC.gridy = 0
        useThisToolGroup.add(self.useThisToolGroup_useItCheckBox, useThisToolGroup_GBLC)
        useThisToolGroup_GBLC.weightx = 0.60
        useThisToolGroup_GBLC.gridx = 1
        useThisToolGroup_GBLC.gridy = 0
        useThisToolGroup.add(JLabel(""), useThisToolGroup_GBLC)
        useThisToolGroup_GBLC.weightx = 0.15
        useThisToolGroup_GBLC.gridx = 2
        useThisToolGroup_GBLC.gridy = 0
        useThisToolGroup.add(useThisToolGroup_loadSettingsButton, useThisToolGroup_GBLC)
        
        # Time group
        timeGroup = JPanel(GridBagLayout())
        timeGroup_GBLC = GridBagConstraints()
        timeGroup_GBLC.fill = GridBagConstraints.HORIZONTAL
        timeGroup.setBorder(TitledBorder("Time"))
        
        ## CMC look-ahead window
        timeGroup_cmcLookAheadWindowLabel = JLabel("CMC look-ahead window")
        self.timeGroup_cmcLookAheadWindowTextField = JTextField("", focusLost=self.setLookAheadWindowEvent)
        
        # Finish Time group
        timeGroup_GBLC.weightx = 0.1
        timeGroup_GBLC.gridx = 0
        timeGroup_GBLC.gridy = 0
        timeGroup.add(JLabel(""), timeGroup_GBLC)
        timeGroup_GBLC.weightx = 0.40
        timeGroup_GBLC.gridx = 1
        timeGroup_GBLC.gridy = 0
        timeGroup.add(timeGroup_cmcLookAheadWindowLabel, timeGroup_GBLC)
        timeGroup_GBLC.weightx = 0.40
        timeGroup_GBLC.gridx = 2
        timeGroup_GBLC.gridy = 0
        timeGroup.add(self.timeGroup_cmcLookAheadWindowTextField, timeGroup_GBLC)
        timeGroup_GBLC.weightx = 0.1
        timeGroup_GBLC.gridx = 3
        timeGroup_GBLC.gridy = 0
        timeGroup.add(JLabel(""), timeGroup_GBLC)
        
        # Output group
        outputGroup = JPanel(GridBagLayout())
        outputGroup_GBLC = GridBagConstraints()
        outputGroup_GBLC.fill = GridBagConstraints.HORIZONTAL
        outputGroup.setBorder(TitledBorder("Output"))
        
        ## Prefix (text field)
        outputGroup_prefixLabel = JLabel("Prefix")
        self.outputGroup_prefixTextField = JTextField("", focusLost=self.setPrefixEvent)
        
        ## Directory (text field, folder chooser)
        outputGroup_directoryLabel = JLabel("Directory")
        self.outputGroup_directoryTextField = JTextField("", focusLost=self.setDirectoryEvent)
        outputGroup_directoryFileChooserButton = JButton("...", actionPerformed = self.selectDirectoryDir)
        
        outputGroup_directoryPanel = JPanel(GridBagLayout())
        outputGroup_directoryPanel_GBLC = GridBagConstraints()
        outputGroup_directoryPanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        outputGroup_directoryPanel_GBLC.weightx = 0.99
        outputGroup_directoryPanel_GBLC.gridx = 0
        outputGroup_directoryPanel_GBLC.gridy = 0
        outputGroup_directoryPanel.add(self.outputGroup_directoryTextField, outputGroup_directoryPanel_GBLC)
        outputGroup_directoryPanel_GBLC.weightx = 0.01
        outputGroup_directoryPanel_GBLC.gridx = 1
        outputGroup_directoryPanel_GBLC.gridy = 0
        outputGroup_directoryPanel.add(outputGroup_directoryFileChooserButton, outputGroup_directoryPanel_GBLC)
        
        ## Precision (text field)
        outputGroup_precisionLabel = JLabel("Precision ")
        self.outputGroup_precisionTextField = JTextField("", focusLost=self.setPrecisionEvent)
        
        # Finish Output group
        outputGroup_GBLC.weightx = 0.1
        outputGroup_GBLC.gridx = 0
        outputGroup_GBLC.gridy = 0
        outputGroup.add(JLabel(""), outputGroup_GBLC)
        outputGroup_GBLC.weightx = 0.40
        outputGroup_GBLC.gridx = 1
        outputGroup_GBLC.gridy = 0
        outputGroup.add(outputGroup_prefixLabel, outputGroup_GBLC)
        outputGroup_GBLC.weightx = 0.40
        outputGroup_GBLC.gridx = 2
        outputGroup_GBLC.gridy = 0
        outputGroup.add(self.outputGroup_prefixTextField, outputGroup_GBLC)
        outputGroup_GBLC.weightx = 0.1
        outputGroup_GBLC.gridx = 3
        outputGroup_GBLC.gridy = 0
        outputGroup.add(JLabel(""), outputGroup_GBLC)
        
        outputGroup_GBLC.weightx = 0.1
        outputGroup_GBLC.gridx = 0
        outputGroup_GBLC.gridy = 1
        outputGroup.add(JLabel(""), outputGroup_GBLC)
        outputGroup_GBLC.weightx = 0.40
        outputGroup_GBLC.gridx = 1
        outputGroup_GBLC.gridy = 1
        outputGroup.add(outputGroup_directoryLabel, outputGroup_GBLC)
        outputGroup_GBLC.weightx = 0.40
        outputGroup_GBLC.gridx = 2
        outputGroup_GBLC.gridy = 1
        outputGroup.add(outputGroup_directoryPanel, outputGroup_GBLC)
        outputGroup_GBLC.weightx = 0.1
        outputGroup_GBLC.gridx = 3
        outputGroup_GBLC.gridy = 1
        outputGroup.add(JLabel(""), outputGroup_GBLC)
        
        outputGroup_GBLC.weightx = 0.1
        outputGroup_GBLC.gridx = 0
        outputGroup_GBLC.gridy = 2
        outputGroup.add(JLabel(""), outputGroup_GBLC)
        outputGroup_GBLC.weightx = 0.40
        outputGroup_GBLC.gridx = 1
        outputGroup_GBLC.gridy = 2
        outputGroup.add(outputGroup_precisionLabel, outputGroup_GBLC)
        outputGroup_GBLC.weightx = 0.40
        outputGroup_GBLC.gridx = 2
        outputGroup_GBLC.gridy = 2
        outputGroup.add(self.outputGroup_precisionTextField, outputGroup_GBLC)
        outputGroup_GBLC.weightx = 0.1
        outputGroup_GBLC.gridx = 3
        outputGroup_GBLC.gridy = 2
        outputGroup.add(JLabel(""), outputGroup_GBLC)
        
        # Finish card
        self.cmcToolPanel_GBLC.weightx = 0.1
        self.cmcToolPanel_GBLC.gridx = 0
        self.cmcToolPanel_GBLC.gridy = 0
        self.cmcToolPanel.add(JLabel(""), self.cmcToolPanel_GBLC)
        self.cmcToolPanel_GBLC.weightx = 0.80
        self.cmcToolPanel_GBLC.gridx = 1
        self.cmcToolPanel_GBLC.gridy = 0
        self.cmcToolPanel.add(timeGroup, self.cmcToolPanel_GBLC)
        self.cmcToolPanel_GBLC.weightx = 0.1
        self.cmcToolPanel_GBLC.gridx = 2
        self.cmcToolPanel_GBLC.gridy = 0
        self.cmcToolPanel.add(JLabel(""), self.cmcToolPanel_GBLC)
        
        self.cmcToolPanel_GBLC.weightx = 0.1
        self.cmcToolPanel_GBLC.gridx = 0
        self.cmcToolPanel_GBLC.gridy = 1
        self.cmcToolPanel.add(JLabel(""), self.cmcToolPanel_GBLC)
        self.cmcToolPanel_GBLC.weightx = 0.80
        self.cmcToolPanel_GBLC.gridx = 1
        self.cmcToolPanel_GBLC.gridy = 1
        self.cmcToolPanel.add(outputGroup, self.cmcToolPanel_GBLC)
        self.cmcToolPanel_GBLC.weightx = 0.1
        self.cmcToolPanel_GBLC.gridx = 2
        self.cmcToolPanel_GBLC.gridy = 1
        self.cmcToolPanel.add(JLabel(""), self.cmcToolPanel_GBLC)
        
        self.cmcToolCard.add(useThisToolGroup, BorderLayout.NORTH)
        self.cmcToolCard.add(self.cmcToolPanel, BorderLayout.CENTER)
        
    def getCard(self):
        return self.cmcToolCard
        
    def onUseItCheckBoxSelect(self, event):
        #print 'DEBUG: onUseItCheckBoxSelect(EVENT) called'
        self.updateUseItCheckBox()
        
    def updateUseItCheckBox(self):
        #print 'DEBUG: updateUseItCheckBox() called'
        if (self.useThisToolGroup_useItCheckBox.isSelected()):
            #print 'DEBUG: Setting useCmcTool to true'
            self.myConfig.useCmcTool = 'true'
            #print 'DEBUG: done'
        else:
            #print 'DEBUG: Setting useCmcTool to false'
            self.myConfig.useCmcTool = 'false'
            #print 'DEBUG: done'
    
    #################################################
    # Save the value in the UI to the config object #
    #################################################
    
    def setDirectory(self):
        self.myConfig.cmcTool_outputDirectory = self.outputGroup_directoryTextField.text
        
    def setPrecision(self):
        self.myConfig.cmcTool_outputPrecision = self.outputGroup_precisionTextField.text
        
    def setPrefix(self):
        self.myConfig.cmcTool_outputPrefix = self.outputGroup_prefixTextField.text
        
    def setLookAheadWindow(self):
        self.myConfig.cmcTool_cmcTimeWindow = self.timeGroup_cmcLookAheadWindowTextField.text
        
    ###############################
    # Save contents of a text box #
    ###############################
    def setPrecisionEvent(self, event):
        self.setPrecision()
        
    def setDirectoryEvent(self, event):
        self.setDirectory()
        
    def setPrefixEvent(self, event):
        self.setPrefix()
        
    def setLookAheadWindowEvent(self, event):
        self.setLookAheadWindow()
    
    ###########################################################
    # Show file chooser dialog when a "..." button is pressed #
    ###########################################################
    
    def selectDirectoryDir(self, event):
        # Create and show a file chooser (True = select file, False = select directory)
        myChooser = bessFileChooser(False)
        # Set the text field text to the results of the file chooser
        self.outputGroup_directoryTextField.text = myChooser.fullPath.toString()
        # Save the value to the config class
        self.setDirectory()
        
    # Save/load from config
    # https://docs.python.org/2/library/xml.etree.elementtree.html
    def loadSettings(self, event):
        # Create and show a file chooser (True = select file, False = select directory)
        myChooser = bessFileChooser(True)
        self.loadSettingsFromFile(myChooser.fullPath.toString())
        
    def loadSettingsFromFile(self, filePath):
        # Check if "use it" is checked
        if(self.myConfig.useCmcTool == 'true'):
            self.useThisToolGroup_useItCheckBox.setSelected(True)
        else:
            self.useThisToolGroup_useItCheckBox.setSelected(False)
            
        # Open the xml file
        if(os.path.exists(filePath)):
            print 'Opening config file: %s' % (filePath)
            tree = ET.parse(filePath)
            
            # Parse the tree
            root = tree.getroot()
            self.parseTree(root)
            
            # Save values to BessConfig
            self.updateUseItCheckBox()
            self.setPrecision()
            self.setDirectory()
            self.setPrefix()
            self.setLookAheadWindow()
            
            print 'Done loading settings'
        else:
            print 'Could not find file: %s' % (filePath)
        
    def parseTree(self, node):
        print 'Node tag: %s,  text: %s' % (node.tag, node.text)
        
        if('results_directory' == node.tag):
            self.outputGroup_directoryTextField.text = node.text
        elif('output_precision' == node.tag):
            self.outputGroup_precisionTextField.text = node.text
        #elif('' == node.tag):
        #    self.outputGroup_prefixTextField.text = node.text
        elif('cmc_time_window' == node.tag):
            self.timeGroup_cmcLookAheadWindowTextField.text = node.text
        
        # Look at any children of children
        for child in node:
            self.parseTree(child)
        
# end bessConfigUICmcCard class

