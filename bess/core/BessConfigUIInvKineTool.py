

################################################################################
#
# This file creates the Configuration UI components and handles user input as
# it relates to BESS configuration.
#
################################################################################
from javax.swing import JFrame, JPanel, JButton, JFileChooser, JTextField, JLabel, JCheckBox, SwingConstants, JTabbedPane, JScrollPane, JComboBox, JTable
from javax.swing.border import TitledBorder
from java.awt import GridLayout, FlowLayout, Color, GridBagLayout, GridBagConstraints, CardLayout, BorderLayout, Dimension

from xml.dom.minidom import parse, parseString, getDOMImplementation, Document
import xml.etree.cElementTree as ET

import os.path

from core.BessConfigUICardBase import bessCardBase
from core.BessUIFileChooser import bessFileChooser

'''
The bessConfigUIMultiRunCard is used as the Multi-run simulation Card UI component
It is used to allow the user to specify multiple runs on the simulation and
variances between the runs
'''
class bessConfigUIInvKineToolCard(bessCardBase):
    def __init__(self, myConfig):
        self.myConfig = myConfig
        
        # Variables for dynamic UI components
        self.markerDataEnabledCheckBoxes = []
        self.markerDataNameTextFields    = []
        self.markerDataValueComboBoxes   = []
        self.markerDataValueTextFields   = []
        self.markerDataWeightTextFields  = []
        
        self.coordDataEnabledCheckBoxes = []
        self.coordDataNameTextFields    = []
        self.coordDataValueComboBoxes   = []
        self.coordDataValueTextFields   = []
        self.coordDataWeightTextFields  = []
        
        self.multiRunMarkerDataEntryList = []
        self.multiRunCoordDataEntryList = []
        self.multiRunMarkerDataEntryListIndex = 0
        self.multiRunCoordDataEntryListIndex = 0
        
        self.ACTIVE_LOCATION_MARKER_DATA = 1
        self.ACTIVE_LOCATION_COORD_DATA = 2
        
        # Create the UI components
        self.invKineToolCard = JPanel(BorderLayout())
        
        self.invKineToolPanel = JPanel(GridBagLayout())
        self.invKineToolPanel_GBLC = GridBagConstraints()
        self.invKineToolPanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        self.invKineTabs = JTabbedPane(SwingConstants.TOP, JTabbedPane.SCROLL_TAB_LAYOUT)
        
        # Use This Tool settings
        useThisToolGroup = JPanel(GridBagLayout())
        useThisToolGroup_GBLC = GridBagConstraints()
        useThisToolGroup_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        self.useThisToolGroup_useItCheckBox = JCheckBox("Perform Inverse Kinematics", True, actionPerformed=self.onUseItCheckBoxSelect)
        useThisToolGroup_loadSettingsButton = JButton("Load settings", actionPerformed = self.loadSettings)
        
        useThisToolGroup_GBLC.weightx = 0.25
        useThisToolGroup_GBLC.gridx = 0
        useThisToolGroup_GBLC.gridy = 0
        useThisToolGroup.add(self.useThisToolGroup_useItCheckBox, useThisToolGroup_GBLC)
        useThisToolGroup_GBLC.weightx = 0.60
        useThisToolGroup_GBLC.gridx = 1
        useThisToolGroup_GBLC.gridy = 0
        useThisToolGroup.add(JLabel(""), useThisToolGroup_GBLC)
        useThisToolGroup_GBLC.weightx = 0.15
        useThisToolGroup_GBLC.gridx = 2
        useThisToolGroup_GBLC.gridy = 0
        useThisToolGroup.add(useThisToolGroup_loadSettingsButton, useThisToolGroup_GBLC)
        
        # Settings tab
        settingsTab = JPanel(GridBagLayout())
        settingsTab_GBLC = GridBagConstraints()
        settingsTab_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        ## IK Trial Group
        settingsTab_IKTrialGroup = JPanel(GridBagLayout())
        settingsTab_IKTrialGroup_GBLC = GridBagConstraints()
        settingsTab_IKTrialGroup_GBLC.fill = GridBagConstraints.HORIZONTAL
        settingsTab_IKTrialGroup.setBorder(TitledBorder("IK Trial"))
        
        ### Marker data for trial (text field, button)
        settingsTab_IKTrialGroup_markerDataFileLabel = JLabel("Marker data file path ", SwingConstants.RIGHT)
        self.settingsTab_IKTrialGroup_markerDataFileTextField = JTextField("", focusLost=self.setMarkerDataFileEvent)
        settingsTab_IKTrialGroup_markerDataFileFileChooserButton = JButton("...", actionPerformed = self.selectMarkerDataFile)
        
        settingsTab_IKTrialGroup_markerDataFilePanel = JPanel(GridBagLayout())
        settingsTab_IKTrialGroup_markerDataFilePanel_GBLC = GridBagConstraints()
        settingsTab_IKTrialGroup_markerDataFilePanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        settingsTab_IKTrialGroup_markerDataFilePanel_GBLC.weightx = 0.99
        settingsTab_IKTrialGroup_markerDataFilePanel_GBLC.gridx = 0
        settingsTab_IKTrialGroup_markerDataFilePanel_GBLC.gridy = 0
        settingsTab_IKTrialGroup_markerDataFilePanel.add(self.settingsTab_IKTrialGroup_markerDataFileTextField, settingsTab_IKTrialGroup_markerDataFilePanel_GBLC)
        settingsTab_IKTrialGroup_markerDataFilePanel_GBLC.weightx = 0.01
        settingsTab_IKTrialGroup_markerDataFilePanel_GBLC.gridx = 1
        settingsTab_IKTrialGroup_markerDataFilePanel_GBLC.gridy = 0
        settingsTab_IKTrialGroup_markerDataFilePanel.add(settingsTab_IKTrialGroup_markerDataFileFileChooserButton, settingsTab_IKTrialGroup_markerDataFilePanel_GBLC)
        
        ### Coordinate data for the trial (check box, text field, button)
        self.settingsTab_IKTrialGroup_coordDataFileCheckBox = JCheckBox("Trial coordinate data file path ", actionPerformed=self.onCoordDataFileCheckBoxSelect)
        self.settingsTab_IKTrialGroup_coordDataFileTextField = JTextField("", focusLost=self.setCoordDataFileEvent)
        settingsTab_IKTrialGroup_coordDataFileFileChooserButton = JButton("...", actionPerformed = self.selectCoordDataFile)
        
        settingsTab_IKTrialGroup_coordDataFilePanel = JPanel(GridBagLayout())
        settingsTab_IKTrialGroup_coordDataFilePanel_GBLC = GridBagConstraints()
        settingsTab_IKTrialGroup_coordDataFilePanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        settingsTab_IKTrialGroup_coordDataFilePanel_GBLC.weightx = 0.99
        settingsTab_IKTrialGroup_coordDataFilePanel_GBLC.gridx = 0
        settingsTab_IKTrialGroup_coordDataFilePanel_GBLC.gridy = 0
        settingsTab_IKTrialGroup_coordDataFilePanel.add(self.settingsTab_IKTrialGroup_coordDataFileTextField, settingsTab_IKTrialGroup_coordDataFilePanel_GBLC)
        settingsTab_IKTrialGroup_coordDataFilePanel_GBLC.weightx = 0.01
        settingsTab_IKTrialGroup_coordDataFilePanel_GBLC.gridx = 1
        settingsTab_IKTrialGroup_coordDataFilePanel_GBLC.gridy = 0
        settingsTab_IKTrialGroup_coordDataFilePanel.add(settingsTab_IKTrialGroup_coordDataFileFileChooserButton, settingsTab_IKTrialGroup_coordDataFilePanel_GBLC)
        
        ## Finish IK Trial Group
        settingsTab_IKTrialGroup_GBLC.weightx = 0.1
        settingsTab_IKTrialGroup_GBLC.gridx = 0
        settingsTab_IKTrialGroup_GBLC.gridy = 0
        settingsTab_IKTrialGroup.add(JLabel(""), settingsTab_IKTrialGroup_GBLC)
        settingsTab_IKTrialGroup_GBLC.weightx = 0.40
        settingsTab_IKTrialGroup_GBLC.gridx = 1
        settingsTab_IKTrialGroup_GBLC.gridy = 0
        settingsTab_IKTrialGroup.add(settingsTab_IKTrialGroup_markerDataFileLabel, settingsTab_IKTrialGroup_GBLC)
        settingsTab_IKTrialGroup_GBLC.weightx = 0.4
        settingsTab_IKTrialGroup_GBLC.gridx = 2
        settingsTab_IKTrialGroup_GBLC.gridy = 0
        settingsTab_IKTrialGroup.add(settingsTab_IKTrialGroup_markerDataFilePanel, settingsTab_IKTrialGroup_GBLC)
        settingsTab_IKTrialGroup_GBLC.weightx = 0.1
        settingsTab_IKTrialGroup_GBLC.gridx = 3
        settingsTab_IKTrialGroup_GBLC.gridy = 0
        settingsTab_IKTrialGroup.add(JLabel(""), settingsTab_IKTrialGroup_GBLC)
        
        settingsTab_IKTrialGroup_GBLC.weightx = 0.1
        settingsTab_IKTrialGroup_GBLC.gridx = 0
        settingsTab_IKTrialGroup_GBLC.gridy = 1
        settingsTab_IKTrialGroup.add(JLabel(""), settingsTab_IKTrialGroup_GBLC)
        settingsTab_IKTrialGroup_GBLC.weightx = 0.40
        settingsTab_IKTrialGroup_GBLC.gridx = 1
        settingsTab_IKTrialGroup_GBLC.gridy = 1
        settingsTab_IKTrialGroup.add(self.settingsTab_IKTrialGroup_coordDataFileCheckBox, settingsTab_IKTrialGroup_GBLC)
        settingsTab_IKTrialGroup_GBLC.weightx = 0.4
        settingsTab_IKTrialGroup_GBLC.gridx = 2
        settingsTab_IKTrialGroup_GBLC.gridy = 1
        settingsTab_IKTrialGroup.add(settingsTab_IKTrialGroup_coordDataFilePanel, settingsTab_IKTrialGroup_GBLC)
        settingsTab_IKTrialGroup_GBLC.weightx = 0.1
        settingsTab_IKTrialGroup_GBLC.gridx = 3
        settingsTab_IKTrialGroup_GBLC.gridy = 1
        settingsTab_IKTrialGroup.add(JLabel(""), settingsTab_IKTrialGroup_GBLC)
        
        ## Output Group
        settingsTab_OutputGroup = JPanel(GridBagLayout())
        settingsTab_OutputGroup_GBLC = GridBagConstraints()
        settingsTab_OutputGroup_GBLC.fill = GridBagConstraints.HORIZONTAL
        settingsTab_OutputGroup.setBorder(TitledBorder("Output"))
        
        ### Motion file (text field, button)
        settingsTab_OutputGroup_motionFileLabel = JLabel("Motion file path ", SwingConstants.RIGHT)
        self.settingsTab_OutputGroup_motionFileTextField = JTextField("", focusLost=self.setMotionFileEvent)
        settingsTab_OutputGroup_motionFileFileChooserButton = JButton("...", actionPerformed = self.selectMotionFile)
        
        settingsTab_OutputGroup_motionFilePanel = JPanel(GridBagLayout())
        settingsTab_OutputGroup_motionFilePanel_GBLC = GridBagConstraints()
        settingsTab_OutputGroup_motionFilePanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        settingsTab_OutputGroup_motionFilePanel_GBLC.weightx = 0.99
        settingsTab_OutputGroup_motionFilePanel_GBLC.gridx = 0
        settingsTab_OutputGroup_motionFilePanel_GBLC.gridy = 0
        settingsTab_OutputGroup_motionFilePanel.add(self.settingsTab_OutputGroup_motionFileTextField, settingsTab_OutputGroup_motionFilePanel_GBLC)
        settingsTab_OutputGroup_motionFilePanel_GBLC.weightx = 0.01
        settingsTab_OutputGroup_motionFilePanel_GBLC.gridx = 1
        settingsTab_OutputGroup_motionFilePanel_GBLC.gridy = 0
        settingsTab_OutputGroup_motionFilePanel.add(settingsTab_OutputGroup_motionFileFileChooserButton, settingsTab_OutputGroup_motionFilePanel_GBLC)
        
        ## Finish Output Group
        settingsTab_OutputGroup_GBLC.weightx = 0.1
        settingsTab_OutputGroup_GBLC.gridx = 0
        settingsTab_OutputGroup_GBLC.gridy = 0
        settingsTab_OutputGroup.add(JLabel(""), settingsTab_OutputGroup_GBLC)
        settingsTab_OutputGroup_GBLC.weightx = 0.40
        settingsTab_OutputGroup_GBLC.gridx = 1
        settingsTab_OutputGroup_GBLC.gridy = 0
        settingsTab_OutputGroup.add(settingsTab_OutputGroup_motionFileLabel, settingsTab_OutputGroup_GBLC)
        settingsTab_OutputGroup_GBLC.weightx = 0.4
        settingsTab_OutputGroup_GBLC.gridx = 2
        settingsTab_OutputGroup_GBLC.gridy = 0
        settingsTab_OutputGroup.add(settingsTab_OutputGroup_motionFilePanel, settingsTab_OutputGroup_GBLC)
        settingsTab_OutputGroup_GBLC.weightx = 0.1
        settingsTab_OutputGroup_GBLC.gridx = 3
        settingsTab_OutputGroup_GBLC.gridy = 0
        settingsTab_OutputGroup.add(JLabel(""), settingsTab_OutputGroup_GBLC)
        
        # Finish Settings tab
        settingsTab_GBLC.weightx = 0.1
        settingsTab_GBLC.gridx = 0
        settingsTab_GBLC.gridy = 0
        settingsTab.add(JLabel(""), settingsTab_GBLC)
        settingsTab_GBLC.weightx = 0.80
        settingsTab_GBLC.gridx = 1
        settingsTab_GBLC.gridy = 0
        settingsTab.add(settingsTab_IKTrialGroup, settingsTab_GBLC)
        settingsTab_GBLC.weightx = 0.1
        settingsTab_GBLC.gridx = 3
        settingsTab_GBLC.gridy = 0
        settingsTab.add(JLabel(""), settingsTab_GBLC)
        
        settingsTab_GBLC.weightx = 0.1
        settingsTab_GBLC.gridx = 0
        settingsTab_GBLC.gridy = 1
        settingsTab.add(JLabel(""), settingsTab_GBLC)
        settingsTab_GBLC.weightx = 0.80
        settingsTab_GBLC.gridx = 1
        settingsTab_GBLC.gridy = 1
        settingsTab.add(settingsTab_OutputGroup, settingsTab_GBLC)
        settingsTab_GBLC.weightx = 0.1
        settingsTab_GBLC.gridx = 2
        settingsTab_GBLC.gridy = 1
        settingsTab.add(JLabel(""), settingsTab_GBLC)
        
        self.invKineTabs.addTab("Settings", settingsTab)
        
        
        # Weight tab (~~~~~ dynamic ~~~~~)
        # TODO: make static for now, change to dynamic when adding logic/ability to load marker data
        self.weightTab = JPanel(GridBagLayout())
        weightTab_GBLC = GridBagConstraints()
        #weightTab_GBLC.fill = GridBagConstraints.HORIZONTAL
        weightTab_GBLC.fill = GridBagConstraints.BOTH
        
        ## For each marker name within the Marker data...
        self.weightTab_markerDataGroup = JPanel(GridBagLayout())
        self.weightTab_markerDataGroup_GBLC = GridBagConstraints()
        self.weightTab_markerDataGroup_GBLC.fill = GridBagConstraints.HORIZONTAL
        self.weightTab_markerDataGroup.setBorder(TitledBorder("Marker Data"))
        
        ## For each coordinate name within the Coordinate data...
        self.weightTab_coordDataGroup = JPanel(GridBagLayout())
        self.weightTab_coordDataGroup_GBLC = GridBagConstraints()
        self.weightTab_coordDataGroup_GBLC.fill = GridBagConstraints.HORIZONTAL
        self.weightTab_coordDataGroup.setBorder(TitledBorder("Coordinate Data"))
        
        self.addMultiRunEntry(self.ACTIVE_LOCATION_MARKER_DATA)
        self.addMultiRunEntry(self.ACTIVE_LOCATION_COORD_DATA)

        # Finish Weight tab
        self.weightTab_markerDataScrollPane = JScrollPane(self.weightTab_markerDataGroup, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED)
        self.weightTab_coordDataScrollPane = JScrollPane(self.weightTab_coordDataGroup, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED)
        
        weightTab_GBLC.weightx = 1.0
        weightTab_GBLC.weighty = 0.5
        weightTab_GBLC.gridx = 0
        weightTab_GBLC.gridy = 0
        self.weightTab.add(self.weightTab_markerDataScrollPane, weightTab_GBLC)
        
        weightTab_GBLC.weightx = 1.0
        weightTab_GBLC.weighty = 0.5
        weightTab_GBLC.gridx = 0
        weightTab_GBLC.gridy = 1
        self.weightTab.add(self.weightTab_coordDataScrollPane, weightTab_GBLC)
        
        self.invKineTabs.addTab("Weights", self.weightTab)
        
        self.invKineToolPanel_GBLC.weightx = 0.1
        self.invKineToolPanel_GBLC.weighty = 1.0
        self.invKineToolPanel_GBLC.gridx = 0
        self.invKineToolPanel_GBLC.gridy = 0
        self.invKineToolPanel.add(JLabel(""), self.invKineToolPanel_GBLC)
        self.invKineToolPanel_GBLC.weightx = 0.80
        self.invKineToolPanel_GBLC.weighty = 1.0
        self.invKineToolPanel_GBLC.gridx = 1
        self.invKineToolPanel_GBLC.gridy = 0
        self.invKineToolPanel.add(self.invKineTabs, self.invKineToolPanel_GBLC)
        self.invKineToolPanel_GBLC.weightx = 0.1
        self.invKineToolPanel_GBLC.weighty = 1.0
        self.invKineToolPanel_GBLC.gridx = 2
        self.invKineToolPanel_GBLC.gridy = 0
        self.invKineToolPanel.add(JLabel(""), self.invKineToolPanel_GBLC)
        
        #useThisToolGroup.setMaximumSize(Dimension(200,200))
        #self.invKineToolPanel.setMinimumSize(Dimension(1200,1200))
        self.invKineToolCard.add(useThisToolGroup, BorderLayout.NORTH)
        self.invKineToolCard.add(self.invKineToolPanel, BorderLayout.CENTER)
        
    # Start Multi-run stuff #################################################################################################################################################
    
    # Calls createMultiRunEntry() and places the resulting JPanel into the UI
    def addMultiRunEntry(self, activeLocation):
        #print "-- Creating new multi-run entry"
        multiRunEntry = self.createMultiRunEntry(activeLocation)
        
        #print "-- Determining active location"
        if(activeLocation == self.ACTIVE_LOCATION_MARKER_DATA):
            self.multiRunMarkerDataEntryList.append(multiRunEntry)
            #self.multiRunEntryList.append(multiRunEntry)
            
            #print "-- Creating UI elements (Marker Data)"
            self.weightTab_markerDataGroup_GBLC.weightx = 0.1
            self.weightTab_markerDataGroup_GBLC.gridx = 0
            self.weightTab_markerDataGroup_GBLC.gridy = self.multiRunMarkerDataEntryListIndex
            self.weightTab_markerDataGroup.add(JLabel(""), self.weightTab_markerDataGroup_GBLC)
            self.weightTab_markerDataGroup_GBLC.weightx = 0.8
            self.weightTab_markerDataGroup_GBLC.gridx = 1
            self.weightTab_markerDataGroup_GBLC.gridy = self.multiRunMarkerDataEntryListIndex
            self.weightTab_markerDataGroup.add(self.multiRunMarkerDataEntryList[self.multiRunMarkerDataEntryListIndex], self.weightTab_markerDataGroup_GBLC)
            self.weightTab_markerDataGroup_GBLC.weightx = 0.1
            self.weightTab_markerDataGroup_GBLC.gridx = 2
            self.weightTab_markerDataGroup_GBLC.gridy = self.multiRunMarkerDataEntryListIndex
            self.weightTab_markerDataGroup.add(JLabel(""), self.weightTab_markerDataGroup_GBLC)
            self.multiRunMarkerDataEntryListIndex = self.multiRunMarkerDataEntryListIndex + 1
            
            #print "-- Repainting"
            self.weightTab_markerDataGroup.revalidate()
            self.weightTab_markerDataGroup.repaint()
        elif(activeLocation == self.ACTIVE_LOCATION_COORD_DATA):
            self.multiRunCoordDataEntryList.append(multiRunEntry)
            
            #print "-- Creating UI elements (Coordinate Data)"
            self.weightTab_coordDataGroup_GBLC.weightx = 0.1
            self.weightTab_coordDataGroup_GBLC.gridx = 0
            self.weightTab_coordDataGroup_GBLC.gridy = self.multiRunCoordDataEntryListIndex
            self.weightTab_coordDataGroup.add(JLabel(""), self.weightTab_coordDataGroup_GBLC)
            self.weightTab_coordDataGroup_GBLC.weightx = 0.8
            self.weightTab_coordDataGroup_GBLC.gridx = 1
            self.weightTab_coordDataGroup_GBLC.gridy = self.multiRunCoordDataEntryListIndex
            self.weightTab_coordDataGroup.add(self.multiRunCoordDataEntryList[self.multiRunCoordDataEntryListIndex], self.weightTab_coordDataGroup_GBLC)
            self.weightTab_coordDataGroup_GBLC.weightx = 0.1
            self.weightTab_coordDataGroup_GBLC.gridx = 2
            self.weightTab_coordDataGroup_GBLC.gridy = self.multiRunCoordDataEntryListIndex
            self.weightTab_coordDataGroup.add(JLabel(""), self.weightTab_coordDataGroup_GBLC)
            self.multiRunCoordDataEntryListIndex = self.multiRunCoordDataEntryListIndex + 1
            
            #print "-- Repainting"
            self.weightTab_coordDataGroup.revalidate()
            self.weightTab_coordDataGroup.repaint()
        
        #self.weightTab_markerDataScrollPane.revalidate()
        #self.weightTab_markerDataScrollPane.repaint()
        #self.weightTab_coordDataScrollPane.revalidate()
        #self.weightTab_coordDataScrollPane.repaint()
        self.weightTab.revalidate()
        self.weightTab.repaint()
        
        #print "-- Done"
        
    # Creates the JPanel containing all the multi-run entry ui elements
    def createMultiRunEntry(self, activeLocation):
        entryPanel = JPanel(GridBagLayout())
        entryPanel_GBLC = GridBagConstraints()
        entryPanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        ### Enabled (Check box)
        entryPanel_enabledCheckBox = JCheckBox(actionPerformed=self.checkForEmptyFieldsMarkerData)
        
        ### Name (text field)
        if(activeLocation == self.ACTIVE_LOCATION_MARKER_DATA):
            entryPanel_nameTextField = JTextField("", focusLost=self.checkForEmptyFieldsMarkerData)
        elif(activeLocation == self.ACTIVE_LOCATION_COORD_DATA):
            entryPanel_nameTextField = JTextField("", focusLost=self.checkForEmptyFieldsCoordData)
        
        ### Value (combo box - From File, default value, manual value)
        self.entryPanel_valueComboBoxOptions = ["From file", "Default value", "Manual value"]
        entryPanel_valueComboBox = JComboBox(self.entryPanel_valueComboBoxOptions)
        if(activeLocation == self.ACTIVE_LOCATION_MARKER_DATA):
            entryPanel_valueTextField = JTextField("", focusLost=self.checkForEmptyFieldsMarkerData)
        elif(activeLocation == self.ACTIVE_LOCATION_COORD_DATA):
            entryPanel_valueTextField = JTextField("", focusLost=self.checkForEmptyFieldsCoordData)
        
        ### Weight (text field)
        if(activeLocation == self.ACTIVE_LOCATION_MARKER_DATA):
            entryPanel_weightTextField = JTextField("", focusLost=self.checkForEmptyFieldsMarkerData)
        elif(activeLocation == self.ACTIVE_LOCATION_COORD_DATA):
            entryPanel_weightTextField = JTextField("", focusLost=self.checkForEmptyFieldsCoordData)
            
        
        # Add UI components to the arrays
        if(activeLocation == self.ACTIVE_LOCATION_MARKER_DATA):
            self.markerDataEnabledCheckBoxes.append(entryPanel_enabledCheckBox)
            self.markerDataNameTextFields.append(entryPanel_nameTextField)
            self.markerDataValueComboBoxes.append(entryPanel_valueComboBox)
            self.markerDataValueTextFields.append(entryPanel_valueTextField)
            self.markerDataWeightTextFields.append(entryPanel_weightTextField)
        elif(activeLocation == self.ACTIVE_LOCATION_COORD_DATA):
            self.coordDataEnabledCheckBoxes.append(entryPanel_enabledCheckBox)
            self.coordDataNameTextFields.append(entryPanel_nameTextField)
            self.coordDataValueComboBoxes.append(entryPanel_valueComboBox)
            self.coordDataValueTextFields.append(entryPanel_valueTextField)
            self.coordDataWeightTextFields.append(entryPanel_weightTextField)
        
        # Finish creating Entry Panel
        entryPanel_GBLC.weightx = 0.10
        entryPanel_GBLC.gridx = 0
        entryPanel_GBLC.gridy = 0
        entryPanel.add(JLabel("Enabled"), entryPanel_GBLC)
        entryPanel_GBLC.weightx = 0.10
        entryPanel_GBLC.gridx = 1
        entryPanel_GBLC.gridy = 0
        entryPanel.add(JLabel("Name"), entryPanel_GBLC)
        entryPanel_GBLC.weightx = 0.10
        entryPanel_GBLC.gridx = 2
        entryPanel_GBLC.gridy = 0
        entryPanel.add(JLabel("Value source"), entryPanel_GBLC)
        entryPanel_GBLC.weightx = 0.10
        entryPanel_GBLC.gridx = 3
        entryPanel_GBLC.gridy = 0
        entryPanel.add(JLabel("Value"), entryPanel_GBLC)
        entryPanel_GBLC.weightx = 0.10
        entryPanel_GBLC.gridx = 4
        entryPanel_GBLC.gridy = 0
        entryPanel.add(JLabel("Weight"), entryPanel_GBLC)
        
        entryPanel_GBLC.weightx = 0.10
        entryPanel_GBLC.gridx = 0
        entryPanel_GBLC.gridy = 1
        entryPanel.add(entryPanel_enabledCheckBox, entryPanel_GBLC)
        entryPanel_GBLC.weightx = 0.10
        entryPanel_GBLC.gridx = 1
        entryPanel_GBLC.gridy = 1
        entryPanel.add(entryPanel_nameTextField, entryPanel_GBLC)
        entryPanel_GBLC.weightx = 0.10
        entryPanel_GBLC.gridx = 2
        entryPanel_GBLC.gridy = 1
        entryPanel.add(entryPanel_valueComboBox, entryPanel_GBLC)
        entryPanel_GBLC.weightx = 0.10
        entryPanel_GBLC.gridx = 3
        entryPanel_GBLC.gridy = 1
        entryPanel.add(entryPanel_valueTextField, entryPanel_GBLC)
        entryPanel_GBLC.weightx = 0.10
        entryPanel_GBLC.gridx = 4
        entryPanel_GBLC.gridy = 1
        entryPanel.add(entryPanel_weightTextField, entryPanel_GBLC)
        
        return entryPanel
        
    def removeMultiRunEntry(self, index, activeLocation):
        #print 'Begin removeMultiRunEntry()'
        if(activeLocation == self.ACTIVE_LOCATION_MARKER_DATA):
            multiRunPanel = self.weightTab_markerDataGroup
            multiRunEntryList = self.multiRunMarkerDataEntryList
            multiRunEntryListIndex = self.multiRunMarkerDataEntryListIndex
            
            activeEnabledCheckBoxes = self.markerDataEnabledCheckBoxes
            activeNameTextFields = self.markerDataNameTextFields
            activeValueComboBoxes = self.markerDataValueComboBoxes
            activeValueTextFields = self.markerDataValueTextFields
            activeWeightTextFields = self.markerDataWeightTextFields
            
        elif(activeLocation == self.ACTIVE_LOCATION_COORD_DATA):
            multiRunPanel = self.weightTab_coordDataGroup
            multiRunEntryList = self.multiRunCoordDataEntryList
            multiRunEntryListIndex = self.multiRunCoordDataEntryListIndex
        
            activeEnabledCheckBoxes = self.coordDataEnabledCheckBoxes
            activeNameTextFields = self.coordDataNameTextFields
            activeValueComboBoxes = self.coordDataValueComboBoxes
            activeValueTextFields = self.coordDataValueTextFields
            activeWeightTextFields = self.coordDataWeightTextFields
        
        # Here we use isEnabled similar to a lock to prevent us from trying to make changes while we're in the process of making changes.
        #print 'References set. Beginning logic.'
        isEnabled = multiRunPanel.isEnabled()
        
        if(isEnabled):
            #print ">> Disable all multiRun___Entries"
            multiRunPanel.setEnabled(False)
            multiRunEntryListLength = len(multiRunEntryList)
            
            # Create temp variables
            tempEnabledCheckBoxes = []
            tempNameTextFields = []
            tempValueComboBoxes = []
            tempValueTextFields = []
            tempWeightTextFields = []
            
            # Gather the existing values so we can recreate them
            #print ">> Gathering existing values between indexes %d and %d" % (index+1, len(multiRunEntryList))
            for multiRunEntryIndex in range(index+1, multiRunEntryListLength):
                #print '))))Enabled Check box: %d' % (activeEnabledCheckBoxes[multiRunEntryIndex].isSelected())
                tempEnabledCheckBoxes.append(activeEnabledCheckBoxes[multiRunEntryIndex].isSelected())
                #print '))))Name text field: %s' % (activeNameTextFields[multiRunEntryIndex].text)
                tempNameTextFields.append(activeNameTextFields[multiRunEntryIndex].text)
                tempValueComboBoxes.append(activeValueComboBoxes[multiRunEntryIndex].getSelectedIndex())
                tempValueTextFields.append(activeValueTextFields[multiRunEntryIndex].text)
                tempWeightTextFields.append(activeWeightTextFields[multiRunEntryIndex].text)
            
            # Remove all multiRunEntries from the multiRunPanel and ui components in arrays starting from the given index
            #print ">> Removing all multiRunEntries between indexes %d and %d" % (index, multiRunEntryListLength)
            for multiRunEntryIndex in range(index, multiRunEntryListLength):
                #print ">>>> Removing entry %d" % (multiRunEntryIndex)
                multiRunPanel.remove(multiRunEntryList[index])
                multiRunEntryList.pop(index)
                activeEnabledCheckBoxes.pop(index)
                activeNameTextFields.pop(index)
                activeValueComboBoxes.pop(index)
                activeValueTextFields.pop(index)
                activeWeightTextFields.pop(index)
            
            # Set the multi-run index variable
            multiRunEntryListIndex = index
            if(activeLocation == self.ACTIVE_LOCATION_MARKER_DATA):
                self.multiRunMarkerDataEntryListIndex = index
            elif(activeLocation == self.ACTIVE_LOCATION_COORD_DATA):
                self.multiRunCoordDataEntryListIndex = index
                
            #print ">> Set multiRunEntryListIndex to %d" % (multiRunEntryListIndex)
            
            # Recreate the other multiRunEntries
            #print ">> Recreating %d multi-run entries" % (len(tempEnabledCheckBoxes))
            multiRunPanel.setEnabled(True)
            for multiRunEntryIndex in range (len(tempEnabledCheckBoxes)):
                #print ">>>> Adding new entry at index %d" % (multiRunEntryListIndex)
                # Add a new multi-run entry (blank)
                #multiRunPanel.setEnabled(True)
                self.addMultiRunEntry(activeLocation)
                #print ">>>> Blank entry added, setting values"
                
                # Populate it with values
                activeEnabledCheckBoxes[multiRunEntryListIndex].setSelected(tempEnabledCheckBoxes[multiRunEntryIndex])
                #print 'Check box set'
                activeNameTextFields[multiRunEntryListIndex].text = tempNameTextFields[multiRunEntryIndex]
                #print 'Name set to %s' % (tempNameTextFields[multiRunEntryIndex])
                activeValueComboBoxes[multiRunEntryListIndex].setSelectedIndex(tempValueComboBoxes[multiRunEntryIndex])
                #print 'Value chosen'
                activeValueTextFields[multiRunEntryListIndex].text = tempValueTextFields[multiRunEntryIndex]
                #print 'Value set'
                activeWeightTextFields[multiRunEntryListIndex].text = tempWeightTextFields[multiRunEntryIndex]
                #print 'Weight set'
                
                if(activeLocation == self.ACTIVE_LOCATION_MARKER_DATA):
                    multiRunEntryListIndex = self.multiRunMarkerDataEntryListIndex
                elif(activeLocation == self.ACTIVE_LOCATION_COORD_DATA):
                    multiRunEntryListIndex = self.multiRunCoordDataEntryListIndex
                
            #print '>>>> Values populated'
                
            # Revalidate and repaint everything so Jython shows UI correctly on screen
            #print ">> Refreshing UI"
            multiRunPanel.revalidate()
            multiRunPanel.repaint()
            self.invKineToolCard.revalidate()
            self.invKineToolCard.repaint()
            
            multiRunPanel.setEnabled(True)
            #print "Finished removeMultiRunEntry()"
            
    #https://stackoverflow.com/questions/1213295/handling-swing-focus-events-with-jython
    #See frame stuff in above link to get the UI to refresh
    #https://stackoverflow.com/questions/4279435/java-how-would-i-dynamically-add-swing-component-to-gui-on-click
    #https://wiki.python.org/jython/SwingExamples
    
    def checkForEmptyFieldsMarkerData(self, event):
        self.checkForEmptyFieldsGivenLocation(self.ACTIVE_LOCATION_MARKER_DATA)
        
    def checkForEmptyFieldsCoordData(self, event):
        self.checkForEmptyFieldsGivenLocation(self.ACTIVE_LOCATION_COORD_DATA)
        
    def checkForEmptyFieldsGivenLocation(self, activeLocation):
        #print '==== Entering checkForEmptyFieldsGivenLocation() ===='
        if(activeLocation == self.ACTIVE_LOCATION_MARKER_DATA):
            multiRunPanel = self.weightTab_markerDataGroup
            multiRunEntryList = self.multiRunMarkerDataEntryList
            multiRunEntryListIndex = self.multiRunMarkerDataEntryListIndex
            
            activeEnabledCheckBoxes = self.markerDataEnabledCheckBoxes
            activeNameTextFields = self.markerDataNameTextFields
            activeValueComboBoxes = self.markerDataValueComboBoxes
            activeValueTextFields = self.markerDataValueTextFields
            activeWeightTextFields = self.markerDataWeightTextFields
            
            # Delete all values in the config (we'll update with new values later)
            del self.myConfig.ikTool_markerTask_names     [:]
            del self.myConfig.ikTool_markerTask_weights   [:]
            del self.myConfig.ikTool_markerTask_applies   [:]
        
        elif(activeLocation == self.ACTIVE_LOCATION_COORD_DATA):
            multiRunPanel = self.weightTab_coordDataGroup
            multiRunEntryList = self.multiRunCoordDataEntryList
            multiRunEntryListIndex = self.multiRunCoordDataEntryListIndex
        
            activeEnabledCheckBoxes = self.coordDataEnabledCheckBoxes
            activeNameTextFields = self.coordDataNameTextFields
            activeValueComboBoxes = self.coordDataValueComboBoxes
            activeValueTextFields = self.coordDataValueTextFields
            activeWeightTextFields = self.coordDataWeightTextFields
            
            # Delete all values in the config (we'll update with new values later)
            del self.myConfig.ikTool_coordTask_names     [:]
            del self.myConfig.ikTool_coordTask_applies   [:]
            del self.myConfig.ikTool_coordTask_weights   [:]
            del self.myConfig.ikTool_coordTask_valueTypes[:]
            del self.myConfig.ikTool_coordTask_values    [:]
        
        if(multiRunPanel.isEnabled()):
            #print "[cfefgl] == Checking for empty fields =="
            #print "[cfefgl] There are %d multiRunEntries" % ( len(multiRunEntryList) )
            
            emptyTextTypeA = " "
            emptyTextTypeB = ""
            isEmpty = True
            
            # Loop through the entry list
            multiRunEntryListLength = len(multiRunEntryList)
            tempIndex = 0
            for i in range(multiRunEntryListLength):
                #print "[cfefgl] Inspecting multiRunEntry %d at index %d" % (i, tempIndex)
                
                isEmpty = True
                
                # Check if any of the fields are empty (for this item in entry list)
                tempApplyCheckBoxVal = activeEnabledCheckBoxes[tempIndex].isSelected()
                tempNameTextFieldText = activeNameTextFields[tempIndex].getText()
                tempValueComboBoxesText = activeValueComboBoxes[tempIndex].getSelectedItem()
                tempValueComboBoxesText = tempValueComboBoxesText.lower()
                tempValueComboBoxesText = tempValueComboBoxesText.replace(' ','_')
                tempValueTextFieldText = activeValueTextFields[tempIndex].getText()
                tempWeightTextFieldText = activeWeightTextFields[tempIndex].getText()
                
                #print '[cfefgl] Looking for empty fields'
                if( (tempNameTextFieldText != emptyTextTypeA) and (tempNameTextFieldText != emptyTextTypeB) ):
                    #print '[cfefgl] The Name value is specified, the multiRunEntry is not empty'
                    isEmpty = False
                elif( (tempValueTextFieldText != emptyTextTypeA) and (tempValueTextFieldText != emptyTextTypeB) ):
                    #print '[cfefgl] The Value value is specified, the multiRunEntry is not empty'
                    isEmpty = False
                elif( (tempWeightTextFieldText != emptyTextTypeA) and (tempWeightTextFieldText != emptyTextTypeB) ):
                    #print '[cfefgl] The Weight value is specified, the multiRunEntry is not empty'
                    isEmpty = False
                elif(True == tempApplyCheckBoxVal):
                    #print '[cfefgl] Check box is checked, the multiRunEntry is not empty'
                    isEmpty = False
                else:
                    print "[cfefgl] The multiRunEntry is empty"
                    
                # If all fields empty and not the last item in the entry list
                if( isEmpty ):
                    # if it is not the last one in the list and it is not the last thing TODO: same thing?
                    #if( (i != (self.multiRunEntryListIndex-1) ) and (self.multiRunEntryListIndex > 1) ):
                    if( tempIndex != (multiRunEntryListIndex-1) ):
                        # Remove the item from the entry list, UI
                        #print "[cfefgl] Removing multiRunEntry %d" % (tempIndex)
                        self.removeMultiRunEntry(tempIndex, activeLocation)
                        
                        #print '[cfefgl] multiRunEntryListLength was: %d' % (multiRunEntryListLength)
                        #print '[cfefgl] multiRunEntryListIndex was: %d' % (multiRunEntryListIndex)
                        # Update the length variable
                        if(activeLocation == self.ACTIVE_LOCATION_MARKER_DATA):
                            multiRunEntryList = self.multiRunMarkerDataEntryList
                            multiRunEntryListIndex = self.multiRunMarkerDataEntryListIndex
                        elif(activeLocation == self.ACTIVE_LOCATION_COORD_DATA):
                            multiRunEntryList = self.multiRunCoordDataEntryList
                            multiRunEntryListIndex = self.multiRunCoordDataEntryListIndex
                        multiRunEntryListLength = len(multiRunEntryList)
                        #print '[cfefgl] multiRunEntryListLength is now: %d' % (multiRunEntryListLength)
                        #print '[cfefgl] multiRunEntryListIndex is now: %d' % (multiRunEntryListIndex)
                else:
                    # Save values to config
                    if(activeLocation == self.ACTIVE_LOCATION_MARKER_DATA):
                        #print '[cfefgl] Saving marker values to config at index %d' % (multiRunEntryListIndex)
                        self.myConfig.ikTool_markerTask_names.append(tempNameTextFieldText)
                        self.myConfig.ikTool_markerTask_weights.append(tempWeightTextFieldText)
                        if(True == tempApplyCheckBoxVal):
                            self.myConfig.ikTool_markerTask_applies.append('true')
                        else:
                            self.myConfig.ikTool_markerTask_applies.append('false')
                    elif(activeLocation == self.ACTIVE_LOCATION_COORD_DATA):
                        #print '[cfefgl] Saving coordinate values to config at index %d' % (multiRunEntryListIndex)
                        self.myConfig.ikTool_coordTask_names.append(tempNameTextFieldText)
                        self.myConfig.ikTool_coordTask_weights.append(tempWeightTextFieldText)
                        #print '[cfefgl] combo box value: %s' % (tempValueComboBoxesText)
                        self.myConfig.ikTool_coordTask_valueTypes.append(tempValueComboBoxesText)
                        self.myConfig.ikTool_coordTask_values.append(tempValueTextFieldText)
                        if(True == tempApplyCheckBoxVal):
                            self.myConfig.ikTool_coordTask_applies.append('true')
                        else:
                            self.myConfig.ikTool_coordTask_applies.append('false')
                    
                    # if not empty and is the last item in the entry list
                    if( tempIndex == (multiRunEntryListIndex-1) ):
                        # Add new item to the entry list, UI
                        #print "[cfefgl] The last multiRunEntry in the list is not empty - Adding multiRunEntry"
                        self.addMultiRunEntry(activeLocation)
                        # TODO: Update multiRunEntryListIndex???
                    tempIndex = tempIndex + 1
                    
                #print '[cfefgl] Finished inspecting multiRunEntry %d' % (i)
    
    # End multi-run stuff  ############################################################################################################################################
    
    def getCard(self):
        return self.invKineToolCard
    
    def onCoordDataFileCheckBoxSelect(self, event):
        self.setCoordDataFile()
        
    def onUseItCheckBoxSelect(self, event):
        if (self.useThisToolGroup_useItCheckBox.isSelected()):
            self.myConfig.useIkTool = 'true'
        else:
            self.myConfig.useIkTool = 'false'
    
    #################################################
    # Save the value in the UI to the config object #
    #################################################
    
    def setMarkerDataFile(self):
        self.myConfig.ikTool_markerFile = self.settingsTab_IKTrialGroup_markerDataFileTextField.text
        
    def setCoordDataFile(self):
        if (self.settingsTab_IKTrialGroup_coordDataFileCheckBox.isSelected()):
            self.myConfig.ikTool_coordinateFile = self.settingsTab_IKTrialGroup_coordDataFileTextField.text
        else:
            self.myConfig.ikTool_coordinateFile = 'Unassigned'
        
    def setMotionFile(self):
        self.myConfig.ikTool_outputMotionFile = self.settingsTab_OutputGroup_motionFileTextField.text
        
    ###############################
    # Save contents of a text box #
    ###############################
    def setMarkerDataFileEvent(self, event):
        self.setMarkerDataFile()
        
    def setCoordDataFileEvent(self, event):
        self.setCoordDataFile()
        
    def setMotionFileEvent(self, event):
        self.setMotionFile()
        
    
    ###########################################################
    # Show file chooser dialog when a "..." button is pressed #
    ###########################################################
    
    def selectMarkerDataFile(self, event):
        # Create and show a file chooser (True = select file, False = select directory)
        myChooser = bessFileChooser(True)
        # Set the text field text to the results of the file chooser
        self.settingsTab_IKTrialGroup_markerDataFileTextField.text = myChooser.fullPath.toString()
        # Save the value to the config class
        self.setMarkerDataFile()
        
    def selectCoordDataFile(self, event):
        # Create and show a file chooser (True = select file, False = select directory)
        myChooser = bessFileChooser(True)
        # Set the text field text to the results of the file chooser
        self.settingsTab_IKTrialGroup_coordDataFileTextField.text = myChooser.fullPath.toString()
        # Save the value to the config class
        self.setCoordDataFile()
        
    def selectMotionFile(self, event):
        # Create and show a file chooser (True = select file, False = select directory)
        myChooser = bessFileChooser(True)
        # Set the text field text to the results of the file chooser
        self.settingsTab_OutputGroup_motionFileTextField.text = myChooser.fullPath.toString()
        # Save the value to the config class
        self.setMotionFile()
    
    # Save/load from config
    # https://docs.python.org/2/library/xml.etree.elementtree.html
    def loadSettings(self, event):
        # Create and show a file chooser (True = select file, False = select directory)
        myChooser = bessFileChooser(True)
        self.loadSettingsFromFile(myChooser.fullPath.toString())
        
    def loadSettingsFromFile(self, filePath):
        # Check if "use it" is checked
        if(self.myConfig.useIkTool == 'true'):
            self.useThisToolGroup_useItCheckBox.setSelected(True)
        else:
            self.useThisToolGroup_useItCheckBox.setSelected(False)
            
        # Open the xml file
        if(os.path.exists(filePath)):
            print 'Opening config file: %s' % (filePath)
            tree = ET.parse(filePath)
            
            # Remove existing multirun marker data
            # TODO: This takes FOREVER! Find a better way...
            maxRange = self.multiRunMarkerDataEntryListIndex
            for i in range(1, maxRange):
                self.removeMultiRunEntry(0, self.ACTIVE_LOCATION_MARKER_DATA)
            
            # Remove existing multirun coord data
            # TODO: This takes FOREVER! Find a better way...
            maxRange = self.multiRunCoordDataEntryListIndex
            for i in range(1, maxRange):
                self.removeMultiRunEntry(0, self.ACTIVE_LOCATION_COORD_DATA)
            
            # Parse the tree
            root = tree.getroot()
            self.parseTree(root)
            
            # Save values to BessConfig (note: dynamic ui items were saved during the call to parseTree())
            self.checkForEmptyFieldsGivenLocation(self.ACTIVE_LOCATION_COORD_DATA)
            self.checkForEmptyFieldsGivenLocation(self.ACTIVE_LOCATION_MARKER_DATA)
            self.setMarkerDataFile()
            self.setCoordDataFile()
            self.setMotionFile()
            
            print 'Done loading settings'
        else:
            print 'Could not find file: %s' % (filePath)
        
    def parseTree(self, node):
        #print 'Node tag: %s,  text: %s' % (node.tag, node.text)
        
        if('output_motion_file' == node.tag):
            self.settingsTab_OutputGroup_motionFileTextField.text = node.text
        elif('marker_file' == node.tag):
            self.settingsTab_IKTrialGroup_markerDataFileTextField.text = node.text
        elif('coordinate_file' == node.tag):
            self.settingsTab_IKTrialGroup_coordDataFileTextField.text = node.text
            if( ('Unassigned' == node.text) or ('' == node.text) or (not node.text) ):
                self.settingsTab_IKTrialGroup_coordDataFileCheckBox.setSelected(False)
                self.settingsTab_IKTrialGroup_coordDataFileTextField.text = ''
            else:
                self.settingsTab_IKTrialGroup_coordDataFileCheckBox.setSelected(True)
        elif('IKCoordinateTask' == node.tag):
            print '[debug] Coordinate Task tag found'
            name = node.get('name')
            weight = ''
            valueType = ''
            value = ''
            apply = ''
            
            for child in node:
                if('weight' == child.tag):
                    weight = child.text
                elif('value_type' == child.tag):
                    valueType = child.text
                elif('value' == child.tag):
                    value = child.text
                elif('apply' == child.tag):
                    apply = child.text
            
            #print '  apply: %s' % (apply)
            #print '  weight: %s' % (weight)
            #print '  value_type: %s' % (valueType)
            #print '  value: %s' % (value)
            #print '  coord index: %d' % (self.multiRunCoordDataEntryListIndex-1)
            
            # Set the values to the ui
            if('true' == apply):
                self.coordDataEnabledCheckBoxes[self.multiRunCoordDataEntryListIndex-1].setSelected(True)
            else:
                self.coordDataEnabledCheckBoxes[self.multiRunCoordDataEntryListIndex-1].setSelected(False)
            self.coordDataNameTextFields[self.multiRunCoordDataEntryListIndex-1].text = name
            # entryPanel_valueComboBoxOptions = ["From file", "Default value", "Manual value"]
            for index,item in enumerate(self.entryPanel_valueComboBoxOptions):
                # Adjust the text to match the xml tag values
                item = item.lower()
                item = item.replace(' ','_')
                if(item == valueType):
                    self.coordDataValueComboBoxes[self.multiRunCoordDataEntryListIndex-1].setSelectedIndex(index)
            self.coordDataValueTextFields[self.multiRunCoordDataEntryListIndex-1].text = value
            self.coordDataWeightTextFields [self.multiRunCoordDataEntryListIndex-1].text = weight
            #self.checkForEmptyFieldsGivenLocation(self.ACTIVE_LOCATION_COORD_DATA)
            self.addMultiRunEntry(self.ACTIVE_LOCATION_COORD_DATA)
        elif('IKMarkerTask' == node.tag):
            #print '[debug] Marker tag found'
            name = node.get('name')
            weight = ''
            apply = ''
            
            #print 'name: %s' % (name)
            for child in node:
                #print 'children...'
                if('weight' == child.tag):
                    weight = child.text
                    #print '  weight: %s' % (weight)
                elif('apply' == child.tag):
                    apply = child.text
            
            #print '  weight: %s' % (weight)
            #print '  value_type: %s' % (valueType)
            #print '  value: %s' % (value)
            #print '  marker index: %d' % (self.multiRunMarkerDataEntryListIndex-1)
            
            # Set the values to the ui
            if('true' == apply):
                self.markerDataEnabledCheckBoxes[self.multiRunMarkerDataEntryListIndex-1].setSelected(True)
            else:
                self.markerDataEnabledCheckBoxes[self.multiRunMarkerDataEntryListIndex-1].setSelected(False)
            #print '[debug] Set the enabled check box'
            self.markerDataNameTextFields[self.multiRunMarkerDataEntryListIndex-1].text = name
            #print '[debug] Set the name text field'
            # entryPanel_valueComboBoxOptions = ["From file", "Default value", "Manual value"]
            #for index,item in enumerate(self.entryPanel_valueComboBoxOptions):
            #    # Adjust the text to match the xml tag values
            #    item = item.lower()
            #    item = item.replace(' ','_')
            #    if(item == valueType):
            #        self.markerDataValueComboBoxes[self.multiRunMarkerDataEntryListIndex-1].setSelectedIndex(index)
            #self.markerDataValueTextFields[self.multiRunMarkerDataEntryListIndex-1].text = value
            self.markerDataWeightTextFields [self.multiRunMarkerDataEntryListIndex-1].text = weight
            #print '[debug] Set the weight text field'
            #self.checkForEmptyFieldsGivenLocation(self.ACTIVE_LOCATION_MARKER_DATA)
            self.addMultiRunEntry(self.ACTIVE_LOCATION_MARKER_DATA)
            #print '[debug] Finished looking for empties'
            
        # Look at any children of children
        for child in node:
            self.parseTree(child)
            
# end bessConfigUIMultiRunCard class

