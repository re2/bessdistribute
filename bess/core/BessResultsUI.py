

################################################################################
#
# This file creates the Results UI components and handles user input as
# it relates to visualizing the data from a simulation.
#
################################################################################

from javax.swing import JFrame, JButton
from java.awt import BorderLayout

import os
import sys
from threading import Thread

import org.opensim.console.gui as gui
import org.opensim.plotter.PlotterSourceFile as PlotterSourceFile
import org.opensim.console.OpenSimPlotter as OpenSimPlotter

# Find the parent "bess" directory so that we can import JAR files from
# different directories.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])

import utilities.toolsCommon as TC

# Get the main GUI components from our NetBeans/Java project.
TC.importJar(os.path.join(bessDir, 'lib', 'BessCoreGui.jar'))
from bess.gui.core.displayui import DisplayUI as DisplayUIJFrame
from bess.gui.core.displayui import AddCurvePanel as CurvePanel

class bessResultsUI:

    def __init__(self, myConfig, myData):
        self.myConfig = myConfig
        self.myData = myData
        
        self.bessOutputFrame = DisplayUIJFrame(self.myConfig.common_rootDir)
        self.figureIndex = 1
        
        plotButton = JButton("Plot", actionPerformed = self.plotStuff)
        self.bessOutputFrame.add(plotButton, BorderLayout.SOUTH)
        
        self.bessOutputFrame.pack()
        self.bessOutputFrame.visible = False
        self.bessOutputFrame.setSize(self.bessOutputFrame.getWidth(), 800)
        self.bessOutputFrame.revalidate()
        self.bessOutputFrame.repaint()
        
    def setVisible(self, show):
        self.bessOutputFrame.visible = show
        
    def plotStuff(self, event):
        Thread(target=lambda: self.displayGraph()).start()
        
    def displayGraph(self):
        #print("Plotting stuff...")
        
        dataFilePaths = self.bessOutputFrame.getDataFilePaths()
        
        plotterPanel = OpenSimPlotter.createPlotterPanel(("Figure "+str(self.figureIndex)))
        self.figureIndex = self.figureIndex + 1
        
        filePath = ""
        xAxis = ""
        yAxis = ""
        yAxisLabel = ""
        
        for index, dataFilePath in enumerate(dataFilePaths):
            
            filePath = dataFilePath
            
            print("Data file path: " + dataFilePath)
            
            xAxis = (self.bessOutputFrame.getXAxisList())[index]
            print("X axis: " + xAxis)
            
            yAxis = (self.bessOutputFrame.getYAxisList())[index]
            print("Y axis: " + yAxis)
            
            if(yAxisLabel == ""):
                yAxisLabel = yAxis
            else:
                yAxisLabel = yAxisLabel + ", " + yAxis
            
            # Make a label for the legend
            legendLabel = self.appendRunToLegend(filePath, yAxis)
            
            #print("Loading data...")
            dataSource = OpenSimPlotter.addDataSource(plotterPanel, filePath)            
            #print("Adding a curve to the graph")
            curve = OpenSimPlotter.addCurve(plotterPanel, dataSource, xAxis, yAxis)
            #print("Setting the curve legend")
            OpenSimPlotter.setCurveLegend(curve, legendLabel)
        
        #print("Setting x axis label")
        plotterPanel.setXAxisLabel(xAxis)
        #print("Setting y axis label")
        plotterPanel.setYAxisLabel(yAxisLabel)
        #print("Finished creating the graph")
        
    def appendRunToLegend(self, fullFilePath, runLegend):
        runIndex = ""
        
        # Find the last index of / in our full file path string and split that into file name and directory path
        fileNameIndex = fullFilePath.rfind('/') + 1  # Go one past where we find the index
        
        # If we didn't find a slash... try looking for the other slash
        if( (len(fullFilePath) != 0) and fileNameIndex == 0 ):
            fileNameIndex = fullFilePath.rfind('\\') + 1  # Go one past where we find the index
            
        
        directoryPath = fullFilePath[:fileNameIndex]
        fileName = fullFilePath[fileNameIndex:]
        #print("[DEBUG] File name index: "+str(fileNameIndex))
        #print("[DEBUG] directoryPath: "+directoryPath)
        #print("[DEBUG] File name: "+fileName)
        
        # Look for a run number
        if(fileName.startswith("run_")):
            # We are expecting to see:    run_<some number here>_<the rest of the file name>
            # Look between the first and secon "_" character
            prependIndex = fileName.find("_") + 1   # Go one past where we find the first "_" character
            stopIndex = fileName.find("_", prependIndex)
            
            runIndex = fileName[prependIndex:stopIndex]
            
            #print("[DEBUG] prependIndex: "+str(prependIndex))
            #print("[DEBUG] stopIndex: "+str(stopIndex))
            #print("[DEBUG] runIndex: "+runIndex)
        
        # Construct the string with the prepend
        if(runIndex != ""):
            runLegend = "Run " + runIndex + " " + runLegend
        
        #print("[DEBUG] runLegend: "+runLegend)
        return runLegend
#end of bessResultsUI class

