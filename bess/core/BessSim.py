

################################################################################
#
# This file provides the interface to OpenSim's simulation libraries and tools.
#
################################################################################

import os
import org.opensim.utils as utils
import org.opensim.modeling as modeling
import org.opensim.console.gui as gui

from configure_forces import configureForces_Support

class bessSim:

    def __init__(self, myConfig, displayUI):
        self.myConfig = myConfig
        self.displayUI = displayUI

    def startSimulation(self):
        print "Starting BESS Simulation"
        
        runIndex = 0
        
        #print("[DEBUG] <<<<<<<<<<<<<<<<< self.myConfig.multiRun_numberOfRuns: " + str(len(self.myConfig.multiRun_numberOfRuns)))
        if(0 == len(self.myConfig.multiRun_numberOfRuns)):
            self.myConfig.multiRun_numberOfRuns.append("1")
            
        for index,numRunsAsString in enumerate(self.myConfig.multiRun_numberOfRuns):
            numRuns = int(numRunsAsString)
            
            # If specific values are not specified, calculate our own values
            if( numRuns == 0 or numRuns == None ):
                specificValuesText = self.myConfig.multiRun_specificValues[index]
                # Split the string up and determine how many are inside
                specificValuesList = specificValuesText.split(",")
                numRuns = len(specificValuesList)
                
            #print("[DEBUG] <<<<<<<<<<<<<<<<< numRuns: " + str(numRuns))
            
            # Loop through the values that change
            for index in range(numRuns):
                
                print ("---------------> Starting simulation run " + str(runIndex) )
                self.myConfig.commonConfigFile          = self.appendRunToFileName(self.myConfig.commonConfigFile, runIndex)
                self.myConfig.scaleConfigFile           = self.appendRunToFileName(self.myConfig.scaleConfigFile, runIndex)
                self.myConfig.multiRunConfigFile        = self.appendRunToFileName(self.myConfig.multiRunConfigFile, runIndex)
                self.myConfig.cmcConfigFile             = self.appendRunToFileName(self.myConfig.cmcConfigFile, runIndex)
                self.myConfig.ikConfigFile              = self.appendRunToFileName(self.myConfig.ikConfigFile, runIndex)
                self.myConfig.configureForcesConfigFile = self.appendRunToFileName(self.myConfig.configureForcesConfigFile, runIndex)
                self.myConfig.rraConfigFile             = self.appendRunToFileName(self.myConfig.rraConfigFile, runIndex)
                self.myConfig.staticOptConfigFile       = self.appendRunToFileName(self.myConfig.staticOptConfigFile, runIndex)
                self.myConfig.invDynamicsConfigFile     = self.appendRunToFileName(self.myConfig.invDynamicsConfigFile, runIndex)
                self.myConfig.jrConfigFile              = self.appendRunToFileName(self.myConfig.jrConfigFile, runIndex)
                
                #########
                # SCALE #
                #########
                
                #self.myConfig.scale_output_modelName              = self.appendRunToFileName(self.myConfig.scale_output_modelName, runIndex)
                self.myConfig.scale_modelScaler_outputModelFile   = self.appendRunToFileName(self.myConfig.scale_modelScaler_outputModelFile, runIndex)
                self.myConfig.scale_modelScaler_outputScaleFile   = self.appendRunToFileName(self.myConfig.scale_modelScaler_outputScaleFile, runIndex)
                self.myConfig.scale_markerPlacer_outputMotionFile = self.appendRunToFileName(self.myConfig.scale_markerPlacer_outputMotionFile, runIndex)
                self.myConfig.scale_markerPlacer_outputModelFile  = self.appendRunToFileName(self.myConfig.scale_markerPlacer_outputModelFile, runIndex)
                self.myConfig.scale_markerPlacer_outputMarkerFile = self.appendRunToFileName(self.myConfig.scale_markerPlacer_outputMarkerFile, runIndex)
                
                ############
                # CMC TOOL #
                ############
                
                self.myConfig.cmcTool_outputPrefix    = self.appendRunToFileName(self.myConfig.cmcTool_outputPrefix, runIndex)
                self.myConfig.cmcTool_outputDirectory = self.appendRunToFileName(self.myConfig.cmcTool_outputDirectory, runIndex)
                
                ###########
                # IK TOOL #
                ###########
                
                self.myConfig.ikTool_outputMotionFile = self.appendRunToFileName(self.myConfig.ikTool_outputMotionFile, runIndex)
                # ????????  self.ikTool_resultsDir = 'C:/Users/david.rusbarsky/Documents/programming/bess_sandbox/tutorial 3/results/IK/'
                
                #########################
                # CONFIGURE FORCES TOOL #
                #########################
                
                # ????????  self.configureForcesTool_Model = None
                
                ############
                # RRA TOOL #
                ############
                
                self.myConfig.rraTool_outputModelFile = self.appendRunToFileName(self.myConfig.rraTool_outputModelFile, runIndex)
                self.myConfig.rraTool_resultsDir      = self.appendRunToFileName(self.myConfig.rraTool_resultsDir, runIndex)
                
                # TODO: I'm not sure where the prefix goes ub tge xml file...
                self.myConfig.rraTool_prefix = self.appendRunToFileName(self.myConfig.rraTool_prefix, runIndex)
                
                #####################
                # Static Optim TOOL #
                #####################
                
                self.myConfig.staticOptTool_resultsDir = self.appendRunToFileName(self.myConfig.staticOptTool_resultsDir, runIndex)
                
                ################
                # Inv Dyn TOOL #
                ################
                
                # ?????? Nope. OpenSim InvDyn tool will construct the full paths. We do not need to touch: self.inverseDynTool_resultsDir = ''
                # TODO: Will chaning this file name break things for OpenSim? Is it expecting this exact file name?
                self.myConfig.inverseDynTool_outputGenForceFile   = self.appendRunToFileName(self.myConfig.inverseDynTool_outputGenForceFile, runIndex)
                self.myConfig.inverseDynTool_outputBodyForcesFile = self.appendRunToFileName(self.myConfig.inverseDynTool_outputBodyForcesFile, runIndex)
                
                ###########
                # JR TOOL #
                ###########
                
                # TODO: No output files?
                
                #print( "Using config files:")
                #print("    "+self.myConfig.commonConfigFile          )
                #print("    "+self.myConfig.scaleConfigFile           )
                #print("    "+self.myConfig.multiRunConfigFile        )
                #print("    "+self.myConfig.cmcConfigFile             )
                #print("    "+self.myConfig.ikConfigFile              )
                #print("    "+self.myConfig.configureForcesConfigFile )
                #print("    "+self.myConfig.rraConfigFile             )
                #print("    "+self.myConfig.staticOptConfigFile       )
                #print("    "+self.myConfig.invDynamicsConfigFile     )
                #print("    "+self.myConfig.jrConfigFile              )
                
                try:
                    ##################
                    ## General setup
                    ##################
                    
                    ###############
                    ## Load model
                    ###############
                    
                    ###### modelFile = getScriptsDir()+"/GUI_Scripting/testData/Subject01/Subject01.osim"
                    ###### if not os.path.exists(modelFile):
                    ######     modelFile = utils.FileUtils.getInstance().browseForFilename(".osim", "Select the Model", 1)
                    
                    print '[SIM] Loading initial model: %s' % (self.myConfig.common_modelFile)
                    model = modeling.Model(self.myConfig.common_modelFile)
                    model.initSystem()
                    gui.loadModel(model)
                    myModel = gui.getCurrentModel()
                    myState = myModel.initSystem()
                    
                    ###############
                    ## Import exp data
                    ##   Marker data
                    ##   Ground reaction forces
                    ###############
                    # TODO: Motion code probably needs to go somewhere else, but it is causing warning windows to appear here (seen in OpenSim tutorial 3)
                    #print '[SIM] Loading motion: %s' % (self.myConfig.common_motionFile)
                    #motionFile = self.myConfig.common_motionFile
                    #gui.loadMotion(motionFile)
                    
                    # TODO: Import other data here
                    ### Add a MarkerSet to the model
                    ##	Create a MarkerSet object
                    #newMarkers = modeling.MarkerSet(self.myConfig.???)
                    ## 	Replace the markerSet to the model
                    #myModel.replaceMarkerSet(myState,newMarkers)
                    ##	Re-initialize State
                    #myState = myModel.initSystem()
                    
                    ###############
                    ## Scale model
                    ###############
                    if(self.myConfig.useScale == 'true'):
                        print '[SIM] Scaling model'
                        
                        print '[SIM] Loading markers'
                        markerSetFile = self.myConfig.scale_genericModelMaker_markerSetFile
                        newMarkers = modeling.MarkerSet(markerSetFile)
                        myModel.replaceMarkerSet(myState, newMarkers)
                        myState = myModel.initSystem()
                        
                        # Create the scale tool object from existing xml
                        #print '[debug] creating scale tool'
                        scaleTool = modeling.ScaleTool(self.myConfig.scaleConfigFile)
                        mass = scaleTool.getSubjectMass()
                        
                        # Get the path to the subject
                        #path2subject = scaleTool.getPathToSubject()
                        #print '[debug] Path to subject: %s' % (path2subject)
                        # Opensim seems to want to put path2subject prior to the model name. This is not consistant with other tools.
                        #  BESS attempts to have the user enter things once (such as the model file path).
                        #  Different tools require different things (full path vs relative path vs multiple variables to construct the path).
                        #  Here we set path2subject to an empty string since we already have the full file path and don't need to construct it with multiple variables.
                        path2subject = ''
                        #print '[debug] Path to subject (expecting blank value here): %s' % (path2subject)
                        
                        # Run model scaler
                        #print '[debug] Model Scaler - Processing model'
                        scaleTool.getModelScaler().processModel(myState,myModel,path2subject, mass)
                        
                        #print '[debug] Loading scaled model %s' % (self.myConfig.scale_modelScaler_outputModelFile)
                        model = modeling.Model(self.myConfig.scale_modelScaler_outputModelFile)
                        model.initSystem()
                        gui.loadModel(model)
                        myModel = gui.getCurrentModel()
                        myState = myModel.initSystem()
                        
                        # Run Marker Placer
                        #print '[debug] Processing marker placer model'
                        scaleTool.getMarkerPlacer().processModel(myState,myModel,path2subject)
                        
                        #print '[debug] Loading markered up model'
                        model = modeling.Model(self.myConfig.scale_markerPlacer_outputModelFile)
                        model.initSystem()
                        gui.loadModel(model)
                        myModel = gui.getCurrentModel()
                        myState = myModel.initSystem()
                    
                    #########################
                    # Run the various tools #
                    #########################
                    
                    if(self.myConfig.useIkTool == 'true'):
                        print '[SIM] Performing IK tool'
                        ikTool = modeling.InverseKinematicsTool(self.myConfig.ikConfigFile)
                        ikTool.setModel(myModel)
                        ikTool.run()
                        gui.loadMotion(self.myConfig.ikTool_outputMotionFile)
                        myState = myModel.initSystem()
                        
                    if(self.myConfig.useConfigureForcesTool == 'true'):
                        print '[SIM] Performing Configure Forces tool'
                        # Different than the other OpenSim tools we don't specify time
                        # in the XML, but rather in when running the tool.  Borrow the
                        # time range from BESS.
                        timeRange = configureForces_Support.TimeRange()
                        # If the BESS time strings are empty these min or max values
                        # default to None.  Configure Forces is happy with that -- it
                        # will just take the min/max from the min/max of the input
                        # files (like IK, GRF, etc.).
                        if self.myConfig.common_timeRangeFrom != '':
                            timeRange.minTime = float(self.myConfig.common_timeRangeFrom)
                        if self.myConfig.common_timeRangeTo != '':
                            timeRange.maxTime = float(self.myConfig.common_timeRangeTo)
                        
                        # Configure Forces can throw an error if the BESS time range
                        # exceeds the time range that Configure Forces calculates
                        # from the input data files.  We choose not to for now.
                        timeRange.errorIfTimeNotInMinMaxRange = False
                                        
                        configureForces_Support.mainWorker(
                            self.myConfig.configureForcesConfigFile, 
                            timeRange)              
                        
                    if(self.myConfig.useInverseDynTool == 'true'):
                        print '[SIM] Performing Inverse Dynamics tool'
                        inverseDynTool = modeling.InverseDynamicsTool(self.myConfig.invDynamicsConfigFile)
                        inverseDynTool.run()
                    
                    if(self.myConfig.useStaticOptTool == 'true'):
                        print '[SIM] Performing Static Opt tool'
                        staticOptTool = modeling.AnalyzeTool(self.myConfig.staticOptConfigFile)
                        staticOptTool.run()
                    
                    if(self.myConfig.useRraTool == 'true'):
                        print '[SIM] Performing RRA tool'
                        rraTool = modeling.RRATool(self.myConfig.rraConfigFile)
                        rraTool.run()
                    
                    if(self.myConfig.useCmcTool == 'true'):
                        print '[SIM] Performing CMC tool'
                        cmcTool = modeling.CMCTool(self.myConfig.cmcConfigFile)
                        cmcTool.run()
                    
                    if(self.myConfig.useJointReactionTool == 'true'):
                        print '[SIM] Performing Joint Reaction tool'
                        jointReactionTool = modeling.AnalyzeTool(self.myConfig.jrConfigFile)
                        jointReactionTool.run()
                    
                except:
                    print "=== Begin error info  ======================================="
                    import sys
                    print "Something went wrong with BESS!"
                    print("Unexpected error: ", sys.exc_info()[0])
                    print "------------------------------------------"
                    import traceback
                    print traceback.format_exc()
                    # TODO: PDB doesn't seem to be working...
                    #print "------------------------------------------"
                    #import pdb
                    #pdb.post_mortem()
                    print "=== End error info  ======================================="
                    raise
                
                print("Finished simulation run " + str(runIndex))                
                runIndex = runIndex + 1
            
        print "Finished BESS Simulation"
        self.displayUI.setVisible(True)
    
    def appendRunToFileName(self, fullFilePath, runIndex):
        if(0 != runIndex):
            # Given a full file path and a run index, append "run_<runIndex>_" to the file name
            # E.g. Given fullFilePath = /some/path/to/file.txt and runIndex = 3...
            # The resulting string will be /some/path/to/run_3_file.txt
            
            # Find the last index of / in our full file path string and split that into file name and directory path
            fileNameIndex = fullFilePath.rfind('/') + 1  # Go one past where we find the index
            
            # If we didn't find a slash... try looking for the other slash
            if( (len(fullFilePath) != 0) and fileNameIndex == 0 ):
                fileNameIndex = fullFilePath.rfind('\\') + 1  # Go one past where we find the index
                
            
            directoryPath = fullFilePath[:fileNameIndex]
            fileName = fullFilePath[fileNameIndex:]
            #print("[DEBUG] File name index: "+str(fileNameIndex))
            #print("[DEBUG] directoryPath: "+directoryPath)
            #print("[DEBUG] File name: "+fileName)
            
            # Has this string been appended already?
            if(fileName.startswith("run_")):
                # We are expecting to see:    run_<some number here>_<the rest of the file name>
                # Look for the second "_" character
                prependIndex = fileName.find("_", 4) + 1   # 4 = "run_" and then go one past where we find that index
                
                # Remove the existing prepend
                fileName = fileName[prependIndex:]
                #print("[DEBUG] File name (renumbering): "+fileName)
            
            # Construct the string with the prepend
            fileName = "run_" + str(runIndex) + "_" + fileName
            #print("[DEBUG] Final file name: "+fileName)
            finalFullFilePath = directoryPath + fileName
            #print("[DEBUG] Final full path: "+fullFilePath)
        else:
            finalFullFilePath = fullFilePath
        return finalFullFilePath
        
#end of bessSim class

