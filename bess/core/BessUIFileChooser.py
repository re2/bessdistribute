

################################################################################
#
#
#
################################################################################
from javax.swing import JFrame, JPanel, JButton, JFileChooser, JTextField, JLabel, JCheckBox, SwingConstants

"""     
A helper GUI class to make using the JFileChooser a little easier
"""
class bessFileChooser:

    def __init__(self, chooseFile):
        #print "initializing the file chooser"
        # Create and setup the file chooser
        fileChooser = JFileChooser()
        #print "configuring file chooser"
        if not chooseFile:
            fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY)
            
        #print "showing the file chooser"
        # Show the file chooser and save the path
        if(fileChooser.showOpenDialog(fileChooser) == JFileChooser.APPROVE_OPTION):
            #print "saving path"
            #self.fullPath = fileChooser.getSelectedFile().getAbsolutePath()
            self.fullPath = fileChooser.getSelectedFile()
            #print "path saved"
            
        #print "finished Bess File chooser"
# end of bessFileChooser


