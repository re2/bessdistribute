

################################################################################
#
#
################################################################################

from xml.dom.minidom import parse, parseString, getDOMImplementation, Document
import xml.etree.cElementTree as ET

class bessXmlJrIO:
    def __init__(self, myConfig):
        print "Initializing bessXmlJr"
        self.myConfig = myConfig
        print "Done initializing bessXmlJr"
        
    # http://stackoverflow.com/questions/3605680/creating-a-simple-xml-file-using-python
    def writeConfigFile(self):
        print "Writing JR xml file"
        #<?xml version="1.0" encoding="UTF-8" ?>
        #<OpenSimDocument Version="30000">
        self.xmlTag_OpenSimDocument = ET.Element("OpenSimDocument", Version="30000")
        
        #   <AnalyzeTool name="loadedwalking_subject07_adjusted_loaded_markered_adjusted"> ----------> TODO: This is probably not the value we want here... Duplicate of the next value
        xmlTag_JointReactionTool = ET.SubElement(self.xmlTag_OpenSimDocument, "AnalyzeTool", name=self.myConfig.common_modelFile)
        
        #       <!--Name of the .osim file used to construct a model.-->
        #       <model_file>subject07_adjusted_loaded_markered_adjusted.osim</model_file>
        xmlTag_JointReactionTool_modelFile = ET.SubElement(xmlTag_JointReactionTool, "model_file")
        xmlTag_JointReactionTool_modelFile.text = self.myConfig.common_modelFile
        
        #       <!--Replace the model's force set with sets specified in <force_set_files>? If false, the force set is appended to.-->
        #       <replace_force_set>false</replace_force_set>
        xmlTag_JointReactionTool_replaceForceSet = ET.SubElement(xmlTag_JointReactionTool, "replace_force_set")
        xmlTag_JointReactionTool_replaceForceSet.text = self.myConfig.common_replaceForceSet
        
        #       <!--List of xml files used to construct an force set for the model.-->
        #       <force_set_files> cmc_actuators_gait_23dofs_92muscles_patella.xml loaded_residual_actuators.xml</force_set_files>
        xmlTag_JointReactionTool_forceSetFiles = ET.SubElement(xmlTag_JointReactionTool, "force_set_files")
        xmlTag_JointReactionTool_forceSetFiles.text = self.myConfig.common_forceSetFile
        
        #       <!--Directory used for writing results.-->
        #       <results_directory>C:\Users\david.rusbarsky\Documents\programming\bess\testing\sandbox\TestScript\results_JR</results_directory>
        xmlTag_JointReactionTool_resultsDirectory = ET.SubElement(xmlTag_JointReactionTool, "results_directory")
        xmlTag_JointReactionTool_resultsDirectory.text = self.myConfig.jointReactionTool_resultsDir
        
        #       <!--Output precision.  It is 8 by default.-->
        #       <output_precision>8</output_precision>
        xmlTag_JointReactionTool_outputPrecision = ET.SubElement(xmlTag_JointReactionTool, "output_precision")
        xmlTag_JointReactionTool_outputPrecision.text = self.myConfig.jointReactionTool_outputPrecision
        
        #       <!--Initial time for the simulation.-->
        #       <initial_time>0.5</initial_time>
        xmlTag_JointReactionTool_initialTime = ET.SubElement(xmlTag_JointReactionTool, "initial_time")
        xmlTag_JointReactionTool_initialTime.text = self.myConfig.common_timeRangeFrom
        
        #       <!--Final time for the simulation.-->
        #       <final_time>1.8311</final_time>
        xmlTag_JointReactionTool_finalTime = ET.SubElement(xmlTag_JointReactionTool, "final_time")
        xmlTag_JointReactionTool_finalTime.text = self.myConfig.common_timeRangeTo
        
        #       <!--Flag indicating whether or not to compute equilibrium values for states other than the coordinates or speeds.  For example, equilibrium muscle fiber lengths or muscle forces.-->
        #       <solve_for_equilibrium_for_auxiliary_states>false</solve_for_equilibrium_for_auxiliary_states>
        xmlTag_JointReactionTool_solveForEquilibriumForAuxStates = ET.SubElement(xmlTag_JointReactionTool, "solve_for_equilibrium_for_auxiliary_states")
        xmlTag_JointReactionTool_solveForEquilibriumForAuxStates.text = self.myConfig.jointReactionTool_solveForEquilibriumForAuxStates
        
        #       <!--Maximum number of integrator steps.-->
        #       <maximum_number_of_integrator_steps>20000</maximum_number_of_integrator_steps>
        xmlTag_JointReactionTool_maxNumOfIntegratorSteps = ET.SubElement(xmlTag_JointReactionTool, "maximum_number_of_integrator_steps")
        xmlTag_JointReactionTool_maxNumOfIntegratorSteps.text = self.myConfig.common_integratorMaxSteps
        
        #       <!--Maximum integration step size.-->
        #       <maximum_integrator_step_size>1</maximum_integrator_step_size>
        xmlTag_JointReactionTool_maxIntegratorStepSize = ET.SubElement(xmlTag_JointReactionTool, "maximum_integrator_step_size")
        xmlTag_JointReactionTool_maxIntegratorStepSize.text = self.myConfig.common_integratorMaxStepSize
        
        #       <!--Minimum integration step size.-->
        #       <minimum_integrator_step_size>1e-008</minimum_integrator_step_size>
        xmlTag_JointReactionTool_minIntegratorStepSize = ET.SubElement(xmlTag_JointReactionTool, "minimum_integrator_step_size")
        xmlTag_JointReactionTool_minIntegratorStepSize.text = self.myConfig.common_integratorMinStepSize
        
        #       <!--Integrator error tolerance. When the error is greater, the integrator step size is decreased.-->
        #       <integrator_error_tolerance>1e-005</integrator_error_tolerance>
        xmlTag_JointReactionTool_integratorErrTolerance = ET.SubElement(xmlTag_JointReactionTool, "integrator_error_tolerance")
        xmlTag_JointReactionTool_integratorErrTolerance.text = self.myConfig.common_integratorErrTolerance
        
        #       <!--Set of analyses to be run during the investigation.-->
        #       <AnalysisSet name="Analyses">
        xmlTag_JointReactionTool_analysisSet = ET.SubElement(xmlTag_JointReactionTool, "AnalysisSet", name="Analyses")
        
        #           <objects>
        xmlTag_JointReactionTool_analysisSet_objects = ET.SubElement(xmlTag_JointReactionTool_analysisSet, "objects")
        
        #               <JointReaction name="JointReaction">
        xmlTag_JointReactionTool_analysisSet_objects_jointReaction = ET.SubElement(xmlTag_JointReactionTool_analysisSet_objects, "JointReaction", name="JointReaction")
        
        #                   <!--Flag (true or false) specifying whether whether on. True by default.-->
        #                   <on>true</on>
        xmlTag_JointReactionTool_analysisSet_objects_jointReaction_on = ET.SubElement(xmlTag_JointReactionTool_analysisSet_objects_jointReaction, "on")
        xmlTag_JointReactionTool_analysisSet_objects_jointReaction_on.text = 'true'
        
        #                   <!--Start time.-->
        #                   <start_time>0.5</start_time>
        xmlTag_JointReactionTool_analysisSet_objects_jointReaction_startTime = ET.SubElement(xmlTag_JointReactionTool_analysisSet_objects_jointReaction, "start_time")
        xmlTag_JointReactionTool_analysisSet_objects_jointReaction_startTime.text = self.myConfig.common_timeRangeFrom
        
        #                   <!--End time.-->
        #                   <end_time>1.8311</end_time>
        xmlTag_JointReactionTool_analysisSet_objects_jointReaction_endTime = ET.SubElement(xmlTag_JointReactionTool_analysisSet_objects_jointReaction, "end_time")
        xmlTag_JointReactionTool_analysisSet_objects_jointReaction_endTime.text = self.myConfig.common_timeRangeTo
        
        #                   <!--Specifies how often to store results during a simulation. More specifically, the interval (a positive integer) specifies how many successful integration steps should be taken before results are recorded again.-->
        #                   <step_interval>1</step_interval>
        xmlTag_JointReactionTool_analysisSet_objects_jointReaction_stepInterval = ET.SubElement(xmlTag_JointReactionTool_analysisSet_objects_jointReaction, "step_interval")
        xmlTag_JointReactionTool_analysisSet_objects_jointReaction_stepInterval.text = self.myConfig.common_integratorMinStepSize
        
        #                   <!--Flag (true or false) indicating whether the results are in degrees or not.-->
        #                   <in_degrees>true</in_degrees>
        xmlTag_JointReactionTool_analysisSet_objects_jointReaction_inDegrees = ET.SubElement(xmlTag_JointReactionTool_analysisSet_objects_jointReaction, "in_degrees")
        xmlTag_JointReactionTool_analysisSet_objects_jointReaction_inDegrees.text = self.myConfig.common_inDegrees
        
        #                   <!--The name of a file containing forces storage.If a file name is provided, the applied forces for all actuators will be constructed from the forces_file instead of from the states.  This option should be used to calculated joint loads from static optimization results.-->
        #                   <forces_file />
        xmlTag_JointReactionTool_analysisSet_objects_jointReaction_forcesFile = ET.SubElement(xmlTag_JointReactionTool_analysisSet_objects_jointReaction, "forces_file")
        xmlTag_JointReactionTool_analysisSet_objects_jointReaction_forcesFile.text = self.myConfig.jointReactionTool_analysis_forcesFile
        
        #                   <!--Names of the joints on which to perform the analysis.The key word 'All' indicates that the analysis should be performed for all bodies.-->
        #                   <joint_names> ALL</joint_names>
        xmlTag_JointReactionTool_analysisSet_objects_jointReaction_jointNames = ET.SubElement(xmlTag_JointReactionTool_analysisSet_objects_jointReaction, "joint_names")
        xmlTag_JointReactionTool_analysisSet_objects_jointReaction_jointNames.text = self.myConfig.jointReactionTool_analysis_jointNames
        
        #                   <!--Choice of body (parent or child) for which the reaction loads are calculated.  Child body is default.  If the array has one entry only, that selection is applied to all chosen joints.-->
        #                   <apply_on_bodies> child</apply_on_bodies>
        xmlTag_JointReactionTool_analysisSet_objects_jointReaction_applyOnBodies = ET.SubElement(xmlTag_JointReactionTool_analysisSet_objects_jointReaction, "apply_on_bodies")
        xmlTag_JointReactionTool_analysisSet_objects_jointReaction_applyOnBodies.text = self.myConfig.jointReactionTool_analysis_applyOnBodies
        
        #                   <!--Choice of frame (ground, parent, or child) in which the calculated reactions are expressed.  ground body is default.  If the array has one entry only, that selection is applied to all chosen joints.-->
        #                   <express_in_frame> ground</express_in_frame>
        xmlTag_JointReactionTool_analysisSet_objects_jointReaction_expressInFrame = ET.SubElement(xmlTag_JointReactionTool_analysisSet_objects_jointReaction, "express_in_frame")
        xmlTag_JointReactionTool_analysisSet_objects_jointReaction_expressInFrame.text = self.myConfig.jointReactionTool_analysis_expressInFrame
        
        #               </JointReaction>
        #           </objects>
        #           <groups />
        xmlTag_JointReactionTool_analysisSet_groups = ET.SubElement(xmlTag_JointReactionTool_analysisSet, "groups")
        
        #       </AnalysisSet>
        
        #       <!--Controller objects in the model.-->
        #       <ControllerSet name="Controllers">
        xmlTag_JointReactionTool_controllerSet = ET.SubElement(xmlTag_JointReactionTool, "ControllerSet", name="Controllers")
        
        #           <objects>
        xmlTag_JointReactionTool_controllerSet_objects = ET.SubElement(xmlTag_JointReactionTool_controllerSet, "objects")
        
        #               <ControlSetController>
        xmlTag_JointReactionTool_controllerSet_objects_controlSetController = ET.SubElement(xmlTag_JointReactionTool_controllerSet_objects, "ControlSetController")
        
        #                   <!--A Storage (.sto) or an XML control nodes file containing the controls for this controlSet.-->
        #                   <controls_file>ControlConstraints.xml</controls_file>
        # TODO: The config value may need to change
        xmlTag_JointReactionTool_controllerSet_objects_controlSetController_controlsFile = ET.SubElement(xmlTag_JointReactionTool_controllerSet_objects_controlSetController, "controls_file")
        xmlTag_JointReactionTool_controllerSet_objects_controlSetController_controlsFile.text = self.myConfig.cmcTool_constraintsFile
        
        #               </ControlSetController>
        #           </objects>
        #           <groups />
        xmlTag_JointReactionTool_controllerSet_groups = ET.SubElement(xmlTag_JointReactionTool_controllerSet, "groups")
        
        #       </ControllerSet>
        
        #       <!--XML file (.xml) containing the forces applied to the model as ExternalLoads.-->
        #       <external_loads_file>external_loads.xml</external_loads_file>
        xmlTag_JointReactionTool_externalLoadsFile = ET.SubElement(xmlTag_JointReactionTool, "external_loads_file")
        xmlTag_JointReactionTool_externalLoadsFile.text = self.myConfig.common_externalLoadsSpecFile
        
        #       <!--Storage file (.sto) containing the time history of states for the model. This file often contains multiple rows of data, each row being a time-stamped array of states. The first column contains the time.  The rest of the columns contain the states in the order appropriate for the model. In a storage file, unlike a motion file (.mot), non-uniform time spacing is allowed.  If the user-specified initial time for a simulation does not correspond exactly to one of the time stamps in this file, inerpolation is NOT used because it is sometimes necessary to an exact set of states for analyses.  Instead, the closest earlier set of states is used.-->
        #       <states_file>results_CMC/loadedwalking_subject07_loaded_free_trial02_cmc_states.sto</states_file>
        xmlTag_JointReactionTool_statesFile = ET.SubElement(xmlTag_JointReactionTool, "states_file")
        xmlTag_JointReactionTool_statesFile.text = self.myConfig.jointReactionTool_statesFile
        
        #       <!--Motion file (.mot) or storage file (.sto) containing the time history of the generalized coordinates for the model. These can be specified in place of the states file.-->
        #       <coordinates_file />
        xmlTag_JointReactionTool_coordinatesFile = ET.SubElement(xmlTag_JointReactionTool, "coordinates_file")
        xmlTag_JointReactionTool_coordinatesFile.text = self.myConfig.jointReactionTool_coordinatesFile
        
        #       <!--Storage file (.sto) containing the time history of the generalized speeds for the model. If coordinates_file is used in place of states_file, these can be optionally set as well to give the speeds. If not specified, speeds will be computed from coordinates by differentiation.-->
        #       <speeds_file />
        xmlTag_JointReactionTool_speedsFile = ET.SubElement(xmlTag_JointReactionTool, "speeds_file")
        xmlTag_JointReactionTool_speedsFile.text = self.myConfig.jointReactionTool_speedsFile
        
        #       <!--Low-pass cut-off frequency for filtering the coordinates_file data (currently does not apply to states_file or speeds_file). A negative value results in no filtering. The default value is -1.0, so no filtering.-->
        #       <lowpass_cutoff_frequency_for_coordinates>-1</lowpass_cutoff_frequency_for_coordinates>
        xmlTag_JointReactionTool_lowpassCutoffFreqForCoords = ET.SubElement(xmlTag_JointReactionTool, "lowpass_cutoff_frequency_for_coordinates")
        xmlTag_JointReactionTool_lowpassCutoffFreqForCoords.text = self.myConfig.jointReactionTool_lowpassCutoffFreqForCoords
        
        #   </AnalyzeTool>
        #</OpenSimDocument>
        
        # Fix the indentation
        self.indent(self.xmlTag_OpenSimDocument)
        tree = ET.ElementTree(self.xmlTag_OpenSimDocument)
        
        # Write out the xml file
        xmlFileName = self.myConfig.jrConfigFile
        tree.write(xmlFileName, encoding='utf-8')
        print "Wrote JR xml file"
        
    def indent(self, elem, level=0):
        i = "\n" + level*"  "
        j = "\n" + (level-1)*"  "
        if len(elem):
            if not elem.text or not elem.text.strip():
                elem.text = i + "  "
            if not elem.tail or not elem.tail.strip():
                elem.tail = i
            for subelem in elem:
                self.indent(subelem, level+1)
            if not elem.tail or not elem.tail.strip():
                elem.tail = j
        else:
            if level and (not elem.tail or not elem.tail.strip()):
                elem.tail = j
        return elem        

