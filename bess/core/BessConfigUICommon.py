

################################################################################
#
# This file creates the Configuration UI components and handles user input as
# it relates to BESS configuration.
#
################################################################################
from javax.swing import JFrame, JPanel, JButton, JFileChooser, JTextField, JLabel, JCheckBox, SwingConstants, JTabbedPane
from javax.swing.border import TitledBorder
from java.awt import GridLayout, FlowLayout, Color, GridBagLayout, GridBagConstraints, CardLayout

from xml.dom.minidom import parse, parseString, getDOMImplementation, Document
import xml.etree.cElementTree as ET

import os.path

from core.BessConfigUICardBase import bessCardBase
from core.BessUIFileChooser import bessFileChooser

from utilities import toolsCommon

'''
The bessConfigUICommonCard is used as the Common Card UI component
'''
class bessConfigUICommonCard(bessCardBase):
    def __init__(self, myConfig, cardCreators):
        self.myConfig = myConfig
        self.myCardCreators = cardCreators
        
        # Create the UI components
        
        self.commonTabs = JTabbedPane(SwingConstants.TOP, JTabbedPane.SCROLL_TAB_LAYOUT)
        
        # General tab
        generalTab = JPanel(GridBagLayout())
        generalTab_GBLC = GridBagConstraints()
        generalTab_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        ## Root file path
        generalTab_rootDirLabel = JLabel("Root directory for simulation run(s) ", SwingConstants.RIGHT)
        self.generalTab_rootDirTextField = JTextField("", focusLost=self.setRootDirEvent)
        generalTab_rootDirFileChooserButton = JButton("...", actionPerformed = self.selectRootDir)
                
        generalTab_rootDirPanel = JPanel(GridBagLayout())
        generalTab_rootDirPanel_GBLC = GridBagConstraints()
        generalTab_rootDirPanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        generalTab_rootDirPanel_GBLC.weightx = 0.99
        generalTab_rootDirPanel_GBLC.gridx = 0
        generalTab_rootDirPanel_GBLC.gridy = 0
        generalTab_rootDirPanel.add(self.generalTab_rootDirTextField, generalTab_rootDirPanel_GBLC)
        generalTab_rootDirPanel_GBLC.weightx = 0.01
        generalTab_rootDirPanel_GBLC.gridx = 1  # TODO: Try using GridBagConstraints.RELATIVE here?
        generalTab_rootDirPanel_GBLC.gridy = 0
        generalTab_rootDirPanel.add(generalTab_rootDirFileChooserButton, generalTab_rootDirPanel_GBLC)
        
        ### Scale file path
        #generalTab_scaleFileLabel = JLabel("Scale configuration file ", SwingConstants.RIGHT)
        #self.generalTab_scaleFileTextField = JTextField("", focusLost=self.setScaleFileEvent)
        #generalTab_scaleFileFileChooserButton = JButton("...", actionPerformed = self.selectScaleFile)
        #
        #generalTab_scaleFilePanel = JPanel(GridBagLayout())
        #generalTab_scaleFilePanel_GBLC = GridBagConstraints()
        #generalTab_scaleFilePanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        #
        #generalTab_scaleFilePanel_GBLC.weightx = 0.99
        #generalTab_scaleFilePanel_GBLC.gridx = 0
        #generalTab_scaleFilePanel_GBLC.gridy = 0
        #generalTab_scaleFilePanel.add(self.generalTab_scaleFileTextField, generalTab_scaleFilePanel_GBLC)
        #generalTab_scaleFilePanel_GBLC.weightx = 0.01
        #generalTab_scaleFilePanel_GBLC.gridx = 1  # TODO: Try using GridBagConstraints.RELATIVE here?
        #generalTab_scaleFilePanel_GBLC.gridy = 0
        #generalTab_scaleFilePanel.add(generalTab_scaleFileFileChooserButton, generalTab_scaleFilePanel_GBLC)
        
        ## Model file path
        generalTab_modelFileLabel = JLabel("Model file path ", SwingConstants.RIGHT)
        self.generalTab_modelFileTextField = JTextField("", focusLost=self.setModelFileEvent)
        generalTab_modelFileFileChooserButton = JButton("...", actionPerformed = self.selectModelFile)
        
        generalTab_modelFilePanel = JPanel(GridBagLayout())
        generalTab_modelFilePanel_GBLC = GridBagConstraints()
        generalTab_modelFilePanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        generalTab_modelFilePanel_GBLC.weightx = 0.99
        generalTab_modelFilePanel_GBLC.gridx = 0
        generalTab_modelFilePanel_GBLC.gridy = 0
        generalTab_modelFilePanel.add(self.generalTab_modelFileTextField, generalTab_modelFilePanel_GBLC)
        generalTab_modelFilePanel_GBLC.weightx = 0.01
        generalTab_modelFilePanel_GBLC.gridx = 1
        generalTab_modelFilePanel_GBLC.gridy = 0
        generalTab_modelFilePanel.add(generalTab_modelFileFileChooserButton, generalTab_modelFilePanel_GBLC)
        
        ## Motion file path
        generalTab_motionFileLabel = JLabel("Motion file path ", SwingConstants.RIGHT)
        self.generalTab_motionFileTextField = JTextField("", focusLost=self.setMotionFileEvent)
        generalTab_motionFileFileChooserButton = JButton("...", actionPerformed = self.selectMotionFile)
        
        generalTab_motionFilePanel = JPanel(GridBagLayout())
        generalTab_motionFilePanel_GBLC = GridBagConstraints()
        generalTab_motionFilePanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        generalTab_motionFilePanel_GBLC.weightx = 0.99
        generalTab_motionFilePanel_GBLC.gridx = 0
        generalTab_motionFilePanel_GBLC.gridy = 0
        generalTab_motionFilePanel.add(self.generalTab_motionFileTextField, generalTab_motionFilePanel_GBLC)
        generalTab_motionFilePanel_GBLC.weightx = 0.01
        generalTab_motionFilePanel_GBLC.gridx = 1
        generalTab_motionFilePanel_GBLC.gridy = 0
        generalTab_motionFilePanel.add(generalTab_motionFileFileChooserButton, generalTab_motionFilePanel_GBLC)
        
        ## Ground reaction forces file path
        generalTab_groundReactionsFileLabel = JLabel("Ground reaction forces file path ", SwingConstants.RIGHT)
        self.generalTab_groundReactionsFileTextField = JTextField("", focusLost=self.setGroundReactionsFileEvent)
        generalTab_groundReactionsFileFileChooserButton = JButton("...", actionPerformed = self.selectGroundReactionsFile)
        
        generalTab_groundReactionsFilePanel = JPanel(GridBagLayout())
        generalTab_groundReactionsFilePanel_GBLC = GridBagConstraints()
        generalTab_groundReactionsFilePanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        generalTab_groundReactionsFilePanel_GBLC.weightx = 0.99
        generalTab_groundReactionsFilePanel_GBLC.gridx = 0
        generalTab_groundReactionsFilePanel_GBLC.gridy = 0
        generalTab_groundReactionsFilePanel.add(self.generalTab_groundReactionsFileTextField, generalTab_groundReactionsFilePanel_GBLC)
        generalTab_groundReactionsFilePanel_GBLC.weightx = 0.01
        generalTab_groundReactionsFilePanel_GBLC.gridx = 1
        generalTab_groundReactionsFilePanel_GBLC.gridy = 0
        generalTab_groundReactionsFilePanel.add(generalTab_groundReactionsFileFileChooserButton, generalTab_groundReactionsFilePanel_GBLC)
        
        ## Time range
        generalTab_timeRangeLabel = JLabel("Time range to process ", SwingConstants.RIGHT)
        self.generalTab_timeRangeFromTextField = JTextField("", focusLost=self.setTimeRangeFromEvent)
        generalTab_timeRangeToLabel = JLabel(" to ", SwingConstants.RIGHT)
        self.generalTab_timeRangeToTextField = JTextField("", focusLost=self.setTimeRangeToEvent)
        
        generalTab_timeRangePanel = JPanel(GridBagLayout())
        generalTab_timeRangePanel_GBLC = GridBagConstraints()
        generalTab_timeRangePanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        generalTab_timeRangePanel_GBLC.weightx = 0.49
        generalTab_timeRangePanel_GBLC.gridx = 0
        generalTab_timeRangePanel_GBLC.gridy = 0
        generalTab_timeRangePanel.add(self.generalTab_timeRangeFromTextField, generalTab_timeRangePanel_GBLC)
        generalTab_timeRangePanel_GBLC.weightx = 0.02
        generalTab_timeRangePanel_GBLC.gridx = 1
        generalTab_timeRangePanel_GBLC.gridy = 0
        generalTab_timeRangePanel.add(generalTab_timeRangeToLabel, generalTab_timeRangePanel_GBLC)
        generalTab_timeRangePanel_GBLC.weightx = 0.49
        generalTab_timeRangePanel_GBLC.gridx = 2
        generalTab_timeRangePanel_GBLC.gridy = 0
        generalTab_timeRangePanel.add(self.generalTab_timeRangeToTextField, generalTab_timeRangePanel_GBLC)
        
        # Finish constructing the General tab
        gridyVal = 0
        generalTab_GBLC.weightx = 0.1
        generalTab_GBLC.gridx = 0
        generalTab_GBLC.gridy = gridyVal
        generalTab.add(JLabel(""), generalTab_GBLC)
        generalTab_GBLC.weightx = 0.40
        generalTab_GBLC.gridx = 1
        generalTab_GBLC.gridy = gridyVal
        generalTab.add(generalTab_rootDirLabel, generalTab_GBLC)
        generalTab_GBLC.weightx = 0.4
        generalTab_GBLC.gridx = 2
        generalTab_GBLC.gridy = gridyVal
        generalTab.add(generalTab_rootDirPanel, generalTab_GBLC)
        generalTab_GBLC.weightx = 0.1
        generalTab_GBLC.gridx = 3
        generalTab_GBLC.gridy = gridyVal
        generalTab.add(JLabel(""), generalTab_GBLC)
        
        gridyVal = gridyVal + 1
        generalTab_GBLC.weightx = 0.1
        generalTab_GBLC.gridx = 0
        generalTab_GBLC.gridy = gridyVal
        generalTab.add(JLabel(""), generalTab_GBLC)
        generalTab_GBLC.weightx = 0.40
        generalTab_GBLC.gridx = 1
        generalTab_GBLC.gridy = gridyVal
        generalTab.add(generalTab_modelFileLabel, generalTab_GBLC)
        generalTab_GBLC.weightx = 0.4
        generalTab_GBLC.gridx = 2
        generalTab_GBLC.gridy = gridyVal
        generalTab.add(generalTab_modelFilePanel, generalTab_GBLC)
        generalTab_GBLC.weightx = 0.1
        generalTab_GBLC.gridx = 3
        generalTab_GBLC.gridy = gridyVal
        generalTab.add(JLabel(""), generalTab_GBLC)
        
        gridyVal = gridyVal + 1
        generalTab_GBLC.weightx = 0.1
        generalTab_GBLC.gridx = 0
        generalTab_GBLC.gridy = gridyVal
        generalTab.add(JLabel(""), generalTab_GBLC)
        generalTab_GBLC.weightx = 0.40
        generalTab_GBLC.gridx = 1
        generalTab_GBLC.gridy = gridyVal
        generalTab.add(generalTab_motionFileLabel, generalTab_GBLC)
        generalTab_GBLC.weightx = 0.4
        generalTab_GBLC.gridx = 2
        generalTab_GBLC.gridy = gridyVal
        generalTab.add(generalTab_motionFilePanel, generalTab_GBLC)
        generalTab_GBLC.weightx = 0.1
        generalTab_GBLC.gridx = 3
        generalTab_GBLC.gridy = gridyVal
        generalTab.add(JLabel(""), generalTab_GBLC)
        
        #gridyVal = gridyVal + 1
        #generalTab_GBLC.weightx = 0.1
        #generalTab_GBLC.gridx = 0
        #generalTab_GBLC.gridy = gridyVal
        #generalTab.add(JLabel(""), generalTab_GBLC)
        #generalTab_GBLC.weightx = 0.40
        #generalTab_GBLC.gridx = 1
        #generalTab_GBLC.gridy = gridyVal
        #generalTab.add(generalTab_scaleFileLabel, generalTab_GBLC)
        #generalTab_GBLC.weightx = 0.4
        #generalTab_GBLC.gridx = 2
        #generalTab_GBLC.gridy = gridyVal
        #generalTab.add(generalTab_scaleFilePanel, generalTab_GBLC)
        #generalTab_GBLC.weightx = 0.1
        #generalTab_GBLC.gridx = 3
        #generalTab_GBLC.gridy = gridyVal
        #generalTab.add(JLabel(""), generalTab_GBLC)
        
        gridyVal = gridyVal + 1
        generalTab_GBLC.weightx = 0.1
        generalTab_GBLC.gridx = 0
        generalTab_GBLC.gridy = gridyVal
        generalTab.add(JLabel(""), generalTab_GBLC)
        generalTab_GBLC.weightx = 0.40
        generalTab_GBLC.gridx = 1
        generalTab_GBLC.gridy = gridyVal
        generalTab.add(generalTab_groundReactionsFileLabel, generalTab_GBLC)
        generalTab_GBLC.weightx = 0.4
        generalTab_GBLC.gridx = 2
        generalTab_GBLC.gridy = gridyVal
        generalTab.add(generalTab_groundReactionsFilePanel, generalTab_GBLC)
        generalTab_GBLC.weightx = 0.1
        generalTab_GBLC.gridx = 3
        generalTab_GBLC.gridy = gridyVal
        generalTab.add(JLabel(""), generalTab_GBLC)
        
        gridyVal = gridyVal + 1
        generalTab_GBLC.weightx = 0.1
        generalTab_GBLC.gridx = 0
        generalTab_GBLC.gridy = gridyVal
        generalTab.add(JLabel(""), generalTab_GBLC)
        generalTab_GBLC.weightx = 0.40
        generalTab_GBLC.gridx = 1
        generalTab_GBLC.gridy = gridyVal
        generalTab.add(generalTab_timeRangeLabel, generalTab_GBLC)
        generalTab_GBLC.weightx = 0.4
        generalTab_GBLC.gridx = 2
        generalTab_GBLC.gridy = gridyVal
        generalTab.add(generalTab_timeRangePanel, generalTab_GBLC)
        generalTab_GBLC.weightx = 0.1
        generalTab_GBLC.gridx = 3
        generalTab_GBLC.gridy = gridyVal
        generalTab.add(JLabel(""), generalTab_GBLC)
        
        self.commonTabs.addTab("General", generalTab)
        
        
        # Main Settings tab
        mainSettingsTab = JPanel(GridBagLayout())
        mainSettingsTab_GBLC = GridBagConstraints()
        mainSettingsTab_GBLC.fill = GridBagConstraints.HORIZONTAL
                
        ## Input group
        mainSettingsTab_inputPanel = JPanel(GridBagLayout())
        mainSettingsTab_inputPanel_GBLC = GridBagConstraints()
        mainSettingsTab_inputPanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        mainSettingsTab_inputPanel.setBorder(TitledBorder("Input"))
        
        ### Desired kinematics file path
        mainSettingsTab_inputPanel_desiredKinematicsFileLabel = JLabel("Desired kinematics ", SwingConstants.RIGHT)
        self.mainSettingsTab_inputPanel_desiredKinematicsFileTextField = JTextField("", focusLost=self.setDesiredKinematicsFileEvent)
        mainSettingsTab_inputPanel_desiredKinematicsFileFileChooserButton = JButton("...", actionPerformed = self.selectDesiredKinematicsFile)
        
        mainSettingsTab_inputPanel_desiredKinematicsFilePanel = JPanel(GridBagLayout())
        mainSettingsTab_inputPanel_desiredKinematicsFilePanel_GBLC = GridBagConstraints()
        mainSettingsTab_inputPanel_desiredKinematicsFilePanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        mainSettingsTab_inputPanel_desiredKinematicsFilePanel_GBLC.weightx = 0.99
        mainSettingsTab_inputPanel_desiredKinematicsFilePanel_GBLC.gridx = 0
        mainSettingsTab_inputPanel_desiredKinematicsFilePanel_GBLC.gridy = 0
        mainSettingsTab_inputPanel_desiredKinematicsFilePanel.add(self.mainSettingsTab_inputPanel_desiredKinematicsFileTextField, mainSettingsTab_inputPanel_desiredKinematicsFilePanel_GBLC)
        mainSettingsTab_inputPanel_desiredKinematicsFilePanel_GBLC.weightx = 0.01
        mainSettingsTab_inputPanel_desiredKinematicsFilePanel_GBLC.gridx = 1
        mainSettingsTab_inputPanel_desiredKinematicsFilePanel_GBLC.gridy = 0
        mainSettingsTab_inputPanel_desiredKinematicsFilePanel.add(mainSettingsTab_inputPanel_desiredKinematicsFileFileChooserButton, mainSettingsTab_inputPanel_desiredKinematicsFilePanel_GBLC)
        
        ### Filter Kinematics
        self.mainSettingsTab_inputPanel_filterKinematicsCheckBox = JCheckBox("Filter Kinematics", actionPerformed=self.onFilterKinematicsCheckBoxSelect)
        self.mainSettingsTab_inputPanel_filterKinematicsTextField = JTextField("", focusLost=self.setFilterKinematicsEvent)
        mainSettingsTab_inputPanel_filterKinematicsTextFieldUnitsLabel = JLabel(" Hz")
        
        mainSettingsTab_inputPanel_filterKinematicsPanel = JPanel(GridBagLayout())
        mainSettingsTab_inputPanel_filterKinematicsPanel_GBLC = GridBagConstraints()
        mainSettingsTab_inputPanel_filterKinematicsPanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        mainSettingsTab_inputPanel_filterKinematicsPanel_GBLC.weightx = 0.01
        mainSettingsTab_inputPanel_filterKinematicsPanel_GBLC.gridx = 0
        mainSettingsTab_inputPanel_filterKinematicsPanel_GBLC.gridy = 0
        mainSettingsTab_inputPanel_filterKinematicsPanel.add(self.mainSettingsTab_inputPanel_filterKinematicsCheckBox, mainSettingsTab_inputPanel_filterKinematicsPanel_GBLC)
        mainSettingsTab_inputPanel_filterKinematicsPanel_GBLC.weightx = 0.90
        mainSettingsTab_inputPanel_filterKinematicsPanel_GBLC.gridx = 1
        mainSettingsTab_inputPanel_filterKinematicsPanel_GBLC.gridy = 0
        mainSettingsTab_inputPanel_filterKinematicsPanel.add(self.mainSettingsTab_inputPanel_filterKinematicsTextField, mainSettingsTab_inputPanel_filterKinematicsPanel_GBLC)
        mainSettingsTab_inputPanel_filterKinematicsPanel_GBLC.weightx = 0.09
        mainSettingsTab_inputPanel_filterKinematicsPanel_GBLC.gridx = 2
        mainSettingsTab_inputPanel_filterKinematicsPanel_GBLC.gridy = 0
        mainSettingsTab_inputPanel_filterKinematicsPanel.add(mainSettingsTab_inputPanel_filterKinematicsTextFieldUnitsLabel, mainSettingsTab_inputPanel_filterKinematicsPanel_GBLC)
        
        ### Tracking tasks
        mainSettingsTab_inputPanel_trackingTasksFileLabel = JLabel("Tracking tasks ", SwingConstants.RIGHT)
        self.mainSettingsTab_inputPanel_trackingTasksFileTextField = JTextField("", focusLost=self.setTrackingTasksFileEvent)
        mainSettingsTab_inputPanel_trackingTasksFileFileChooserButton = JButton("...", actionPerformed = self.selectTrackingTasksFile)
        
        mainSettingsTab_inputPanel_trackingTasksFilePanel = JPanel(GridBagLayout())
        mainSettingsTab_inputPanel_trackingTasksFilePanel_GBLC = GridBagConstraints()
        mainSettingsTab_inputPanel_trackingTasksFilePanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        mainSettingsTab_inputPanel_trackingTasksFilePanel_GBLC.weightx = 0.99
        mainSettingsTab_inputPanel_trackingTasksFilePanel_GBLC.gridx = 0
        mainSettingsTab_inputPanel_trackingTasksFilePanel_GBLC.gridy = 0
        mainSettingsTab_inputPanel_trackingTasksFilePanel.add(self.mainSettingsTab_inputPanel_trackingTasksFileTextField, mainSettingsTab_inputPanel_trackingTasksFilePanel_GBLC)
        mainSettingsTab_inputPanel_trackingTasksFilePanel_GBLC.weightx = 0.01
        mainSettingsTab_inputPanel_trackingTasksFilePanel_GBLC.gridx = 1
        mainSettingsTab_inputPanel_trackingTasksFilePanel_GBLC.gridy = 0
        mainSettingsTab_inputPanel_trackingTasksFilePanel.add(mainSettingsTab_inputPanel_trackingTasksFileFileChooserButton, mainSettingsTab_inputPanel_trackingTasksFilePanel_GBLC)
        
        ### TODO: Filter motion file coordinates
        
        ## Finish creating input group
        mainSettingsTab_inputPanel_GBLC.weightx = 0.2
        mainSettingsTab_inputPanel_GBLC.gridx = 0
        mainSettingsTab_inputPanel_GBLC.gridy = 0
        mainSettingsTab_inputPanel.add(mainSettingsTab_inputPanel_desiredKinematicsFileLabel, mainSettingsTab_inputPanel_GBLC)
        mainSettingsTab_inputPanel_GBLC.weightx = 0.8
        mainSettingsTab_inputPanel_GBLC.gridx = 1
        mainSettingsTab_inputPanel_GBLC.gridy = 0
        mainSettingsTab_inputPanel.add(mainSettingsTab_inputPanel_desiredKinematicsFilePanel, mainSettingsTab_inputPanel_GBLC)
        
        mainSettingsTab_inputPanel_GBLC.weightx = 0.2
        mainSettingsTab_inputPanel_GBLC.gridx = 0
        mainSettingsTab_inputPanel_GBLC.gridy = 1
        mainSettingsTab_inputPanel.add(JLabel(" "), mainSettingsTab_inputPanel_GBLC)
        mainSettingsTab_inputPanel_GBLC.weightx = 0.8
        mainSettingsTab_inputPanel_GBLC.gridx = 1
        mainSettingsTab_inputPanel_GBLC.gridy = 1
        mainSettingsTab_inputPanel.add(mainSettingsTab_inputPanel_filterKinematicsPanel, mainSettingsTab_inputPanel_GBLC)
        
        mainSettingsTab_inputPanel_GBLC.weightx = 0.2
        mainSettingsTab_inputPanel_GBLC.gridx = 0
        mainSettingsTab_inputPanel_GBLC.gridy = 2
        mainSettingsTab_inputPanel.add(mainSettingsTab_inputPanel_trackingTasksFileLabel, mainSettingsTab_inputPanel_GBLC)
        mainSettingsTab_inputPanel_GBLC.weightx = 0.8
        mainSettingsTab_inputPanel_GBLC.gridx = 1
        mainSettingsTab_inputPanel_GBLC.gridy = 2
        mainSettingsTab_inputPanel.add(mainSettingsTab_inputPanel_trackingTasksFilePanel, mainSettingsTab_inputPanel_GBLC)
        
        # Finish creating the Main Settings tab
        mainSettingsTab_GBLC.weightx = 0.1
        mainSettingsTab_GBLC.gridx = 0
        mainSettingsTab_GBLC.gridy = 0
        mainSettingsTab.add(JLabel(" "), mainSettingsTab_GBLC)
        mainSettingsTab_GBLC.weightx = 0.80
        mainSettingsTab_GBLC.gridx = 1
        mainSettingsTab_GBLC.gridy = 0
        mainSettingsTab.add(mainSettingsTab_inputPanel, mainSettingsTab_GBLC)
        mainSettingsTab_GBLC.weightx = 0.1
        mainSettingsTab_GBLC.gridx = 2
        mainSettingsTab_GBLC.gridy = 0
        mainSettingsTab.add(JLabel(" "), mainSettingsTab_GBLC)
        
        self.commonTabs.addTab("Main Settings", mainSettingsTab)
        
        
        # Actuators and External Loads tab
        ActuatorsExternalLoadsTab = JPanel(GridBagLayout())
        ActuatorsExternalLoadsTab_GBLC = GridBagConstraints()
        ActuatorsExternalLoadsTab_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        # Actuators group
        ActuatorsExternalLoadsTab_actuatorsPanel = JPanel(GridBagLayout())
        ActuatorsExternalLoadsTab_actuatorsPanel_GBLC = GridBagConstraints()
        ActuatorsExternalLoadsTab_actuatorsPanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        ActuatorsExternalLoadsTab_actuatorsPanel.setBorder(TitledBorder("Actuators"))
        
        ## Additional force set files
        ActuatorsExternalLoadsTab_actuatorsPanel_forceSetFileLabel = JLabel("Additional force set files ", SwingConstants.RIGHT)
        self.ActuatorsExternalLoadsTab_actuatorsPanel_forceSetFileTextField = JTextField("", focusLost=self.setForceSetFileEvent)
        ActuatorsExternalLoadsTab_actuatorsPanel_forceSetFileFileChooserButton = JButton("...", actionPerformed = self.selectForceSetFile)
        
        ActuatorsExternalLoadsTab_actuatorsPanel_forceSetFilePanel = JPanel(GridBagLayout())
        ActuatorsExternalLoadsTab_actuatorsPanel_forceSetFilePanel_GBLC = GridBagConstraints()
        ActuatorsExternalLoadsTab_actuatorsPanel_forceSetFilePanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        ActuatorsExternalLoadsTab_actuatorsPanel_forceSetFilePanel_GBLC.weightx = 0.99
        ActuatorsExternalLoadsTab_actuatorsPanel_forceSetFilePanel_GBLC.gridx = 0
        ActuatorsExternalLoadsTab_actuatorsPanel_forceSetFilePanel_GBLC.gridy = 0
        ActuatorsExternalLoadsTab_actuatorsPanel_forceSetFilePanel.add(self.ActuatorsExternalLoadsTab_actuatorsPanel_forceSetFileTextField, ActuatorsExternalLoadsTab_actuatorsPanel_forceSetFilePanel_GBLC)
        ActuatorsExternalLoadsTab_actuatorsPanel_forceSetFilePanel_GBLC.weightx = 0.01
        ActuatorsExternalLoadsTab_actuatorsPanel_forceSetFilePanel_GBLC.gridx = 1
        ActuatorsExternalLoadsTab_actuatorsPanel_forceSetFilePanel_GBLC.gridy = 0
        ActuatorsExternalLoadsTab_actuatorsPanel_forceSetFilePanel.add(ActuatorsExternalLoadsTab_actuatorsPanel_forceSetFileFileChooserButton, ActuatorsExternalLoadsTab_actuatorsPanel_forceSetFilePanel_GBLC)
        
        ## Force set options
        self.ActuatorsExternalLoadsTab_actuatorsPanel_replaceModelForceSetCheckBox = JCheckBox("Replace model's force set", actionPerformed=self.setReplaceModelForceSetEvent)
        
        ## Finish creating actuators group
        ActuatorsExternalLoadsTab_actuatorsPanel_GBLC.weightx = 0.2
        ActuatorsExternalLoadsTab_actuatorsPanel_GBLC.gridx = 0
        ActuatorsExternalLoadsTab_actuatorsPanel_GBLC.gridy = 0
        ActuatorsExternalLoadsTab_actuatorsPanel.add(ActuatorsExternalLoadsTab_actuatorsPanel_forceSetFileLabel, ActuatorsExternalLoadsTab_actuatorsPanel_GBLC)
        ActuatorsExternalLoadsTab_actuatorsPanel_GBLC.weightx = 0.8
        ActuatorsExternalLoadsTab_actuatorsPanel_GBLC.gridx = 1
        ActuatorsExternalLoadsTab_actuatorsPanel_GBLC.gridy = 0
        ActuatorsExternalLoadsTab_actuatorsPanel.add(ActuatorsExternalLoadsTab_actuatorsPanel_forceSetFilePanel, ActuatorsExternalLoadsTab_actuatorsPanel_GBLC)
        ActuatorsExternalLoadsTab_actuatorsPanel_GBLC.weightx = 1.0
        ActuatorsExternalLoadsTab_actuatorsPanel_GBLC.gridx = 1
        ActuatorsExternalLoadsTab_actuatorsPanel_GBLC.gridy = 1
        ActuatorsExternalLoadsTab_actuatorsPanel.add(self.ActuatorsExternalLoadsTab_actuatorsPanel_replaceModelForceSetCheckBox, ActuatorsExternalLoadsTab_actuatorsPanel_GBLC)
        
        # External Loads group
        ActuatorsExternalLoadsTab_externalLoadsPanel = JPanel(GridBagLayout())
        ActuatorsExternalLoadsTab_externalLoadsPanel_GBLC = GridBagConstraints()
        ActuatorsExternalLoadsTab_externalLoadsPanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        ActuatorsExternalLoadsTab_externalLoadsPanel.setBorder(TitledBorder("External loads"))
        
        ## Enable
        self.ActuatorsExternalLoadsTab_externalLoadsPanel_externalLoadsCheckBox = JCheckBox("Use external loads", actionPerformed=self.onExternalLoadsCheckBoxSelect)
        
        ## External loads specification file
        ActuatorsExternalLoadsTab_externalLoadsPanel_externalLoadsSpecFileLabel = JLabel("External loads specification file ", SwingConstants.RIGHT)
        self.ActuatorsExternalLoadsTab_externalLoadsPanel_externalLoadsSpecFileTextField = JTextField("", focusLost=self.setExternalLoadsSpecFileEvent)
        ActuatorsExternalLoadsTab_externalLoadsPanel_externalLoadsSpecFileFileChooserButton = JButton("...", actionPerformed = self.selectExternalLoadsSpecFile)
        
        ActuatorsExternalLoadsTab_externalLoadsPanel_externalLoadsSpecFilePanel = JPanel(GridBagLayout())
        ActuatorsExternalLoadsTab_externalLoadsPanel_externalLoadsSpecFilePanel_GBLC = GridBagConstraints()
        ActuatorsExternalLoadsTab_externalLoadsPanel_externalLoadsSpecFilePanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        ActuatorsExternalLoadsTab_externalLoadsPanel_externalLoadsSpecFilePanel_GBLC.weightx = 0.99
        ActuatorsExternalLoadsTab_externalLoadsPanel_externalLoadsSpecFilePanel_GBLC.gridx = 0
        ActuatorsExternalLoadsTab_externalLoadsPanel_externalLoadsSpecFilePanel_GBLC.gridy = 0
        ActuatorsExternalLoadsTab_externalLoadsPanel_externalLoadsSpecFilePanel.add(self.ActuatorsExternalLoadsTab_externalLoadsPanel_externalLoadsSpecFileTextField, ActuatorsExternalLoadsTab_externalLoadsPanel_externalLoadsSpecFilePanel_GBLC)
        ActuatorsExternalLoadsTab_externalLoadsPanel_externalLoadsSpecFilePanel_GBLC.weightx = 0.01
        ActuatorsExternalLoadsTab_externalLoadsPanel_externalLoadsSpecFilePanel_GBLC.gridx = 1
        ActuatorsExternalLoadsTab_externalLoadsPanel_externalLoadsSpecFilePanel_GBLC.gridy = 0
        ActuatorsExternalLoadsTab_externalLoadsPanel_externalLoadsSpecFilePanel.add(ActuatorsExternalLoadsTab_externalLoadsPanel_externalLoadsSpecFileFileChooserButton, ActuatorsExternalLoadsTab_externalLoadsPanel_externalLoadsSpecFilePanel_GBLC)
        
        # Finish creating external loads group
        ActuatorsExternalLoadsTab_externalLoadsPanel_GBLC.weightx = 1.0
        ActuatorsExternalLoadsTab_externalLoadsPanel_GBLC.gridx = 0
        ActuatorsExternalLoadsTab_externalLoadsPanel_GBLC.gridy = 0
        ActuatorsExternalLoadsTab_externalLoadsPanel.add(self.ActuatorsExternalLoadsTab_externalLoadsPanel_externalLoadsCheckBox, ActuatorsExternalLoadsTab_externalLoadsPanel_GBLC)
        ActuatorsExternalLoadsTab_externalLoadsPanel_GBLC.weightx = 0.2
        ActuatorsExternalLoadsTab_externalLoadsPanel_GBLC.gridx = 0
        ActuatorsExternalLoadsTab_externalLoadsPanel_GBLC.gridy = 1
        ActuatorsExternalLoadsTab_externalLoadsPanel.add(ActuatorsExternalLoadsTab_externalLoadsPanel_externalLoadsSpecFileLabel, ActuatorsExternalLoadsTab_externalLoadsPanel_GBLC)
        ActuatorsExternalLoadsTab_externalLoadsPanel_GBLC.weightx = 0.8
        ActuatorsExternalLoadsTab_externalLoadsPanel_GBLC.gridx = 1
        ActuatorsExternalLoadsTab_externalLoadsPanel_GBLC.gridy = 1
        ActuatorsExternalLoadsTab_externalLoadsPanel.add(ActuatorsExternalLoadsTab_externalLoadsPanel_externalLoadsSpecFilePanel, ActuatorsExternalLoadsTab_externalLoadsPanel_GBLC)
        
        # Finish creating the Actuators and External Loads tab
        ActuatorsExternalLoadsTab_GBLC.weightx = 0.1
        ActuatorsExternalLoadsTab_GBLC.gridx = 0
        ActuatorsExternalLoadsTab_GBLC.gridy = 0
        ActuatorsExternalLoadsTab.add(JLabel(""), ActuatorsExternalLoadsTab_GBLC)
        ActuatorsExternalLoadsTab_GBLC.weightx = 0.8
        ActuatorsExternalLoadsTab_GBLC.gridx = 0
        ActuatorsExternalLoadsTab_GBLC.gridy = 0
        ActuatorsExternalLoadsTab.add(ActuatorsExternalLoadsTab_actuatorsPanel, ActuatorsExternalLoadsTab_GBLC)
        ActuatorsExternalLoadsTab_GBLC.weightx = 0.1
        ActuatorsExternalLoadsTab_GBLC.gridx = 0
        ActuatorsExternalLoadsTab_GBLC.gridy = 0
        ActuatorsExternalLoadsTab.add(JLabel(""), ActuatorsExternalLoadsTab_GBLC)
        
        ActuatorsExternalLoadsTab_GBLC.weightx = 0.1
        ActuatorsExternalLoadsTab_GBLC.gridx = 0
        ActuatorsExternalLoadsTab_GBLC.gridy = 0
        ActuatorsExternalLoadsTab.add(JLabel(""), ActuatorsExternalLoadsTab_GBLC)
        ActuatorsExternalLoadsTab_GBLC.weightx = 0.8
        ActuatorsExternalLoadsTab_GBLC.gridx = 0
        ActuatorsExternalLoadsTab_GBLC.gridy = 1
        ActuatorsExternalLoadsTab.add(ActuatorsExternalLoadsTab_externalLoadsPanel, ActuatorsExternalLoadsTab_GBLC)
        ActuatorsExternalLoadsTab_GBLC.weightx = 0.1
        ActuatorsExternalLoadsTab_GBLC.gridx = 0
        ActuatorsExternalLoadsTab_GBLC.gridy = 0
        ActuatorsExternalLoadsTab.add(JLabel(""), ActuatorsExternalLoadsTab_GBLC)
        
        self.commonTabs.addTab("Actuators and External Loads", ActuatorsExternalLoadsTab)
        
        
        # Integrator Settings tab
        integratorSettingsTab = JPanel(GridBagLayout())
        integratorSettingsTab_GBLC = GridBagConstraints()
        integratorSettingsTab_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        # Integrator settings group
        integratorSettingsTab_integratorSettingsPanel = JPanel(GridBagLayout())
        integratorSettingsTab_integratorSettingsPanel_GBLC = GridBagConstraints()
        integratorSettingsTab_integratorSettingsPanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        integratorSettingsTab_integratorSettingsPanel.setBorder(TitledBorder("Integrator Settings"))
        
        ## Maximum number of steps
        integratorSettingsTab_integratorSettingsPanel_maxStepsLabel = JLabel("Maximum number of steps ", SwingConstants.RIGHT)
        self.integratorSettingsTab_integratorSettingsPanel_maxStepsTextField = JTextField("", focusLost=self.setIntegratorMaxStepsEvent)
        
        ## Maximum step size
        integratorSettingsTab_integratorSettingsPanel_maxStepSizeLabel = JLabel("Maximum step size ", SwingConstants.RIGHT)
        self.integratorSettingsTab_integratorSettingsPanel_maxStepSizeTextField = JTextField("", focusLost=self.setIntegratorMaxStepSizeEvent)
        
        ## Minimum step size
        integratorSettingsTab_integratorSettingsPanel_minStepSizeLabel = JLabel("Minimum step size ", SwingConstants.RIGHT)
        self.integratorSettingsTab_integratorSettingsPanel_minStepSizeTextField = JTextField("", focusLost=self.setIntegratorMinStepSizeEvent)
        
        ## Integrator error tolerance
        integratorSettingsTab_integratorSettingsPanel_integratorErrToleranceLabel = JLabel("Integrator error tolerance ", SwingConstants.RIGHT)
        self.integratorSettingsTab_integratorSettingsPanel_integratorErrToleranceTextField = JTextField("", focusLost=self.setIntegratorErrToleranceEvent)
        
        # Finish creating integrator settings group
        integratorSettingsTab_integratorSettingsPanel_GBLC.weightx = 0.4
        integratorSettingsTab_integratorSettingsPanel_GBLC.gridx = 0
        integratorSettingsTab_integratorSettingsPanel_GBLC.gridy = 0
        integratorSettingsTab_integratorSettingsPanel.add(integratorSettingsTab_integratorSettingsPanel_maxStepsLabel, integratorSettingsTab_integratorSettingsPanel_GBLC)
        integratorSettingsTab_integratorSettingsPanel_GBLC.weightx = 0.6
        integratorSettingsTab_integratorSettingsPanel_GBLC.gridx = 1
        integratorSettingsTab_integratorSettingsPanel_GBLC.gridy = 0
        integratorSettingsTab_integratorSettingsPanel.add(self.integratorSettingsTab_integratorSettingsPanel_maxStepsTextField, integratorSettingsTab_integratorSettingsPanel_GBLC)
        
        integratorSettingsTab_integratorSettingsPanel_GBLC.weightx = 0.4
        integratorSettingsTab_integratorSettingsPanel_GBLC.gridx = 0
        integratorSettingsTab_integratorSettingsPanel_GBLC.gridy = 1
        integratorSettingsTab_integratorSettingsPanel.add(integratorSettingsTab_integratorSettingsPanel_maxStepSizeLabel, integratorSettingsTab_integratorSettingsPanel_GBLC)
        integratorSettingsTab_integratorSettingsPanel_GBLC.weightx = 0.6
        integratorSettingsTab_integratorSettingsPanel_GBLC.gridx = 1
        integratorSettingsTab_integratorSettingsPanel_GBLC.gridy = 1
        integratorSettingsTab_integratorSettingsPanel.add(self.integratorSettingsTab_integratorSettingsPanel_maxStepSizeTextField, integratorSettingsTab_integratorSettingsPanel_GBLC)
        
        integratorSettingsTab_integratorSettingsPanel_GBLC.weightx = 0.4
        integratorSettingsTab_integratorSettingsPanel_GBLC.gridx = 0
        integratorSettingsTab_integratorSettingsPanel_GBLC.gridy = 2
        integratorSettingsTab_integratorSettingsPanel.add(integratorSettingsTab_integratorSettingsPanel_minStepSizeLabel, integratorSettingsTab_integratorSettingsPanel_GBLC)
        integratorSettingsTab_integratorSettingsPanel_GBLC.weightx = 0.6
        integratorSettingsTab_integratorSettingsPanel_GBLC.gridx = 1
        integratorSettingsTab_integratorSettingsPanel_GBLC.gridy = 2
        integratorSettingsTab_integratorSettingsPanel.add(self.integratorSettingsTab_integratorSettingsPanel_minStepSizeTextField, integratorSettingsTab_integratorSettingsPanel_GBLC)
        
        integratorSettingsTab_integratorSettingsPanel_GBLC.weightx = 0.4
        integratorSettingsTab_integratorSettingsPanel_GBLC.gridx = 0
        integratorSettingsTab_integratorSettingsPanel_GBLC.gridy = 3
        integratorSettingsTab_integratorSettingsPanel.add(integratorSettingsTab_integratorSettingsPanel_integratorErrToleranceLabel, integratorSettingsTab_integratorSettingsPanel_GBLC)
        integratorSettingsTab_integratorSettingsPanel_GBLC.weightx = 0.6
        integratorSettingsTab_integratorSettingsPanel_GBLC.gridx = 1
        integratorSettingsTab_integratorSettingsPanel_GBLC.gridy = 3
        integratorSettingsTab_integratorSettingsPanel.add(self.integratorSettingsTab_integratorSettingsPanel_integratorErrToleranceTextField, integratorSettingsTab_integratorSettingsPanel_GBLC)
        
        # Finish creating the Integrator Settings tab
        integratorSettingsTab_GBLC.weightx = 0.1
        integratorSettingsTab_GBLC.gridx = 0
        integratorSettingsTab_GBLC.gridy = 0
        integratorSettingsTab.add(JLabel(" "), integratorSettingsTab_GBLC)
        integratorSettingsTab_GBLC.weightx = 0.8
        integratorSettingsTab_GBLC.gridx = 0
        integratorSettingsTab_GBLC.gridy = 0
        integratorSettingsTab.add(integratorSettingsTab_integratorSettingsPanel, integratorSettingsTab_GBLC)
        integratorSettingsTab_GBLC.weightx = 0.1
        integratorSettingsTab_GBLC.gridx = 0
        integratorSettingsTab_GBLC.gridy = 0
        integratorSettingsTab.add(JLabel(" "), integratorSettingsTab_GBLC)
        
        self.commonTabs.addTab("Integrator Settings", integratorSettingsTab)
        
    def onFilterKinematicsCheckBoxSelect(self, event):
        self.setFilterKinematics()
    
    def setUseExternalLoadsCheckBox(self):
        if( (None != self.ActuatorsExternalLoadsTab_externalLoadsPanel_externalLoadsSpecFileTextField.text) and ('' != self.ActuatorsExternalLoadsTab_externalLoadsPanel_externalLoadsSpecFileTextField.text) and (' ' != self.ActuatorsExternalLoadsTab_externalLoadsPanel_externalLoadsSpecFileTextField.text) ):
            self.ActuatorsExternalLoadsTab_externalLoadsPanel_externalLoadsCheckBox.setSelected(True)
        else:
            self.ActuatorsExternalLoadsTab_externalLoadsPanel_externalLoadsCheckBox.setSelected(False)
        
    def onExternalLoadsCheckBoxSelect(self, event):
        self.setExternalLoadsSpecFile()
    
    def getCard(self):
        return self.commonTabs
        
    def onCardCreationComplete(self):
        """
        When all the cards in the Config UI have been created, let's try to
        set the BESS root directory to the last set value.
        """
        previousRootDirName = toolsCommon.getPersistentString('Bess', 
            'root_dir', 
            '')
        
        if previousRootDirName == '':
            return
            
        self.generalTab_rootDirTextField.text = previousRootDirName
        self.setRootDirCommon()        
        
    # End onCardCreationComplete.
    
    def setRootDirCommon(self):
        """
        We have atleast three places where we set the root directory -- this
        unifies common operations.
        """
        
        # Save the value to the config class.
        self.setRootDir()
        # Open the XML config file if it exists and continue loading settings.
        self.loadCommonConfig()
        
    # End setRootDirCommon.        
        
    #################################################
    # Save the value in the UI to the config object #
    #################################################
    
    def setRootDir(self):
        self.myConfig.common_rootDir = self.generalTab_rootDirTextField.text
        self.myConfig.commonConfigFile = self.myConfig.common_rootDir + "\\bess_commonSettings.xml"
        self.myConfig.scaleConfigFile = self.myConfig.common_rootDir + "\\bess_scaleSettings.xml"
        self.myConfig.multiRunConfigFile = self.myConfig.common_rootDir + "\\bess_multiRunSettings.xml"
        self.myConfig.cmcConfigFile = self.myConfig.common_rootDir + "\\bess_cmcSettings.xml"
        self.myConfig.ikConfigFile = self.myConfig.common_rootDir + "\\bess_ikSettings.xml"
        self.myConfig.configureForcesConfigFile = self.myConfig.common_rootDir + "\\bess_configureForcesSettings.xml"
        self.myConfig.rraConfigFile = self.myConfig.common_rootDir + "\\bess_rraSettings.xml"
        self.myConfig.staticOptConfigFile = self.myConfig.common_rootDir + "\\bess_staticOptSettings.xml"
        self.myConfig.invDynamicsConfigFile = self.myConfig.common_rootDir + "\\bess_invDynSettings.xml"
        self.myConfig.jrConfigFile = self.myConfig.common_rootDir + "\\bess_jrSettings.xml"
    
        # When setting a root dir, make a persistent note of what directory
        # has been set so we can load this automatically next time we start
        # BESS.
        toolsCommon.setPersistentString('Bess', 
            'root_dir', 
            self.myConfig.common_rootDir)
    
    #def setScaleFile(self):
    #    self.myConfig.scaleConfigFile = self.myConfig.common_rootDir + "/bess_scaleSettings.xml"
        
    def setModelFile(self):
        self.myConfig.common_modelFile = self.generalTab_modelFileTextField.text
        
    def setMotionFile(self):
        self.myConfig.common_motionFile = self.generalTab_motionFileTextField.text
        
    def setGroundReactionsFile(self):
        self.myConfig.common_groundReactionsFile = self.generalTab_groundReactionsFileTextField.text
        
    def setDesiredKinematicsFile(self):
        self.myConfig.common_desiredKinematicsFile = self.mainSettingsTab_inputPanel_desiredKinematicsFileTextField.text
        
    def setTrackingTasksFile(self):
        self.myConfig.common_trackingTasksFile = self.mainSettingsTab_inputPanel_trackingTasksFileTextField.text
        
    def setForceSetFile(self):
        self.myConfig.common_forceSetFile = self.ActuatorsExternalLoadsTab_actuatorsPanel_forceSetFileTextField.text
        
    def setExternalLoadsSpecFile(self):
        if (self.ActuatorsExternalLoadsTab_externalLoadsPanel_externalLoadsCheckBox.isSelected()):
            self.myConfig.common_externalLoadsSpecFile = self.ActuatorsExternalLoadsTab_externalLoadsPanel_externalLoadsSpecFileTextField.text
        else:
            self.myConfig.common_externalLoadsSpecFile = ''
        
    def setIntegratorErrTolerance(self):
        self.myConfig.common_integratorErrTolerance = self.integratorSettingsTab_integratorSettingsPanel_integratorErrToleranceTextField.text
        
    def setIntegratorMinStepSize(self):
        self.myConfig.common_integratorMinStepSize = self.integratorSettingsTab_integratorSettingsPanel_minStepSizeTextField.text
    
    def setIntegratorMaxStepSize(self):
        self.myConfig.common_integratorMaxStepSize = self.integratorSettingsTab_integratorSettingsPanel_maxStepSizeTextField.text
    
    def setIntegratorMaxSteps(self):
        self.myConfig.common_integratorMaxSteps = self.integratorSettingsTab_integratorSettingsPanel_maxStepsTextField.text
    
    def setFilterKinematics(self):
        if (self.mainSettingsTab_inputPanel_filterKinematicsCheckBox.isSelected()):
            self.myConfig.common_filterKinematics = self.mainSettingsTab_inputPanel_filterKinematicsTextField.text
        else:
            self.myConfig.common_filterKinematics = ''
            
    def setTimeRangeTo(self):
        self.myConfig.common_timeRangeTo = self.generalTab_timeRangeToTextField.text
    
    def setTimeRangeFrom(self):
        self.myConfig.common_timeRangeFrom = self.generalTab_timeRangeFromTextField.text
        
    def setReplaceModelForceSet(self):
        if(self.ActuatorsExternalLoadsTab_actuatorsPanel_replaceModelForceSetCheckBox.isSelected() == True):
            self.myConfig.common_replaceForceSet = 'true'
        else:
            self.myConfig.common_replaceForceSet = 'false'
    
    ###############################
    # Save contents of a text box #
    ###############################
    
    def setRootDirEvent(self, event):
        
        self.setRootDirCommon()
        
    #def setScaleFileEvent(self, event):
    #    self.setScaleFile()
        
    def setModelFileEvent(self, event):
        self.setModelFile()
        
    def setMotionFileEvent(self, event):
        self.setMotionFile()
        
    def setGroundReactionsFileEvent(self, event):
        self.setGroundReactionsFile()
        
    def setDesiredKinematicsFileEvent(self, event):
        self.setDesiredKinematicsFile()
        
    def setTrackingTasksFileEvent(self, event):
        self.setTrackingTasksFile()
        
    def setForceSetFileEvent(self, event):
        self.setForceSetFile()
        
    def setExternalLoadsSpecFileEvent(self, event):
        self.setUseExternalLoadsCheckBox()
        self.setExternalLoadsSpecFile()
        
    def setIntegratorErrToleranceEvent(self, event):
        self.setIntegratorErrTolerance()
        
    def setIntegratorMinStepSizeEvent(self, event):
        self.setIntegratorMinStepSize()
        
    def setIntegratorMaxStepSizeEvent(self, event):
        self.setIntegratorMaxStepSize()
        
    def setIntegratorMaxStepsEvent(self, event):
        self.setIntegratorMaxSteps()
        
    def setFilterKinematicsEvent(self, event):
        self.setFilterKinematics()
        
    def setTimeRangeToEvent(self, event):
        self.setTimeRangeTo()
        
    def setTimeRangeFromEvent(self, event):
        self.setTimeRangeFrom()
    
    def setReplaceModelForceSetEvent(self, event):
        self.setReplaceModelForceSet()
            
    ###########################################################
    # Show file chooser dialog when a "..." button is pressed #
    ###########################################################
    
    def selectRootDir(self, event):
        # Create and show a file chooser (True = select file, False = select directory)
        myChooser = bessFileChooser(False)
        # Set the text field text to the results of the file chooser
        self.generalTab_rootDirTextField.text = myChooser.fullPath.toString()
        
        self.setRootDirCommon()
    
    #def selectScaleFile(self, event):
    #    # Create and show a file chooser (True = select file, False = select directory)
    #    myChooser = bessFileChooser(True)
    #    # Set the text field text to the results of the file chooser
    #    self.generalTab_scaleFileTextField.text = myChooser.fullPath.toString()
    #    # Save the value to the config class
    #    self.setScaleFile()
        
    def selectModelFile(self, event):
        # Create and show a file chooser (True = select file, False = select directory)
        myChooser = bessFileChooser(True)
        # Set the text field text to the results of the file chooser
        self.generalTab_modelFileTextField.text = myChooser.fullPath.toString()
        # Save the value to the config class
        self.setModelFile()
        
    def selectMotionFile(self, event):
        # Create and show a file chooser (True = select file, False = select directory)
        myChooser = bessFileChooser(True)
        # Set the text field text to the results of the file chooser
        self.generalTab_motionFileTextField.text = myChooser.fullPath.toString()
        # Save the value to the config class
        self.setMotionFile()
        
    def selectGroundReactionsFile(self, event):
        # Create and show a file chooser (True = select file, False = select directory)
        myChooser = bessFileChooser(True)
        # Set the text field text to the results of the file chooser
        self.generalTab_groundReactionsFileTextField.text = myChooser.fullPath.toString()
        # Save the value to the config class
        self.setGroundReactionsFile()
        
    def selectDesiredKinematicsFile(self, event):
        # Create and show a file chooser (True = select file, False = select directory)
        myChooser = bessFileChooser(True)
        # Set the text field text to the results of the file chooser
        self.mainSettingsTab_inputPanel_desiredKinematicsFileTextField.text = myChooser.fullPath.toString()
        # Save the value to the config class
        self.setDesiredKinematicsFile()
        
    def selectTrackingTasksFile(self, event):
        # Create and show a file chooser (True = select file, False = select directory)
        myChooser = bessFileChooser(True)
        # Set the text field text to the results of the file chooser
        self.mainSettingsTab_inputPanel_trackingTasksFileTextField.text = myChooser.fullPath.toString()
        # Save the value to the config class
        self.setTrackingTasksFile()
        
    def selectForceSetFile(self, event):
        # Create and show a file chooser (True = select file, False = select directory)
        myChooser = bessFileChooser(True)
        # Set the text field text to the results of the file chooser
        self.ActuatorsExternalLoadsTab_actuatorsPanel_forceSetFileTextField.text = myChooser.fullPath.toString()
        # Save the value to the config class
        self.setForceSetFile()
        
    def selectExternalLoadsSpecFile(self, event):
        # Create and show a file chooser (True = select file, False = select directory)
        myChooser = bessFileChooser(True)
        # Set the text field text to the results of the file chooser
        self.ActuatorsExternalLoadsTab_externalLoadsPanel_externalLoadsSpecFileTextField.text = myChooser.fullPath.toString()
        # Save the value to the config class
        self.setUseExternalLoadsCheckBox()
        self.setExternalLoadsSpecFile()
    
    def loadCommonConfig(self):
        # Open the xml file, if it exists
        rootConfigPath = self.myConfig.commonConfigFile
        #rootConfigPathObject = pathlib.Path(rootConfigPath)
        
        if(os.path.exists(rootConfigPath)):
            print 'Opening config file: %s' % (rootConfigPath)
            
            try:
                tree = ET.parse(rootConfigPath)
                #print '1'
                # Parse the tree
                root = tree.getroot()
                #print '2'
                self.parseTree(root)
                #print '3'
                
                # Save the values to config object
                ## setRootDir() already called above
                #self.setScaleFile()
                self.setModelFile()
                self.setMotionFile()
                self.setGroundReactionsFile()
                self.setDesiredKinematicsFile()
                self.setTrackingTasksFile()
                self.setForceSetFile()
                self.setUseExternalLoadsCheckBox()
                self.setExternalLoadsSpecFile()
                self.setIntegratorErrTolerance()
                self.setIntegratorMinStepSize()
                self.setIntegratorMaxStepSize()
                self.setIntegratorMaxSteps()
                self.setFilterKinematics()
                self.setTimeRangeTo()
                self.setTimeRangeFrom()
                self.setReplaceModelForceSet()
                #print '4'
            except:
                print "=== Begin error info  ======================================="
                import sys
                print "Something went wrong with BESS!"
                print("Unexpected error: ", sys.exc_info()[0])
                print "------------------------------------------"
                import traceback
                print traceback.format_exc()
                # TODO: PDB doesn't seem to be working...
                #print "------------------------------------------"
                #import pdb
                #pdb.post_mortem()
                print "=== End error info  ======================================="
                
                # Don't "raise" an exception from this point.  As it stands now,
                # this code is being run by the GUI thread (we are in a button 
                # handler).  Raising from here terminates the GUI thread which will
                # lock up OpenSim.  Since this is in a try/except block, this 
                # exception has been "handled" and raising from here will not
                # trigger the "unhandled exception" handler.
                            
            # Load the config files for the other cards
            self.loadSettingsFromAllCards()
        else:
            print 'Config file does not exist: %s' % (rootConfigPath)
            
    # End loadCommonConfig.
    
    # Save/load from config
    # https://docs.python.org/2/library/xml.etree.elementtree.html
    #def loadSettings(self, event):
    #    # Create and show a file chooser (True = select file, False = select directory)
    #    myChooser = bessFileChooser(True)
    #    
    #    # Open the xml file
    #    print 'Opening config file: %s' % (myChooser.fullPath.toString())
    #    tree = ET.parse(myChooser.fullPath.toString())
    #    
    #    # Parse the tree
    #    root = tree.getroot()
    #    self.parseTree(root)
    #    
    #    print 'Done loading settings'
    
    def loadSettingsFromAllCards(self):
        try:
            WELCOME_CARD_NAME = "Welcome"
            COMMON_CARD_NAME = "Common"
            SCALE_CARD_NAME = "Scale"
            MULTI_RUN_CARD_NAME = "Multi-run"
            CMC_TOOL_CARD_NAME = "Computed Muscle Control"
            INV_KINE_TOOL_CARD_NAME = "Inverse Kinematics Tool"
            CONFIGURE_FORCES_TOOL_CARD_NAME = "Configure Forces Tool"
            RRA_TOOL_CARD_NAME = "Reduce Residuals Tool"
            STATIC_OPTIM_TOOL_CARD_NAME = "Static Optimization Tool"
            INV_DYN_TOOL_CARD_NAME = "Inverse Dynamics Tool"
            JOINT_REACT_TOOL_CARD_NAME = "Joint Reaction Analysis Tool"
            SUMMARY_CARD_NAME = "Summary"
            
            print 'Loading settings from all cards:'
            
            # Turm the JPanel object (deck of cards) into an arrary of components (array of cards)
            #cardCreators = self.myCardCreators.getComponents()
            #for cardCreator in cardCreators:
            for cardCreator in self.myCardCreators:
                # Get the proper file name
                xmlFileName = ''
                if(SCALE_CARD_NAME == cardCreator.getCard().getName()):
                    xmlFileName = self.myConfig.scaleConfigFile
                elif(MULTI_RUN_CARD_NAME == cardCreator.getCard().getName()):
                    xmlFileName = self.myConfig.multiRunConfigFile
                elif(CMC_TOOL_CARD_NAME == cardCreator.getCard().getName()):
                    xmlFileName = self.myConfig.cmcConfigFile
                elif(INV_KINE_TOOL_CARD_NAME == cardCreator.getCard().getName()):
                    xmlFileName = self.myConfig.ikConfigFile
                elif(CONFIGURE_FORCES_TOOL_CARD_NAME == cardCreator.getCard().getName()):
                    xmlFileName = self.myConfig.configureForcesConfigFile
                elif(RRA_TOOL_CARD_NAME == cardCreator.getCard().getName()):
                    xmlFileName = self.myConfig.rraConfigFile
                elif(STATIC_OPTIM_TOOL_CARD_NAME == cardCreator.getCard().getName()):
                    xmlFileName = self.myConfig.staticOptConfigFile
                elif(INV_DYN_TOOL_CARD_NAME == cardCreator.getCard().getName()):
                    xmlFileName = self.myConfig.invDynamicsConfigFile
                elif(JOINT_REACT_TOOL_CARD_NAME == cardCreator.getCard().getName()):
                    xmlFileName = self.myConfig.jrConfigFile
                
                # Load the settings
                if('' != xmlFileName):
                    print '    Loading settings: %s' % (cardCreator.getCard().getName())
                    cardCreator.loadSettingsFromFile(xmlFileName)
            
            print 'finished loading settings from all cards'
        except:            
            print "=== Begin error info  ======================================="
            import sys
            print "Something went wrong with BESS!"
            print("Unexpected error: ", sys.exc_info()[0])
            print "------------------------------------------"
            import traceback
            print traceback.format_exc()
            # TODO: PDB doesn't seem to be working...
            #print "------------------------------------------"
            #import pdb
            #pdb.post_mortem()
            print "=== End error info  ======================================="
            
            # Don't "raise" an exception from this point.  As it stands now,
            # this code is being run by the GUI thread (we are in a button 
            # handler).  Raising from here terminates the GUI thread which will
            # lock up OpenSim.  Since this is in a try/except block, this 
            # exception has been "handled" and raising from here will not
            # trigger the "unhandled exception" handler.
               
    def parseTree(self, node):
        print 'Node tag: %s,  text: %s' % (node.tag, node.text)
        
        if('root_directory' == node.tag):
            self.generalTab_rootDirTextField.text = node.text
        elif('model' == node.tag):
            self.generalTab_modelFileTextField.text = node.text
        elif('motion' == node.tag):
            self.generalTab_motionFileTextField.text = node.text
        elif('ground_reactions' == node.tag):
            self.generalTab_groundReactionsFileTextField.text = node.text
        elif('desired_kinematics' == node.tag):
            self.mainSettingsTab_inputPanel_desiredKinematicsFileTextField.text = node.text
        elif('tracking_tasks' == node.tag):
            self.mainSettingsTab_inputPanel_trackingTasksFileTextField.text = node.text
        elif('replace_force_set' == node.tag):
            # TODO: textfield is wrong...
            if( ('' == node.text) or (' ' == node.text) or (not node.text) or ('false' == node.text) ):
                self.ActuatorsExternalLoadsTab_actuatorsPanel_replaceModelForceSetCheckBox.setSelected(False)
            else:
                self.ActuatorsExternalLoadsTab_actuatorsPanel_replaceModelForceSetCheckBox.setSelected(True)
        elif('force_set' == node.tag):
            self.ActuatorsExternalLoadsTab_actuatorsPanel_forceSetFileTextField.text = node.text
        elif('external_loads' == node.tag):
            self.ActuatorsExternalLoadsTab_externalLoadsPanel_externalLoadsSpecFileTextField.text = node.text
            self.setUseExternalLoadsCheckBox()
        elif('integrator_error_tolerance' == node.tag):
            self.integratorSettingsTab_integratorSettingsPanel_integratorErrToleranceTextField.text = node.text
        elif('integrator_minimum_step_size' == node.tag):
            self.integratorSettingsTab_integratorSettingsPanel_minStepSizeTextField.text = node.text
        elif('integrator_maximum_step_size' == node.tag):
            self.integratorSettingsTab_integratorSettingsPanel_maxStepSizeTextField.text = node.text
        elif('integrator_maximum_steps' == node.tag):
            self.integratorSettingsTab_integratorSettingsPanel_maxStepsTextField.text = node.text
        elif('filter_kinematics' == node.tag):
            self.mainSettingsTab_inputPanel_filterKinematicsTextField.text = node.text
            if( ('' == node.text) or (' ' == node.text) or (not node.text) ):
                self.mainSettingsTab_inputPanel_filterKinematicsCheckBox.setSelected(False)
            else:
                self.mainSettingsTab_inputPanel_filterKinematicsCheckBox.setSelected(True)
        elif('final_time' == node.tag):
            self.generalTab_timeRangeToTextField.text = node.text
        elif('initial_time' == node.tag):
            self.generalTab_timeRangeFromTextField.text = node.text
        elif('use_scale_tool' == node.tag):
            self.myConfig.useScale = node.text
        elif('use_cmc_tool' == node.tag):
            self.myConfig.useCmcTool = node.text
        elif('use_ik_tool' == node.tag):
            self.myConfig.useIkTool = node.text
        elif('use_configure_forces_tool' == node.tag):
            self.myConfig.useConfigureForcesTool = node.text
        elif('use_rra_tool' == node.tag):
            self.myConfig.useRraTool = node.text
        elif('use_static_optimization_tool' == node.tag):
            self.myConfig.useStaticOptTool = node.text
        elif('use_inverse_dynamics_tool' == node.tag):
            self.myConfig.useInverseDynTool = node.text
        elif('use_jr_tool' == node.tag):
            self.myConfig.useJointReactionTool = node.text
        
        # Look at any children of children
        for child in node:
            self.parseTree(child)
            
# end bessConfigUICommonCard class

