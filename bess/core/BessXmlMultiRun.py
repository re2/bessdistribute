

################################################################################
#
#
################################################################################

from xml.dom.minidom import parse, parseString, getDOMImplementation, Document
import xml.etree.cElementTree as ET

from core.BessXmlScale import bessXmlScaleIO as xmlScaleIO
from core.BessXmlCommon import bessXmlCommonIO as xmlCommonIO
#from core.BessXmlMultiRun import bessXmlMultiRunIO as xmlMultiRunIO
from core.BessXmlCmc import bessXmlCmcIO as xmlCmcIO
from core.BessXmlIk import bessXmlIkIO as xmlIkIO
from core.BessXmlRra import bessXmlRraIO as xmlRraIO
from core.BessXmlStaticOpt import bessXmlStaticOptIO as xmlStaticOptIO
from core.BessXmlInvDyn import bessXmlInvDynIO as xmlInvDynIO
from core.BessXmlJr import bessXmlJrIO as xmlJrIO

from utilities import toolsCommon as TC

from configure_forces import configureForces_Support

class bessXmlMultiRunIO:
    def __init__(self, myConfig):
        print "Initializing bessXmlMultiRun"
        self.myConfig = myConfig
        print "Done initializing bessXmlMultiRun"
        
    # http://stackoverflow.com/questions/3605680/creating-a-simple-xml-file-using-python
    def writeConfigFile(self):
        print "Writing Multi-run xml file"
        #<?xml version="1.0" encoding="UTF-8" ?>
        
        # TODO: Change version to re2version?
        #<BessConfig Version="1">
        xmlTag_OpenSimDocument = ET.Element("BessMultiRunConfig", Version="1")
        
        for index,item in enumerate(self.myConfig.multiRun_numberOfRuns):
            #  <multiRunEntry>
            xmlTag_multiRunEntry = ET.SubElement(xmlTag_OpenSimDocument, "multiRunEntry")
            
            #    <number_of_runs />
            xmlTag_multiRunEntry_numberOfRuns = ET.SubElement(xmlTag_multiRunEntry, "number_of_runs")
            xmlTag_multiRunEntry_numberOfRuns.text = self.myConfig.multiRun_numberOfRuns[index]
            
            #    <variable_to_modify />
            xmlTag_multiRunEntry_variableToModify = ET.SubElement(xmlTag_multiRunEntry, "variable_to_modify")
            xmlTag_multiRunEntry_variableToModify.text = self.myConfig.multiRun_variableToModify[index]
                        
            #    <low_range />
            xmlTag_multiRunEntry_lowRange = ET.SubElement(xmlTag_multiRunEntry, "low_range")
            xmlTag_multiRunEntry_lowRange.text = self.myConfig.multiRun_lowRange[index]
            
            #    <high_range />
            xmlTag_multiRunEntry_highRange = ET.SubElement(xmlTag_multiRunEntry, "high_range")
            xmlTag_multiRunEntry_highRange.text = self.myConfig.multiRun_highRange[index]
            
            #    <specific_values />
            xmlTag_multiRunEntry_specificValues = ET.SubElement(xmlTag_multiRunEntry, "specific_values")
            xmlTag_multiRunEntry_specificValues.text = self.myConfig.multiRun_specificValues[index]
            
            #  </multiRunEntry>
        #</BessConfig>
        
        # Fix the indentation
        #print "Correcting indentations..."
        self.indent(xmlTag_OpenSimDocument)
        #print "finished correcting indentations"
        tree = ET.ElementTree(xmlTag_OpenSimDocument)
        #print "created tree"
        
        # Write out the xml file
        xmlFileName = self.myConfig.multiRunConfigFile
        #print "perform writing"
        tree.write(xmlFileName, encoding='utf-8')
        print "Wrote multi-run xml file"
    
    
    def writeMultiRunConfigFiles(self):
        # Write the config files for each run of a multi-run scenario while modifying the variables
        
        runIndex = 1  # Used for naming files
        
        # Write the multi-run config file
        self.writeConfigFile()
        
        #if( len(self.myConfig.multiRun_numberOfRuns) == 0 ):
        # Always write out the basic config files. This is what BESS will look for when loading/populating the config ui
        print("------------------------>  Writing Run " + str(runIndex) + " Config files")
        self.writeSingleRunConfigFiles()
        
        # Next, we want to make all the multi-run config files
        # Config files will take on the naming scheme of bess_ikSettings.xml, run_1_bess_ikSettings.xml, ...
        # bess_okSettings.xml and run_1_bess_ikSettings.xml will have the same contents
        
        # Loop through the variables that need to change (if any)
        for index,numRunsAsString in enumerate(self.myConfig.multiRun_numberOfRuns):
            numRuns            = int(numRunsAsString)
            variableToModify   = self.myConfig.multiRun_variableToModify[index]
            lowRange           = float(self.myConfig.multiRun_lowRange[index])
            highRange          = float(self.myConfig.multiRun_highRange[index])
            specificValuesText = self.myConfig.multiRun_specificValues[index]
            specificValues = []
            
            #print("[DEBUG] numRunsAsString: "+numRunsAsString)
            #print("[DEBUG] numRuns"+str(numRuns))
            #print("[DEBUG] lowRange"+str(lowRange))
            #print("[DEBUG] highRange"+str(highRange))
            #print("[DEBUG] specificValuesText"+str(specificValuesText))
            
            # If specific values are not specified, calculate our own values
            if( len(specificValuesText) == 0 ):
                #print("[DEBUG] A range of values has been specified")
                diffBetweenHighLow = highRange - lowRange
                delta = diffBetweenHighLow / (numRuns-1)
                
                currentValue = lowRange
                
                #print("[DEBUG] diffBetweenHighLow"+str(diffBetweenHighLow))
                #print("[DEBUG] delta"+str(delta))
                #print("[DEBUG] Initial currentValue"+str(currentValue))
                
                # Calculate the values
                for index in range(numRuns):
                    currentValue = lowRange + (index * delta)
                    #print("[DEBUG] index"+str(index))
                    #print("[DEBUG] currentValue"+str(currentValue))
                    specificValues.append(currentValue)
                    #print("[DEBUG] specificValues"+str(specificValues))
            else:
                #print("[DEBUG] Specific values have been specified")
                # Split the string up
                specificValuesList = specificValuesText.split(",")
                #print("[DEBUG] specificValuesList"+str(specificValuesList))
                
                # Convert to float and add to list
                for specificValueString in enumerate(specificValuesList):
                    #print("[DEBUG] specificValueString"+str(specificValueString))
                    specificValues.append(float(specificValueString))
                
                
            #print("[DEBUG] specificValues"+str(specificValues))
            # Loop through the values that change
            for index, valueUnknownType in enumerate(specificValues):
                print("------------------------>  Preparing run " + str(runIndex) + " Config files")
                
                #value = None
                #
                #if not isinstance(valueUnknownType, float):
                #    value = float(valueUnknownType)
                #else:
                #    value = valueUnknownType
                
                value = float(valueUnknownType)
                #print("[DEBUG] Variable to modify: " + variableToModify)
                #print("[DEBUG] value"+str(value))
                
                # Change the variable
                if (variableToModify == "scale_-_mass"):
                    
                    # Set the value we wish to change
                    print("Modified mass value: " + str(value))
                    self.myConfig.scale_mass = str(value)
                    
                # Change all variables that specify an output file
                ## Bess config files
                self.myConfig.commonConfigFile          = self.appendRunToFileName(self.myConfig.commonConfigFile, runIndex)
                self.myConfig.scaleConfigFile           = self.appendRunToFileName(self.myConfig.scaleConfigFile, runIndex)
                self.myConfig.multiRunConfigFile        = self.appendRunToFileName(self.myConfig.multiRunConfigFile, runIndex)
                self.myConfig.cmcConfigFile             = self.appendRunToFileName(self.myConfig.cmcConfigFile, runIndex)
                self.myConfig.ikConfigFile              = self.appendRunToFileName(self.myConfig.ikConfigFile, runIndex)
                self.myConfig.configureForcesConfigFile = self.appendRunToFileName(self.myConfig.configureForcesConfigFile, runIndex)
                self.myConfig.rraConfigFile             = self.appendRunToFileName(self.myConfig.rraConfigFile, runIndex)
                self.myConfig.staticOptConfigFile       = self.appendRunToFileName(self.myConfig.staticOptConfigFile, runIndex)
                self.myConfig.invDynamicsConfigFile     = self.appendRunToFileName(self.myConfig.invDynamicsConfigFile, runIndex)
                self.myConfig.jrConfigFile              = self.appendRunToFileName(self.myConfig.jrConfigFile, runIndex)
                
                #########
                # SCALE #
                #########
                
                #self.myConfig.scale_output_modelName              = self.appendRunToFileName(self.myConfig.scale_output_modelName, runIndex)
                self.myConfig.scale_modelScaler_outputModelFile   = self.appendRunToFileName(self.myConfig.scale_modelScaler_outputModelFile, runIndex)
                self.myConfig.scale_modelScaler_outputScaleFile   = self.appendRunToFileName(self.myConfig.scale_modelScaler_outputScaleFile, runIndex)
                self.myConfig.scale_markerPlacer_outputMotionFile = self.appendRunToFileName(self.myConfig.scale_markerPlacer_outputMotionFile, runIndex)
                self.myConfig.scale_markerPlacer_outputModelFile  = self.appendRunToFileName(self.myConfig.scale_markerPlacer_outputModelFile, runIndex)
                self.myConfig.scale_markerPlacer_outputMarkerFile = self.appendRunToFileName(self.myConfig.scale_markerPlacer_outputMarkerFile, runIndex)
                
                ############
                # CMC TOOL #
                ############
                
                self.myConfig.cmcTool_outputPrefix    = self.appendRunToFileName(self.myConfig.cmcTool_outputPrefix, runIndex)
                self.myConfig.cmcTool_outputDirectory = self.appendRunToFileName(self.myConfig.cmcTool_outputDirectory, runIndex)
                
                ###########
                # IK TOOL #
                ###########
                
                self.myConfig.ikTool_outputMotionFile = self.appendRunToFileName(self.myConfig.ikTool_outputMotionFile, runIndex)
                # ????????  self.ikTool_resultsDir = 'C:/Users/david.rusbarsky/Documents/programming/bess_sandbox/tutorial 3/results/IK/'
                
                #########################
                # CONFIGURE FORCES TOOL #
                #########################
                
                # ????????  self.configureForcesTool_Model = None
                
                ############
                # RRA TOOL #
                ############
                
                self.myConfig.rraTool_outputModelFile = self.appendRunToFileName(self.myConfig.rraTool_outputModelFile, runIndex)
                self.myConfig.rraTool_resultsDir      = self.appendRunToFileName(self.myConfig.rraTool_resultsDir, runIndex)
                
                # TODO: I'm not sure where the prefix goes ub tge xml file...
                self.myConfig.rraTool_prefix = self.appendRunToFileName(self.myConfig.rraTool_prefix, runIndex)
                
                #####################
                # Static Optim TOOL #
                #####################
                
                self.myConfig.staticOptTool_resultsDir = self.appendRunToFileName(self.myConfig.staticOptTool_resultsDir, runIndex)
                
                ################
                # Inv Dyn TOOL #
                ################
                
                # ?????? Nope. OpenSim InvDyn tool will construct the full paths. We do not need to touch: self.inverseDynTool_resultsDir = ''
                # TODO: Will chaning this file name break things for OpenSim? Is it expecting this exact file name?
                self.myConfig.inverseDynTool_outputGenForceFile   = self.appendRunToFileName(self.myConfig.inverseDynTool_outputGenForceFile, runIndex)
                self.myConfig.inverseDynTool_outputBodyForcesFile = self.appendRunToFileName(self.myConfig.inverseDynTool_outputBodyForcesFile, runIndex)
                
                ###########
                # JR TOOL #
                ###########
                
                # TODO: No output files?
                
                        
                # Write the config files
                print("------------------------>  Writing Run " + str(runIndex) + " Config files")
                self.writeSingleRunConfigFiles()
                
                # Update our runIndex variable for the next possible run
                runIndex = runIndex + 1
                print("Done")
                
        
    def appendRunToFileName(self, fullFilePath, runIndex):
        # Given a full file path and a run index, append "run_<runIndex>_" to the file name
        # E.g. Given fullFilePath = /some/path/to/file.txt and runIndex = 3...
        # The resulting string will be /some/path/to/run_3_file.txt
        
        # Find the last index of / in our full file path string and split that into file name and directory path
        fileNameIndex = fullFilePath.rfind('/') + 1  # Go one past where we find the index
        
        # If we didn't find a slash... try looking for the other slash
        if( (len(fullFilePath) != 0) and fileNameIndex == 0 ):
            fileNameIndex = fullFilePath.rfind('\\') + 1  # Go one past where we find the index
            
        
        directoryPath = fullFilePath[:fileNameIndex]
        fileName = fullFilePath[fileNameIndex:]
        #print("[DEBUG] File name index: "+str(fileNameIndex))
        #print("[DEBUG] directoryPath: "+directoryPath)
        #print("[DEBUG] File name: "+fileName)
        
        # Has this string been appended already?
        if(fileName.startswith("run_")):
            # We are expecting to see:    run_<some number here>_<the rest of the file name>
            # Look for the second "_" character
            prependIndex = fileName.find("_", 4) + 1   # 4 = "run_" and then go one past where we find that index
            
            # Remove the existing prepend
            fileName = fileName[prependIndex:]
            #print("[DEBUG] File name (renumbering): "+fileName)
        
        # Construct the string with the prepend
        fileName = "run_" + str(runIndex) + "_" + fileName
        #print("[DEBUG] Final file name: "+fileName)
        finalFullFilePath = directoryPath + fileName
        #print("[DEBUG] Final full path: "+fullFilePath)
        
        return finalFullFilePath
        
        
    def writeSingleRunConfigFiles(self):
        # Write the config files for a single run scenario
        # Note: This does not write out the config file for multi-run config files
        
        # Common
        myXmlCommonIO = xmlCommonIO(self.myConfig)
        myXmlCommonIO.writeConfigFile()
        
        # Scale
        if(self.myConfig.useScale == 'true'):
            myXmlScaleIO = xmlScaleIO(self.myConfig)
            myXmlScaleIO.writeConfigFile()
        
        ## Multi-run
        #myXmlMultiRunIO = xmlMultiRunIO(self.myConfig)
        #myXmlMultiRunIO.writeConfigFile()
        
        # CMC
        if(self.myConfig.useCmcTool == 'true'):
            myXmlCmcIO = xmlCmcIO(self.myConfig)
            myXmlCmcIO.writeConfigFile()
        
        # IK
        if(self.myConfig.useIkTool == 'true'):
            myXmlIkIO = xmlIkIO(self.myConfig)
            myXmlIkIO.writeConfigFile()
        
        # Configure Forces
        if(TC.boolFromLowerCaseString(self.myConfig.useConfigureForcesTool) and 
            self.myConfig.configureForcesTool_Model):
                        
            # The Configure Forces embedded UI already has the ability to
            # save XML from its model data.  However, it's probably a better
            # path to directly save from the "config" object's copy of the
            # model data.
            configureForces_Support.Config.writeToXmlFile(
                self.myConfig.configureForcesConfigFile, 
                self.myConfig.configureForcesTool_Model.filePaths, 
                self.myConfig.configureForcesTool_Model.strideConfig,
                self.myConfig.configureForcesTool_Model.grfForceSets,
                self.myConfig.configureForcesTool_Model.trackedPositions,
                self.myConfig.configureForcesTool_Model.realLoadCellForceSets,
                (self.myConfig.configureForcesTool_Model.
                    syntheticLoadCellForceSets),
                self.myConfig.configureForcesTool_Model.syntheticDataConfig)            
        
        # RRA
        if(self.myConfig.useRraTool == 'true'):
            myXmlRraIO = xmlRraIO(self.myConfig)
            myXmlRraIO.writeConfigFile()
        
        # SO
        if(self.myConfig.useStaticOptTool == 'true'):
            myXmlStaticOptIO = xmlStaticOptIO(self.myConfig)
            myXmlStaticOptIO.writeConfigFile()
        
        # Inv Dyn
        if(self.myConfig.useInverseDynTool == 'true'):
            myXmlInvDynIO = xmlInvDynIO(self.myConfig)
            myXmlInvDynIO.writeConfigFile()
        
        # JR
        if(self.myConfig.useJointReactionTool == 'true'):
            myXmlJrIO = xmlJrIO(self.myConfig)
            myXmlJrIO.writeConfigFile()
        
        
        
    def indent(self, elem, level=0):
        #print "fixing element: %s" % (elem)
        i = "\n" + level*"  "
        j = "\n" + (level-1)*"  "
        if len(elem):
            #print "1"
            if not elem.text or not elem.text.strip():
                #print "2"
                elem.text = i + "  "
            if not elem.tail or not elem.tail.strip():
                #print "3"
                elem.tail = i
            for subelem in elem:
                #print "4"
                self.indent(subelem, level+1)
            if not elem.tail or not elem.tail.strip():
                #print "5"
                elem.tail = j
        else:
            #print "6"
            if level and (not elem.tail or not elem.tail.strip()):
                #print "7"
                elem.tail = j
        #print "8"
        return elem        

