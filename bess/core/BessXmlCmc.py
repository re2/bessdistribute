

################################################################################
#
#
################################################################################

from xml.dom.minidom import parse, parseString, getDOMImplementation, Document
import xml.etree.cElementTree as ET
#from xml.etree import ElementTree as ET
#from xml.etree.ElementTree import ElementTree as ET

'''
cmcData = (myCmcCard.getAAAA(), myCmcCard.getBBBB(), myCmcCard.getCCC())
bessXmlCreator = BessXmlCmc()
bessXmlCreator.writeConfigFile()
'''
class bessXmlCmcIO:
    def __init__(self, myConfig):
        print "Initializing bessXmlCmc"
        self.myConfig = myConfig
        print "Done initializing bessXmlCmc"
        
    # http://stackoverflow.com/questions/3605680/creating-a-simple-xml-file-using-python
    def writeConfigFile(self):
        #<?xml version="1.0" encoding="UTF-8" ?>
        #<OpenSimDocument Version="30000">
        self.xmlTag_OpenSimDocument = ET.Element("OpenSimDocument", Version="30000")
        
        #   <CMCTool name="loadedwalking_subject07_loaded_free_trial02_cmc">
        xmlTag_CMCTool = ET.SubElement(self.xmlTag_OpenSimDocument, "CMCTool", name=self.myConfig.cmcTool_outputPrefix)
        
        #       <!--Name of the .osim file used to construct a model.-->
        #       <model_file>subject07_adjusted_loaded_markered_adjusted.osim</model_file>
        xmlTag_CMCTool_modelFile = ET.SubElement(xmlTag_CMCTool, "model_file")
        #print "Saving model file to xml file"
        xmlTag_CMCTool_modelFile.text = self.myConfig.common_modelFile
        #print "Model file path saved to xml file"
        
        #       <!--Replace the model's force set with sets specified in <force_set_files>? If false, the force set is appended to.-->
        #       <replace_force_set>false</replace_force_set>
        xmlTag_CMCTool_replaceForceSet = ET.SubElement(xmlTag_CMCTool, "replace_force_set")
        xmlTag_CMCTool_replaceForceSet.text = self.myConfig.common_replaceForceSet
        
        #       <!--List of xml files used to construct an force set for the model.-->
        #       <force_set_files> cmc_actuators_gait_23dofs_92muscles_patella.xml loaded_residual_actuators.xml</force_set_files>
        xmlTag_CMCTool_forceSetFiles = ET.SubElement(xmlTag_CMCTool, "force_set_files")
        xmlTag_CMCTool_forceSetFiles.text = self.myConfig.common_forceSetFile
        
        #       <!--Directory used for writing results.-->
        #       <results_directory>results_CMC</results_directory>
        xmlTag_CMCTool_resultsDirectory = ET.SubElement(xmlTag_CMCTool, "results_directory")
        xmlTag_CMCTool_resultsDirectory.text = self.myConfig.cmcTool_outputDirectory
        
        #       <!--Output precision.  It is 8 by default.-->
        #       <output_precision>8</output_precision>
        #print 'writing precision to xml'
        xmlTag_CMCTool_outputPrecision = ET.SubElement(xmlTag_CMCTool, "output_precision")
        #print 'still working on it'
        xmlTag_CMCTool_outputPrecision.text = self.myConfig.cmcTool_outputPrecision
        #print 'wrote precision to xml'
        
        #       <!--Initial time for the simulation.-->
        #       <initial_time>0.5</initial_time>
        xmlTag_CMCTool_initialTime = ET.SubElement(xmlTag_CMCTool, "initial_time")
        xmlTag_CMCTool_initialTime.text = self.myConfig.common_timeRangeFrom
        
        #       <!--Final time for the simulation.-->
        #       <final_time>1.8331</final_time>
        xmlTag_CMCTool_finalTime = ET.SubElement(xmlTag_CMCTool, "final_time")
        xmlTag_CMCTool_finalTime.text = self.myConfig.common_timeRangeTo
        
        #       <!--Flag indicating whether or not to compute equilibrium values for states other than the coordinates or speeds.  For example, equilibrium muscle fiber lengths or muscle forces.-->
        #       <solve_for_equilibrium_for_auxiliary_states>true</solve_for_equilibrium_for_auxiliary_states>
        xmlTag_CMCTool_solveForEquilibriumForAuxiliary = ET.SubElement(xmlTag_CMCTool, "solve_for_equilibrium_for_auxiliary_states")
        xmlTag_CMCTool_solveForEquilibriumForAuxiliary.text = self.myConfig.cmcTool_solveForEquilibriumForAuxiliary
        
        #       <!--Maximum number of integrator steps.-->
        #       <maximum_number_of_integrator_steps>30000</maximum_number_of_integrator_steps>
        xmlTag_CMCTool_maximumNumberOfIntegratorSteps = ET.SubElement(xmlTag_CMCTool, "maximum_number_of_integrator_steps")
        xmlTag_CMCTool_maximumNumberOfIntegratorSteps.text = self.myConfig.common_integratorMaxSteps
        
        #       <!--Maximum integration step size.-->
        #       <maximum_integrator_step_size>1</maximum_integrator_step_size>
        xmlTag_CMCTool_maximumIntegratorStepSize = ET.SubElement(xmlTag_CMCTool, "maximum_integrator_step_size")
        xmlTag_CMCTool_maximumIntegratorStepSize.text = self.myConfig.common_integratorMaxStepSize
        
        #       <!--Minimum integration step size.-->
        #       <minimum_integrator_step_size>1e-008</minimum_integrator_step_size>
        xmlTag_CMCTool_minimumIntegratorStepSize = ET.SubElement(xmlTag_CMCTool, "minimum_integrator_step_size")
        xmlTag_CMCTool_minimumIntegratorStepSize.text = self.myConfig.common_integratorMinStepSize
        
        #       <!--Integrator error tolerance. When the error is greater, the integrator step size is decreased.-->
        #       <integrator_error_tolerance>1e-005</integrator_error_tolerance>
        xmlTag_CMCTool_integratorErrorTolerance = ET.SubElement(xmlTag_CMCTool, "integrator_error_tolerance")
        xmlTag_CMCTool_integratorErrorTolerance.text = self.myConfig.common_integratorErrTolerance
        
        #       <!--Set of analyses to be run during the investigation.-->
        #       <AnalysisSet name="Analyses">
        xmlTag_CMCTool_analyses = ET.SubElement(xmlTag_CMCTool, "AnalysisSet", name="Analyses")
        
        #           <objects>
        xmlTag_CMCTool_analyses_objects = ET.SubElement(xmlTag_CMCTool_analyses, "objects")
        
        ##               <ProbeReporter name="ProbeReporter">
        #xmlTag_CMCTool_analyses_objects_probeReporter = ET.SubElement(xmlTag_CMCTool_analyses_objects, "ProbeReporter", name="ProbeReporter")
        #
        ##                   <!--Flag (true or false) specifying whether whether on. True by default.-->
        ##                   <on>true</on>
        #xmlTag_CMCTool_analyses_objects_probeReporter_on = ET.SubElement(xmlTag_CMCTool_analyses_objects_probeReporter, "on")
        #xmlTag_CMCTool_analyses_objects_probeReporter_on.text = self.myConfig.cmcTool_analyses_objects_probeReporter_on
        #
        ##                   <!--Start time.-->
        ##                   <start_time>0.5</start_time>
        #xmlTag_CMCTool_analyses_objects_probeReporter_startTime = ET.SubElement(xmlTag_CMCTool_analyses_objects_probeReporter, "start_time")
        #xmlTag_CMCTool_analyses_objects_probeReporter_startTime.text = self.myConfig.cmcTool_analyses_objects_probeReporter_startTime
        #
        ##                   <!--End time.-->
        ##                   <end_time>1.8331</end_time>
        #xmlTag_CMCTool_analyses_objects_probeReporter_endTime = ET.SubElement(xmlTag_CMCTool_analyses_objects_probeReporter, "end_time")
        #xmlTag_CMCTool_analyses_objects_probeReporter_endTime.text = self.myConfig.cmcTool_analyses_objects_probeReporter_endTime
        #
        ##                   <!--Specifies how often to store results during a simulation. More specifically, the interval (a positive integer) specifies how many successful integration steps should be taken before results are recorded again.-->
        ##                   <step_interval>10</step_interval>
        #xmlTag_CMCTool_analyses_objects_probeReporter_stepInterval = ET.SubElement(xmlTag_CMCTool_analyses_objects_probeReporter, "step_interval")
        #xmlTag_CMCTool_analyses_objects_probeReporter_stepInterval.text = self.myConfig.cmcTool_analyses_objects_probeReporter_stepInterval
        #
        ##                   <!--Flag (true or false) indicating whether the results are in degrees or not.-->
        ##                   <in_degrees>true</in_degrees>
        #xmlTag_CMCTool_analyses_objects_probeReporter_inDegrees = ET.SubElement(xmlTag_CMCTool_analyses_objects_probeReporter, "in_degrees")
        #xmlTag_CMCTool_analyses_objects_probeReporter_inDegrees.text = self.myConfig.common_inDegrees
        #
        ##               </ProbeReporter>
        #           </objects>
        #           <groups />
        xmlTag_CMCTool_analyses_groups = ET.SubElement(xmlTag_CMCTool_analyses, "groups")
        
        #       </AnalysisSet>
        #       <!--Controller objects in the model.-->
        #       <ControllerSet name="Controllers">
        xmlTag_CMCTool_controllerSet = ET.SubElement(xmlTag_CMCTool, "ControllerSet", name="Controllers")
        
        #           <objects />
        xmlTag_CMCTool_controllerSet_objects = ET.SubElement(xmlTag_CMCTool_controllerSet, "objects")
        
        #           <groups />
        xmlTag_CMCTool_controllerSet_groups = ET.SubElement(xmlTag_CMCTool_controllerSet, "groups")
        
        #       </ControllerSet>
        #       <!--XML file (.xml) containing the forces applied to the model as ExternalLoads.-->
        #       <external_loads_file>external_loads.xml</external_loads_file>
        xmlTag_CMCTool_externalLoadsFile = ET.SubElement(xmlTag_CMCTool, "external_loads_file")
        xmlTag_CMCTool_externalLoadsFile.text = self.myConfig.common_externalLoadsSpecFile
        
        #       <!--Motion (.mot) or storage (.sto) file containing the desired point trajectories.-->
        #       <desired_points_file />
        xmlTag_CMCTool_desiredPointsFile = ET.SubElement(xmlTag_CMCTool, "desired_points_file")
        
        #       <!--Motion (.mot) or storage (.sto) file containing the desired kinematic trajectories.-->
        #       <desired_kinematics_file>loadedwalking_subject07_loaded_free_trial02_rrakin_Kinematics_q.sto</desired_kinematics_file>
        xmlTag_CMCTool_desiredKinematicsFile = ET.SubElement(xmlTag_CMCTool, "desired_kinematics_file")
        xmlTag_CMCTool_desiredKinematicsFile.text = self.myConfig.common_desiredKinematicsFile
        
        #       <!--File containing the tracking tasks. Which coordinates are tracked and with what weights are specified here.-->
        #       <task_set_file>tasks.xml</task_set_file>
        xmlTag_CMCTool_taskSetFile = ET.SubElement(xmlTag_CMCTool, "task_set_file")
        xmlTag_CMCTool_taskSetFile.text = self.myConfig.common_trackingTasksFile
        
        #       <!--File containing the constraints on the controls.-->
        #       <constraints_file />
        xmlTag_CMCTool_constraintsFile = ET.SubElement(xmlTag_CMCTool, "constraints_file")
        
        #       <!--File containing the controls output by RRA. These can be used to place constraints on the residuals during CMC.-->
        #       <rra_controls_file />
        xmlTag_CMCTool_rraControlsFile = ET.SubElement(xmlTag_CMCTool, "rra_controls_file")
        
        #       <!--Low-pass cut-off frequency for filtering the desired kinematics. A negative value results in no filtering. The default value is -1.0, so no filtering.-->
        #       <lowpass_cutoff_frequency>-1</lowpass_cutoff_frequency>
        xmlTag_CMCTool_lowpassCutoffFrequency = ET.SubElement(xmlTag_CMCTool, "lowpass_cutoff_frequency")
        xmlTag_CMCTool_lowpassCutoffFrequency.text = self.myConfig.cmcTool_lowpassCutoffFrequency
        
        #       <!--Time window over which the desired actuator forces are achieved. Muscles forces cannot change instantaneously, so a finite time window must be allowed. The recommended time window for RRA is about 0.001 sec, and for CMC is about 0.010 sec.-->
        #       <cmc_time_window>0.01</cmc_time_window>
        xmlTag_CMCTool_cmcTimeWindow = ET.SubElement(xmlTag_CMCTool, "cmc_time_window")
        xmlTag_CMCTool_cmcTimeWindow.text = self.myConfig.cmcTool_cmcTimeWindow
        
        #       <!--Flag (true or false) indicating whether to use the fast CMC optimization target. The fast target requires the desired accelerations to be met. The optimizer fails if the acclerations constraints cannot be met, so the fast target can be less robust.  The regular target does not require the acceleration constraints to be met; it meets them as well as it can, but it is slower and less accurate.-->
        #       <use_fast_optimization_target>true</use_fast_optimization_target>
        xmlTag_CMCTool_useFastOptimizationTarget = ET.SubElement(xmlTag_CMCTool, "use_fast_optimization_target")
        xmlTag_CMCTool_useFastOptimizationTarget.text = self.myConfig.cmcTool_useFastOptimizationTarget
        
        #       <!--Preferred optimizer algorithm (currently support "ipopt" or "cfsqp", the latter requiring the osimFSQP library.-->
        #       <optimizer_algorithm>cfsqp</optimizer_algorithm>
        xmlTag_CMCTool_optimizerAlgorithm = ET.SubElement(xmlTag_CMCTool, "optimizer_algorithm")
        xmlTag_CMCTool_optimizerAlgorithm.text = self.myConfig.cmcTool_optimizerAlgorithm
        
        #       <!--Maximum number of iterations for the optimizer.-->
        #       <optimizer_max_iterations>2000</optimizer_max_iterations>
        xmlTag_CMCTool_optimizerMaxIterations = ET.SubElement(xmlTag_CMCTool, "optimizer_max_iterations")
        xmlTag_CMCTool_optimizerMaxIterations.text = self.myConfig.cmcTool_optimizerMaxIterations
        
        #       <!--Print level for the optimizer, 0 - 3. 0=no printing, 3=detailed printing, 2=in between-->
        #       <optimizer_print_level>0</optimizer_print_level>
        xmlTag_CMCTool_optimizerPrintLevel = ET.SubElement(xmlTag_CMCTool, "optimizer_print_level")
        xmlTag_CMCTool_optimizerPrintLevel.text = self.myConfig.cmcTool_optimizerPrintLevel
        
        #       <!--True-false flag indicating whether or not to turn on verbose printing for cmc.-->
        #       <use_verbose_printing>false</use_verbose_printing>
        xmlTag_CMCTool_useVerbosePrinting = ET.SubElement(xmlTag_CMCTool, "use_verbose_printing")
        xmlTag_CMCTool_useVerbosePrinting.text = self.myConfig.cmcTool_useVerbosePrinting
        
        #   </CMCTool>
        #</OpenSimDocument>
        
        xmlFileName = self.myConfig.cmcConfigFile
        self.indent(self.xmlTag_OpenSimDocument)
        tree = ET.ElementTree(self.xmlTag_OpenSimDocument)
        
        #print "About to write xml file"
        tree.write(xmlFileName, encoding='utf-8')
        
        print "Wrote xml file"
        
    def indent(self, elem, level=0):
        i = "\n" + level*"  "
        j = "\n" + (level-1)*"  "
        if len(elem):
            if not elem.text or not elem.text.strip():
                elem.text = i + "  "
            if not elem.tail or not elem.tail.strip():
                elem.tail = i
            for subelem in elem:
                self.indent(subelem, level+1)
            if not elem.tail or not elem.tail.strip():
                elem.tail = j
        else:
            if level and (not elem.tail or not elem.tail.strip()):
                elem.tail = j
        return elem        

