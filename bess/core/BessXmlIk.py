

################################################################################
#
#
################################################################################

from xml.dom.minidom import parse, parseString, getDOMImplementation, Document
import xml.etree.cElementTree as ET

class bessXmlIkIO:
    def __init__(self, myConfig):
        print "Initializing bessXmlIk"
        self.myConfig = myConfig
        print "Done initializing bessXmlIk"
        
    # http://stackoverflow.com/questions/3605680/creating-a-simple-xml-file-using-python
    def writeConfigFile(self):
        print "Writing IK xml file"
        #<?xml version="1.0" encoding="UTF-8" ?>
        #<OpenSimDocument Version="30000">
        self.xmlTag_OpenSimDocument = ET.Element("OpenSimDocument", Version="30000")
        
        #    <InverseKinematicsTool>
        xmlTag_IKTool = ET.SubElement(self.xmlTag_OpenSimDocument, "InverseKinematicsTool", name=self.myConfig.common_modelFile)
        
        #        <!--Name of the .osim file used to construct a model.-->
        #        <model_file>Unassigned</model_file>
        xmlTag_IKTool_modelFile = ET.SubElement(xmlTag_IKTool, "model_file")
        xmlTag_IKTool_modelFile.text = self.myConfig.getPostScalingModelFilename()
                
        #        <!--A positive scalar that is used to weight the importance of satisfying constraints.A weighting of 'Infinity' or if it is unassigned results in the constraints being strictly enforced.-->
        #        <constraint_weight>Inf</constraint_weight>
        xmlTag_IKTool_constraintWeight = ET.SubElement(xmlTag_IKTool, "constraint_weight")
        xmlTag_IKTool_constraintWeight.text = self.myConfig.ikTool_constraintWeight
        
        #        <!--The accuracy of the solution in absolute terms. I.e. the number of significantdigits to which the solution can be trusted.-->
        #        <accuracy>1e-005</accuracy>
        xmlTag_IKTool_accuracy = ET.SubElement(xmlTag_IKTool, "accuracy")
        xmlTag_IKTool_accuracy.text = self.myConfig.ikTool_accuracy
        
        #        <!--Markers and coordinates to be considered (tasks) and their weightings.-->
        #        <IKTaskSet>
        xmlTag_IKTool_ikTaskSet = ET.SubElement(xmlTag_IKTool, "IKTaskSet", name=self.myConfig.ikTool_outputMotionFile)
        #            <objects>
        xmlTag_IKTool_ikTaskSet_objects = ET.SubElement(xmlTag_IKTool_ikTaskSet, "objects")
        
        for index,item in enumerate(self.myConfig.ikTool_markerTask_names):
            xmlTag_IKTool_ikTaskSet_objects_ikMarkerTask = ET.SubElement(xmlTag_IKTool_ikTaskSet_objects, "IKMarkerTask", name=item)
            
            xmlTag_IKTool_ikTaskSet_objects_ikMarkerTask_apply = ET.SubElement(xmlTag_IKTool_ikTaskSet_objects_ikMarkerTask, "apply")
            xmlTag_IKTool_ikTaskSet_objects_ikMarkerTask_apply.text = self.myConfig.ikTool_markerTask_applies[index]
            
            xmlTag_IKTool_ikTaskSet_objects_ikMarkerTask_weight = ET.SubElement(xmlTag_IKTool_ikTaskSet_objects_ikMarkerTask, "weight")
            xmlTag_IKTool_ikTaskSet_objects_ikMarkerTask_weight.text = self.myConfig.ikTool_markerTask_weights[index]
            
        for index,item in enumerate(self.myConfig.ikTool_coordTask_names):
        #               <IKCoordinateTask name="pelvis_tilt">
            xmlTag_IKTool_ikTaskSet_objects_ikCoordinateTask = ET.SubElement(xmlTag_IKTool_ikTaskSet_objects, "IKCoordinateTask", name=item)
            
        #                   <!--Whether or not this task will be used during inverse kinematics solve.-->
        #                   <apply>true</apply>
            xmlTag_IKTool_ikTaskSet_objects_ikCoordinateTask_apply = ET.SubElement(xmlTag_IKTool_ikTaskSet_objects_ikCoordinateTask, "apply")
            xmlTag_IKTool_ikTaskSet_objects_ikCoordinateTask_apply.text = self.myConfig.ikTool_coordTask_applies[index]
            
        #                   <!--Weight given to a marker or coordinate for solving inverse kinematics problems.-->
        #                   <weight>1</weight>
            xmlTag_IKTool_ikTaskSet_objects_ikCoordinateTask_weight = ET.SubElement(xmlTag_IKTool_ikTaskSet_objects_ikCoordinateTask, "weight")
            xmlTag_IKTool_ikTaskSet_objects_ikCoordinateTask_weight.text = self.myConfig.ikTool_coordTask_weights[index]
            
        #                   <!--Indicates the source of the coordinate value for this task.  Possible values are default_value (use default value of coordinate, as specified in the model file, as the fixed target value), manual_value (use the value specified in the value property of this task as the fixed target value), or from_file (use the coordinate values from the coordinate data specified by the coordinates_file property).-->
        #                   <value_type>default_value</value_type>
            xmlTag_IKTool_ikTaskSet_objects_ikCoordinateTask_valueType = ET.SubElement(xmlTag_IKTool_ikTaskSet_objects_ikCoordinateTask, "value_type")
            xmlTag_IKTool_ikTaskSet_objects_ikCoordinateTask_valueType.text = self.myConfig.ikTool_coordTask_valueTypes[index]
            
        #                   <!--This value will be used as the desired (or prescribed) coordinate value if value_type is set to manual_value.-->
        #                   <value>0</value>
            xmlTag_IKTool_ikTaskSet_objects_ikCoordinateTask_value = ET.SubElement(xmlTag_IKTool_ikTaskSet_objects_ikCoordinateTask, "value")
            xmlTag_IKTool_ikTaskSet_objects_ikCoordinateTask_value.text = self.myConfig.ikTool_coordTask_values[index]
            
        #               </IKCoordinateTask>
        #            </objects>
        #            <groups />
        xmlTag_IKTool_ikTaskSet_groups = ET.SubElement(xmlTag_IKTool_ikTaskSet, "groups")
        #        </IKTaskSet>
        
        #        <!--TRC file (.trc) containing the time history of observations of marker positions.-->
        #        <marker_file />
        xmlTag_IKTool_markerFile = ET.SubElement(xmlTag_IKTool, "marker_file")
        xmlTag_IKTool_markerFile.text = self.myConfig.ikTool_markerFile
        
        #        <!--The name of the storage (.sto or .mot) file containing coordinate observations.Coordinate values from this file are included if there is a corresponding coordinate task. -->
        #        <coordinate_file>Unassigned</coordinate_file>
        xmlTag_IKTool_coordinateFile = ET.SubElement(xmlTag_IKTool, "coordinate_file")
        xmlTag_IKTool_coordinateFile.text = self.myConfig.ikTool_coordinateFile
        
        #        <!--Time range over which the inverse kinematics problem is solved.-->
        #        <time_range> -1 -1</time_range>
        xmlTag_IKTool_timeRange = ET.SubElement(xmlTag_IKTool, "time_range")
        xmlTag_IKTool_timeRange.text = str(self.myConfig.common_timeRangeFrom) + ' ' + str(self.myConfig.common_timeRangeTo)
        
        #        <!--Directory used for writing results.-->
        #        <results_directory>./</results_directory>
        xmlTag_IKTool_resultsDir = ET.SubElement(xmlTag_IKTool, "results_directory")
        xmlTag_IKTool_resultsDir.text = self.myConfig.ikTool_resultsDir
        
        #        <!--Directory for input files-->
        #        <input_directory />
        xmlTag_IKTool_inputDir = ET.SubElement(xmlTag_IKTool, "input_directory")
        xmlTag_IKTool_inputDir.text = self.myConfig.ikTool_inputDir
        
        #        <!--Flag (true or false) indicating whether or not to report marker errors from the inverse kinematics solution.-->
        #        <report_errors>true</report_errors>
        xmlTag_IKTool_reportErrors = ET.SubElement(xmlTag_IKTool, "report_errors")
        xmlTag_IKTool_reportErrors.text = self.myConfig.ikTool_reportErrors
        
        #        <!--Name of the motion file (.mot) to which the results should be written.-->
        #        <output_motion_file>Unassigned</output_motion_file>
        xmlTag_IKTool_outputMotionFile = ET.SubElement(xmlTag_IKTool, "output_motion_file")
        xmlTag_IKTool_outputMotionFile.text = self.myConfig.ikTool_outputMotionFile
        
        #        <!--Flag indicating whether or not to report model marker locations in ground.-->
        #        <report_marker_locations>false</report_marker_locations>
        xmlTag_IKTool_reportMarkerLocations = ET.SubElement(xmlTag_IKTool, "report_marker_locations")
        xmlTag_IKTool_reportMarkerLocations.text = self.myConfig.ikTool_reportMarkerLocations
        
        #    </InverseKinematicsTool>
        #</OpenSimDocument>
        
        # Fix the indentation
        self.indent(self.xmlTag_OpenSimDocument)
        tree = ET.ElementTree(self.xmlTag_OpenSimDocument)
        
        # Write out the xml file
        xmlFileName = self.myConfig.ikConfigFile
        tree.write(xmlFileName, encoding='utf-8')
        print "Wrote IK xml file"
        
    def indent(self, elem, level=0):
        i = "\n" + level*"  "
        j = "\n" + (level-1)*"  "
        if len(elem):
            if not elem.text or not elem.text.strip():
                elem.text = i + "  "
            if not elem.tail or not elem.tail.strip():
                elem.tail = i
            for subelem in elem:
                self.indent(subelem, level+1)
            if not elem.tail or not elem.tail.strip():
                elem.tail = j
        else:
            if level and (not elem.tail or not elem.tail.strip()):
                elem.tail = j
        return elem        

