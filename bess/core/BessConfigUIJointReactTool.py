

################################################################################
#
# This file creates the Configuration UI components and handles user input as
# it relates to BESS configuration.
#
################################################################################
from javax.swing import JFrame, JPanel, JButton, JFileChooser, JTextField, JLabel, JCheckBox, SwingConstants, JTabbedPane, JScrollPane, JComboBox, JTable
from javax.swing.border import TitledBorder
from java.awt import GridLayout, FlowLayout, Color, GridBagLayout, GridBagConstraints, CardLayout, BorderLayout

from xml.dom.minidom import parse, parseString, getDOMImplementation, Document
import xml.etree.cElementTree as ET

import os.path

from core.BessConfigUICardBase import bessCardBase
from core.BessUIFileChooser import bessFileChooser

'''
TODO: Write description
'''
class bessConfigUIJointReactToolCard(bessCardBase):
    def __init__(self, myConfig):
        self.myConfig = myConfig
        
        # Create the UI components
        self.jointReactToolCard = JPanel(BorderLayout())
        
        self.jointReactToolPanel = JPanel(GridBagLayout())
        self.jointReactToolPanel_GBLC = GridBagConstraints()
        self.jointReactToolPanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        self.jointReactTabs = JTabbedPane(SwingConstants.TOP, JTabbedPane.SCROLL_TAB_LAYOUT)
        
        # Use This Tool settings
        useThisToolGroup = JPanel(GridBagLayout())
        useThisToolGroup_GBLC = GridBagConstraints()
        useThisToolGroup_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        self.useThisToolGroup_useItCheckBox = JCheckBox("Perform Joint Reaction Analysis Tool", True, actionPerformed=self.onUseItCheckBoxSelect)
        useThisToolGroup_loadSettingsButton = JButton("Load settings", actionPerformed = self.loadSettings)
        
        useThisToolGroup_GBLC.weightx = 0.25
        useThisToolGroup_GBLC.gridx = 0
        useThisToolGroup_GBLC.gridy = 0
        useThisToolGroup.add(self.useThisToolGroup_useItCheckBox, useThisToolGroup_GBLC)
        useThisToolGroup_GBLC.weightx = 0.60
        useThisToolGroup_GBLC.gridx = 1
        useThisToolGroup_GBLC.gridy = 0
        useThisToolGroup.add(JLabel(""), useThisToolGroup_GBLC)
        useThisToolGroup_GBLC.weightx = 0.15
        useThisToolGroup_GBLC.gridx = 2
        useThisToolGroup_GBLC.gridy = 0
        useThisToolGroup.add(useThisToolGroup_loadSettingsButton, useThisToolGroup_GBLC)
        
        # Main Settings tab
        mainTab = JPanel(GridBagLayout())
        mainTab_GBLC = GridBagConstraints()
        mainTab_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        ## Input group
        mainTab_inputGroup = JPanel(GridBagLayout())
        mainTab_inputGroup_GBLC = GridBagConstraints()
        mainTab_inputGroup_GBLC.fill = GridBagConstraints.HORIZONTAL
        mainTab_inputGroup.setBorder(TitledBorder("Input"))
        
        ### Controls (Check box, text field, file chooser)
        mainTab_inputGroup_controlsCheckBox = JCheckBox("Controls")
        
        self.mainTab_inputGroup_controlsDirectoryTextField = JTextField("", focusLost=self.setControlsDirectoryEvent)
        mainTab_inputGroup_controlsDirectoryFileChooserButton = JButton("...", actionPerformed = self.selectControlsDirectoryDir)
        
        mainTab_inputGroup_controlsDirectoryPanel = JPanel(GridBagLayout())
        mainTab_inputGroup_controlsDirectoryPanel_GBLC = GridBagConstraints()
        mainTab_inputGroup_controlsDirectoryPanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        mainTab_inputGroup_controlsDirectoryPanel_GBLC.weightx = 0.99
        mainTab_inputGroup_controlsDirectoryPanel_GBLC.gridx = 0
        mainTab_inputGroup_controlsDirectoryPanel_GBLC.gridy = 0
        mainTab_inputGroup_controlsDirectoryPanel.add(self.mainTab_inputGroup_controlsDirectoryTextField, mainTab_inputGroup_controlsDirectoryPanel_GBLC)
        mainTab_inputGroup_controlsDirectoryPanel_GBLC.weightx = 0.01
        mainTab_inputGroup_controlsDirectoryPanel_GBLC.gridx = 1
        mainTab_inputGroup_controlsDirectoryPanel_GBLC.gridy = 0
        mainTab_inputGroup_controlsDirectoryPanel.add(mainTab_inputGroup_controlsDirectoryFileChooserButton, mainTab_inputGroup_controlsDirectoryPanel_GBLC)
        
        ### Solve for equilibrium for actuator states (Check box)
        mainTab_inputGroup_solveCheckBox = JCheckBox("Solve for equilibrium for actuator states")
        
        ### In degrees (check box)
        #mainTab_inputGroup_inDegreesCheckBox = JCheckBox("in degrees")
        
        ## Finish Input group
        mainTab_inputGroup_GBLC.weightx = 0.1
        mainTab_inputGroup_GBLC.gridx = 0
        mainTab_inputGroup_GBLC.gridy = 0
        mainTab_inputGroup.add(JLabel(""), mainTab_inputGroup_GBLC)
        mainTab_inputGroup_GBLC.weightx = 0.40
        mainTab_inputGroup_GBLC.gridx = 1
        mainTab_inputGroup_GBLC.gridy = 0
        mainTab_inputGroup.add(mainTab_inputGroup_controlsCheckBox, mainTab_inputGroup_GBLC)
        mainTab_inputGroup_GBLC.weightx = 0.40
        mainTab_inputGroup_GBLC.gridx = 2
        mainTab_inputGroup_GBLC.gridy = 0
        mainTab_inputGroup.add(mainTab_inputGroup_controlsDirectoryPanel, mainTab_inputGroup_GBLC)
        mainTab_inputGroup_GBLC.weightx = 0.1
        mainTab_inputGroup_GBLC.gridx = 3
        mainTab_inputGroup_GBLC.gridy = 0
        mainTab_inputGroup.add(JLabel(""), mainTab_inputGroup_GBLC)
        
        mainTab_inputGroup_GBLC.weightx = 0.1
        mainTab_inputGroup_GBLC.gridx = 0
        mainTab_inputGroup_GBLC.gridy = 1
        mainTab_inputGroup.add(JLabel(""), mainTab_inputGroup_GBLC)
        mainTab_inputGroup_GBLC.weightx = 0.80
        mainTab_inputGroup_GBLC.gridx = 1
        mainTab_inputGroup_GBLC.gridy = 1
        mainTab_inputGroup.add(mainTab_inputGroup_solveCheckBox, mainTab_inputGroup_GBLC)
        mainTab_inputGroup_GBLC.weightx = 0.1
        mainTab_inputGroup_GBLC.gridx = 2
        mainTab_inputGroup_GBLC.gridy = 1
        mainTab_inputGroup.add(JLabel(""), mainTab_inputGroup_GBLC)
        
        # TODO: Might need to move this to a different group/section? Or maybe as part of common?
        #mainTab_inputGroup_GBLC.weightx = 0.1
        #mainTab_inputGroup_GBLC.gridx = 0
        #mainTab_inputGroup_GBLC.gridy = 2
        #mainTab_inputGroup.add(JLabel(""), mainTab_inputGroup_GBLC)
        #mainTab_inputGroup_GBLC.weightx = 0.80
        #mainTab_inputGroup_GBLC.gridx = 1
        #mainTab_inputGroup_GBLC.gridy = 2
        #mainTab_inputGroup.add(mainTab_inputGroup_inDegreesCheckBox, mainTab_inputGroup_GBLC)
        #mainTab_inputGroup_GBLC.weightx = 0.1
        #mainTab_inputGroup_GBLC.gridx = 2
        #mainTab_inputGroup_GBLC.gridy = 2
        #mainTab_inputGroup.add(JLabel(""), mainTab_inputGroup_GBLC)
        
        ## Output group
        mainTab_outputGroup = JPanel(GridBagLayout())
        mainTab_outputGroup_GBLC = GridBagConstraints()
        mainTab_outputGroup_GBLC.fill = GridBagConstraints.HORIZONTAL
        mainTab_outputGroup.setBorder(TitledBorder("Output"))
        
        ### Prefix (text field)
        mainTab_outputGroup_prefixLabel = JLabel("Prefix")
        self.mainTab_outputGroup_prefixTextField = JTextField("", focusLost=self.setPrefixEvent)
        
        ### Directory (text field, folder chooser)
        mainTab_outputGroup_directoryLabel = JLabel("Directory")
        self.mainTab_outputGroup_directoryTextField = JTextField("", focusLost=self.setDirectoryEvent)
        mainTab_outputGroup_directoryFileChooserButton = JButton("...", actionPerformed = self.selectDirectoryDir)
        
        mainTab_outputGroup_directoryPanel = JPanel(GridBagLayout())
        mainTab_outputGroup_directoryPanel_GBLC = GridBagConstraints()
        mainTab_outputGroup_directoryPanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        mainTab_outputGroup_directoryPanel_GBLC.weightx = 0.99
        mainTab_outputGroup_directoryPanel_GBLC.gridx = 0
        mainTab_outputGroup_directoryPanel_GBLC.gridy = 0
        mainTab_outputGroup_directoryPanel.add(self.mainTab_outputGroup_directoryTextField, mainTab_outputGroup_directoryPanel_GBLC)
        mainTab_outputGroup_directoryPanel_GBLC.weightx = 0.01
        mainTab_outputGroup_directoryPanel_GBLC.gridx = 1
        mainTab_outputGroup_directoryPanel_GBLC.gridy = 0
        mainTab_outputGroup_directoryPanel.add(mainTab_outputGroup_directoryFileChooserButton, mainTab_outputGroup_directoryPanel_GBLC)
        
        ### Precision (text field)
        mainTab_outputGroup_precisionLabel = JLabel("Precision ")
        self.mainTab_outputGroup_precisionTextField = JTextField("", focusLost=self.setPrecisionEvent)
        
        ## Finish Output group
        mainTab_outputGroup_GBLC.weightx = 0.1
        mainTab_outputGroup_GBLC.gridx = 0
        mainTab_outputGroup_GBLC.gridy = 0
        mainTab_outputGroup.add(JLabel(""), mainTab_outputGroup_GBLC)
        mainTab_outputGroup_GBLC.weightx = 0.40
        mainTab_outputGroup_GBLC.gridx = 1
        mainTab_outputGroup_GBLC.gridy = 0
        mainTab_outputGroup.add(mainTab_outputGroup_prefixLabel, mainTab_outputGroup_GBLC)
        mainTab_outputGroup_GBLC.weightx = 0.40
        mainTab_outputGroup_GBLC.gridx = 2
        mainTab_outputGroup_GBLC.gridy = 0
        mainTab_outputGroup.add(self.mainTab_outputGroup_prefixTextField, mainTab_outputGroup_GBLC)
        mainTab_outputGroup_GBLC.weightx = 0.1
        mainTab_outputGroup_GBLC.gridx = 3
        mainTab_outputGroup_GBLC.gridy = 0
        mainTab_outputGroup.add(JLabel(""), mainTab_outputGroup_GBLC)
        
        mainTab_outputGroup_GBLC.weightx = 0.1
        mainTab_outputGroup_GBLC.gridx = 0
        mainTab_outputGroup_GBLC.gridy = 1
        mainTab_outputGroup.add(JLabel(""), mainTab_outputGroup_GBLC)
        mainTab_outputGroup_GBLC.weightx = 0.40
        mainTab_outputGroup_GBLC.gridx = 1
        mainTab_outputGroup_GBLC.gridy = 1
        mainTab_outputGroup.add(mainTab_outputGroup_directoryLabel, mainTab_outputGroup_GBLC)
        mainTab_outputGroup_GBLC.weightx = 0.40
        mainTab_outputGroup_GBLC.gridx = 2
        mainTab_outputGroup_GBLC.gridy = 1
        mainTab_outputGroup.add(mainTab_outputGroup_directoryPanel, mainTab_outputGroup_GBLC)
        mainTab_outputGroup_GBLC.weightx = 0.1
        mainTab_outputGroup_GBLC.gridx = 3
        mainTab_outputGroup_GBLC.gridy = 1
        mainTab_outputGroup.add(JLabel(""), mainTab_outputGroup_GBLC)
        
        mainTab_outputGroup_GBLC.weightx = 0.1
        mainTab_outputGroup_GBLC.gridx = 0
        mainTab_outputGroup_GBLC.gridy = 2
        mainTab_outputGroup.add(JLabel(""), mainTab_outputGroup_GBLC)
        mainTab_outputGroup_GBLC.weightx = 0.40
        mainTab_outputGroup_GBLC.gridx = 1
        mainTab_outputGroup_GBLC.gridy = 2
        mainTab_outputGroup.add(mainTab_outputGroup_precisionLabel, mainTab_outputGroup_GBLC)
        mainTab_outputGroup_GBLC.weightx = 0.40
        mainTab_outputGroup_GBLC.gridx = 2
        mainTab_outputGroup_GBLC.gridy = 2
        mainTab_outputGroup.add(self.mainTab_outputGroup_precisionTextField, mainTab_outputGroup_GBLC)
        mainTab_outputGroup_GBLC.weightx = 0.1
        mainTab_outputGroup_GBLC.gridx = 3
        mainTab_outputGroup_GBLC.gridy = 2
        mainTab_outputGroup.add(JLabel(""), mainTab_outputGroup_GBLC)
        
        ## Finish Main Settings tab
        mainTab_GBLC.weightx = 0.1
        mainTab_GBLC.gridx = 0
        mainTab_GBLC.gridy = 0
        mainTab.add(JLabel(""), mainTab_GBLC)
        mainTab_GBLC.weightx = 0.80
        mainTab_GBLC.gridx = 1
        mainTab_GBLC.gridy = 0
        mainTab.add(mainTab_inputGroup, mainTab_GBLC)
        mainTab_GBLC.weightx = 0.1
        mainTab_GBLC.gridx = 2
        mainTab_GBLC.gridy = 0
        mainTab.add(JLabel(""), mainTab_GBLC)
        
        mainTab_GBLC.weightx = 0.1
        mainTab_GBLC.gridx = 0
        mainTab_GBLC.gridy = 1
        mainTab.add(JLabel(""), mainTab_GBLC)
        mainTab_GBLC.weightx = 0.80
        mainTab_GBLC.gridx = 1
        mainTab_GBLC.gridy = 1
        mainTab.add(mainTab_outputGroup, mainTab_GBLC)
        mainTab_GBLC.weightx = 0.1
        mainTab_GBLC.gridx = 2
        mainTab_GBLC.gridy = 1
        mainTab.add(JLabel(""), mainTab_GBLC)
        
        self.jointReactTabs.addTab("Main Settings", mainTab)
        
        ## Analyses Tab
        analysesTab = JPanel(GridBagLayout())
        analysesTab_GBLC = GridBagConstraints()
        analysesTab_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        ### Entry group (Dynamic)
        analysesTab_entryGroup = JPanel(GridBagLayout())
        analysesTab_entryGroup_GBLC = GridBagConstraints()
        analysesTab_entryGroup_GBLC.fill = GridBagConstraints.HORIZONTAL
        analysesTab_entryGroup.setBorder(TitledBorder("Analysis"))
        
        #### Joint name (Combo box)
        analysesTab_entryGroup_jointNameComboBoxOptions = ["a", "b", "c"]
        analysesTab_entryGroup_jointNameComboBox = JComboBox(analysesTab_entryGroup_jointNameComboBoxOptions)
        
        #### Apply on body (Combo box)
        analysesTab_entryGroup_applyOnBodiesComboBoxOptions = ["a", "b", "c"]
        analysesTab_entryGroup_applyOnBodiesComboBox = JComboBox(analysesTab_entryGroup_applyOnBodiesComboBoxOptions)
        
        ### Finish Entry group
        analysesTab_entryGroup_GBLC.weightx = 0.1
        analysesTab_entryGroup_GBLC.gridx = 0
        analysesTab_entryGroup_GBLC.gridy = 0
        analysesTab_entryGroup.add(JLabel(""), analysesTab_entryGroup_GBLC)
        analysesTab_entryGroup_GBLC.weightx = 0.80
        analysesTab_entryGroup_GBLC.gridx = 1
        analysesTab_entryGroup_GBLC.gridy = 0
        analysesTab_entryGroup.add(analysesTab_entryGroup_jointNameComboBox, analysesTab_entryGroup_GBLC)
        analysesTab_entryGroup_GBLC.weightx = 0.1
        analysesTab_entryGroup_GBLC.gridx = 2
        analysesTab_entryGroup_GBLC.gridy = 0
        analysesTab_entryGroup.add(JLabel(""), analysesTab_entryGroup_GBLC)
        
        analysesTab_entryGroup_GBLC.weightx = 0.1
        analysesTab_entryGroup_GBLC.gridx = 0
        analysesTab_entryGroup_GBLC.gridy = 1
        analysesTab_entryGroup.add(JLabel(""), analysesTab_entryGroup_GBLC)
        analysesTab_entryGroup_GBLC.weightx = 0.80
        analysesTab_entryGroup_GBLC.gridx = 1
        analysesTab_entryGroup_GBLC.gridy = 1
        analysesTab_entryGroup.add(analysesTab_entryGroup_applyOnBodiesComboBox, analysesTab_entryGroup_GBLC)
        analysesTab_entryGroup_GBLC.weightx = 0.1
        analysesTab_entryGroup_GBLC.gridx = 2
        analysesTab_entryGroup_GBLC.gridy = 1
        analysesTab_entryGroup.add(JLabel(""), analysesTab_entryGroup_GBLC)
        
        ## Finish Analyses tab
        analysesTab_GBLC.weightx = 0.1
        analysesTab_GBLC.gridx = 0
        analysesTab_GBLC.gridy = 0
        analysesTab.add(JLabel(""), analysesTab_GBLC)
        analysesTab_GBLC.weightx = 0.80
        analysesTab_GBLC.gridx = 1
        analysesTab_GBLC.gridy = 0
        analysesTab.add(analysesTab_entryGroup, analysesTab_GBLC)
        analysesTab_GBLC.weightx = 0.1
        analysesTab_GBLC.gridx = 2
        analysesTab_GBLC.gridy = 0
        analysesTab.add(JLabel(""), analysesTab_GBLC)
        
        self.jointReactTabs.addTab("Analyses", analysesTab)
        
        # Finish card
        self.jointReactToolPanel_GBLC.weightx = 0.1
        self.jointReactToolPanel_GBLC.gridx = 0
        self.jointReactToolPanel_GBLC.gridy = 0
        self.jointReactToolPanel.add(JLabel(""), self.jointReactToolPanel_GBLC)
        self.jointReactToolPanel_GBLC.weightx = 0.80
        self.jointReactToolPanel_GBLC.gridx = 1
        self.jointReactToolPanel_GBLC.gridy = 0
        self.jointReactToolPanel.add(self.jointReactTabs, self.jointReactToolPanel_GBLC)
        self.jointReactToolPanel_GBLC.weightx = 0.1
        self.jointReactToolPanel_GBLC.gridx = 2
        self.jointReactToolPanel_GBLC.gridy = 0
        self.jointReactToolPanel.add(JLabel(""), self.jointReactToolPanel_GBLC)
        
        self.jointReactToolCard.add(useThisToolGroup, BorderLayout.NORTH)
        self.jointReactToolCard.add(self.jointReactToolPanel, BorderLayout.CENTER)
        
    def getCard(self):
        return self.jointReactToolCard
    
    def onUseItCheckBoxSelect(self, event):
        if (self.useThisToolGroup_useItCheckBox.isSelected()):
            self.myConfig.useJointReactionTool = 'true'
        else:
            self.myConfig.useJointReactionTool = 'false'
    
    
    #################################################
    # Save the value in the UI to the config object #
    #################################################
    def setControlsDirectory(self):
        self.myConfig.jointReactionTool_controlsFile = self.mainTab_inputGroup_controlsDirectoryTextField.text
    
    def setPrefix(self):
        self.myConfig.jointReactionTool_resultsDir = self.mainTab_outputGroup_prefixTextField.text
        
    def setDirectory(self):
        self.myConfig.jointReactionTool_resultsDir = self.mainTab_outputGroup_directoryTextField.text
        
    def setPrecision(self):
        self.myConfig.jointReactionTool_outputPrecision = self.mainTab_outputGroup_precisionTextField.text
        
    
    ###############################
    # Save contents of a text box #
    ###############################
    def setControlsDirectoryEvent(self):
        self.setControlsDirectory()
    
    def setPrefixEvent(self, event):
        self.setPrefix()
        
    def setDirectoryEvent(self, event):
        self.setDirectory()
        
    def setPrecisionEvent(self, event):
        self.setPrecision()
        
    
    ###########################################################
    # Show file chooser dialog when a "..." button is pressed #
    ###########################################################
    def selectControlsDirectoryDir(self, event):
        # Create and show a file chooser (True = select file, False = select directory)
        myChooser = bessFileChooser(False)
        # Set the text field text to the results of the file chooser
        self.outputGroup_directoryTextField.text = myChooser.fullPath.toString()
        # Save the value to the config class
        self.setControlsDirectoryEvent()
    
    def selectDirectoryDir(self, event):
        # Create and show a file chooser (True = select file, False = select directory)
        myChooser = bessFileChooser(False)
        # Set the text field text to the results of the file chooser
        self.outputGroup_directoryTextField.text = myChooser.fullPath.toString()
        # Save the value to the config class
        self.setDirectory()
        
    # Save/load from config
    # https://docs.python.org/2/library/xml.etree.elementtree.html
    def loadSettings(self, event):
        # Create and show a file chooser (True = select file, False = select directory)
        myChooser = bessFileChooser(True)
        self.loadSettingsFromFile(myChooser.fullPath.toString())
        
    def loadSettingsFromFile(self, filePath):
        # Check if "use it" is checked
        if(self.myConfig.useJointReactionTool == 'true'):
            self.useThisToolGroup_useItCheckBox.setSelected(True)
        else:
            self.useThisToolGroup_useItCheckBox.setSelected(False)
            
        # Open the xml file
        if(os.path.exists(filePath)):
            print 'Opening config file: %s' % (filePath)
            tree = ET.parse(filePath)
            
            # Parse the tree
            root = tree.getroot()
            self.parseTree(root)
            
            self.setControlsDirectory()
            #self.setPrefix()
            self.setDirectory()
            self.setPrecision()
            
            print 'Done loading settings'
        else:
            print 'Could not find file: %s' % (filePath)
        
    def parseTree(self, node):
        print 'Node tag: %s,  text: %s' % (node.tag, node.text)
        
        if('controls_file' == node.tag):
            self.mainTab_inputGroup_controlsDirectoryTextField.text = node.text
        elif('results_directory' == node.tag):
            self.mainTab_outputGroup_directoryTextField.text = node.text
        elif('output_precision' == node.tag):
            self.mainTab_outputGroup_precisionTextField.text = node.text
        
        # Look at any children of children
        for child in node:
            self.parseTree(child)
            
# end bessConfigUIJointReactToolCard class

