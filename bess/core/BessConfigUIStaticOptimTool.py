

################################################################################
#
# This file creates the Configuration UI components and handles user input as
# it relates to BESS configuration.
#
################################################################################
from javax.swing import JFrame, JPanel, JButton, JFileChooser, JTextField, JLabel, JCheckBox, SwingConstants, JTabbedPane, JScrollPane, JComboBox, JTable, JRadioButton, ButtonGroup
from javax.swing.border import TitledBorder
from java.awt import GridLayout, FlowLayout, Color, GridBagLayout, GridBagConstraints, CardLayout, BorderLayout

from xml.dom.minidom import parse, parseString, getDOMImplementation, Document
import xml.etree.cElementTree as ET

import os.path

from core.BessConfigUICardBase import bessCardBase
from core.BessUIFileChooser import bessFileChooser

'''
TODO: Add description
'''
class bessConfigUIStaticOptimToolCard(bessCardBase):
    def __init__(self, myConfig):
        self.myConfig = myConfig
        
        # Create the UI components
        self.staticOptimToolCard = JPanel(BorderLayout())
        
        self.staticOptimToolPanel = JPanel(GridBagLayout())
        self.staticOptimToolPanel_GBLC = GridBagConstraints()
        self.staticOptimToolPanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        # Use This Tool settings
        useThisToolGroup = JPanel(GridBagLayout())
        useThisToolGroup_GBLC = GridBagConstraints()
        useThisToolGroup_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        self.useThisToolGroup_useItCheckBox = JCheckBox("Perform Static Optimization", True, actionPerformed=self.onUseItCheckBoxSelect)
        useThisToolGroup_loadSettingsButton = JButton("Load settings", actionPerformed = self.loadSettings)
        
        useThisToolGroup_GBLC.weightx = 0.25
        useThisToolGroup_GBLC.gridx = 0
        useThisToolGroup_GBLC.gridy = 0
        useThisToolGroup.add(self.useThisToolGroup_useItCheckBox, useThisToolGroup_GBLC)
        useThisToolGroup_GBLC.weightx = 0.60
        useThisToolGroup_GBLC.gridx = 1
        useThisToolGroup_GBLC.gridy = 0
        useThisToolGroup.add(JLabel(" "), useThisToolGroup_GBLC)
        useThisToolGroup_GBLC.weightx = 0.15
        useThisToolGroup_GBLC.gridx = 2
        useThisToolGroup_GBLC.gridy = 0
        useThisToolGroup.add(useThisToolGroup_loadSettingsButton, useThisToolGroup_GBLC)
        
        # Input group
        inputGroup = JPanel(GridBagLayout())
        inputGroup_GBLC = GridBagConstraints()
        inputGroup_GBLC.fill = GridBagConstraints.HORIZONTAL
        inputGroup.setBorder(TitledBorder("Input"))
        
        ## States (text field, file chooser)
        self.inputGroup_statesRadioButton = JRadioButton("States", actionPerformed = self.statesRadioButtonPressed)
        
        self.inputGroup_statesTextField = JTextField("", focusLost=self.setInputStatesEvent)
        inputGroup_statesFileChooserButton = JButton("...", actionPerformed = self.selectStatesFile)
        
        inputGroup_statesFilePanel = JPanel(GridBagLayout())
        inputGroup_statesFilePanel_GBLC = GridBagConstraints()
        inputGroup_statesFilePanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        inputGroup_statesFilePanel_GBLC.weightx = 0.99
        inputGroup_statesFilePanel_GBLC.gridx = 0
        inputGroup_statesFilePanel_GBLC.gridy = 0
        inputGroup_statesFilePanel.add(self.inputGroup_statesTextField, inputGroup_statesFilePanel_GBLC)
        inputGroup_statesFilePanel_GBLC.weightx = 0.01
        inputGroup_statesFilePanel_GBLC.gridx = 1
        inputGroup_statesFilePanel_GBLC.gridy = 0
        inputGroup_statesFilePanel.add(inputGroup_statesFileChooserButton, inputGroup_statesFilePanel_GBLC)
        
        ## Motion - Filter coordinates (check box, text field)
        self.inputGroup_motionRadioButton = JRadioButton("Motion", actionPerformed = self.motionRadioButtonPressed)
        
        self.inputGroup_motionFilterCoordsCheckBox = JCheckBox("Filter coordinates", actionPerformed=self.onMotionFilterCoordsCheckBoxSelect)
        self.inputGroup_motionFilterCoordsTextField = JTextField("", focusLost=self.setMotionFilterCoordsEvent)
        inputGroup_motionFilterCoordsLabel = JLabel("Hz")
        
        inputGroup_motionPanel = JPanel(GridBagLayout())
        inputGroup_motionPanel_GBLC = GridBagConstraints()
        inputGroup_motionPanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        inputGroup_motionPanel_GBLC.weightx = 0.20
        inputGroup_motionPanel_GBLC.gridx = 0
        inputGroup_motionPanel_GBLC.gridy = 0
        inputGroup_motionPanel.add(self.inputGroup_motionFilterCoordsCheckBox, inputGroup_motionPanel_GBLC)
        inputGroup_motionPanel_GBLC.weightx = 0.70
        inputGroup_motionPanel_GBLC.gridx = 1
        inputGroup_motionPanel_GBLC.gridy = 0
        inputGroup_motionPanel.add(self.inputGroup_motionFilterCoordsTextField, inputGroup_motionPanel_GBLC)
        inputGroup_motionPanel_GBLC.weightx = 0.10
        inputGroup_motionPanel_GBLC.gridx = 2
        inputGroup_motionPanel_GBLC.gridy = 0
        inputGroup_motionPanel.add(inputGroup_motionFilterCoordsLabel, inputGroup_motionPanel_GBLC)
        
        # Finish input group
        inputGroup_buttonGrouping = ButtonGroup()
        inputGroup_buttonGrouping.add(self.inputGroup_statesRadioButton)
        inputGroup_buttonGrouping.add(self.inputGroup_motionRadioButton)
        
        inputGroup_GBLC.weightx = 0.1
        inputGroup_GBLC.gridx = 0
        inputGroup_GBLC.gridy = 0
        inputGroup.add(JLabel(""), inputGroup_GBLC)
        inputGroup_GBLC.weightx = 0.40
        inputGroup_GBLC.gridx = 1
        inputGroup_GBLC.gridy = 0
        inputGroup.add(self.inputGroup_statesRadioButton, inputGroup_GBLC)
        inputGroup_GBLC.weightx = 0.40
        inputGroup_GBLC.gridx = 2
        inputGroup_GBLC.gridy = 0
        inputGroup.add(inputGroup_statesFilePanel, inputGroup_GBLC)
        inputGroup_GBLC.weightx = 0.1
        inputGroup_GBLC.gridx = 3
        inputGroup_GBLC.gridy = 0
        inputGroup.add(JLabel(""), inputGroup_GBLC)
        
        inputGroup_GBLC.weightx = 0.1
        inputGroup_GBLC.gridx = 0
        inputGroup_GBLC.gridy = 1
        inputGroup.add(JLabel(""), inputGroup_GBLC)
        inputGroup_GBLC.weightx = 0.40
        inputGroup_GBLC.gridx = 1
        inputGroup_GBLC.gridy = 1
        inputGroup.add(self.inputGroup_motionRadioButton, inputGroup_GBLC)
        inputGroup_GBLC.weightx = 0.40
        inputGroup_GBLC.gridx = 2
        inputGroup_GBLC.gridy = 1
        inputGroup.add(inputGroup_motionPanel, inputGroup_GBLC)
        inputGroup_GBLC.weightx = 0.1
        inputGroup_GBLC.gridx = 3
        inputGroup_GBLC.gridy = 1
        inputGroup.add(JLabel(""), inputGroup_GBLC)
        
        # Static optimization group
        staticOptimGroup = JPanel(GridBagLayout())
        staticOptimGroup_GBLC = GridBagConstraints()
        staticOptimGroup_GBLC.fill = GridBagConstraints.HORIZONTAL
        staticOptimGroup.setBorder(TitledBorder("Static Optimization"))
        
        ## Objective function group
        staticOptimGroup_objectiveFuncGroup = JPanel(GridBagLayout())
        staticOptimGroup_objectiveFuncGroup_GBLC = GridBagConstraints()
        staticOptimGroup_objectiveFuncGroup_GBLC.fill = GridBagConstraints.HORIZONTAL
        staticOptimGroup_objectiveFuncGroup.setBorder(TitledBorder("Objective Function"))
        
        ### Sum of muscle activiation (text field)
        staticOptimGroup_objectiveFuncGroup_sumLabel = JLabel("Sum of (muscle activation) ^")
        self.staticOptimGroup_objectiveFuncGroup_sumTextField = JTextField("", focusLost=self.setObjectiveFuncGroupEvent)
        
        staticOptimGroup_objectiveFuncGroup_sumPanel = JPanel(GridBagLayout())
        staticOptimGroup_objectiveFuncGroup_sumPanel_GBLC = GridBagConstraints()
        staticOptimGroup_objectiveFuncGroup_sumPanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        staticOptimGroup_objectiveFuncGroup_sumPanel_GBLC.weightx = 0.50
        staticOptimGroup_objectiveFuncGroup_sumPanel_GBLC.gridx = 0
        staticOptimGroup_objectiveFuncGroup_sumPanel_GBLC.gridy = 0
        staticOptimGroup_objectiveFuncGroup_sumPanel.add(staticOptimGroup_objectiveFuncGroup_sumLabel, staticOptimGroup_objectiveFuncGroup_sumPanel_GBLC)
        staticOptimGroup_objectiveFuncGroup_sumPanel_GBLC.weightx = 0.50
        staticOptimGroup_objectiveFuncGroup_sumPanel_GBLC.gridx = 1
        staticOptimGroup_objectiveFuncGroup_sumPanel_GBLC.gridy = 0
        staticOptimGroup_objectiveFuncGroup_sumPanel.add(self.staticOptimGroup_objectiveFuncGroup_sumTextField, staticOptimGroup_objectiveFuncGroup_sumPanel_GBLC)
        
        ### User muscle force-length-velocity relation (check box)
        staticOptimGroup_objectiveFuncGroup_forceLengthVelocityCheckBox = JCheckBox("Use muscle force-length-velocity relation")
        
        ## Finish Static Optimization group
        staticOptimGroup_objectiveFuncGroup_GBLC.weightx = 0.60
        staticOptimGroup_objectiveFuncGroup_GBLC.gridx = 0
        staticOptimGroup_objectiveFuncGroup_GBLC.gridy = 0
        staticOptimGroup_objectiveFuncGroup.add(staticOptimGroup_objectiveFuncGroup_sumPanel, staticOptimGroup_objectiveFuncGroup_GBLC)
        staticOptimGroup_objectiveFuncGroup_GBLC.weightx = 0.40
        staticOptimGroup_objectiveFuncGroup_GBLC.gridx = 0
        staticOptimGroup_objectiveFuncGroup_GBLC.gridy = 1
        staticOptimGroup_objectiveFuncGroup.add(staticOptimGroup_objectiveFuncGroup_forceLengthVelocityCheckBox, staticOptimGroup_objectiveFuncGroup_GBLC)
        
        ## Step Interval group
        staticOptimGroup_stepIntervalGroup = JPanel(GridBagLayout())
        staticOptimGroup_stepIntervalGroup_GBLC = GridBagConstraints()
        staticOptimGroup_stepIntervalGroup_GBLC.fill = GridBagConstraints.HORIZONTAL
        staticOptimGroup_stepIntervalGroup.setBorder(TitledBorder("Step Interval"))
        
        ### Analyze every (text field)
        staticOptimGroup_stepIntervalGroup_stepLabel = JLabel("Analyze every ")
        self.staticOptimGroup_stepIntervalGroup_stepTextField = JTextField("", focusLost=self.setStaticOptStepIntervalEvent)
        staticOptimGroup_stepIntervalGroup_stepUnitsLabel = JLabel(" step(s)")
        
        ## Finish Step Interval group
        staticOptimGroup_stepIntervalGroup_GBLC.weightx = 0.60
        staticOptimGroup_stepIntervalGroup_GBLC.gridx = 0
        staticOptimGroup_stepIntervalGroup_GBLC.gridy = 0
        staticOptimGroup_stepIntervalGroup.add(staticOptimGroup_stepIntervalGroup_stepLabel, staticOptimGroup_stepIntervalGroup_GBLC)
        staticOptimGroup_stepIntervalGroup_GBLC.weightx = 0.20
        staticOptimGroup_stepIntervalGroup_GBLC.gridx = 1
        staticOptimGroup_stepIntervalGroup_GBLC.gridy = 0
        staticOptimGroup_stepIntervalGroup.add(self.staticOptimGroup_stepIntervalGroup_stepTextField, staticOptimGroup_stepIntervalGroup_GBLC)
        staticOptimGroup_stepIntervalGroup_GBLC.weightx = 0.20
        staticOptimGroup_stepIntervalGroup_GBLC.gridx = 2
        staticOptimGroup_stepIntervalGroup_GBLC.gridy = 0
        staticOptimGroup_stepIntervalGroup.add(staticOptimGroup_stepIntervalGroup_stepUnitsLabel, staticOptimGroup_stepIntervalGroup_GBLC)
        
        ## Finish Static Optimization group
        staticOptimGroup_GBLC.weightx = 0.60
        staticOptimGroup_GBLC.gridx = 0
        staticOptimGroup_GBLC.gridy = 0
        staticOptimGroup.add(staticOptimGroup_objectiveFuncGroup, staticOptimGroup_GBLC)
        staticOptimGroup_GBLC.weightx = 0.40
        staticOptimGroup_GBLC.gridx = 1
        staticOptimGroup_GBLC.gridy = 0
        staticOptimGroup.add(staticOptimGroup_stepIntervalGroup, staticOptimGroup_GBLC)
        
        # Output group
        outputGroup = JPanel(GridBagLayout())
        outputGroup_GBLC = GridBagConstraints()
        outputGroup_GBLC.fill = GridBagConstraints.HORIZONTAL
        outputGroup.setBorder(TitledBorder("Output"))
        
        ## Prefix (text field)
        outputGroup_prefixLabel = JLabel("Prefix")
        self.outputGroup_prefixTextField = JTextField("", focusLost=self.setOutputPrefixEvent)
        
        ## Directory (text field, folder chooser)
        outputGroup_directoryLabel = JLabel("Directory")
        self.outputGroup_directoryTextField = JTextField("", focusLost=self.setOutputDirEvent)
        outputGroup_directoryFileChooserButton = JButton("...", actionPerformed = self.selectDirectoryFile)
        
        outputGroup_directoryPanel = JPanel(GridBagLayout())
        outputGroup_directoryPanel_GBLC = GridBagConstraints()
        outputGroup_directoryPanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        outputGroup_directoryPanel_GBLC.weightx = 0.99
        outputGroup_directoryPanel_GBLC.gridx = 0
        outputGroup_directoryPanel_GBLC.gridy = 0
        outputGroup_directoryPanel.add(self.outputGroup_directoryTextField, outputGroup_directoryPanel_GBLC)
        outputGroup_directoryPanel_GBLC.weightx = 0.01
        outputGroup_directoryPanel_GBLC.gridx = 1
        outputGroup_directoryPanel_GBLC.gridy = 0
        outputGroup_directoryPanel.add(outputGroup_directoryFileChooserButton, outputGroup_directoryPanel_GBLC)
        
        ## Precision (text field)
        outputGroup_precisionLabel = JLabel("Precision ")
        self.outputGroup_precisionTextField = JTextField("", focusLost=self.setOutputPrecisionEvent)
        
        # Finish Output group
        outputGroup_GBLC.weightx = 0.1
        outputGroup_GBLC.gridx = 0
        outputGroup_GBLC.gridy = 0
        outputGroup.add(JLabel(""), outputGroup_GBLC)
        outputGroup_GBLC.weightx = 0.40
        outputGroup_GBLC.gridx = 1
        outputGroup_GBLC.gridy = 0
        outputGroup.add(outputGroup_prefixLabel, outputGroup_GBLC)
        outputGroup_GBLC.weightx = 0.40
        outputGroup_GBLC.gridx = 2
        outputGroup_GBLC.gridy = 0
        outputGroup.add(self.outputGroup_prefixTextField, outputGroup_GBLC)
        outputGroup_GBLC.weightx = 0.1
        outputGroup_GBLC.gridx = 3
        outputGroup_GBLC.gridy = 0
        outputGroup.add(JLabel(""), outputGroup_GBLC)
        
        outputGroup_GBLC.weightx = 0.1
        outputGroup_GBLC.gridx = 0
        outputGroup_GBLC.gridy = 1
        outputGroup.add(JLabel(""), outputGroup_GBLC)
        outputGroup_GBLC.weightx = 0.40
        outputGroup_GBLC.gridx = 1
        outputGroup_GBLC.gridy = 1
        outputGroup.add(outputGroup_directoryLabel, outputGroup_GBLC)
        outputGroup_GBLC.weightx = 0.40
        outputGroup_GBLC.gridx = 2
        outputGroup_GBLC.gridy = 1
        outputGroup.add(outputGroup_directoryPanel, outputGroup_GBLC)
        outputGroup_GBLC.weightx = 0.1
        outputGroup_GBLC.gridx = 3
        outputGroup_GBLC.gridy = 1
        outputGroup.add(JLabel(""), outputGroup_GBLC)
        
        outputGroup_GBLC.weightx = 0.1
        outputGroup_GBLC.gridx = 0
        outputGroup_GBLC.gridy = 2
        outputGroup.add(JLabel(""), outputGroup_GBLC)
        outputGroup_GBLC.weightx = 0.40
        outputGroup_GBLC.gridx = 1
        outputGroup_GBLC.gridy = 2
        outputGroup.add(outputGroup_precisionLabel, outputGroup_GBLC)
        outputGroup_GBLC.weightx = 0.40
        outputGroup_GBLC.gridx = 2
        outputGroup_GBLC.gridy = 2
        outputGroup.add(self.outputGroup_precisionTextField, outputGroup_GBLC)
        outputGroup_GBLC.weightx = 0.1
        outputGroup_GBLC.gridx = 3
        outputGroup_GBLC.gridy = 2
        outputGroup.add(JLabel(""), outputGroup_GBLC)
        
        # Finish card
        self.staticOptimToolPanel_GBLC.weightx = 0.1
        self.staticOptimToolPanel_GBLC.gridx = 0
        self.staticOptimToolPanel_GBLC.gridy = 0
        self.staticOptimToolPanel.add(JLabel(""), self.staticOptimToolPanel_GBLC)
        self.staticOptimToolPanel_GBLC.weightx = 0.80
        self.staticOptimToolPanel_GBLC.gridx = 1
        self.staticOptimToolPanel_GBLC.gridy = 0
        self.staticOptimToolPanel.add(inputGroup, self.staticOptimToolPanel_GBLC)
        self.staticOptimToolPanel_GBLC.weightx = 0.1
        self.staticOptimToolPanel_GBLC.gridx = 2
        self.staticOptimToolPanel_GBLC.gridy = 0
        self.staticOptimToolPanel.add(JLabel(""), self.staticOptimToolPanel_GBLC)
        
        self.staticOptimToolPanel_GBLC.weightx = 0.1
        self.staticOptimToolPanel_GBLC.gridx = 0
        self.staticOptimToolPanel_GBLC.gridy = 1
        self.staticOptimToolPanel.add(JLabel(""), self.staticOptimToolPanel_GBLC)
        self.staticOptimToolPanel_GBLC.weightx = 0.80
        self.staticOptimToolPanel_GBLC.gridx = 1
        self.staticOptimToolPanel_GBLC.gridy = 1
        self.staticOptimToolPanel.add(staticOptimGroup, self.staticOptimToolPanel_GBLC)
        self.staticOptimToolPanel_GBLC.weightx = 0.1
        self.staticOptimToolPanel_GBLC.gridx = 2
        self.staticOptimToolPanel_GBLC.gridy = 1
        self.staticOptimToolPanel.add(JLabel(""), self.staticOptimToolPanel_GBLC)
        
        self.staticOptimToolPanel_GBLC.weightx = 0.1
        self.staticOptimToolPanel_GBLC.gridx = 0
        self.staticOptimToolPanel_GBLC.gridy = 2
        self.staticOptimToolPanel.add(JLabel(" "), self.staticOptimToolPanel_GBLC)
        self.staticOptimToolPanel_GBLC.weightx = 0.80
        self.staticOptimToolPanel_GBLC.gridx = 1
        self.staticOptimToolPanel_GBLC.gridy = 2
        self.staticOptimToolPanel.add(outputGroup, self.staticOptimToolPanel_GBLC)
        self.staticOptimToolPanel_GBLC.weightx = 0.1
        self.staticOptimToolPanel_GBLC.gridx = 2
        self.staticOptimToolPanel_GBLC.gridy = 2
        self.staticOptimToolPanel.add(JLabel(""), self.staticOptimToolPanel_GBLC)
        
        self.staticOptimToolCard.add(useThisToolGroup, BorderLayout.NORTH)
        self.staticOptimToolCard.add(self.staticOptimToolPanel, BorderLayout.CENTER)
        
    def getCard(self):
        return self.staticOptimToolCard
    
    
    def onAdjustModelCheckBoxSelect(self, event):
        self.setAdjustModelFile()
    
    def onMotionFilterCoordsCheckBoxSelect(self, event):
        if(self.inputGroup_motionFilterCoordsCheckBox.isSelected()):
            self.inputGroup_motionRadioButton.setSelected(True)
            self.setMotionFilterCoords()
        else:
            self.inputGroup_statesRadioButton.setSelected(True)
            self.setInputStates()
    
    def onUseItCheckBoxSelect(self, event):
        if (self.useThisToolGroup_useItCheckBox.isSelected()):
            self.myConfig.useStaticOptTool = 'true'
        else:
            self.myConfig.useStaticOptTool = 'false'
    
    def statesRadioButtonPressed(self, event):
        self.setInputStates()
    
    def motionRadioButtonPressed(self, event):
        self.setMotionFilterCoords()
        
    #################################################
    # Save the value in the UI to the config object #
    #################################################
    
    def setInputStates(self):
        if(self.inputGroup_statesRadioButton.isSelected()):
            self.myConfig.staticOptTool_statesFile = self.inputGroup_statesTextField.text
            self.myConfig.staticOptTool_lowpassCutoffFreqForCoord = ""
        else:
            self.myConfig.staticOptTool_statesFile = ""
    
    def setMotionFilterCoords(self):
        if(self.inputGroup_motionRadioButton.isSelected()):
            if (self.inputGroup_motionFilterCoordsCheckBox.isSelected()):
                self.myConfig.staticOptTool_lowpassCutoffFreqForCoord = self.inputGroup_motionFilterCoordsTextField.text
                self.myConfig.staticOptTool_statesFile = ""
            else:
                self.myConfig.staticOptTool_lowpassCutoffFreqForCoord = ""
        else:
            self.myConfig.staticOptTool_lowpassCutoffFreqForCoord = ""
    
    def setObjectiveFuncGroup(self):
        self.myConfig.staticOptTool_analysis_activationExponent = self.staticOptimGroup_objectiveFuncGroup_sumTextField.text
        
    def setStaticOptStepInterval(self):
        self.myConfig.staticOptTool_analysis_stepInterval = self.staticOptimGroup_stepIntervalGroup_stepTextField.text
        
    def setOutputPrefix(self):
        self.setOutputDir()
        
    def setOutputDir(self):
        self.myConfig.staticOptTool_resultsDir = self.outputGroup_directoryTextField.text + self.outputGroup_prefixTextField.text
        
    def setOutputPrecision(self):
        self.myConfig.staticOptTool_outputPrecision = self.outputGroup_precisionTextField.text
        
        
    ###############################
    # Save contents of a text box #
    ###############################
    def setInputStatesEvent(self, event):
        self.setInputStates()
        
    def setMotionFilterCoordsEvent(self, event):
        self.setMotionFilterCoords()
        
    def setObjectiveFuncGroupEvent(self, event):
        self.setObjectiveFuncGroup()
        
    def setStaticOptStepIntervalEvent(self, event):
        self.setStaticOptStepInterval()
        
    def setOutputPrefixEvent(self, event):
        self.setOutputPrefix()
        
    def setOutputDirEvent(self, event):
        self.setOutputDir()
        
    def setOutputPrecisionEvent(self, event):
        self.setOutputPrecision()
        
        
    ###########################################################
    # Show file chooser dialog when a "..." button is pressed #
    ###########################################################
    
    def selectStatesFile(self, event):
        # Create and show a file chooser (True = select file, False = select directory)
        myChooser = bessFileChooser(True)
        # Set the text field text to the results of the file chooser
        self.inputGroup_statesTextField.text = myChooser.fullPath.toString()
        # Save the value to the config class
        self.setInputStates()
        
    def selectDirectoryFile(self, event):
        # Create and show a file chooser (True = select file, False = select directory)
        myChooser = bessFileChooser(False)
        # Set the text field text to the results of the file chooser
        self.outputGroup_directoryTextField.text = myChooser.fullPath.toString()
        # Save the value to the config class
        self.setOutputDir()
    
    
    # Save/load from config
    # https://docs.python.org/2/library/xml.etree.elementtree.html
    def loadSettings(self, event):
        # Create and show a file chooser (True = select file, False = select directory)
        myChooser = bessFileChooser(True)
        self.loadSettingsFromFile(myChooser.fullPath.toString())
        
    def loadSettingsFromFile(self, filePath):
        # Check if "use it" is checked
        if(self.myConfig.useStaticOptTool == 'true'):
            self.useThisToolGroup_useItCheckBox.setSelected(True)
        else:
            self.useThisToolGroup_useItCheckBox.setSelected(False)
            
        if(os.path.exists(filePath)):
            # Open the xml file
            print 'Opening config file: %s' % (filePath)
            tree = ET.parse(filePath)
            
            # Parse the tree
            root = tree.getroot()
            self.parseTree(root)
            
            self.setInputStates()
            self.setMotionFilterCoords()
            self.setObjectiveFuncGroup()
            self.setStaticOptStepInterval()
            self.setOutputPrefix()
            self.setOutputDir()
            self.setOutputPrecision()
            
            print 'Done loading settings'
        else:
            print 'Could not find file: %s' % (filePath)
        
    def parseTree(self, node):
        print 'Node tag: %s,  text: %s' % (node.tag, node.text)
        
        if('states_file' == node.tag):
            if(None == node.text):
                self.inputGroup_statesTextField.text = ""
            else:
                self.inputGroup_statesTextField.text = node.text
                self.inputGroup_statesRadioButton.setSelected(True)
        elif('lowpass_cutoff_frequency_for_coordinates' == node.tag):
            if(None == node.text):
                self.inputGroup_motionFilterCoordsTextField.text = ''
                self.inputGroup_motionFilterCoordsCheckBox.setSelected(False)
            else:
                self.inputGroup_motionFilterCoordsTextField.text = node.text
                self.inputGroup_motionFilterCoordsCheckBox.setSelected(True)
                self.inputGroup_motionRadioButton.setSelected(True)
        elif('activation_exponent' == node.tag):
            self.staticOptimGroup_objectiveFuncGroup_sumTextField.text = node.text
        elif('step_interval' == node.tag):
            self.staticOptimGroup_stepIntervalGroup_stepTextField.text = node.text
        elif('results_directory' == node.tag):
            self.outputGroup_directoryTextField.text = node.text
        elif('output_precision' == node.tag):
            self.outputGroup_precisionTextField.text = node.text
        
        # Look at any children of children
        for child in node:
            self.parseTree(child)
            
# end bessConfigUIStaticOptimToolCard class

