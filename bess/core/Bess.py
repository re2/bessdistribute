

################################################################################
#
# This file provides the starting point for running BESS.
# A user, from within OpenSim, will specify this file from the menu system:
#  "Scripts" -> "run..."
#
################################################################################

import os
import sys

from java.lang import Thread as JThread

# Add the parent "bess" directory to the Python module search path so that we
# can import modules from different directories.  Subsequent imports of BESS
# scripts may use reload() to ensure that any changes during development get
# reloaded by the interpreter.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
if bessDir not in sys.path:
    sys.path.append(bessDir)
    
def reloadBessModules():
    """
    Reloads all imported modules particular to the BESS code base.
    
    This is helpful in development.  When you have OpenSim open and run BESS
    for the first time, all the "import" statements run and get the latest 
    version of the code.  However, if you then change a module and re-run
    BESS, the code change won't be picked up -- you need to reload the module.
    So for now, let's reload anything that is subject to change during 
    development.  This can be disabled when the BESS code is released.
    """
    
    print '-- Reloading core.Bess* modules'
    
    # Don't iterate using iteritems() since by calling reload(module) you
    # may cause the dictionary to change.  Do get a copy of the dictionary 
    # keys and then iterate over that.
    for moduleName in sys.modules.keys():
        # At first I tried to have multiple prepends here like ['core', 
        # 'utilities', 'configure_forces', ...].  Then I realized there were
        # modules like "core.os" and "configure_forces.sys", etc.  So instead
        # of trying to reload all BESS software, I'm limiting this to just the
        # core Bess* files, which is mainly what we're developing anyway.
        for bessModulePrefix in ['core.Bess']:
            if moduleName.startswith(bessModulePrefix):
                reload(sys.modules[moduleName])
                break
        
# End reloadBessModules.

# Reload any modules that may have changed during this session.  This seems to
# need to follow the definition of the function since it is at a global scope.
#
# Also we seem to need to do this a couple of times for the changes to
# have an effect.  I think it may have to do with how we import using the
# "from x input y as z" construct, but I'm not sure.  Doing it once before the
# import statements and once after seems to have the best effect.
reloadBessModules()

from core.BessData import bessData as data
from core.BessConfig import bessConfig as config
from core.BessSim import bessSim as sim
from core.BessConfigUI import bessConfigUI as configUI
from core.BessResultsUI import bessResultsUI as resultsUI

# See comment above.
reloadBessModules()

def guiExceptionHandler(thread, throwable, frame):
    """
    Handles any unhandled exception.
    
    Ostensibly this is installed in the main GUI thread (or what Java calls
    the Event Dispatch Loop).
    """
    
    # You don't really need to specify the parent frame here -- you likely 
    # can get away with None.  But by specifying the parent frame, the dialog
    # we throw up will be positioned over our GUI which looks better than
    # a parent frame of None, which puts the dialog at the center of the 
    # screen.  (And who knows what happens in multiple monitor cases.)
    # Also note that this code persists after we have closed down the 
    # Configure Forces UI -- this function is likely still installed as ability
    # GUI exception handler.  In that case no worries about "frame" not being
    # valid.  This is handled gracefully -- frame is likely "None" at that
    # point.
    JOptionPane.showMessageDialog(frame,
        makeErrorMessage('Unexpected error.', throwable.toString()),
        "BESS Error",
        JOptionPane.ERROR_MESSAGE);

# End guiExceptionHandler.
        
"""
Main logic
"""
if __name__ == '__main__':

    # Install an uncaught exception handler in the GUI thread so that any
    # exceptions thrown from any of our UI callbacks (button clicks, etc.)
    # get handled.  If we don't do this they go to OpenSim's global handler
    # why is very cryptically displayed to the user (i.e. a small red circle-X
    # in the bottom-right tray).
    JThread.currentThread().uncaughtExceptionHandler = (
        lambda thread, throwable: guiExceptionHandler(thread, throwable, None))
        
    myData = data()                 # Create an instance of the data class to pass around
    myConfig = config()             # Create an instance of the configuration class to pass around
    
    myResultsUI = resultsUI(myConfig, myData) # Create the Results UI
    
    mySim = sim(myConfig, myResultsUI)   # Create an instance of the simulation class
    
    myConfigUI = configUI(myConfig, mySim) # Create the Configuration UI
    
# end of main

