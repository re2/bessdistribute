

################################################################################
#
# This file creates the Configuration UI components for the Configure Forces
# tool.
#
################################################################################

import os
import sys

# Find the parent "bess" directory so that we can import JAR files from
# different directories.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
    
import configure_forces.gui.configureForcesGui_Main as CFGM
from core.BessConfigUICardBase import bessCardBase
import utilities.toolsCommon as TC

# Get the main GUI components from our NetBeans/Java project.
TC.importJar(os.path.join(bessDir, 'lib', 'BessGuiElements.jar'))
from bess.gui.elements import BessConfigUiConfigureForcesPanel

class bessConfigUIConfigureForcesCard(bessCardBase):

    def __init__(self, config, parentFrame):
        """
        Creates the UI elements.
        """
        
        self.config = config
        
        # The panel we're going to ultimately give back to the Bess Config UI
        # is actually laid out in NetBeans/Java.
        self.card = BessConfigUiConfigureForcesPanel()
        
        # Only part of the panel is populated -- the main tab control is in
        # another location and there is a placeholder setup for it.
        self.cfMainObject = CFGM.Main(parentFrame)    
        self.card.mainPanelPlaceholder.add(self.cfMainObject.mainPanel)
        
        # Add in handlers for the various buttons.
        self.card.performConfigureForcesCheck.actionPerformed = (
            self.performConfigureForcesCheckHandler)
        self.card.loadSettingsButton.actionPerformed = self.loadSettingsHandler
        
        # The config object needs a reference to the underlying concrete data
        # held by the embedded Configure Forces UI main panel.  We just need
        # to wire them together.
        self.config.configureForcesTool_Model = self.cfMainObject.model
        
        # Set any initial conditions from config.
        self.setPerformConfigureForcesViewFromConfig()
        
    # End __init__.
    
    def getCard(self):
        """
        Override of base class.
        """
        
        return self.card
    
    # End getCard.
    
    def loadSettingsFromFile(self, xmlPath):
        """
        This function is expected to be here so that the main Config UI can
        load Configure Forces settings when the main config file is loaded.
        Override of base class.
        """
        
        # The config object will be updated.  Adjust the checkbox as
        # appropriate.
        self.setPerformConfigureForcesViewFromConfig()
        
        # Check if the file exists, per the other BESS UI cards/tools.
        if not os.path.exists(xmlPath):
            print 'Could not find file: ', xmlPath
            return
        
        # And of course, fill in the main panel/tab control from XML.
        self.cfMainObject.loadXmlToMainPanel(xmlPath)
        
    # End loadSettingsFromFile.
    
    def onShow(self):
        """
        Override of base class.
        
        Update the Configure Forces UI with the 4 files specified upstream
        in BESS.
        """
        
        # For the model file it is ok to use the initial model file (as
        # opposed to the possibly scaled model) since all we're doing at this
        # point is using it to populate lists of body segments.  And of course
        # the scaled model might not even exist at this point.  So when we
        # save to XML and run the sim we will resolve the proper model to use.
        self.cfMainObject.specifyModelFile(CFGM.SpecifiedFile(mutable=False,
            visible=True,
            immutableFilename=self.config.common_modelFile))
            
        # A note on visibility.  If we made this False the user wouldn't even
        # see the text fields.  That might be what we want -- not sure.  For
        # now I'm defaulting to visible but immutable, so at least the user
        # can see what files are being used.
        self.cfMainObject.specifyIkFile(CFGM.SpecifiedFile(mutable=False,
            visible=True,
            immutableFilename=self.config.common_motionFile))
            
        self.cfMainObject.specifyGroundReactionForceFile(CFGM.SpecifiedFile(
            mutable=False,
            visible=True,
            immutableFilename=self.config.common_groundReactionsFile))
            
        self.cfMainObject.specifyExternalLoadSpecificationFile(
            CFGM.SpecifiedFile(mutable=False,
                visible=True,
                immutableFilename=self.config.common_externalLoadsSpecFile))
        
    # End onShow.
    
    def performConfigureForcesCheckHandler(self, event):
    
        self.setPerformConfigureForcesConfigFromView()
        
    # End performConfigureForcesCheckHandler.
        
    def loadSettingsHandler(self, event):
        # Throw up a file browser.  I'm preferring the version we have in
        # toolsCommon and I'll leave it to my inheritor to unify our codebase
        # around a single file chooser.
        try:
            xmlPath = TC.getSingleFilePath(
                'Select configure forces setup file to load', 
                TC.FileFilter('Configure forces file (.xml)', 'xml'))
        except:
            # If nothing is selected, an exception is thrown.
            return
        
        self.loadSettingsFromFile(xmlPath)
            
    # End loadSettingsHandler.
        
    def setPerformConfigureForcesConfigFromView(self):
        
        self.config.useConfigureForcesTool = TC.boolToLowerCaseString(
            self.card.performConfigureForcesCheck.selected)
        
    # End setPerformConfigureForcesConfigFromView.
    
    def setPerformConfigureForcesViewFromConfig(self):
        
        self.card.performConfigureForcesCheck.selected = (
            TC.boolFromLowerCaseString(self.config.useConfigureForcesTool))
            
    # End setPerformConfigureForcesViewFromConfig.
    
    
    
# End bessConfigUIConfigureForcesCard.

