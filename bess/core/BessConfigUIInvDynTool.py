

################################################################################
#
# This file creates the Configuration UI components and handles user input as
# it relates to BESS configuration.
#
################################################################################
from javax.swing import JFrame, JPanel, JButton, JFileChooser, JTextField, JLabel, JCheckBox, SwingConstants, JTabbedPane, JScrollPane, JComboBox, JTable
from javax.swing.border import TitledBorder
from java.awt import GridLayout, FlowLayout, Color, GridBagLayout, GridBagConstraints, CardLayout, BorderLayout

from xml.dom.minidom import parse, parseString, getDOMImplementation, Document
import xml.etree.cElementTree as ET

import os.path

from core.BessConfigUICardBase import bessCardBase
from core.BessUIFileChooser import bessFileChooser

'''
The bessConfigUIMultiRunCard is used as the Multi-run simulation Card UI component
It is used to allow the user to specify multiple runs on the simulation and
variances between the runs
'''
class bessConfigUIInvDynToolCard(bessCardBase):
    def __init__(self, myConfig):
        self.myConfig = myConfig
        
        # Create the UI components
        self.invDynToolCard = JPanel(BorderLayout())
        
        self.invDynToolPanel = JPanel(GridBagLayout())
        self.invDynToolPanel_GBLC = GridBagConstraints()
        self.invDynToolPanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        # Use This Tool settings
        useThisToolGroup = JPanel(GridBagLayout())
        useThisToolGroup_GBLC = GridBagConstraints()
        useThisToolGroup_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        self.useThisToolGroup_useItCheckBox = JCheckBox("Perform Inverse Dynamics", True, actionPerformed=self.onUseItCheckBoxSelect)
        useThisToolGroup_loadSettingsButton = JButton("Load settings", actionPerformed = self.loadSettings)
        
        useThisToolGroup_GBLC.weightx = 0.25
        useThisToolGroup_GBLC.gridx = 0
        useThisToolGroup_GBLC.gridy = 0
        useThisToolGroup.add(self.useThisToolGroup_useItCheckBox, useThisToolGroup_GBLC)
        useThisToolGroup_GBLC.weightx = 0.60
        useThisToolGroup_GBLC.gridx = 1
        useThisToolGroup_GBLC.gridy = 0
        useThisToolGroup.add(JLabel(""), useThisToolGroup_GBLC)
        useThisToolGroup_GBLC.weightx = 0.15
        useThisToolGroup_GBLC.gridx = 2
        useThisToolGroup_GBLC.gridy = 0
        useThisToolGroup.add(useThisToolGroup_loadSettingsButton, useThisToolGroup_GBLC)
        
        # Output group
        outputGroup = JPanel(GridBagLayout())
        outputGroup_GBLC = GridBagConstraints()
        outputGroup_GBLC.fill = GridBagConstraints.HORIZONTAL
        outputGroup.setBorder(TitledBorder("Output"))
        
        ## Directory (text field, folder chooser)
        outputGroup_directoryLabel = JLabel("Directory")
        self.outputGroup_directoryTextField = JTextField("", focusLost=self.setDirectoryEvent)
        outputGroup_directoryFileChooserButton = JButton("...", actionPerformed = self.selectDirectoryDir)
        
        outputGroup_directoryPanel = JPanel(GridBagLayout())
        outputGroup_directoryPanel_GBLC = GridBagConstraints()
        outputGroup_directoryPanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        outputGroup_directoryPanel_GBLC.weightx = 0.99
        outputGroup_directoryPanel_GBLC.gridx = 0
        outputGroup_directoryPanel_GBLC.gridy = 0
        outputGroup_directoryPanel.add(self.outputGroup_directoryTextField, outputGroup_directoryPanel_GBLC)
        outputGroup_directoryPanel_GBLC.weightx = 0.01
        outputGroup_directoryPanel_GBLC.gridx = 1
        outputGroup_directoryPanel_GBLC.gridy = 0
        outputGroup_directoryPanel.add(outputGroup_directoryFileChooserButton, outputGroup_directoryPanel_GBLC)
        
        # Finish Output group
        outputGroup_GBLC.weightx = 0.1
        outputGroup_GBLC.gridx = 0
        outputGroup_GBLC.gridy = 1
        outputGroup.add(JLabel(""), outputGroup_GBLC)
        outputGroup_GBLC.weightx = 0.40
        outputGroup_GBLC.gridx = 1
        outputGroup_GBLC.gridy = 1
        outputGroup.add(outputGroup_directoryLabel, outputGroup_GBLC)
        outputGroup_GBLC.weightx = 0.40
        outputGroup_GBLC.gridx = 2
        outputGroup_GBLC.gridy = 1
        outputGroup.add(outputGroup_directoryPanel, outputGroup_GBLC)
        outputGroup_GBLC.weightx = 0.1
        outputGroup_GBLC.gridx = 3
        outputGroup_GBLC.gridy = 1
        outputGroup.add(JLabel(""), outputGroup_GBLC)
        
        # Finish card
        self.invDynToolPanel_GBLC.weightx = 0.1
        self.invDynToolPanel_GBLC.gridx = 0
        self.invDynToolPanel_GBLC.gridy = 0
        self.invDynToolPanel.add(JLabel(""), self.invDynToolPanel_GBLC)
        self.invDynToolPanel_GBLC.weightx = 0.80
        self.invDynToolPanel_GBLC.gridx = 1
        self.invDynToolPanel_GBLC.gridy = 0
        self.invDynToolPanel.add(outputGroup, self.invDynToolPanel_GBLC)
        self.invDynToolPanel_GBLC.weightx = 0.1
        self.invDynToolPanel_GBLC.gridx = 2
        self.invDynToolPanel_GBLC.gridy = 0
        self.invDynToolPanel.add(JLabel(""), self.invDynToolPanel_GBLC)
        
        self.invDynToolCard.add(useThisToolGroup, BorderLayout.NORTH)
        self.invDynToolCard.add(self.invDynToolPanel, BorderLayout.CENTER)
        
    def getCard(self):
        return self.invDynToolCard
    
    def onUseItCheckBoxSelect(self, event):
        if (self.useThisToolGroup_useItCheckBox.isSelected()):
            self.myConfig.useInverseDynTool = 'true'
        else:
            self.myConfig.useInverseDynTool = 'false'
    
    #################################################
    # Save the value in the UI to the config object #
    #################################################
    
    def setDirectory(self):
        self.myConfig.inverseDynTool_resultsDir = self.outputGroup_directoryTextField.text
        
    ###############################
    # Save contents of a text box #
    ###############################
    
    def setDirectoryEvent(self, event):
        self.setDirectory()
        
    ###########################################################
    # Show file chooser dialog when a "..." button is pressed #
    ###########################################################
    
    def selectDirectoryDir(self, event):
        # Create and show a file chooser (True = select file, False = select directory)
        myChooser = bessFileChooser(False)
        # Set the text field text to the results of the file chooser
        self.outputGroup_directoryTextField.text = myChooser.fullPath.toString()
        # Save the value to the config class
        self.setDirectory()
        
    # Save/load from config
    # https://docs.python.org/2/library/xml.etree.elementtree.html
    def loadSettings(self, event):
        print 'Loading settings...'
        # Create and show a file chooser (True = select file, False = select directory)
        myChooser = bessFileChooser(True)
        self.loadSettingsFromFile(myChooser.fullPath.toString())
        
    def loadSettingsFromFile(self, filePath):
        # Check if "use it" is checked
        if(self.myConfig.useInverseDynTool == 'true'):
            self.useThisToolGroup_useItCheckBox.setSelected(True)
        else:
            self.useThisToolGroup_useItCheckBox.setSelected(False)
            
        # Open the xml file
        if(os.path.exists(filePath)):
            print 'Opening config file: %s' % (filePath)
            tree = ET.parse(filePath)
            
            # Parse the tree
            root = tree.getroot()
            self.parseTree(root)
            
            # Save values to BessConfig
            self.setDirectory()
            
            print 'Done loading settings'
        else:
            print 'Could not find file: %s' % (filePath)
        
    def parseTree(self, node):
        print 'Node tag: %s,  text: %s' % (node.tag, node.text)
        
        if('results_directory' == node.tag):
            self.outputGroup_directoryTextField.text = node.text
        
        # Look at any children of children
        for child in node:
            self.parseTree(child)
            
# end bessConfigUIInvDynToolCard class

