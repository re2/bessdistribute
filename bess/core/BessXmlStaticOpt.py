

################################################################################
#
#
################################################################################

from xml.dom.minidom import parse, parseString, getDOMImplementation, Document
import xml.etree.cElementTree as ET

class bessXmlStaticOptIO:
    def __init__(self, myConfig):
        print "Initializing bessXmlStaticOpt"
        self.myConfig = myConfig
        print "Done initializing bessXmlStaticOpt"
        
    # http://stackoverflow.com/questions/3605680/creating-a-simple-xml-file-using-python
    def writeConfigFile(self):
        print "Writing Static Optimization xml file"
        #<?xml version="1.0" encoding="UTF-8" ?>
        #<OpenSimDocument Version="30000">
        self.xmlTag_OpenSimDocument = ET.Element("OpenSimDocument", Version="30000")
        
        #   <AnalyzeTool name="3DGaitModel2392"> ----------> TODO: This is probably not the value we want here... Duplicate of the next value
        xmlTag_AnalyzeTool = ET.SubElement(self.xmlTag_OpenSimDocument, "AnalyzeTool", name=self.myConfig.common_modelFile)
        
        #       <!--Name of the .osim file used to construct a model.-->
        #       <model_file />
        xmlTag_AnalyzeTool_modelFile = ET.SubElement(xmlTag_AnalyzeTool, "model_file")
        xmlTag_AnalyzeTool_modelFile.text = self.myConfig.common_modelFile
        
        #       <!--Replace the model's force set with sets specified in <force_set_files>? If false, the force set is appended to.-->
        #       <replace_force_set>false</replace_force_set>
        xmlTag_AnalyzeTool_replaceForceSet = ET.SubElement(xmlTag_AnalyzeTool, "replace_force_set")
        xmlTag_AnalyzeTool_replaceForceSet.text = self.myConfig.common_replaceForceSet
        
        #       <!--List of xml files used to construct an force set for the model.-->
        #       <force_set_files />
        xmlTag_AnalyzeTool_forceSetFiles = ET.SubElement(xmlTag_AnalyzeTool, "force_set_files")
        xmlTag_AnalyzeTool_forceSetFiles.text = self.myConfig.common_forceSetFile
        
        #       <!--Directory used for writing results.-->
        #       <results_directory>../../../../OpenSim 3.3/Models/Gait2392_Simbody</results_directory>
        xmlTag_AnalyzeTool_resultsDir = ET.SubElement(xmlTag_AnalyzeTool, "results_directory")
        xmlTag_AnalyzeTool_resultsDir.text = self.myConfig.staticOptTool_resultsDir
        
        #       <!--Output precision.  It is 8 by default.-->
        #       <output_precision>8</output_precision>
        xmlTag_AnalyzeTool_outputPrecision = ET.SubElement(xmlTag_AnalyzeTool, "output_precision")
        xmlTag_AnalyzeTool_outputPrecision.text = self.myConfig.staticOptTool_outputPrecision
        
        #       <!--Initial time for the simulation.-->
        #       <initial_time>0</initial_time>
        xmlTag_AnalyzeTool_initialTime = ET.SubElement(xmlTag_AnalyzeTool, "initial_time")
        xmlTag_AnalyzeTool_initialTime.text = self.myConfig.common_timeRangeFrom
        
        #       <!--Final time for the simulation.-->
        #       <final_time>1</final_time>
        xmlTag_AnalyzeTool_finalTime = ET.SubElement(xmlTag_AnalyzeTool, "final_time")
        xmlTag_AnalyzeTool_finalTime.text = self.myConfig.common_timeRangeTo
        
        #       <!--Flag indicating whether or not to compute equilibrium values for states other than the coordinates or speeds.  For example, equilibrium muscle fiber lengths or muscle forces.-->
        #       <solve_for_equilibrium_for_auxiliary_states>false</solve_for_equilibrium_for_auxiliary_states>
        xmlTag_AnalyzeTool_solveForEquilibriumForAuxStates = ET.SubElement(xmlTag_AnalyzeTool, "solve_for_equilibrium_for_auxiliary_states")
        xmlTag_AnalyzeTool_solveForEquilibriumForAuxStates.text = self.myConfig.staticOptTool_solveForEquilibriumForAuxStates
        
        #       <!--Maximum number of integrator steps.-->
        #       <maximum_number_of_integrator_steps>20000</maximum_number_of_integrator_steps>
        xmlTag_AnalyzeTool_maxNumOfIntegratorSteps = ET.SubElement(xmlTag_AnalyzeTool, "maximum_number_of_integrator_steps")
        xmlTag_AnalyzeTool_maxNumOfIntegratorSteps.text = self.myConfig.common_integratorMaxSteps
        
        #       <!--Maximum integration step size.-->
        #       <maximum_integrator_step_size>1</maximum_integrator_step_size>
        xmlTag_AnalyzeTool_maxIntegratorStepSize = ET.SubElement(xmlTag_AnalyzeTool, "maximum_integrator_step_size")
        xmlTag_AnalyzeTool_maxIntegratorStepSize.text = self.myConfig.common_integratorMaxStepSize
        
        #       <!--Minimum integration step size.-->
        #       <minimum_integrator_step_size>1e-008</minimum_integrator_step_size>
        xmlTag_AnalyzeTool_minIntegratorStepSize = ET.SubElement(xmlTag_AnalyzeTool, "minimum_integrator_step_size")
        xmlTag_AnalyzeTool_minIntegratorStepSize.text = self.myConfig.common_integratorMinStepSize
        
        #       <!--Integrator error tolerance. When the error is greater, the integrator step size is decreased.-->
        #       <integrator_error_tolerance>1e-005</integrator_error_tolerance>
        xmlTag_AnalyzeTool_integratorErrTolerance = ET.SubElement(xmlTag_AnalyzeTool, "integrator_error_tolerance")
        xmlTag_AnalyzeTool_integratorErrTolerance.text = self.myConfig.common_integratorErrTolerance
        
        #       <!--Set of analyses to be run during the investigation.-->
        #       <AnalysisSet name="Analyses">
        xmlTag_AnalyzeTool_analysisSet = ET.SubElement(xmlTag_AnalyzeTool, "AnalysisSet", name="Analyses")
        #           <objects>
        xmlTag_AnalyzeTool_analysisSet_objects = ET.SubElement(xmlTag_AnalyzeTool_analysisSet, "objects")
        
        #               <StaticOptimization name="StaticOptimization">
        xmlTag_AnalyzeTool_analysisSet_objects_staticOptimization = ET.SubElement(xmlTag_AnalyzeTool_analysisSet_objects, "StaticOptimization", name="StaticOptimization")
        
        #                   <!--Flag (true or false) specifying whether whether on. True by default.-->
        #                   <on>true</on>
        xmlTag_AnalyzeTool_analysisSet_objects_staticOptimization_on = ET.SubElement(xmlTag_AnalyzeTool_analysisSet_objects_staticOptimization, "on")
        xmlTag_AnalyzeTool_analysisSet_objects_staticOptimization_on.text = self.myConfig.jointReactionTool_analysis_on
        
        #                   <!--Start time.-->
        #                   <start_time>0</start_time>
        xmlTag_AnalyzeTool_analysisSet_objects_staticOptimization_startTime = ET.SubElement(xmlTag_AnalyzeTool_analysisSet_objects_staticOptimization, "start_time")
        xmlTag_AnalyzeTool_analysisSet_objects_staticOptimization_startTime.text = self.myConfig.common_timeRangeFrom
        
        #                   <!--End time.-->
        #                   <end_time>1</end_time>
        xmlTag_AnalyzeTool_analysisSet_objects_staticOptimization_endTime = ET.SubElement(xmlTag_AnalyzeTool_analysisSet_objects_staticOptimization, "end_time")
        xmlTag_AnalyzeTool_analysisSet_objects_staticOptimization_endTime.text = self.myConfig.common_timeRangeTo
        
        #                   <!--Specifies how often to store results during a simulation. More specifically, the interval (a positive integer) specifies how many successful integration steps should be taken before results are recorded again.-->
        #                   <step_interval>1</step_interval>
        xmlTag_AnalyzeTool_analysisSet_objects_staticOptimization_stepInterval = ET.SubElement(xmlTag_AnalyzeTool_analysisSet_objects_staticOptimization, "step_interval")
        xmlTag_AnalyzeTool_analysisSet_objects_staticOptimization_stepInterval.text = self.myConfig.common_integratorMinStepSize
        
        #                   <!--Flag (true or false) indicating whether the results are in degrees or not.-->
        #                   <in_degrees>true</in_degrees>
        xmlTag_AnalyzeTool_analysisSet_objects_staticOptimization_inDegrees = ET.SubElement(xmlTag_AnalyzeTool_analysisSet_objects_staticOptimization, "in_degrees")
        xmlTag_AnalyzeTool_analysisSet_objects_staticOptimization_inDegrees.text = self.myConfig.common_inDegrees
        
        #                   <!--If true, the model's own force set will be used in the static optimization computation.  Otherwise, inverse dynamics for coordinate actuators will be computed for all unconstrained degrees of freedom.-->
        #                   <use_model_force_set>true</use_model_force_set>
        xmlTag_AnalyzeTool_analysisSet_objects_staticOptimization_useModelForceSet = ET.SubElement(xmlTag_AnalyzeTool_analysisSet_objects_staticOptimization, "use_model_force_set")
        xmlTag_AnalyzeTool_analysisSet_objects_staticOptimization_useModelForceSet.text = self.myConfig.staticOptTool_analysis_useModelForceSet
        
        #                   <!--A double indicating the exponent to raise activations to when solving static optimization.  -->
        #                   <activation_exponent>2</activation_exponent>
        xmlTag_AnalyzeTool_analysisSet_objects_staticOptimization_activationExponent = ET.SubElement(xmlTag_AnalyzeTool_analysisSet_objects_staticOptimization, "activation_exponent")
        xmlTag_AnalyzeTool_analysisSet_objects_staticOptimization_activationExponent.text = self.myConfig.staticOptTool_analysis_activationExponent
        
        #                   <!--If true muscle force-length curve is observed while running optimization.-->
        #                   <use_muscle_physiology>true</use_muscle_physiology>
        xmlTag_AnalyzeTool_analysisSet_objects_staticOptimization_useMusclePhysiology = ET.SubElement(xmlTag_AnalyzeTool_analysisSet_objects_staticOptimization, "use_muscle_physiology")
        xmlTag_AnalyzeTool_analysisSet_objects_staticOptimization_useMusclePhysiology.text = self.myConfig.staticOptTool_analysis_useMusclePhysiology
        
        #                   <!--Value used to determine when the optimization solution has converged-->
        #                   <optimizer_convergence_criterion>0.0001</optimizer_convergence_criterion>
        xmlTag_AnalyzeTool_analysisSet_objects_staticOptimization_optimizerConvergenceCriterion = ET.SubElement(xmlTag_AnalyzeTool_analysisSet_objects_staticOptimization, "optimizer_convergence_criterion")
        xmlTag_AnalyzeTool_analysisSet_objects_staticOptimization_optimizerConvergenceCriterion.text = self.myConfig.staticOptTool_analysis_optimizerConvergenceCriterion
        
        #                   <!--An integer for setting the maximum number of iterations the optimizer can use at each time.  -->
        #                   <optimizer_max_iterations>100</optimizer_max_iterations>
        xmlTag_AnalyzeTool_analysisSet_objects_staticOptimization_optimizerMaxIter = ET.SubElement(xmlTag_AnalyzeTool_analysisSet_objects_staticOptimization, "optimizer_max_iterations")
        xmlTag_AnalyzeTool_analysisSet_objects_staticOptimization_optimizerMaxIter.text = self.myConfig.staticOptTool_analysis_optimizerMaxIter
        
        #               </StaticOptimization>
        #           </objects>
        
        #           <groups />
        xmlTag_AnalyzeTool_analysisSet_groups = ET.SubElement(xmlTag_AnalyzeTool_analysisSet, "groups")
        
        #       </AnalysisSet>
        #       <!--Controller objects in the model.-->
        #       <ControllerSet name="Controllers">
        xmlTag_AnalyzeTool_controllerSet = ET.SubElement(xmlTag_AnalyzeTool, "ControllerSet", name="Controllers")
        
        #           <objects />
        xmlTag_AnalyzeTool_controllerSet_objects = ET.SubElement(xmlTag_AnalyzeTool_controllerSet, "objects")
        
        #           <groups />
        xmlTag_AnalyzeTool_controllerSet_groups = ET.SubElement(xmlTag_AnalyzeTool_controllerSet, "groups")
        
        #       </ControllerSet>
        
        #       <!--XML file (.xml) containing the forces applied to the model as ExternalLoads.-->
        #       <external_loads_file />
        xmlTag_AnalyzeTool_externalLoadsFile = ET.SubElement(xmlTag_AnalyzeTool, "external_loads_file")
        xmlTag_AnalyzeTool_externalLoadsFile.text = self.myConfig.common_externalLoadsSpecFile
        
        #       <!--Storage file (.sto) containing the time history of states for the model. This file often contains multiple rows of data, each row being a time-stamped array of states. The first column contains the time.  The rest of the columns contain the states in the order appropriate for the model. In a storage file, unlike a motion file (.mot), non-uniform time spacing is allowed.  If the user-specified initial time for a simulation does not correspond exactly to one of the time stamps in this file, inerpolation is NOT used because it is sometimes necessary to an exact set of states for analyses.  Instead, the closest earlier set of states is used.-->
        #       <states_file />
        xmlTag_AnalyzeTool_statesFile = ET.SubElement(xmlTag_AnalyzeTool, "states_file")
        xmlTag_AnalyzeTool_statesFile.text = self.myConfig.staticOptTool_statesFile
        
        #       <!--Motion file (.mot) or storage file (.sto) containing the time history of the generalized coordinates for the model. These can be specified in place of the states file.-->
        #       <coordinates_file />
        xmlTag_AnalyzeTool_coordFiles = ET.SubElement(xmlTag_AnalyzeTool, "coordinates_file")
        xmlTag_AnalyzeTool_coordFiles.text = self.myConfig.staticOptTool_coordFile
        
        #       <!--Storage file (.sto) containing the time history of the generalized speeds for the model. If coordinates_file is used in place of states_file, these can be optionally set as well to give the speeds. If not specified, speeds will be computed from coordinates by differentiation.-->
        #       <speeds_file />
        xmlTag_AnalyzeTool_speedsFile = ET.SubElement(xmlTag_AnalyzeTool, "speeds_file")
        xmlTag_AnalyzeTool_speedsFile.text = self.myConfig.staticOptTool_speedFile
        
        #       <!--Low-pass cut-off frequency for filtering the coordinates_file data (currently does not apply to states_file or speeds_file). A negative value results in no filtering. The default value is -1.0, so no filtering.-->
        #       <lowpass_cutoff_frequency_for_coordinates>-1</lowpass_cutoff_frequency_for_coordinates>
        xmlTag_AnalyzeTool_lowpassCutoffFreqForCoord = ET.SubElement(xmlTag_AnalyzeTool, "lowpass_cutoff_frequency_for_coordinates")
        xmlTag_AnalyzeTool_lowpassCutoffFreqForCoord.text = self.myConfig.staticOptTool_lowpassCutoffFreqForCoord
        
        #   </AnalyzeTool>
        #</OpenSimDocument>
        
        # Fix the indentation
        #print "Correcting indentations..."
        self.indent(self.xmlTag_OpenSimDocument)
        #print "finished correcting indentations"
        tree = ET.ElementTree(self.xmlTag_OpenSimDocument)
        #print "created tree"
        
        # Write out the xml file
        xmlFileName = self.myConfig.staticOptConfigFile
        #print "perform writing"
        tree.write(xmlFileName, encoding='utf-8')
        print "Wrote Static Opt xml file"
        
    def indent(self, elem, level=0):
        #print "fixing element: %s" % (elem)
        i = "\n" + level*"  "
        j = "\n" + (level-1)*"  "
        if len(elem):
            #print "1"
            if not elem.text or not elem.text.strip():
                #print "2"
                elem.text = i + "  "
            if not elem.tail or not elem.tail.strip():
                #print "3"
                elem.tail = i
            for subelem in elem:
                #print "4"
                self.indent(subelem, level+1)
            if not elem.tail or not elem.tail.strip():
                #print "5"
                elem.tail = j
        else:
            #print "6"
            if level and (not elem.tail or not elem.tail.strip()):
                #print "7"
                elem.tail = j
        #print "8"
        return elem        

