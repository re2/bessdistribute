

################################################################################
#
#
################################################################################

from xml.dom.minidom import parse, parseString, getDOMImplementation, Document
import xml.etree.cElementTree as ET

class bessXmlRraIO:
    def __init__(self, myConfig):
        print "Initializing bessXmlRra"
        self.myConfig = myConfig
        print "Done initializing bessXmlRra"
        
    # http://stackoverflow.com/questions/3605680/creating-a-simple-xml-file-using-python
    def writeConfigFile(self):
        print "Writing RRA xml file"
        #<?xml version="1.0" encoding="UTF-8" ?>
        #<OpenSimDocument Version="30000">
        self.xmlTag_OpenSimDocument = ET.Element("OpenSimDocument", Version="30000")
        
        #   <RRATool name="3DGaitModel2392"> ----------> TODO: This is probably not the value we want here... Duplicate of the next value
        #xmlTag_RRATool = ET.SubElement(self.xmlTag_OpenSimDocument, "RRATool", name=self.myConfig.common_modelFile)
        xmlTag_RRATool = ET.SubElement(self.xmlTag_OpenSimDocument, "RRATool", name='')
        
        #       <!--Name of the .osim file used to construct a model.-->
        #       <model_file>../../../../OpenSim 3.3/Models/Gait2392_Simbody/gait2392_simbody.osim</model_file>
        xmlTag_RRATool_modelFile = ET.SubElement(xmlTag_RRATool, "model_file")
        xmlTag_RRATool_modelFile.text = self.myConfig.common_modelFile
        
        #       <!--Replace the model's force set with sets specified in <force_set_files>? If false, the force set is appended to.-->
        #       <replace_force_set>true</replace_force_set>
        xmlTag_RRATool_replaceForceSet = ET.SubElement(xmlTag_RRATool, "replace_force_set")
        xmlTag_RRATool_replaceForceSet.text = self.myConfig.common_replaceForceSet
        
        #       <!--List of xml files used to construct an force set for the model.-->
        #       <force_set_files />
        xmlTag_RRATool_forceSetFiles = ET.SubElement(xmlTag_RRATool, "force_set_files")
        xmlTag_RRATool_forceSetFiles.text = self.myConfig.common_forceSetFile
        
        #       <!--Directory used for writing results.-->
        #       <results_directory>../../../../OpenSim 3.3/Models/Gait2392_Simbody</results_directory>
        xmlTag_RRATool_resultsDirectory = ET.SubElement(xmlTag_RRATool, "results_directory")
        xmlTag_RRATool_resultsDirectory.text = self.myConfig.rraTool_resultsDir
        
        #       <!--Output precision.  It is 8 by default.-->
        #       <output_precision>8</output_precision>
        xmlTag_RRATool_outputPrecision = ET.SubElement(xmlTag_RRATool, "output_precision")
        xmlTag_RRATool_outputPrecision.text = self.myConfig.rraTool_outputPrecision
        
        #       <!--Initial time for the simulation.-->
        #       <initial_time>0</initial_time>
        xmlTag_RRATool_initialTime = ET.SubElement(xmlTag_RRATool, "initial_time")
        xmlTag_RRATool_initialTime.text = self.myConfig.common_timeRangeFrom
        
        #       <!--Final time for the simulation.-->
        #       <final_time>1</final_time>
        xmlTag_RRATool_finalTime = ET.SubElement(xmlTag_RRATool, "final_time")
        xmlTag_RRATool_finalTime.text = self.myConfig.common_timeRangeTo
        
        #       <!--Flag indicating whether or not to compute equilibrium values for states other than the coordinates or speeds.  For example, equilibrium muscle fiber lengths or muscle forces.-->
        #       <solve_for_equilibrium_for_auxiliary_states>false</solve_for_equilibrium_for_auxiliary_states>
        xmlTag_RRATool_solveForEquilibriumForAuxStates = ET.SubElement(xmlTag_RRATool, "solve_for_equilibrium_for_auxiliary_states")
        xmlTag_RRATool_solveForEquilibriumForAuxStates.text = self.myConfig.rraTool_solveForEquilibriumForAuxStates
        
        #       <!--Maximum number of integrator steps.-->
        #       <maximum_number_of_integrator_steps>20000</maximum_number_of_integrator_steps>
        xmlTag_RRATool_maxNumOfIntegratorSteps = ET.SubElement(xmlTag_RRATool, "maximum_number_of_integrator_steps")
        xmlTag_RRATool_maxNumOfIntegratorSteps.text = self.myConfig.common_integratorMaxSteps
        
        #       <!--Maximum integration step size.-->
        #       <maximum_integrator_step_size>1</maximum_integrator_step_size>
        xmlTag_RRATool_maxIntegratorStepSize = ET.SubElement(xmlTag_RRATool, "maximum_integrator_step_size")
        xmlTag_RRATool_maxIntegratorStepSize.text = self.myConfig.common_integratorMaxStepSize
        
        #       <!--Minimum integration step size.-->
        #       <minimum_integrator_step_size>1e-008</minimum_integrator_step_size>
        xmlTag_RRATool_minIntegratorStepSize = ET.SubElement(xmlTag_RRATool, "minimum_integrator_step_size")
        xmlTag_RRATool_minIntegratorStepSize.text = self.myConfig.common_integratorMinStepSize
        
        #       <!--Integrator error tolerance. When the error is greater, the integrator step size is decreased.-->
        #       <integrator_error_tolerance>1e-005</integrator_error_tolerance>
        xmlTag_RRATool_integratorErrTolerance = ET.SubElement(xmlTag_RRATool, "integrator_error_tolerance")
        xmlTag_RRATool_integratorErrTolerance.text = self.myConfig.common_integratorErrTolerance
        
        #       <!--Set of analyses to be run during the investigation.-->
        #       <AnalysisSet name="Analyses">
        xmlTag_RRATool_analysisSet = ET.SubElement(xmlTag_RRATool, "AnalysisSet", name="Analyses")
        #           <objects />
        xmlTag_RRATool_analysisSet_objects = ET.SubElement(xmlTag_RRATool_analysisSet, "objects")
        #           <groups />
        xmlTag_RRATool_analysisSet_groups = ET.SubElement(xmlTag_RRATool_analysisSet, "groups")
        #       </AnalysisSet>
        
        #       <!--Controller objects in the model.-->
        #       <ControllerSet name="Controllers">
        xmlTag_RRATool_controllerSet = ET.SubElement(xmlTag_RRATool, "ControllerSet", name="Controllers")
        #           <objects />
        xmlTag_RRATool_controllerSet_objects = ET.SubElement(xmlTag_RRATool_controllerSet, "objects")
        #           <groups />
        xmlTag_RRATool_controllerSet_groups = ET.SubElement(xmlTag_RRATool_controllerSet, "groups")
        #       </ControllerSet>
        
        #       <!--XML file (.xml) containing the forces applied to the model as ExternalLoads.-->
        #       <external_loads_file />
        xmlTag_RRATool_externalLoadsFile = ET.SubElement(xmlTag_RRATool, "external_loads_file")
        xmlTag_RRATool_externalLoadsFile.text = self.myConfig.common_externalLoadsSpecFile
        
        #       <!--Motion (.mot) or storage (.sto) file containing the desired point trajectories.-->
        #       <desired_points_file />
        xmlTag_RRATool_desiredPointsFile = ET.SubElement(xmlTag_RRATool, "desired_points_file")
        xmlTag_RRATool_desiredPointsFile.text = self.myConfig.cmcTool_desiredPointsFile
        
        #       <!--Motion (.mot) or storage (.sto) file containing the desired kinematic trajectories.-->
        #       <desired_kinematics_file />
        xmlTag_RRATool_desiredKinematicsFile = ET.SubElement(xmlTag_RRATool, "desired_kinematics_file")
        if('true' == self.myConfig.useIkTool):
            xmlTag_RRATool_desiredKinematicsFile.text = self.myConfig.ikTool_outputMotionFile
        else:
            xmlTag_RRATool_desiredKinematicsFile.text = self.myConfig.common_desiredKinematicsFile
        
        #       <!--File containing the tracking tasks. Which coordinates are tracked and with what weights are specified here.-->
        #       <task_set_file />
        xmlTag_RRATool_taskSetFile = ET.SubElement(xmlTag_RRATool, "task_set_file")
        xmlTag_RRATool_taskSetFile.text = self.myConfig.common_trackingTasksFile
        
        #       <!--DEPRECATED File containing the constraints on the controls.-->
        #       <constraints_file />
        xmlTag_RRATool_constraintsFile = ET.SubElement(xmlTag_RRATool, "constraints_file")
        #xmlTag_RRATool_.text = self.myConfig.
        
        #       <!--Low-pass cut-off frequency for filtering the desired kinematics. A negative value results in no filtering. The default value is -1.0, so no filtering.-->
        #       <lowpass_cutoff_frequency>-1</lowpass_cutoff_frequency>
        xmlTag_RRATool_lowpassCutoffFreq = ET.SubElement(xmlTag_RRATool, "lowpass_cutoff_frequency")
        #xmlTag_RRATool_lowpassCutoffFreq.text = self.myConfig.rraTool_lowpassCutoffFreq
        xmlTag_RRATool_lowpassCutoffFreq.text = self.myConfig.common_filterKinematics
        
        #       <!--Preferred optimizer algorithm (currently support "ipopt" or "cfsqp", the latter requiring the osimFSQP library.-->
        #       <optimizer_algorithm>ipopt</optimizer_algorithm>
        xmlTag_RRATool_optimizerAlg = ET.SubElement(xmlTag_RRATool, "optimizer_algorithm")
        xmlTag_RRATool_optimizerAlg.text = self.myConfig.rraTool_optimizerAlg
        
        #       <!--Step size used by the optimizer to compute numerical derivatives. A value between 1.0e-4 and 1.0e-8 is usually appropriate.-->
        #       <numerical_derivative_step_size>0.0001</numerical_derivative_step_size>
        xmlTag_RRATool_numericalDerivativeStepSize = ET.SubElement(xmlTag_RRATool, "numerical_derivative_step_size")
        xmlTag_RRATool_numericalDerivativeStepSize.text = self.myConfig.rraTool_numericalDerivativeStepSize
        
        #       <!--Convergence criterion for the optimizer. The smaller this value, the deeper the convergence. Decreasing this number can improve a solution, but will also likely increase computation time.-->
        #       <optimization_convergence_tolerance>1e-005</optimization_convergence_tolerance>
        xmlTag_RRATool_optimizationConvergenceTolerance = ET.SubElement(xmlTag_RRATool, "optimization_convergence_tolerance")
        xmlTag_RRATool_optimizationConvergenceTolerance.text = self.myConfig.rraTool_optimizationConvergenceTolerance
        
        #       <!--Flag (true or false) indicating whether or not to make an adjustment in the center of mass of a body to reduced DC offsets in MX and MZ. If true, a new model is writen out that has altered anthropometry.-->
        #       <adjust_com_to_reduce_residuals>true</adjust_com_to_reduce_residuals>
        xmlTag_RRATool_adjustComToReduceResiduals = ET.SubElement(xmlTag_RRATool, "adjust_com_to_reduce_residuals")
        xmlTag_RRATool_adjustComToReduceResiduals.text = self.myConfig.rraTool_adjustComToReduceResiduals
        
        #       <!--Initial time used when computing average residuals in order to adjust the body's center of mass.  If both initial and final time are set to -1 (their default value) then the main initial and final time settings will be used.-->
        #       <initial_time_for_com_adjustment>-1</initial_time_for_com_adjustment>
        xmlTag_RRATool_initialTimeForComAdjustment = ET.SubElement(xmlTag_RRATool, "initial_time_for_com_adjustment")
        xmlTag_RRATool_initialTimeForComAdjustment.text = self.myConfig.rraTool_initialTimeForComAdjustment
        
        #       <!--Final time used when computing average residuals in order to adjust the body's center of mass.-->
        #       <final_time_for_com_adjustment>-1</final_time_for_com_adjustment>
        xmlTag_RRATool_finalTimeForComAdjustment = ET.SubElement(xmlTag_RRATool, "final_time_for_com_adjustment")
        xmlTag_RRATool_finalTimeForComAdjustment.text = self.myConfig.rraTool_finalTimeForComAdjustment
        
        #       <!--Name of the body whose center of mass is adjusted. The heaviest segment in the model should normally be chosen. For a gait model, the torso segment is usually the best choice.-->
        #       <adjusted_com_body />
        xmlTag_RRATool_adjustedComBody = ET.SubElement(xmlTag_RRATool, "adjusted_com_body")
        xmlTag_RRATool_adjustedComBody.text = self.myConfig.rraTool_adjustedComBody
        
        #       <!--Name of the output model file (.osim) containing adjustments to anthropometry made to reduce average residuals. This file is written if the property adjust_com_to_reduce_residuals is set to true. If a name is not specified, the model is written out to a file called adjusted_model.osim.-->
        #       <output_model_file />
        xmlTag_RRATool_outputModelFile = ET.SubElement(xmlTag_RRATool, "output_model_file")
        xmlTag_RRATool_outputModelFile.text = self.myConfig.rraTool_outputModelFile
        
        #       <!--True-false flag indicating whether or not to turn on verbose printing for cmc.-->
        #       <use_verbose_printing>false</use_verbose_printing>
        xmlTag_RRATool_useVerbosePrinting = ET.SubElement(xmlTag_RRATool, "use_verbose_printing")
        xmlTag_RRATool_useVerbosePrinting.text = self.myConfig.rraTool_useVerbosePrinting
        
        #   </RRATool>
        #</OpenSimDocument>
        
        # Fix the indentation
        self.indent(self.xmlTag_OpenSimDocument)
        tree = ET.ElementTree(self.xmlTag_OpenSimDocument)
        
        # Write out the xml file
        xmlFileName = self.myConfig.rraConfigFile
        tree.write(xmlFileName, encoding='utf-8')
        print "Wrote RRA xml file"
        
    def indent(self, elem, level=0):
        i = "\n" + level*"  "
        j = "\n" + (level-1)*"  "
        if len(elem):
            if not elem.text or not elem.text.strip():
                elem.text = i + "  "
            if not elem.tail or not elem.tail.strip():
                elem.tail = i
            for subelem in elem:
                self.indent(subelem, level+1)
            if not elem.tail or not elem.tail.strip():
                elem.tail = j
        else:
            if level and (not elem.tail or not elem.tail.strip()):
                elem.tail = j
        return elem        

