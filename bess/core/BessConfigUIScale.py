

################################################################################
#
# This file creates the Configuration UI components and handles user input as
# it relates to BESS configuration.
#
################################################################################
from javax.swing import JFrame, JPanel, JButton, JFileChooser, JTextField, JLabel, JCheckBox, SwingConstants, JTabbedPane, JScrollPane, JList, JComboBox, JTable, DefaultCellEditor
from javax.swing.table import TableModel, TableColumnModel
from javax.swing.border import TitledBorder
from java.awt import GridLayout, FlowLayout, Color, GridBagLayout, GridBagConstraints, CardLayout, BorderLayout

from xml.dom.minidom import parse, parseString, getDOMImplementation, Document
import xml.etree.cElementTree as ET

import org.opensim.modeling as modeling
import org.opensim.console.gui as gui

import os.path

from core.BessConfigUICardBase import bessCardBase
from core.BessUIFileChooser import bessFileChooser



import os
import sys

# Find the parent "bess" directory so that we can import JAR files from
# different directories.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
    
from core.BessConfigUICardBase import bessCardBase
import utilities.toolsCommon as TC

# Get the main GUI components from our NetBeans/Java project.
TC.importJar(os.path.join(bessDir, 'lib', 'BessCoreGui.jar'))
from bess.gui.core import ScaleToolMeasurementSet as BessScaleToolMeasurementSetPanel
from bess.gui.core import ScaleToolScaleFactors as BessScaleToolScaleFactorsPanel
from bess.gui.core import ScaleToolStaticPoseWeights as BessScaleToolStaticPoseWeightsPanel

#class measurementTableModelListener( TableModelListener ):
#    def tableChanged( TableModelEvent e):
#        

'''
The bessConfigUIScaleCard is used as the Scale Card UI component
'''
class bessConfigUIScaleCard(bessCardBase):
    def __init__(self, myConfig):
        self.myConfig = myConfig
        self.myConfig.scaleConfigFile = self.myConfig.common_rootDir + "\\bess_scaleSettings.xml"
        
        self.myConfig.scale_genericModelMaker_name = " "
        self.myConfig.scale_modelScaler_name = " "
        
        # Lists from the model
        self.bodies = []
        self.markers = []
        self.coords = []
        self.measurementSetTableData = []
        self.staticPoseWeightsMarkerTableData = []
        self.staticPoseWeightsCoordTableData = []
        self.scaleFactorsTableData = []
        #self.dontForget = []
        
        # Use This Tool settings
        useThisToolGroup = JPanel(GridBagLayout())
        useThisToolGroup_GBLC = GridBagConstraints()
        useThisToolGroup_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        self.useThisToolGroup_useItCheckBox = JCheckBox("Perform Scaling", True, actionPerformed=self.onUseItCheckBoxSelect)
        useThisToolGroup_loadSettingsButton = JButton("Load settings", actionPerformed = self.loadSettings)
        
        useThisToolGroup_GBLC.weightx = 0.25
        useThisToolGroup_GBLC.gridx = 0
        useThisToolGroup_GBLC.gridy = 0
        useThisToolGroup.add(self.useThisToolGroup_useItCheckBox, useThisToolGroup_GBLC)
        useThisToolGroup_GBLC.weightx = 0.60
        useThisToolGroup_GBLC.gridx = 1
        useThisToolGroup_GBLC.gridy = 0
        useThisToolGroup.add(JLabel(""), useThisToolGroup_GBLC)
        useThisToolGroup_GBLC.weightx = 0.15
        useThisToolGroup_GBLC.gridx = 2
        useThisToolGroup_GBLC.gridy = 0
        useThisToolGroup.add(useThisToolGroup_loadSettingsButton, useThisToolGroup_GBLC)
        
        # Create the UI components
        self.scaleCard = JPanel(BorderLayout())
        
        self.scalePanel = JPanel(GridBagLayout())
        self.scalePanel_GBLC = GridBagConstraints()
        self.scalePanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        self.scaleTabs = JTabbedPane(SwingConstants.TOP, JTabbedPane.SCROLL_TAB_LAYOUT)
        
        # Settings tab  ###############################################################################################
        settingsTab = JPanel(GridBagLayout())
        settingsTab_GBLC = GridBagConstraints()
        settingsTab_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        ## Subject Data group
        settingsTab_subjectDataGroup = JPanel(GridBagLayout())
        settingsTab_subjectDataGroup_GBLC = GridBagConstraints()
        settingsTab_subjectDataGroup_GBLC.fill = GridBagConstraints.HORIZONTAL
        settingsTab_subjectDataGroup.setBorder(TitledBorder("Subject Data"))
        
        ### Mass [label, text, kg]
        settingsTab_subjectDataGroup_massLabel = JLabel("Mass", SwingConstants.RIGHT)
        self.settingsTab_subjectDataGroup_massTextField = JTextField("", focusLost=self.setMassEvent)
        settingsTab_subjectDataGroup_massKgLabel = JLabel("kg", SwingConstants.LEFT)
        
        settingsTab_subjectDataGroup_massPanel = JPanel(GridBagLayout())
        settingsTab_subjectDataGroup_massPanel_GBLC = GridBagConstraints()
        settingsTab_subjectDataGroup_massPanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        settingsTab_subjectDataGroup_massPanel_GBLC.weightx = 0.01
        settingsTab_subjectDataGroup_massPanel_GBLC.gridx = 0
        settingsTab_subjectDataGroup_massPanel_GBLC.gridy = 0
        settingsTab_subjectDataGroup_massPanel.add(settingsTab_subjectDataGroup_massLabel, settingsTab_subjectDataGroup_massPanel_GBLC)
        settingsTab_subjectDataGroup_massPanel_GBLC.weightx = 0.98
        settingsTab_subjectDataGroup_massPanel_GBLC.gridx = 1
        settingsTab_subjectDataGroup_massPanel_GBLC.gridy = 0
        settingsTab_subjectDataGroup_massPanel.add(self.settingsTab_subjectDataGroup_massTextField, settingsTab_subjectDataGroup_massPanel_GBLC)
        settingsTab_subjectDataGroup_massPanel_GBLC.weightx = 0.01
        settingsTab_subjectDataGroup_massPanel_GBLC.gridx = 2
        settingsTab_subjectDataGroup_massPanel_GBLC.gridy = 0
        settingsTab_subjectDataGroup_massPanel.add(settingsTab_subjectDataGroup_massKgLabel, settingsTab_subjectDataGroup_massPanel_GBLC)
        
        ### Add markers from file [checkbox, label, path, browse]
        self.settingsTab_subjectDataGroup_addMarkersFileCheckBox = JCheckBox("Add markers from file ", actionPerformed=self.onAddMarkersFileCheckBoxSelect)
        self.settingsTab_subjectDataGroup_addMarkersFileTextField = JTextField("", focusLost=self.setAddMarkersFileEvent)
        settingsTab_subjectDataGroup_addMarkersFileChooserButton = JButton("...", actionPerformed = self.selectMarkerDataFile)
        
        settingsTab_subjectDataGroup_addMarkersPanel = JPanel(GridBagLayout())
        settingsTab_subjectDataGroup_addMarkersPanel_GBLC = GridBagConstraints()
        settingsTab_subjectDataGroup_addMarkersPanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        settingsTab_subjectDataGroup_addMarkersPanel_GBLC.weightx = 0.40
        settingsTab_subjectDataGroup_addMarkersPanel_GBLC.gridx = 0
        settingsTab_subjectDataGroup_addMarkersPanel_GBLC.gridy = 0
        settingsTab_subjectDataGroup_addMarkersPanel.add(self.settingsTab_subjectDataGroup_addMarkersFileCheckBox, settingsTab_subjectDataGroup_addMarkersPanel_GBLC)
        settingsTab_subjectDataGroup_addMarkersPanel_GBLC.weightx = 0.59
        settingsTab_subjectDataGroup_addMarkersPanel_GBLC.gridx = 1
        settingsTab_subjectDataGroup_addMarkersPanel_GBLC.gridy = 0
        settingsTab_subjectDataGroup_addMarkersPanel.add(self.settingsTab_subjectDataGroup_addMarkersFileTextField, settingsTab_subjectDataGroup_addMarkersPanel_GBLC)
        settingsTab_subjectDataGroup_addMarkersPanel_GBLC.weightx = 0.01
        settingsTab_subjectDataGroup_addMarkersPanel_GBLC.gridx = 2
        settingsTab_subjectDataGroup_addMarkersPanel_GBLC.gridy = 0
        settingsTab_subjectDataGroup_addMarkersPanel.add(settingsTab_subjectDataGroup_addMarkersFileChooserButton, settingsTab_subjectDataGroup_addMarkersPanel_GBLC)
        
        ## Finish Subject Data group
        settingsTab_subjectDataGroup_GBLC.weightx = 0.1
        settingsTab_subjectDataGroup_GBLC.gridx = 0
        settingsTab_subjectDataGroup_GBLC.gridy = 0
        settingsTab_subjectDataGroup.add(JLabel(""), settingsTab_subjectDataGroup_GBLC)
        settingsTab_subjectDataGroup_GBLC.weightx = 0.80
        settingsTab_subjectDataGroup_GBLC.gridx = 1
        settingsTab_subjectDataGroup_GBLC.gridy = 0
        settingsTab_subjectDataGroup.add(settingsTab_subjectDataGroup_massPanel, settingsTab_subjectDataGroup_GBLC)
        settingsTab_subjectDataGroup_GBLC.weightx = 0.1
        settingsTab_subjectDataGroup_GBLC.gridx = 2
        settingsTab_subjectDataGroup_GBLC.gridy = 0
        settingsTab_subjectDataGroup.add(JLabel(""), settingsTab_subjectDataGroup_GBLC)
        
        settingsTab_subjectDataGroup_GBLC.weightx = 0.1
        settingsTab_subjectDataGroup_GBLC.gridx = 0
        settingsTab_subjectDataGroup_GBLC.gridy = 1
        settingsTab_subjectDataGroup.add(JLabel(""), settingsTab_subjectDataGroup_GBLC)
        settingsTab_subjectDataGroup_GBLC.weightx = 0.80
        settingsTab_subjectDataGroup_GBLC.gridx = 1
        settingsTab_subjectDataGroup_GBLC.gridy = 1
        settingsTab_subjectDataGroup.add(settingsTab_subjectDataGroup_addMarkersPanel, settingsTab_subjectDataGroup_GBLC)
        settingsTab_subjectDataGroup_GBLC.weightx = 0.1
        settingsTab_subjectDataGroup_GBLC.gridx = 2
        settingsTab_subjectDataGroup_GBLC.gridy = 1
        settingsTab_subjectDataGroup.add(JLabel(""), settingsTab_subjectDataGroup_GBLC)
        
        ## Scale Model group [check box]
        settingsTab_scaleModelGroup = JPanel(GridBagLayout())
        settingsTab_scaleModelGroup_GBLC = GridBagConstraints()
        settingsTab_scaleModelGroup_GBLC.fill = GridBagConstraints.HORIZONTAL
        settingsTab_scaleModelGroup.setBorder(TitledBorder("Scale Model"))
        
        ### Preserve mass distribution during scale [checkbox, label]
        self.settingsTab_scaleModelGroup_preserveMassDistCheckBox = JCheckBox("Preserve mass distribution during scale", actionPerformed=self.onPreserveMassDistCheckBoxSelect)
        
        settingsTab_scaleModelGroup_preserveMassDistPanel = JPanel(GridBagLayout())
        settingsTab_scaleModelGroup_preserveMassDistPanel_GBLC = GridBagConstraints()
        settingsTab_scaleModelGroup_preserveMassDistPanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        settingsTab_scaleModelGroup_preserveMassDistPanel_GBLC.weightx = 1.00
        settingsTab_scaleModelGroup_preserveMassDistPanel_GBLC.gridx = 0
        settingsTab_scaleModelGroup_preserveMassDistPanel_GBLC.gridy = 0
        settingsTab_scaleModelGroup_preserveMassDistPanel.add(self.settingsTab_scaleModelGroup_preserveMassDistCheckBox, settingsTab_scaleModelGroup_preserveMassDistPanel_GBLC)
        
        ### Marker data for measurements [checkbox, label, path, browse]
        self.settingsTab_scaleModelGroup_markerDataMeasurementsFileCheckBox = JCheckBox("Marker data for measurements ", actionPerformed=self.onMarkerDataMeasurementsFileCheckBoxSelect)
        self.settingsTab_scaleModelGroup_markerDataMeasurementsFileTextField = JTextField("", focusLost=self.setMarkerDataMeasurementsFileEvent)
        settingsTab_scaleModelGroup_markerDataMeasurementsFileChooserButton = JButton("...", actionPerformed = self.selectMarkerDataMeasurementsFile)
        
        settingsTab_scaleModelGroup_markerDataMeasurementPanel = JPanel(GridBagLayout())
        settingsTab_scaleModelGroup_markerDataMeasurementPanel_GBLC = GridBagConstraints()
        settingsTab_scaleModelGroup_markerDataMeasurementPanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        settingsTab_scaleModelGroup_markerDataMeasurementPanel_GBLC.weightx = 0.40
        settingsTab_scaleModelGroup_markerDataMeasurementPanel_GBLC.gridx = 0
        settingsTab_scaleModelGroup_markerDataMeasurementPanel_GBLC.gridy = 0
        settingsTab_scaleModelGroup_markerDataMeasurementPanel.add(self.settingsTab_scaleModelGroup_markerDataMeasurementsFileCheckBox, settingsTab_scaleModelGroup_markerDataMeasurementPanel_GBLC)
        settingsTab_scaleModelGroup_markerDataMeasurementPanel_GBLC.weightx = 0.59
        settingsTab_scaleModelGroup_markerDataMeasurementPanel_GBLC.gridx = 1
        settingsTab_scaleModelGroup_markerDataMeasurementPanel_GBLC.gridy = 0
        settingsTab_scaleModelGroup_markerDataMeasurementPanel.add(self.settingsTab_scaleModelGroup_markerDataMeasurementsFileTextField, settingsTab_scaleModelGroup_markerDataMeasurementPanel_GBLC)
        settingsTab_scaleModelGroup_markerDataMeasurementPanel_GBLC.weightx = 0.01
        settingsTab_scaleModelGroup_markerDataMeasurementPanel_GBLC.gridx = 2
        settingsTab_scaleModelGroup_markerDataMeasurementPanel_GBLC.gridy = 0
        settingsTab_scaleModelGroup_markerDataMeasurementPanel.add(settingsTab_scaleModelGroup_markerDataMeasurementsFileChooserButton, settingsTab_scaleModelGroup_markerDataMeasurementPanel_GBLC)
        
        ### Average measurements between times [label, text, and, text]
        settingsTab_scaleModelGroup_avgMeasurementBetweenTimesFromLabel = JLabel("Average measurements between times ", SwingConstants.RIGHT)
        self.settingsTab_scaleModelGroup_avgMeasurementBetweenTimesFromTextField = JTextField("", focusLost=self.setAvgMeasurementBetweenTimesFromEvent)
        settingsTab_scaleModelGroup_avgMeasurementBetweenTimesToLabel = JLabel(" and ", SwingConstants.RIGHT)
        self.settingsTab_scaleModelGroup_avgMeasurementBetweenTimesToTextField = JTextField("", focusLost=self.setAvgMeasurementBetweenTimesToEvent)
        
        settingsTab_scaleModelGroup_avgMeasurementBetweenTimesPanel = JPanel(GridBagLayout())
        settingsTab_scaleModelGroup_avgMeasurementBetweenTimesPanel_GBLC = GridBagConstraints()
        settingsTab_scaleModelGroup_avgMeasurementBetweenTimesPanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        settingsTab_scaleModelGroup_avgMeasurementBetweenTimesPanel_GBLC.weightx = 0.35
        settingsTab_scaleModelGroup_avgMeasurementBetweenTimesPanel_GBLC.gridx = 0
        settingsTab_scaleModelGroup_avgMeasurementBetweenTimesPanel_GBLC.gridy = 0
        settingsTab_scaleModelGroup_avgMeasurementBetweenTimesPanel.add(settingsTab_scaleModelGroup_avgMeasurementBetweenTimesFromLabel, settingsTab_scaleModelGroup_avgMeasurementBetweenTimesPanel_GBLC)
        settingsTab_scaleModelGroup_avgMeasurementBetweenTimesPanel_GBLC.weightx = 0.25
        settingsTab_scaleModelGroup_avgMeasurementBetweenTimesPanel_GBLC.gridx = 1
        settingsTab_scaleModelGroup_avgMeasurementBetweenTimesPanel_GBLC.gridy = 0
        settingsTab_scaleModelGroup_avgMeasurementBetweenTimesPanel.add(self.settingsTab_scaleModelGroup_avgMeasurementBetweenTimesFromTextField, settingsTab_scaleModelGroup_avgMeasurementBetweenTimesPanel_GBLC)
        settingsTab_scaleModelGroup_avgMeasurementBetweenTimesPanel_GBLC.weightx = 0.15
        settingsTab_scaleModelGroup_avgMeasurementBetweenTimesPanel_GBLC.gridx = 2
        settingsTab_scaleModelGroup_avgMeasurementBetweenTimesPanel_GBLC.gridy = 0
        settingsTab_scaleModelGroup_avgMeasurementBetweenTimesPanel.add(settingsTab_scaleModelGroup_avgMeasurementBetweenTimesToLabel, settingsTab_scaleModelGroup_avgMeasurementBetweenTimesPanel_GBLC)
        settingsTab_scaleModelGroup_avgMeasurementBetweenTimesPanel_GBLC.weightx = 0.25
        settingsTab_scaleModelGroup_avgMeasurementBetweenTimesPanel_GBLC.gridx = 3
        settingsTab_scaleModelGroup_avgMeasurementBetweenTimesPanel_GBLC.gridy = 0
        settingsTab_scaleModelGroup_avgMeasurementBetweenTimesPanel.add(self.settingsTab_scaleModelGroup_avgMeasurementBetweenTimesToTextField, settingsTab_scaleModelGroup_avgMeasurementBetweenTimesPanel_GBLC)
        
        ## Finish Scale Model group
        self.settingsTab_scaleModelGroup_scaleModelCheckBox = JCheckBox("Scale Model", actionPerformed=self.onScaleModelCheckBoxSelect)
        
        settingsTab_scaleModelGroup_GBLC.weightx = 0.80
        settingsTab_scaleModelGroup_GBLC.gridx = 0
        settingsTab_scaleModelGroup_GBLC.gridy = 0
        settingsTab_scaleModelGroup.add(self.settingsTab_scaleModelGroup_scaleModelCheckBox, settingsTab_scaleModelGroup_GBLC)
        settingsTab_scaleModelGroup_GBLC.weightx = 0.2
        settingsTab_scaleModelGroup_GBLC.gridx = 1
        settingsTab_scaleModelGroup_GBLC.gridy = 0
        settingsTab_scaleModelGroup.add(JLabel(""), settingsTab_scaleModelGroup_GBLC)
        
        settingsTab_scaleModelGroup_GBLC.weightx = 0.1
        settingsTab_scaleModelGroup_GBLC.gridx = 0
        settingsTab_scaleModelGroup_GBLC.gridy = 1
        settingsTab_scaleModelGroup.add(JLabel(""), settingsTab_scaleModelGroup_GBLC)
        settingsTab_scaleModelGroup_GBLC.weightx = 0.80
        settingsTab_scaleModelGroup_GBLC.gridx = 1
        settingsTab_scaleModelGroup_GBLC.gridy = 1
        settingsTab_scaleModelGroup.add(settingsTab_scaleModelGroup_preserveMassDistPanel, settingsTab_scaleModelGroup_GBLC)
        settingsTab_scaleModelGroup_GBLC.weightx = 0.1
        settingsTab_scaleModelGroup_GBLC.gridx = 2
        settingsTab_scaleModelGroup_GBLC.gridy = 1
        settingsTab_scaleModelGroup.add(JLabel(""), settingsTab_scaleModelGroup_GBLC)
        
        settingsTab_scaleModelGroup_GBLC.weightx = 0.1
        settingsTab_scaleModelGroup_GBLC.gridx = 0
        settingsTab_scaleModelGroup_GBLC.gridy = 2
        settingsTab_scaleModelGroup.add(JLabel(""), settingsTab_scaleModelGroup_GBLC)
        settingsTab_scaleModelGroup_GBLC.weightx = 0.80
        settingsTab_scaleModelGroup_GBLC.gridx = 1
        settingsTab_scaleModelGroup_GBLC.gridy = 2
        settingsTab_scaleModelGroup.add(settingsTab_scaleModelGroup_markerDataMeasurementPanel, settingsTab_scaleModelGroup_GBLC)
        settingsTab_scaleModelGroup_GBLC.weightx = 0.1
        settingsTab_scaleModelGroup_GBLC.gridx = 2
        settingsTab_scaleModelGroup_GBLC.gridy = 2
        settingsTab_scaleModelGroup.add(JLabel(""), settingsTab_scaleModelGroup_GBLC)
        
        settingsTab_scaleModelGroup_GBLC.weightx = 0.1
        settingsTab_scaleModelGroup_GBLC.gridx = 0
        settingsTab_scaleModelGroup_GBLC.gridy = 3
        settingsTab_scaleModelGroup.add(JLabel(""), settingsTab_scaleModelGroup_GBLC)
        settingsTab_scaleModelGroup_GBLC.weightx = 0.80
        settingsTab_scaleModelGroup_GBLC.gridx = 1
        settingsTab_scaleModelGroup_GBLC.gridy = 3
        settingsTab_scaleModelGroup.add(settingsTab_scaleModelGroup_avgMeasurementBetweenTimesPanel, settingsTab_scaleModelGroup_GBLC)
        settingsTab_scaleModelGroup_GBLC.weightx = 0.1
        settingsTab_scaleModelGroup_GBLC.gridx = 2
        settingsTab_scaleModelGroup_GBLC.gridy = 3
        settingsTab_scaleModelGroup.add(JLabel(""), settingsTab_scaleModelGroup_GBLC)
        
        ## Adjust Model Markers Group [checkbox]
        settingsTab_adjustModelMarkersGroup = JPanel(GridBagLayout())
        settingsTab_adjustModelMarkersGroup_GBLC = GridBagConstraints()
        settingsTab_adjustModelMarkersGroup_GBLC.fill = GridBagConstraints.HORIZONTAL
        settingsTab_adjustModelMarkersGroup.setBorder(TitledBorder("Adjust Model Markers"))
        
        ### Marker data for static pose [label, file, browse]
        settingsTab_adjustModelMarkersGroup_markerDataStaticPoseFileLabel = JLabel("Marker data for static pose ", SwingConstants.RIGHT)
        self.settingsTab_adjustModelMarkersGroup_markerDataStaticPoseFileTextField = JTextField("", focusLost=self.setMarkerDataStaticPoseFileEvent)
        settingsTab_adjustModelMarkersGroup_markerDataStaticPoseFileChooserButton = JButton("...", actionPerformed = self.selectMarkerDataStaticPoseFile)
        
        settingsTab_adjustModelMarkersGroup_markerDataStaticPosePanel = JPanel(GridBagLayout())
        settingsTab_adjustModelMarkersGroup_markerDataStaticPosePanel_GBLC = GridBagConstraints()
        settingsTab_adjustModelMarkersGroup_markerDataStaticPosePanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        settingsTab_adjustModelMarkersGroup_markerDataStaticPosePanel_GBLC.weightx = 0.40
        settingsTab_adjustModelMarkersGroup_markerDataStaticPosePanel_GBLC.gridx = 0
        settingsTab_adjustModelMarkersGroup_markerDataStaticPosePanel_GBLC.gridy = 0
        settingsTab_adjustModelMarkersGroup_markerDataStaticPosePanel.add(settingsTab_adjustModelMarkersGroup_markerDataStaticPoseFileLabel, settingsTab_adjustModelMarkersGroup_markerDataStaticPosePanel_GBLC)
        settingsTab_adjustModelMarkersGroup_markerDataStaticPosePanel_GBLC.weightx = 0.59
        settingsTab_adjustModelMarkersGroup_markerDataStaticPosePanel_GBLC.gridx = 1
        settingsTab_adjustModelMarkersGroup_markerDataStaticPosePanel_GBLC.gridy = 0
        settingsTab_adjustModelMarkersGroup_markerDataStaticPosePanel.add(self.settingsTab_adjustModelMarkersGroup_markerDataStaticPoseFileTextField, settingsTab_adjustModelMarkersGroup_markerDataStaticPosePanel_GBLC)
        settingsTab_adjustModelMarkersGroup_markerDataStaticPosePanel_GBLC.weightx = 0.01
        settingsTab_adjustModelMarkersGroup_markerDataStaticPosePanel_GBLC.gridx = 2
        settingsTab_adjustModelMarkersGroup_markerDataStaticPosePanel_GBLC.gridy = 0
        settingsTab_adjustModelMarkersGroup_markerDataStaticPosePanel.add(settingsTab_adjustModelMarkersGroup_markerDataStaticPoseFileChooserButton, settingsTab_adjustModelMarkersGroup_markerDataStaticPosePanel_GBLC)
        
        ### Average markers between times [label, text, and, text]
        settingsTab_adjustModelMarkersGroup_avgMarkersBetweenTimesFromLabel = JLabel("Average markers between times ", SwingConstants.RIGHT)
        self.settingsTab_adjustModelMarkersGroup_avgMarkersBetweenTimesFromTextField = JTextField("", focusLost=self.setAvgMarkersBetweenTimesFromEvent)
        settingsTab_adjustModelMarkersGroup_avgMarkersBetweenTimesToLabel = JLabel(" and ", SwingConstants.RIGHT)
        self.settingsTab_adjustModelMarkersGroup_avgMarkersBetweenTimesToTextField = JTextField("", focusLost=self.setAvgMarkersBetweenTimesToEvent)
        
        settingsTab_adjustModelMarkersGroup_avgMarkersBetweenTimesPanel = JPanel(GridBagLayout())
        settingsTab_adjustModelMarkersGroup_avgMarkersBetweenTimesPanel_GBLC = GridBagConstraints()
        settingsTab_adjustModelMarkersGroup_avgMarkersBetweenTimesPanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        settingsTab_adjustModelMarkersGroup_avgMarkersBetweenTimesPanel_GBLC.weightx = 0.35
        settingsTab_adjustModelMarkersGroup_avgMarkersBetweenTimesPanel_GBLC.gridx = 0
        settingsTab_adjustModelMarkersGroup_avgMarkersBetweenTimesPanel_GBLC.gridy = 0
        settingsTab_adjustModelMarkersGroup_avgMarkersBetweenTimesPanel.add(settingsTab_adjustModelMarkersGroup_avgMarkersBetweenTimesFromLabel, settingsTab_adjustModelMarkersGroup_avgMarkersBetweenTimesPanel_GBLC)
        settingsTab_adjustModelMarkersGroup_avgMarkersBetweenTimesPanel_GBLC.weightx = 0.25
        settingsTab_adjustModelMarkersGroup_avgMarkersBetweenTimesPanel_GBLC.gridx = 1
        settingsTab_adjustModelMarkersGroup_avgMarkersBetweenTimesPanel_GBLC.gridy = 0
        settingsTab_adjustModelMarkersGroup_avgMarkersBetweenTimesPanel.add(self.settingsTab_adjustModelMarkersGroup_avgMarkersBetweenTimesFromTextField, settingsTab_adjustModelMarkersGroup_avgMarkersBetweenTimesPanel_GBLC)
        settingsTab_adjustModelMarkersGroup_avgMarkersBetweenTimesPanel_GBLC.weightx = 0.15
        settingsTab_adjustModelMarkersGroup_avgMarkersBetweenTimesPanel_GBLC.gridx = 2
        settingsTab_adjustModelMarkersGroup_avgMarkersBetweenTimesPanel_GBLC.gridy = 0
        settingsTab_adjustModelMarkersGroup_avgMarkersBetweenTimesPanel.add(settingsTab_adjustModelMarkersGroup_avgMarkersBetweenTimesToLabel, settingsTab_adjustModelMarkersGroup_avgMarkersBetweenTimesPanel_GBLC)
        settingsTab_adjustModelMarkersGroup_avgMarkersBetweenTimesPanel_GBLC.weightx = 0.25
        settingsTab_adjustModelMarkersGroup_avgMarkersBetweenTimesPanel_GBLC.gridx = 3
        settingsTab_adjustModelMarkersGroup_avgMarkersBetweenTimesPanel_GBLC.gridy = 0
        settingsTab_adjustModelMarkersGroup_avgMarkersBetweenTimesPanel.add(self.settingsTab_adjustModelMarkersGroup_avgMarkersBetweenTimesToTextField, settingsTab_adjustModelMarkersGroup_avgMarkersBetweenTimesPanel_GBLC)
        
        ### Coodinate data for static pose [checkbox, label, file, browse]
        self.settingsTab_adjustModelMarkersGroup_coordDataForStaticPoseFileCheckBox = JCheckBox("Coordinate data for static pose ", actionPerformed=self.onCoordDataForStaticPoseFileCheckBoxSelect)
        self.settingsTab_adjustModelMarkersGroup_coordDataForStaticPoseFileTextField = JTextField("", focusLost=self.setCoordDataForStaticPoseFileEvent)
        settingsTab_adjustModelMarkersGroup_coordDataForStaticPoseFileChooserButton = JButton("...", actionPerformed = self.selectCoordDataForStaticPoseFile)
        
        settingsTab_adjustModelMarkersGroup_coordDataForStaticPosePanel = JPanel(GridBagLayout())
        settingsTab_adjustModelMarkersGroup_coordDataForStaticPosePanel_GBLC = GridBagConstraints()
        settingsTab_adjustModelMarkersGroup_coordDataForStaticPosePanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        settingsTab_adjustModelMarkersGroup_coordDataForStaticPosePanel_GBLC.weightx = 0.40
        settingsTab_adjustModelMarkersGroup_coordDataForStaticPosePanel_GBLC.gridx = 0
        settingsTab_adjustModelMarkersGroup_coordDataForStaticPosePanel_GBLC.gridy = 0
        settingsTab_adjustModelMarkersGroup_coordDataForStaticPosePanel.add(self.settingsTab_adjustModelMarkersGroup_coordDataForStaticPoseFileCheckBox, settingsTab_adjustModelMarkersGroup_coordDataForStaticPosePanel_GBLC)
        settingsTab_adjustModelMarkersGroup_coordDataForStaticPosePanel_GBLC.weightx = 0.59
        settingsTab_adjustModelMarkersGroup_coordDataForStaticPosePanel_GBLC.gridx = 1
        settingsTab_adjustModelMarkersGroup_coordDataForStaticPosePanel_GBLC.gridy = 0
        settingsTab_adjustModelMarkersGroup_coordDataForStaticPosePanel.add(self.settingsTab_adjustModelMarkersGroup_coordDataForStaticPoseFileTextField, settingsTab_adjustModelMarkersGroup_coordDataForStaticPosePanel_GBLC)
        settingsTab_adjustModelMarkersGroup_coordDataForStaticPosePanel_GBLC.weightx = 0.01
        settingsTab_adjustModelMarkersGroup_coordDataForStaticPosePanel_GBLC.gridx = 2
        settingsTab_adjustModelMarkersGroup_coordDataForStaticPosePanel_GBLC.gridy = 0
        settingsTab_adjustModelMarkersGroup_coordDataForStaticPosePanel.add(settingsTab_adjustModelMarkersGroup_coordDataForStaticPoseFileChooserButton, settingsTab_adjustModelMarkersGroup_coordDataForStaticPosePanel_GBLC)
        
        ## Finish Adjust Model Markers group
        self.settingsTab_adjustModelMarkersGroup_adjustModelMarkersCheckBox = JCheckBox("Adjust model markers", actionPerformed=self.onAdjustModelMarkersCheckBoxSelect)
        
        settingsTab_adjustModelMarkersGroup_GBLC.weightx = 0.80
        settingsTab_adjustModelMarkersGroup_GBLC.gridx = 0
        settingsTab_adjustModelMarkersGroup_GBLC.gridy = 0
        settingsTab_adjustModelMarkersGroup.add(self.settingsTab_adjustModelMarkersGroup_adjustModelMarkersCheckBox, settingsTab_adjustModelMarkersGroup_GBLC)
        settingsTab_adjustModelMarkersGroup_GBLC.weightx = 0.2
        settingsTab_adjustModelMarkersGroup_GBLC.gridx = 1
        settingsTab_adjustModelMarkersGroup_GBLC.gridy = 0
        settingsTab_adjustModelMarkersGroup.add(JLabel(""), settingsTab_scaleModelGroup_GBLC)
        
        settingsTab_adjustModelMarkersGroup_GBLC.weightx = 0.1
        settingsTab_adjustModelMarkersGroup_GBLC.gridx = 0
        settingsTab_adjustModelMarkersGroup_GBLC.gridy = 1
        settingsTab_adjustModelMarkersGroup.add(JLabel(""), settingsTab_adjustModelMarkersGroup_GBLC)
        settingsTab_adjustModelMarkersGroup_GBLC.weightx = 0.80
        settingsTab_adjustModelMarkersGroup_GBLC.gridx = 1
        settingsTab_adjustModelMarkersGroup_GBLC.gridy = 1
        settingsTab_adjustModelMarkersGroup.add(settingsTab_adjustModelMarkersGroup_markerDataStaticPosePanel, settingsTab_adjustModelMarkersGroup_GBLC)
        settingsTab_adjustModelMarkersGroup_GBLC.weightx = 0.1
        settingsTab_adjustModelMarkersGroup_GBLC.gridx = 2
        settingsTab_adjustModelMarkersGroup_GBLC.gridy = 1
        settingsTab_adjustModelMarkersGroup.add(JLabel(""), settingsTab_adjustModelMarkersGroup_GBLC)
        
        settingsTab_adjustModelMarkersGroup_GBLC.weightx = 0.1
        settingsTab_adjustModelMarkersGroup_GBLC.gridx = 0
        settingsTab_adjustModelMarkersGroup_GBLC.gridy = 2
        settingsTab_adjustModelMarkersGroup.add(JLabel(""), settingsTab_adjustModelMarkersGroup_GBLC)
        settingsTab_adjustModelMarkersGroup_GBLC.weightx = 0.80
        settingsTab_adjustModelMarkersGroup_GBLC.gridx = 1
        settingsTab_adjustModelMarkersGroup_GBLC.gridy = 2
        settingsTab_adjustModelMarkersGroup.add(settingsTab_adjustModelMarkersGroup_avgMarkersBetweenTimesPanel, settingsTab_adjustModelMarkersGroup_GBLC)
        settingsTab_adjustModelMarkersGroup_GBLC.weightx = 0.1
        settingsTab_adjustModelMarkersGroup_GBLC.gridx = 2
        settingsTab_adjustModelMarkersGroup_GBLC.gridy = 2
        settingsTab_adjustModelMarkersGroup.add(JLabel(""), settingsTab_adjustModelMarkersGroup_GBLC)
        
        settingsTab_adjustModelMarkersGroup_GBLC.weightx = 0.1
        settingsTab_adjustModelMarkersGroup_GBLC.gridx = 0
        settingsTab_adjustModelMarkersGroup_GBLC.gridy = 3
        settingsTab_adjustModelMarkersGroup.add(JLabel(""), settingsTab_adjustModelMarkersGroup_GBLC)
        settingsTab_adjustModelMarkersGroup_GBLC.weightx = 0.80
        settingsTab_adjustModelMarkersGroup_GBLC.gridx = 1
        settingsTab_adjustModelMarkersGroup_GBLC.gridy = 3
        settingsTab_adjustModelMarkersGroup.add(settingsTab_adjustModelMarkersGroup_coordDataForStaticPosePanel, settingsTab_adjustModelMarkersGroup_GBLC)
        settingsTab_adjustModelMarkersGroup_GBLC.weightx = 0.1
        settingsTab_adjustModelMarkersGroup_GBLC.gridx = 2
        settingsTab_adjustModelMarkersGroup_GBLC.gridy = 3
        settingsTab_adjustModelMarkersGroup.add(JLabel(""), settingsTab_adjustModelMarkersGroup_GBLC)
        
        ## Output group
        settingsTab_outputGroup = JPanel(GridBagLayout())
        settingsTab_outputGroup_GBLC = GridBagConstraints()
        settingsTab_outputGroup_GBLC.fill = GridBagConstraints.HORIZONTAL
        settingsTab_outputGroup.setBorder(TitledBorder("Output"))
        
        ### Output scaled model file path [label, text, button]
        settingsTab_outputGroup_outputModelScaledFileLabel = JLabel("Output model file path (after scaling) ", SwingConstants.RIGHT)
        self.settingsTab_outputGroup_outputModelScaledFileTextField = JTextField("", focusLost=self.setOutputModelScaledFileEvent)
        settingsTab_outputGroup_outputModelScaledFileChooserButton = JButton("...", actionPerformed = self.selectOutputModelScaledFile)
        
        settingsTab_outputGroup_outputModelScaledFilePanel = JPanel(GridBagLayout())
        settingsTab_outputGroup_outputModelScaledFilePanel_GBLC = GridBagConstraints()
        settingsTab_outputGroup_outputModelScaledFilePanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        settingsTab_outputGroup_outputModelScaledFilePanel_GBLC.weightx = 0.40
        settingsTab_outputGroup_outputModelScaledFilePanel_GBLC.gridx = 0
        settingsTab_outputGroup_outputModelScaledFilePanel_GBLC.gridy = 0
        settingsTab_outputGroup_outputModelScaledFilePanel.add(settingsTab_outputGroup_outputModelScaledFileLabel, settingsTab_outputGroup_outputModelScaledFilePanel_GBLC)
        settingsTab_outputGroup_outputModelScaledFilePanel_GBLC.weightx = 0.59
        settingsTab_outputGroup_outputModelScaledFilePanel_GBLC.gridx = 1
        settingsTab_outputGroup_outputModelScaledFilePanel_GBLC.gridy = 0
        settingsTab_outputGroup_outputModelScaledFilePanel.add(self.settingsTab_outputGroup_outputModelScaledFileTextField, settingsTab_outputGroup_outputModelScaledFilePanel_GBLC)
        settingsTab_outputGroup_outputModelScaledFilePanel_GBLC.weightx = 0.01
        settingsTab_outputGroup_outputModelScaledFilePanel_GBLC.gridx = 2
        settingsTab_outputGroup_outputModelScaledFilePanel_GBLC.gridy = 0
        settingsTab_outputGroup_outputModelScaledFilePanel.add(settingsTab_outputGroup_outputModelScaledFileChooserButton, settingsTab_outputGroup_outputModelScaledFilePanel_GBLC)
        
        ### Output scaled and Markered model file path [label, text, button]
        settingsTab_outputGroup_outputModelFileLabel = JLabel("Output model file path (after scaling and adjusting Markers) ", SwingConstants.RIGHT)
        self.settingsTab_outputGroup_outputModelFileTextField = JTextField("", focusLost=self.setOutputModelFileEvent)
        settingsTab_outputGroup_outputModelFileChooserButton = JButton("...", actionPerformed = self.selectOutputModelFile)
        
        settingsTab_outputGroup_outputModelFilePanel = JPanel(GridBagLayout())
        settingsTab_outputGroup_outputModelFilePanel_GBLC = GridBagConstraints()
        settingsTab_outputGroup_outputModelFilePanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        settingsTab_outputGroup_outputModelFilePanel_GBLC.weightx = 0.40
        settingsTab_outputGroup_outputModelFilePanel_GBLC.gridx = 0
        settingsTab_outputGroup_outputModelFilePanel_GBLC.gridy = 0
        settingsTab_outputGroup_outputModelFilePanel.add(settingsTab_outputGroup_outputModelFileLabel, settingsTab_outputGroup_outputModelFilePanel_GBLC)
        settingsTab_outputGroup_outputModelFilePanel_GBLC.weightx = 0.59
        settingsTab_outputGroup_outputModelFilePanel_GBLC.gridx = 1
        settingsTab_outputGroup_outputModelFilePanel_GBLC.gridy = 0
        settingsTab_outputGroup_outputModelFilePanel.add(self.settingsTab_outputGroup_outputModelFileTextField, settingsTab_outputGroup_outputModelFilePanel_GBLC)
        settingsTab_outputGroup_outputModelFilePanel_GBLC.weightx = 0.01
        settingsTab_outputGroup_outputModelFilePanel_GBLC.gridx = 2
        settingsTab_outputGroup_outputModelFilePanel_GBLC.gridy = 0
        settingsTab_outputGroup_outputModelFilePanel.add(settingsTab_outputGroup_outputModelFileChooserButton, settingsTab_outputGroup_outputModelFilePanel_GBLC)
        
        ## Finish Output group
        settingsTab_outputGroup_GBLC.weightx = 0.1
        settingsTab_outputGroup_GBLC.gridx = 0
        settingsTab_outputGroup_GBLC.gridy = 0
        settingsTab_outputGroup.add(JLabel(""), settingsTab_outputGroup_GBLC)
        settingsTab_outputGroup_GBLC.weightx = 0.80
        settingsTab_outputGroup_GBLC.gridx = 1
        settingsTab_outputGroup_GBLC.gridy = 0
        settingsTab_outputGroup.add(settingsTab_outputGroup_outputModelScaledFilePanel, settingsTab_outputGroup_GBLC)
        settingsTab_outputGroup_GBLC.weightx = 0.1
        settingsTab_outputGroup_GBLC.gridx = 2
        settingsTab_outputGroup_GBLC.gridy = 0
        settingsTab_outputGroup.add(JLabel(""), settingsTab_outputGroup_GBLC)
        
        settingsTab_outputGroup_GBLC.weightx = 0.1
        settingsTab_outputGroup_GBLC.gridx = 0
        settingsTab_outputGroup_GBLC.gridy = 1
        settingsTab_outputGroup.add(JLabel(""), settingsTab_outputGroup_GBLC)
        settingsTab_outputGroup_GBLC.weightx = 0.80
        settingsTab_outputGroup_GBLC.gridx = 1
        settingsTab_outputGroup_GBLC.gridy = 1
        settingsTab_outputGroup.add(settingsTab_outputGroup_outputModelFilePanel, settingsTab_outputGroup_GBLC)
        settingsTab_outputGroup_GBLC.weightx = 0.1
        settingsTab_outputGroup_GBLC.gridx = 2
        settingsTab_outputGroup_GBLC.gridy = 1
        settingsTab_outputGroup.add(JLabel(""), settingsTab_outputGroup_GBLC)
        
        # Finish constructing the Settings tab
        gridyVal = -1
        
        gridyVal = gridyVal + 1
        settingsTab_GBLC.weightx = 0.1
        settingsTab_GBLC.gridx = 0
        settingsTab_GBLC.gridy = gridyVal
        settingsTab.add(JLabel(""), settingsTab_GBLC)
        settingsTab_GBLC.weightx = 0.40
        settingsTab_GBLC.gridx = 1
        settingsTab_GBLC.gridy = gridyVal
        settingsTab.add(settingsTab_adjustModelMarkersGroup, settingsTab_GBLC)
        settingsTab_GBLC.weightx = 0.1
        settingsTab_GBLC.gridx = 2
        settingsTab_GBLC.gridy = gridyVal
        settingsTab.add(JLabel(""), settingsTab_GBLC)
        
        gridyVal = gridyVal + 1
        settingsTab_GBLC.weightx = 0.1
        settingsTab_GBLC.gridx = 0
        settingsTab_GBLC.gridy = gridyVal
        settingsTab.add(JLabel(""), settingsTab_GBLC)
        settingsTab_GBLC.weightx = 0.40
        settingsTab_GBLC.gridx = 1
        settingsTab_GBLC.gridy = gridyVal
        settingsTab.add(settingsTab_scaleModelGroup, settingsTab_GBLC)
        settingsTab_GBLC.weightx = 0.1
        settingsTab_GBLC.gridx = 2
        settingsTab_GBLC.gridy = gridyVal
        settingsTab.add(JLabel(""), settingsTab_GBLC)
        
        gridyVal = gridyVal + 1
        settingsTab_GBLC.weightx = 0.1
        settingsTab_GBLC.gridx = 0
        settingsTab_GBLC.gridy = gridyVal
        settingsTab.add(JLabel(""), settingsTab_GBLC)
        settingsTab_GBLC.weightx = 0.40
        settingsTab_GBLC.gridx = 1
        settingsTab_GBLC.gridy = gridyVal
        settingsTab.add(settingsTab_subjectDataGroup, settingsTab_GBLC)
        settingsTab_GBLC.weightx = 0.1
        settingsTab_GBLC.gridx = 2
        settingsTab_GBLC.gridy = gridyVal
        settingsTab.add(JLabel(""), settingsTab_GBLC)
        
        gridyVal = gridyVal + 1
        settingsTab_GBLC.weightx = 0.1
        settingsTab_GBLC.gridx = 0
        settingsTab_GBLC.gridy = gridyVal
        settingsTab.add(JLabel(""), settingsTab_GBLC)
        settingsTab_GBLC.weightx = 0.40
        settingsTab_GBLC.gridx = 1
        settingsTab_GBLC.gridy = gridyVal
        settingsTab.add(settingsTab_outputGroup, settingsTab_GBLC)
        settingsTab_GBLC.weightx = 0.1
        settingsTab_GBLC.gridx = 2
        settingsTab_GBLC.gridy = gridyVal
        settingsTab.add(JLabel(""), settingsTab_GBLC)
        
        
        # Measurement Sets tab  ##########################################################################################
        self.measurementSetTab = JPanel(GridBagLayout())
        measurementSetTab_GBLC = GridBagConstraints()
        measurementSetTab_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        self.measurementSetsPanel = BessScaleToolMeasurementSetPanel()
        
        measurementSetTab_GBLC.weightx = 1.0
        measurementSetTab_GBLC.gridx = 0
        measurementSetTab_GBLC.gridy = 0
        self.measurementSetTab.add(self.measurementSetsPanel, measurementSetTab_GBLC)
        
        table = self.measurementSetsPanel.measurementSetTable
        tableModel = table.getModel()
        tableModel.tableChanged = self.setMeasurementsEvent
        
        
        # Scale Factors tab  ##########################################################################################
        scaleFactorsTab = JPanel(GridBagLayout())
        scaleFactorsTab_GBLC = GridBagConstraints()
        scaleFactorsTab_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        self.scaleFactorsPanel = BessScaleToolScaleFactorsPanel()
        
        scaleFactorsTab_GBLC.weightx = 1.0
        scaleFactorsTab_GBLC.gridx = 0
        scaleFactorsTab_GBLC.gridy = 0
        scaleFactorsTab.add(self.scaleFactorsPanel, scaleFactorsTab_GBLC)
        
        fTable = self.scaleFactorsPanel.scaleFactorsTable
        fTableModel = fTable.getModel()
        fTableModel.tableChanged = self.setScaleFactorsTableInConfigEvent
        
        
        # Static Pose Weights tab  ########################################################################################
        staticPoseWeightsTab = JPanel(GridBagLayout())
        staticPoseWeightsTab_GBLC = GridBagConstraints()
        staticPoseWeightsTab_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        self.staticPoseWeightsPanel = BessScaleToolStaticPoseWeightsPanel()
        
        staticPoseWeightsTab_GBLC.weightx = 1.0
        staticPoseWeightsTab_GBLC.gridx = 0
        staticPoseWeightsTab_GBLC.gridy = 0
        staticPoseWeightsTab.add(self.staticPoseWeightsPanel, staticPoseWeightsTab_GBLC)
        
        cTable = self.staticPoseWeightsPanel.coordinatesTable
        cTableModel = cTable.getModel()
        cTableModel.tableChanged = self.setCoordTableInConfigEvent
        
        mTable = self.staticPoseWeightsPanel.markerTable
        mTableModel = mTable.getModel()
        mTableModel.tableChanged = self.setMarkerTableInConfigEvent
        
        #I AM HERE. Changes made to table via the top controls don't seem to trigger the tableChanged event. This results in the config not updating to new values.
        
        ###############################################################################################################
        self.scaleTabs.addTab("Settings", settingsTab)
        self.scaleTabs.addTab("Measurement Sets", self.measurementSetTab)
        self.scaleTabs.addTab("Scale Factors", scaleFactorsTab)
        self.scaleTabs.addTab("Static Pose Weights", staticPoseWeightsTab)
        
        self.scalePanel_GBLC.weightx = 0.1
        self.scalePanel_GBLC.weighty = 1.0
        self.scalePanel_GBLC.gridx = 0
        self.scalePanel_GBLC.gridy = 0
        self.scalePanel.add(JLabel(""), self.scalePanel_GBLC)
        self.scalePanel_GBLC.weightx = 0.80
        self.scalePanel_GBLC.weighty = 1.0
        self.scalePanel_GBLC.gridx = 1
        self.scalePanel_GBLC.gridy = 0
        self.scalePanel.add(self.scaleTabs, self.scalePanel_GBLC)
        self.scalePanel_GBLC.weightx = 0.1
        self.scalePanel_GBLC.weighty = 1.0
        self.scalePanel_GBLC.gridx = 2
        self.scalePanel_GBLC.gridy = 0
        self.scalePanel.add(JLabel(""), self.scalePanel_GBLC)
        
        self.scaleCard.add(useThisToolGroup, BorderLayout.NORTH)
        self.scaleCard.add(self.scalePanel, BorderLayout.CENTER)
        
        
    def refreshMeasurementSetTable(self):
        print "[DEBUG] Refreshing measurement set table"
        
        table = self.measurementSetsPanel.measurementSetTable
        tableModel = table.getModel()
        colModel = table.getColumnModel()
        
        rowCount = table.getRowCount()
        colCount = colModel.getColumnCount()
        
        # Clear data
        blank = ""
        for i in range(rowCount):
            for j in range(colCount):
                try:
                    table.setValueAt(blank, i, j)
                except:
                    #continue
                    print "Exception caught while blanking out table"
                    break
        
        # Set data
        for rowIndex, row in enumerate(self.measurementSetTableData):
            for colIndex, myValue in enumerate(row):
                table.setValueAt( myValue, rowIndex, colIndex) 
                
        # Update config
        self.setMeasurements()
    
    def refreshScaleFactorsTable(self):
        print "[DEBUG] Refreshing scale factors table"
        
        table = self.scaleFactorsPanel.scaleFactorsTable
        tableModel = table.getModel()
        colModel = table.getColumnModel()
        
        rowCount = table.getRowCount()
        colCount = colModel.getColumnCount()
        
        # Clear data
        blank = ""
        for i in range(rowCount):
            for j in range(colCount):
                try:
                    table.setValueAt(blank, i, j)
                except:
                    #continue
                    print "Exception caught while blanking out scale factors table"
                    break
        
        # Set data
        for rowIndex, row in enumerate(self.scaleFactorsTableData):
            for colIndex, myValue in enumerate(row):
                table.setValueAt( myValue, rowIndex, colIndex)
        
        
        # Update config
        self.setScaleFactorsTableInConfig()
    
    def refreshStaticPoseMarkerTable(self):
        print "[DEBUG] Refreshing static pose marker table"
        
        table = self.staticPoseWeightsPanel.markerTable
        tableModel = table.getModel()
        colModel = table.getColumnModel()
        
        rowCount = table.getRowCount()
        colCount = colModel.getColumnCount()
        
        # Clear data
        blank = ""
        for i in range(rowCount):
            for j in range(colCount):
                try:
                    table.setValueAt(blank, i, j)
                except:
                    #continue
                    print "Exception caught while blanking out marker table"
                    break
        
        # Set data
        for rowIndex, row in enumerate(self.staticPoseWeightsMarkerTableData):
            for colIndex, myValue in enumerate(row):
                table.setValueAt( myValue, rowIndex, (colIndex))
        
        
        # Update config
        self.setMarkerTableInConfig()
    
    def refreshStaticPoseCoordTable(self):
        print "[DEBUG] Refreshing static pose coordinate table"
        
        table = self.staticPoseWeightsPanel.coordinatesTable
        tableModel = table.getModel()
        colModel = table.getColumnModel()
        
        rowCount = table.getRowCount()
        colCount = colModel.getColumnCount()
        
        # Clear data
        blank = ""
        for i in range(rowCount):
            for j in range(colCount):
                try:
                    table.setValueAt(blank, i, j)
                except:
                    #continue
                    print "Exception caught while blanking out coordinate table"
                    break
        
        # Set data
        for rowIndex, row in enumerate(self.staticPoseWeightsCoordTableData):
            for colIndex, myValue in enumerate(row):
                table.setValueAt( myValue, rowIndex, colIndex)
        
        # Update config
        self.setCoordTableInConfig()
    
    def getCard(self):
        return self.scaleCard
        
    def onUseItCheckBoxSelect(self, event):
        if (self.useThisToolGroup_useItCheckBox.isSelected()):
            self.myConfig.useScale = 'true'
        else:
            self.myConfig.useScale = 'false'
    
    def setMeasurementsEvent(self, event):
        self.setMeasurements()
    
    def setScaleFactorsTableInConfigEvent(self, event):
        #print "setScaleFactorsTableInConfigEvent called"
        if not self.lock:
            
            # Update scaleFactorsTableData
            print "Updating scale factors table data"
            self.scaleFactorsTableData = []
            table = self.scaleFactorsPanel.scaleFactorsTable
            tableModel = table.getModel()
            colModel = table.getColumnModel()
            
            rowCount = table.getRowCount()
            colCount = colModel.getColumnCount()
            
            for i in range(rowCount):
                bodyName = table.getValueAt(i,0)
                if(bodyName == ""):
                    break
                measurementUsed = table.getValueAt(i,1)
                appliedScaleFactor = table.getValueAt(i,2)
                
                row = [bodyName]
                row.append(measurementUsed)
                row.append(appliedScaleFactor)
                self.scaleFactorsTableData.append(row)
            
            self.setScaleFactorsTableInConfig()
    
    def setMarkerTableInConfigEvent(self, event):
        #print "setMarkerTableInConfigEvent called. Locked? %s" % (self.lock)
        if not self.lock:
            #print "1"
            # Update staticPoseWeightsMarkerTableData
            self.staticPoseWeightsMarkerTableData = []
            #print "2"
            table = self.staticPoseWeightsPanel.markerTable
            #print "3"
            tableModel = table.getModel()
            #print "4"
            colModel = table.getColumnModel()
            
            #print "5"
            rowCount = table.getRowCount()
            colCount = colModel.getColumnCount()
            
            #print "6"
            for i in range(rowCount):
                #print "i: %d" % (i)
                apply = table.getValueAt(i,0)
                #print "6.1"
                if(apply == ""):
                    #print "6.11"
                    break
                #print "6.2"
                marker = table.getValueAt(i,1)
                #print "6.3"
                value  = table.getValueAt(i,2)
                #print "6.4"
                weight = table.getValueAt(i,3)
                
                #print "
                
                #print "6.5"
                row = []
                #print "6.5.1"
                row.append(apply)
                #print "6.5.2"
                row.append(marker)
                #print "6.5.3"
                row.append(value)
                #print "6.5.4"
                row.append(weight)
                #print "6.6"
                
                self.staticPoseWeightsMarkerTableData.append(row)
                #print "6.7"
            
            #print "7"
            self.setMarkerTableInConfig()
    
    def setCoordTableInConfigEvent(self, event):
        #print "setCoordTableInConfigEvent called"
        if not self.lock:
        
            # Update staticPoseWeightsCoordTableData
            self.staticPoseWeightsCoordTableData = []
            table = self.staticPoseWeightsPanel.coordinatesTable
            tableModel = table.getModel()
            colModel = table.getColumnModel()
            
            rowCount = table.getRowCount()
            colCount = colModel.getColumnCount()
            
            for i in range(rowCount):
                apply = table.getValueAt(i,0)
                if(apply == ""):
                    break
                coord  = table.getValueAt(i,1)
                value  = table.getValueAt(i,2)
                weight = table.getValueAt(i,3)
                
                row = [apply]
                row.append(coord)
                row.append(value)
                row.append(weight)
                
                self.staticPoseWeightsCoordTableData.append(row)
            
            self.setCoordTableInConfig()
    
    #################################################
    # Save the value in the UI to the config object #
    #################################################
    
    def setOutputModelScaledFile(self):
        # Name of OpenSim model file (.osim) to write when done scaling
        self.myConfig.scale_modelScaler_outputModelFile = self.settingsTab_outputGroup_outputModelScaledFileTextField.text
        
    def setOutputModelFile(self):
        # Output OpenSim model file (.osim) after scaling and maker placement
        self.myConfig.scale_markerPlacer_outputModelFile = self.settingsTab_outputGroup_outputModelFileTextField.text
        
    def setMass(self):
        self.myConfig.scale_mass = self.settingsTab_subjectDataGroup_massTextField.text
        
    def setAddMarkersFile(self):
        self.myConfig.scale_genericModelMaker_markerSetFile = self.settingsTab_subjectDataGroup_addMarkersFileTextField.text
    
    def setMarkerDataMeasurementsFile(self):
        self.myConfig.scale_modelScaler_markerFile = self.settingsTab_scaleModelGroup_markerDataMeasurementsFileTextField.text
    
    def setAvgMeasurementBetweenTimesFrom(self):
        self.myConfig.scale_modelScaler_timeRange = self.settingsTab_scaleModelGroup_avgMeasurementBetweenTimesFromTextField.text + " " + self.settingsTab_scaleModelGroup_avgMeasurementBetweenTimesToTextField.text
    
    def setAvgMeasurementBetweenTimesTo(self):
        self.myConfig.scale_modelScaler_timeRange = self.settingsTab_scaleModelGroup_avgMeasurementBetweenTimesFromTextField.text + " " + self.settingsTab_scaleModelGroup_avgMeasurementBetweenTimesToTextField.text
    
    def setMarkerDataStaticPoseFile(self):
        self.myConfig.scale_markerPlacer_markerFile = self.settingsTab_adjustModelMarkersGroup_markerDataStaticPoseFileTextField.text
    
    def setAvgMarkersBetweenTimesFrom(self):
        self.myConfig.scale_markerPlacer_timeRange = self.settingsTab_adjustModelMarkersGroup_avgMarkersBetweenTimesFromTextField.text + " " + self.settingsTab_adjustModelMarkersGroup_avgMarkersBetweenTimesToTextField.text
    
    def setAvgMarkersBetweenTimesTo(self):
        self.myConfig.scale_markerPlacer_timeRange = self.settingsTab_adjustModelMarkersGroup_avgMarkersBetweenTimesFromTextField.text + " " + self.settingsTab_adjustModelMarkersGroup_avgMarkersBetweenTimesToTextField.text
    
    def setCoordDataForStaticPoseFile(self):
        self.myConfig.scale_markerPlacer_coordinateFile = self.settingsTab_adjustModelMarkersGroup_coordDataForStaticPoseFileTextField.text
    
    #def setOutputModelName(self):
    #    self.myConfig.scale_output_modelName = self.settingsTab_outputGroup_modelNameTextField.text
        
    # Set the measurement values within BESS config
    def setMeasurements(self):
        #print "[DEBUG] Setting measurement values to bess config"
        table = self.measurementSetsPanel.measurementSetTable
        tableModel = table.getModel()
        colModel = table.getColumnModel()
        
        rowCount = table.getRowCount()
        colCount = colModel.getColumnCount()
        
        #print "Rows: %d   Col: %d" % (rowCount, colCount)
        
        measurements = []
        
        self.scaleFactorsPanel.measurementComboBoxA.removeAllItems()
        self.scaleFactorsPanel.measurementComboBoxB.removeAllItems()
        self.scaleFactorsPanel.measurementComboBoxC.removeAllItems()
        self.scaleFactorsPanel.measurementComboBoxA.addItem("Unassigned")
        self.scaleFactorsPanel.measurementComboBoxB.addItem("Unassigned")
        self.scaleFactorsPanel.measurementComboBoxC.addItem("Unassigned")
        
        # Set data
        for rowIndex in range(rowCount):
            markerPairs = []
            markerPair = ""
            bodyScales = []
            bodyScale = ""
            measurement = []
            
            for colIndex in range(colCount):
                myValue = table.getValueAt( rowIndex, colIndex)
                try:
                    if("" == myValue):
                        # We are done with row
                        break
                    else:
                        if ( 0 == colIndex ):
                            # Measurement name
                            measurement.append(myValue)
                            
                            # Update the combo boxes on the scale factors tab
                            self.scaleFactorsPanel.measurementComboBoxA.addItem(myValue)
                            self.scaleFactorsPanel.measurementComboBoxB.addItem(myValue)
                            self.scaleFactorsPanel.measurementComboBoxC.addItem(myValue)
                            
                            # Apply
                            measurement.append("true")
                        elif ( 0 != (colIndex % 2) ):    
                            # MarkerPairs
                            markerPair = myValue
                        elif ( 0 == (colIndex % 2) ):
                            markerPair = markerPair + " " + myValue
                            markerPairs.append(markerPair)
                except:
                    continue
            measurement.append(markerPairs)
        
            # TODO: Body Scales
            
            measurement.append(bodyScales)
            measurements.append(measurement)
        
        #TODO: self.myConfig.scale_modelScaler_measurementSet_objects_measurements
        #print "[DEBUG] Finished setting measurement values to bess config"
    
    def setScaleFactorsTableInConfig(self):
        print "[DEBUG-MARKER] lock: %s" % (self.lock)
        if not self.lock:
            #try:
            # Clear the config value(s)
            self.myConfig.scale_modelScaler_scaleSet_objects_scale = []
            
            scaleSet_scales = []
            scales = []
            
            containsMeasurements = False
            containsManualScale = False
            
            # Set data
            print "[DEBUG-MARKER] Setting config values from scale factors table"
            for rowIndex, row in enumerate(self.scaleFactorsTableData):
                #print "[DEBUG-SFTIC] Row: %s" % (row)
                
                bodyName = row[0]
                measurementsUsed = row[1]
                appliedScaleFactors = row[2]
                
                scale = []
                scale_name = ("scale%s" % (bodyName))
                scale_scales = appliedScaleFactors
                scale_segment = bodyName
                scale_apply = 'true'
                
                if(scale_scales != "CALCULATED"):
                    containsManualScale = True
                    scale.append(scale_name)
                    scale.append(scale_scales)
                    scale.append(scale_segment)
                    scale.append(scale_apply)
                    
                    scales.append(scale)
                else:
                    containsMeasurements = True
                    
            scaleSet_scales.append(scales)
            self.myConfig.scale_modelScaler_scaleSet_objects_scale = scaleSet_scales
            
            scalingOrderString = ""
            if( containsMeasurements ):
                scalingOrderString += " measurements"
            if( containsManualScale ):
                scalingOrderString += " manualScale"
                
            self.myConfig.scale_modelScaler_scalingOrder = scalingOrderString            
            
    
    def setMarkerTableInConfig(self):
        print "[DEBUG-MARKER] lock: %s" % (self.lock)
        if not self.lock:
            #try:
            # Clear the config value(s)
            self.myConfig.scale_markerPlacer_ikTaskSet_objects_ikMarkerTask = []
            
            # Set data
            print "[DEBUG-MARKER] Setting config values from marker table"
            for rowIndex, row in enumerate(self.staticPoseWeightsMarkerTableData):
                #print "[DEBUG-SCTIC] Row: %s" % (row)
                
                apply = row[0]
                if(apply == self.staticPoseWeightsPanel.ENABLED_YES):
                    apply = "true"
                else:
                    apply = "false"
                
                name = row[1]
                
                # Ignore row[2]
                
                weight = row[3]
                
                #print "[DEBUG-SCTIC] Name : %s" % (name)
                #print "[DEBUG-SCTIC] Apply : %s" % (apply)
                #print "[DEBUG-SCTIC] Weight : %s" % (weight)
                
                ikMarkerTask = []
                ikMarkerTask.append(name)
                ikMarkerTask.append(apply)
                ikMarkerTask.append(weight)
                
                self.myConfig.scale_markerPlacer_ikTaskSet_objects_ikMarkerTask.append(ikMarkerTask)
            #except:
            #    print "[DEBUG] Exception thrown in setCoodTableInConfig()"
    
    def setCoordTableInConfig(self):
        print "[DEBUG-COORD] lock: %s" % (self.lock)
        if not self.lock:
            #try:
            # Clear the config value(s)
            self.myConfig.scale_markerPlacer_ikTaskSet_objects_ikCoordinateTask = []
            
            # Set data
            print "[DEBUG-COORD] Setting config values from coord table"
            for rowIndex, row in enumerate(self.staticPoseWeightsCoordTableData):
                
                #print "[DEBUG-SCTIC] Row: %s" % (row)
                
                apply = row[0]
                if(apply == self.staticPoseWeightsPanel.ENABLED_YES):
                    apply = "true"
                else:
                    apply = "false"
                
                name = row[1]
                
                valueType = row[2]
                if(valueType == self.staticPoseWeightsPanel.VALUE_DEFAULT):
                    valueType = "default_value"
                else:
                    valueType = "manual_value"
                
                if(valueType == "manual_value"):
                    #print "[DEBUG-SCTIC] Getting manual value from table"
                    value = row[2]
                else:
                    #print "[DEBUG-SCTIC] Setting default value"
                    value = "0"
                    
                weight = row[3]
                
                #print "[DEBUG-SCTIC] Name : %s" % (name)
                #print "[DEBUG-SCTIC] Apply : %s" % (apply)
                #print "[DEBUG-SCTIC] Weight : %s" % (weight)
                #print "[DEBUG-SCTIC] ValueType : %s" % (valueType)
                #print "[DEBUG-SCTIC] Value : %s" % (value)
                
                ikCoordinateTask = []
                ikCoordinateTask.append(name)
                ikCoordinateTask.append(apply)
                ikCoordinateTask.append(weight)
                ikCoordinateTask.append(valueType)
                ikCoordinateTask.append(value)
                
                self.myConfig.scale_markerPlacer_ikTaskSet_objects_ikCoordinateTask.append(ikCoordinateTask)
            #except:
            #    print "[DEBUG] Exception thrown in setCoodTableInConfig()"
            
    ###############################
    # Save contents of a text box #
    ###############################
    
    def setOutputModelScaledFileEvent(self, event):
        self.setOutputModelScaledFile()
        
    def setOutputModelFileEvent(self, event):
        self.setOutputModelFile()
        
    def setMassEvent(self, event):
        self.setMass()
        
    def setAddMarkersFileEvent(self, event):
        self.setAddMarkersFile()
    
    def setMarkerDataMeasurementsFileEvent(self, event):
        self.setMarkerDataMeasurementsFile()
    
    def setAvgMeasurementBetweenTimesFromEvent(self, event):
        self.setAvgMeasurementBetweenTimesFrom()
    
    def setAvgMeasurementBetweenTimesToEvent(self, event):
        self.setAvgMeasurementBetweenTimesTo()
    
    def setMarkerDataStaticPoseFileEvent(self, event):
        self.setMarkerDataStaticPoseFile()
    
    def setAvgMarkersBetweenTimesFromEvent(self, event):
        self.setAvgMarkersBetweenTimesFrom()
    
    def setAvgMarkersBetweenTimesToEvent(self, event):
        self.setAvgMarkersBetweenTimesTo()
    
    def setCoordDataForStaticPoseFileEvent(self, event):
        self.setCoordDataForStaticPoseFile()
    
    def setOutputModelNameEvent(self, event):
        self.setOutputModelName()
        
    
    ###########################################################
    # Show file chooser dialog when a "..." button is pressed #
    ###########################################################
    
    def selectOutputModelScaledFile(self, event):
        # Create and show a file chooser (True = select file, False = select directory)
        myChooser = bessFileChooser(True)
        # Set the text field text to the results of the file chooser
        self.settingsTab_outputGroup_outputModelScaledFileTextField.text = myChooser.fullPath.toString()
        # Save the value to the config class
        self.setOutputModelScaledFile()
        
    def selectOutputModelFile(self, event):
        # Create and show a file chooser (True = select file, False = select directory)
        myChooser = bessFileChooser(True)
        # Set the text field text to the results of the file chooser
        self.settingsTab_outputGroup_outputModelFileTextField.text = myChooser.fullPath.toString()
        # Save the value to the config class
        self.setOutputModelFile()
        
    def selectMarkerDataFile(self, event):
        # Create and show a file chooser (True = select file, False = select directory)
        myChooser = bessFileChooser(True)
        # Set the text field text to the results of the file chooser
        self.settingsTab_subjectDataGroup_addMarkersFileTextField.text = myChooser.fullPath.toString()
        # Save the value to the config class
        self.setAddMarkersFile()
        
    def selectMarkerDataMeasurementsFile(self, event):
        # Create and show a file chooser (True = select file, False = select directory)
        myChooser = bessFileChooser(True)
        # Set the text field text to the results of the file chooser
        self.settingsTab_scaleModelGroup_markerDataMeasurementsFileTextField.text = myChooser.fullPath.toString()
        # Save the value to the config class
        self.setMarkerDataMeasurementsFile()
        
    def selectMarkerDataStaticPoseFile(self, event):
        # Create and show a file chooser (True = select file, False = select directory)
        myChooser = bessFileChooser(True)
        # Set the text field text to the results of the file chooser
        self.settingsTab_adjustModelMarkersGroup_markerDataStaticPoseFileTextField.text = myChooser.fullPath.toString()
        # Save the value to the config class
        self.setMarkerDataStaticPoseFile()
        
    def selectCoordDataForStaticPoseFile(self, event):
        # Create and show a file chooser (True = select file, False = select directory)
        myChooser = bessFileChooser(True)
        # Set the text field text to the results of the file chooser
        self.settingsTab_adjustModelMarkersGroup_coordDataForStaticPoseFileTextField.text = myChooser.fullPath.toString()
        # Save the value to the config class
        self.setCoordDataForStaticPoseFile()
        
    ###############
    # Check boxes #
    ###############
        
    def onAddMarkersFileCheckBoxSelect(self, event):
        if (self.settingsTab_subjectDataGroup_addMarkersFileCheckBox.isSelected()):
            self.setAddMarkersFile()
        else:
            self.myConfig.scale_genericModelMaker_markerSetFile = ''

    def onPreserveMassDistCheckBoxSelect(self, event):
        if (self.settingsTab_scaleModelGroup_preserveMassDistCheckBox.isSelected()):
            self.myConfig.scale_modelScaler_preserveMassDistribution = 'true'
        else:
            self.myConfig.scale_modelScaler_preserveMassDistribution = 'false'

    def onMarkerDataMeasurementsFileCheckBoxSelect(self, event):
        if (self.settingsTab_scaleModelGroup_markerDataMeasurementsFileCheckBox.isSelected()):
            self.setMarkerDataMeasurementsFile()
        else:
            self.myConfig.scale_modelScaler_markerFile = ''

    def onScaleModelCheckBoxSelect(self, event):
        if (self.settingsTab_scaleModelGroup_scaleModelCheckBox.isSelected()):
            # Preserve mass distribution
            # marker data for measurements (file)
            #avg measurements between times
            self.myConfig.scale_modelScaler_apply = 'true'
        else:
            self.myConfig.scale_modelScaler_apply = 'false'

    def onCoordDataForStaticPoseFileCheckBoxSelect(self, event):
        if (self.settingsTab_adjustModelMarkersGroup_coordDataForStaticPoseFileCheckBox.isSelected()):
            self.setCoordDataForStaticPoseFile()
        else:
            self.myConfig.scale_markerPlacer_coordinateFile = ''

    def onAdjustModelMarkersCheckBoxSelect(self, event):
        if (self.settingsTab_adjustModelMarkersGroup_adjustModelMarkersCheckBox.isSelected()):
            # marker data for static pose
            # avg markers between times
            # coord data for static pose
            self.myConfig.scale_markerPlacer_apply = 'true'
        else:
            self.myConfig.scale_markerPlacer_apply = 'false'
    
    # Save/load from config
    # https://docs.python.org/2/library/xml.etree.elementtree.html
    def loadSettings(self, event):
        # Create and show a file chooser (True = select file, False = select directory)
        myChooser = bessFileChooser(True)
        self.loadSettingsFromFile(myChooser.fullPath.toString())
        # If we loaded a non-bess config file, set the text as the input config file
        #if(self.myConfig.scale_inputConfigFile == ''):
        #    self.settingsTab_inputScaleFileTextField.text = myChooser.fullPath.toString()
        
    def loadSettingsFromFile(self, filePath):
        self.lock = True
        
        # Check if "use it" is checked
        if(self.myConfig.useScale == 'true'):
            self.useThisToolGroup_useItCheckBox.setSelected(True)
        else:
            self.useThisToolGroup_useItCheckBox.setSelected(False)
            
        # Open the xml file
        if(os.path.exists(filePath)):
            print 'Opening config file: %s' % (filePath)
            tree = ET.parse(filePath)
            
            # Parse the tree
            root = tree.getroot()
            self.parseTree(root)
            
            # Save (some) values to BESS config
            self.setAddMarkersFile()
            
            # Populate the ui with data from the model
            model = modeling.Model(self.myConfig.common_modelFile)
            myState = model.initSystem()
            # Get/apply the markers
            # TODO: See if this value is filled in. If not, use common?
            #>>> markerSetFile="gait2354_MarkerSet.xml"           # Define the full path to the Marker Set file
            #>>> newMarkers = modeling.MarkerSet(markerSetFile) # Construct a MarkerSet Object
            newMarkers = modeling.MarkerSet(self.myConfig.scale_genericModelMaker_markerSetFile)
            #>>> myModel.replaceMarkerSet(myState,newMarkers)   # Replace the models existing MarkerSet with new MarkerSet
            # ABM: why do we need to replace the marker set?  This failed when I started with a 
            # previously scaled model rather than the original model.  Updating the marker set does not 
            # cause a crash, but not sure if there is any other issue by updating rather than replacing.
            model.updateMarkerSet(newMarkers)
            #>>> myModel.initSystem()                           # re-initialize the system
            #>>> myState =myModel.initSystem()                  # Define the new state              
            # print "[DEBUG-UIScale] before gui.loadModel(model) %s" % (gui.getInstallDir())
            # print "[DEBUG-UIScale] before gui.loadModel(model) %s" % (model.getName())
            # print "[DEBUG-UIScale] before gui.loadModel(model) %s" % (newMarkers.getName())
            
            model.initSystem()
            gui.loadModel(model)
            myModel = gui.getCurrentModel()
            self.bodySet = myModel.getBodySet()
            self.markerSet = myModel.getMarkerSet()
            self.coordSet = myModel.getCoordinateSet()
            
            self.bodies = []
            self.markers = []
            self.coords = []
            self.measurementSetTableData = []
            self.staticPoseWeightsMarkerTableData = []
            self.staticPoseWeightsCoordTableData = []
            self.scaleFactorsTableData = []
            
            print '[DEBUG] Looping through the bodyset: %d' % (self.bodySet.getSize())
            #self.bodies.append("None")
            for i in range( 0, self.bodySet.getSize()):
                self.bodies.append(self.bodySet.get(i))
                #print "~~~~~~~~~~~~ Body: %s" % bodySet.get(i)
            print '[DEBUG] Final bodies list: %s' % (self.bodies)
            
            print '[DEBUG] Looping through the markerset: %d' % (self.markerSet.getSize())
            #markerSet.getMarkerNames(self.tempMarkers)
            #self.markers.append("None")
            for i in range( 0, self.markerSet.getSize()):
                #currentMarker = markerSet.get(i).toString()
                #self.markers.append(currentMarker)
                #print "~~~~~~~~~~~~~ Marker: %s" % currentMarker
                self.markers.append(self.markerSet.get(i))
                #print "~~~~~~~~~~~~ Marker: %s" % markerSet.get(i)
            
            print '[DEBUG] Final markers list: %s' % (self.markers)
            
            for marker in self.markers:
                #print "Adding marker to UI: %s" % (marker)
                self.measurementSetsPanel.markerPairAComboBox.addItem(marker)
                self.measurementSetsPanel.markerPairBComboBox.addItem(marker)
            
            # Save values to BessConfig (note: dynamic ui items were saved during the call to parseTree())
            #self.setInputScaleFile()
            #self.setOutputModelScaledFile()
            #self.setOutputModelFile()
            self.setMass()
            self.setMarkerDataMeasurementsFile()
            self.setAvgMeasurementBetweenTimesFrom()
            self.setAvgMeasurementBetweenTimesTo()
            self.setMarkerDataStaticPoseFile()
            self.setAvgMarkersBetweenTimesFrom()
            self.setAvgMarkersBetweenTimesTo()
            self.setCoordDataForStaticPoseFile()
            #self.setOutputModelName()
            
            if(self.myConfig.scale_genericModelMaker_markerSetFile.strip() == ''):
                self.settingsTab_subjectDataGroup_addMarkersFileCheckBox.setSelected(False)
            else:
                self.settingsTab_subjectDataGroup_addMarkersFileCheckBox.setSelected(True)
            
            if(self.myConfig.scale_modelScaler_preserveMassDistribution.strip() == 'true'):
                self.settingsTab_scaleModelGroup_preserveMassDistCheckBox.setSelected(True)
            else:
                self.settingsTab_scaleModelGroup_preserveMassDistCheckBox.setSelected(False)
            
            if(self.myConfig.scale_modelScaler_markerFile.strip() == ''):
                self.settingsTab_scaleModelGroup_markerDataMeasurementsFileCheckBox.setSelected(False)
            else:
                self.settingsTab_scaleModelGroup_markerDataMeasurementsFileCheckBox.setSelected(True)
            
            if(self.myConfig.scale_modelScaler_apply.strip() == 'true'):
                self.settingsTab_scaleModelGroup_scaleModelCheckBox.setSelected(True)
            else:
                self.settingsTab_scaleModelGroup_scaleModelCheckBox.setSelected(False)
            
            if(self.myConfig.scale_markerPlacer_coordinateFile.strip() == ''):
                self.settingsTab_adjustModelMarkersGroup_coordDataForStaticPoseFileCheckBox.setSelected(False)
            else:
                self.settingsTab_adjustModelMarkersGroup_coordDataForStaticPoseFileCheckBox.setSelected(True)
            
            if(self.myConfig.scale_markerPlacer_apply.strip() == 'true'):
                self.settingsTab_adjustModelMarkersGroup_adjustModelMarkersCheckBox.setSelected(True)
            else:
                self.settingsTab_adjustModelMarkersGroup_adjustModelMarkersCheckBox.setSelected(False)
            
            print "[DEBUG-MS] Setting measurement sets"
            self.measurementSetTableDataRowIndex = 0
            
            bodyScaleNamePairs = []
            for measurementSet in self.myConfig.scale_modelScaler_measurementSet_objects_measurements:
                #print "[DEBUG-MS] In the for loop"
                
                #print "[DEBUG-MS] measurementSet: %s" % (measurementSet)
                measurementName = measurementSet[0]
                #print "[DEBUG-MS] measurementName: %s" % (measurementName)
                markerPairSet = measurementSet[2]
                #print "[DEBUG-MS] markerPairSet: %s" % (markerPairSet)
                markerPairSetName = markerPairSet[0]
                #print "[DEBUG-MS] markerPairSetName: %s" % (markerPairSetName)
                markerPairSetMarkerPairs = markerPairSet[1]
                #print "[DEBUG-MS] markerPairSetMarkerPairs: %s" % (markerPairSetMarkerPairs)
                
                ## TODO: This is wrong. FIXME
                #measurementName = markerPairSetMarkerPairs[0]
                #print "FIXME ------------------> measurementName: %s" % (measurementName)
                
                measurementSetRow = [measurementName]
                
                markers = markerPairSetMarkerPairs[1]
                #print "[DEBUG-MS] markers: %s" % (markers)
                #### TODO: LOOP HERE
                for markersAgain in markers:
                    #print "[DEBUG-MS] markersAgain: %s" % (markersAgain)
                    markersYetAgain = markersAgain[1]
                    #print "[DEBUG-MS] markersYetAgain: %s" % (markersYetAgain)
                    bothMarkersAsString = markersYetAgain
                    #print "[DEBUG-MS] bothMarkersAsString: %s" % (bothMarkersAsString)
                    
                    bothMarkers = bothMarkersAsString.strip()
                    #print "[DEBUG-MS] bothMarkers: %s" % (bothMarkers)
                    bothMarkersAsArray = bothMarkers.split()
                    #print "[DEBUG-MS] bothMarkersAsArray: %s" % (bothMarkersAsArray)
                    
                    for aMarker in bothMarkersAsArray:
                        measurementSetRow.append(aMarker)
                
                self.measurementSetTableData.append(measurementSetRow)
                self.measurementSetTableDataRowIndex = self.measurementSetTableDataRowIndex + 1
                
                # Get the body scale names
                bodyScaleSet = measurementSet[3]
                tempBodyScales = bodyScaleSet[1]
                bodyScales = tempBodyScales[0]
                #print "[DEBUG-SF] Preparing to add body scale info"
                for bodyScale in bodyScales:
                    bodyScaleName = bodyScale[0]
                    
                    #print "[DEBUG-SF] Adding measurement name and body scale: %s and %s" % (measurementName, bodyScaleName)
                    bodyScaleNamePair = [measurementName]
                    bodyScaleNamePair.append(bodyScaleName)
                    
                    bodyScaleNamePairs.append(bodyScaleNamePair)
                print "[DEBUG-SF] Done adding body scale info"
                
            print "[DEBUG-MS] Setting scale factor sets"
            for i, body in enumerate(self.bodies):
                #scaleFactorsTable.setValueAt(body, i, 0)
                bodyName = body.getName()
                measurementUsed = "Unassigned"
                appliedScaleFactor = "CALCULATED"
                
                # Fill in with config values here
                print "[DEBUG-SF] Entering into for loop" 
                for bodyScaleNamePair in bodyScaleNamePairs:
                    print "[DEBUG-SF] For loop" 
                    measurementName = bodyScaleNamePair[1]
                    print "[DEBUG-SF] Measurement name / body name: >>>%s<<<  >>>>%s<<<" % (measurementName, bodyName)
                    print (type(measurementName))
                    print (type(bodyName))
                    
                    if(measurementName == bodyName):
                        #print "[DEBUG-SF] Setting measurement used"
                        measurementUsed = bodyScaleNamePair[0]
                        print "[DEBUG-SF] Setting measurement used: %s" % (measurementUsed)
                    else:
                        #print "[DEBUG-SF] not equal"
                        pass
                # TODO: applied scale factor values (from config)
                print "[DEBUG-SF] End of for loop"
                
                #if(measurementUsed == "Unassigned"):
                print "[DEBUG-BCUIS-loadSettings]Measurement unassigned. Checking manual values"
                for scaleSet_scales in self.myConfig.scale_modelScaler_scaleSet_objects_scale:
                    print '[DEBUG] scaleSet_scales:'
                    print (scaleSet_scales)
                    
                    if(scaleSet_scales):
                        scales = scaleSet_scales[0]
                        print '[DEBUG] scales:'
                        print (scales)
                        
                        for scale in scaleSet_scales:
                            #########
                            print '[DEBUG] scale:'
                            print (scale)
                            scale_name = scale[0]
                            scale_scales = scale[1]
                            scale_segment = scale[2]
                            scale_apply = scale[3]
                            
                            print "[DEBUG-SF] scale segment / body name: >>>%s<<<  >>>>%s<<<" % (scale_segment, bodyName)
                            print( type(scale_segment) )
                            print( type(bodyName) )
                            
                            if(scale_segment == bodyName):
                                print 'ARE EQUAL'
                                measurementUsed = "MANUAL SCALES"
                                appliedScaleFactor = scale_scales
                                self.scaleFactorsPanel.useManual[i] = True;
                            else:
                                print 'ARE NOT EQUAL'
                row = [bodyName]
                row.append(measurementUsed)
                row.append(appliedScaleFactor)
                self.scaleFactorsTableData.append(row)
            print '[DEBUG-TABLE] scaleFactorsTableData:'
            print (self.scaleFactorsTableData)
            
            for i in range( 0, self.markerSet.getSize()):
                apply = self.staticPoseWeightsPanel.ENABLED_NO
                marker = self.markerSet.get(i).getName()
                value = self.staticPoseWeightsPanel.VALUE_DEFAULT
                weight = "1.0"
                
                # Get actual values from config
                for ikMarkerTask in self.myConfig.scale_markerPlacer_ikTaskSet_objects_ikMarkerTask:
                    #print "[DEBUG-MARKER] Marker Task: %s" % (ikMarkerTask)
                    #for ikMarkerTask in ikMarkerTasks:
                    #[name, apply, weight, valueType, value]
                    name      = ikMarkerTask[0]
                    #print "[DEBUG-Table] name vs marker: %s :: %s" % (name, marker)
                    #print (type(name))
                    #print (type(marker))
                    if(name == marker):
                        apply     = ikMarkerTask[1]
                        #print "[DEBUG-Table] pre-apply: %s" % (apply)
                        if(apply == "true"):
                            apply = self.staticPoseWeightsPanel.ENABLED_YES
                        else:
                            apply = self.staticPoseWeightsPanel.ENABLED_NO
                        weight    = ikMarkerTask[2]
                        #valueType = ikMarkerTask[3]
                        # TODO: If value type is AAAAA then value = BBBBB
                        #value     = ikMarkerTask[4]
                
                if(self.settingsTab_adjustModelMarkersGroup_markerDataStaticPoseFileTextField.text != ""):
                    value = self.staticPoseWeightsPanel.VALUE_FROM_FILE
                #print "[DEBUG-TABLE] Apply: %s"  % (apply)
                #print "[DEBUG-TABLE] Marker: %s" % (marker)
                #print "[DEBUG-TABLE] Value: %s"  % (value)
                #print "[DEBUG-TABLE] Weight: %s" % (weight)
                row = [apply]
                row.append(marker)
                row.append(value)
                row.append(weight)
                
                # NOTE: Not all markers within the config file appear in the UI because we pull
                #       markers from the model. If the model doesn't have that marker, we ignore it
                
                self.staticPoseWeightsMarkerTableData.append(row)
            
            # Setup table with default values and values from model
            for i in range( 0, self.coordSet.getSize()):
                apply = self.staticPoseWeightsPanel.ENABLED_NO
                coord = self.coordSet.get(i).getName()
                value = self.staticPoseWeightsPanel.VALUE_DEFAULT
                weight = "1.0"
                
                # Get actual values from config
                for ikCoordinateTask in self.myConfig.scale_markerPlacer_ikTaskSet_objects_ikCoordinateTask:
                    #for ikCoordinateTask in ikCoordinateTasks:
                    #print "[DEBUG-TABLE] ikCoordinateTask: %s" % (ikCoordinateTask)
                    #[name, apply, weight, valueType, value]
                    name      = ikCoordinateTask[0]
                    
                    #print (type(name))
                    #print (type(coord))
                    
                    if(name == coord):
                        apply     = ikCoordinateTask[1]
                        if(apply == "true"):
                            apply = self.staticPoseWeightsPanel.ENABLED_YES
                        else:
                            apply = self.staticPoseWeightsPanel.ENABLED_NO
                        weight    = ikCoordinateTask[2]
                        valueType = ikCoordinateTask[3]
                        
                        # [manual_value, default_value]
                        if(valueType == "manual_value"):
                            value = ikCoordinateTask[4]
                        elif(valueType == "default_value"):
                            value = self.staticPoseWeightsPanel.VALUE_DEFAULT
                
                #print "[DEBUG-TABLE] Apply: %s"  % (apply)
                #print "[DEBUG-TABLE] Marker: %s" % (coord)
                #print "[DEBUG-TABLE] Value: %s"  % (value)
                #print "[DEBUG-TABLE] Weight: %s" % (weight)
                
                row = [apply]
                row.append(coord)
                row.append(value)
                row.append(weight)
                
                self.staticPoseWeightsCoordTableData.append(row)
                
            # Refresh tables with their updated data
            self.refreshMeasurementSetTable()
            self.refreshScaleFactorsTable()
            self.refreshStaticPoseMarkerTable()
            self.refreshStaticPoseCoordTable()
            
            print 'Done loading settings'
        else:
            print 'Could not find file: %s' % (filePath)
        
        self.lock = False
        #self.setScaleFactorsTableInConfig()
    
    def parseTree(self, node):
        print 'Node tag: %s,  text: %s' % (node.tag, node.text)
        
        if('mass' == node.tag):
            self.settingsTab_subjectDataGroup_massTextField.text = node.text
        elif('height' == node.tag):
            self.myConfig.scale_height = node.text
        elif('age' == node.tag):
            self.myConfig.scale_age = node.text
        elif('notes' == node.tag):
            self.myConfig.scale_notes = node.text
        elif('GenericModelMaker' == node.tag):
            name = node.get('name')
            modelFile = ''
            markerSetFile = ''
            
            for child in node:
                if('model_file' == child.tag):
                    modelFile = child.text
                elif('marker_set_file' == child.tag):
                    markerSetFile = child.text
            
            if(None == name):
                name = " "
            self.myConfig.scale_genericModelMaker_name = name
            self.myConfig.scale_genericModelMaker_modelFile = modelFile # Not used, but saved anyway
            self.settingsTab_subjectDataGroup_addMarkersFileTextField.text = markerSetFile
        elif('ModelScaler' == node.tag):
            name = node.get('name')
            apply = ''
            scalingOrder = ''
            markerFile = ''
            timeRange = ''
            preserveMassDistribution = ''
            outputModelFile = ''
            outputScaleFile = ''
            
            for child in node:
                if('apply' == child.tag):
                    apply = child.text
                elif('scaling_order' == child.tag):
                    scalingOrder = child.text
                elif('MeasurementSet' == child.tag):
                    self.myConfig.scale_modelScaler_measurementSet_name = child.get('name')
                    if( None == self.myConfig.scale_modelScaler_measurementSet_name ):
                        self.myConfig.scale_modelScaler_measurementSet_name = " "
                    
                    for measurementSetChild in child:
                        if('objects' == measurementSetChild.tag):
                            measurements = []
                            for objectsChild in measurementSetChild:
                                if('Measurement' == objectsChild.tag):
                                    measurement = []
                                    measurement_name = objectsChild.get('name')
                                    measurement_apply = ''
                                    measurement_markerPairSet = []
                                    measurement_bodyScaleSet = []
                                    
                                    for measurementChild in objectsChild:
                                        if('apply' == measurementChild.tag):
                                            measurement_apply = measurementChild.text
                                        elif('MarkerPairSet' == measurementChild.tag):
                                            markerPairSet_name = measurementChild.get('name')
                                            if( None == markerPairSet_name ):
                                                markerPairSet_name = " "
                                            markerPairSet_markerPairs = []
                                            
                                            for markerPairSetObjects in measurementChild:
                                                if('objects' == markerPairSetObjects.tag):
                                                    markerPairs = []
                                                    for markerPairSetObjectsChild in markerPairSetObjects:
                                                        # A MarkerPair contains a name and markers (2 objects total)
                                                        if('MarkerPair' == markerPairSetObjectsChild.tag):
                                                            markerPair = []
                                                            markerPair_name = markerPairSetObjectsChild.get('name')
                                                            if(None == markerPair_name):
                                                                markerPair_name = " "
                                                            markerPair.append(markerPair_name)
                                                            for markerPairChild in markerPairSetObjectsChild:
                                                                if('markers' == markerPairChild.tag):
                                                                    markerPair.append(markerPairChild.text)
                                                            markerPairs.append(markerPair)
                                                markerPairSet_markerPairs.append(markerPairs)
                                            measurement_markerPairSet.append(markerPairSet_name)
                                            measurement_markerPairSet.append(markerPairSet_markerPairs)
                                        elif('BodyScaleSet' == measurementChild.tag):
                                            bodyScaleSet_name = measurementChild.get('name')
                                            if(None == bodyScaleSet_name):
                                                bodyScaleSet_name = " "
                                            bodyScaleSet_bodyScales = []
                                            
                                            for bodyScaleSetObjects in measurementChild:
                                                if('objects' == bodyScaleSetObjects.tag):
                                                    bodyScales = []
                                                    for bodyScaleSetObjectsChild in bodyScaleSetObjects:
                                                        if('BodyScale' == bodyScaleSetObjectsChild.tag):
                                                            bodyScale = []
                                                            bodyScale_name = bodyScaleSetObjectsChild.get('name')
                                                            if(None == bodyScale_name):
                                                                bodyScale_name = " "
                                                            bodyScale.append(bodyScale_name)
                                                            for bodyScaleChild in bodyScaleSetObjectsChild:
                                                                if('axes' == bodyScaleChild.tag):
                                                                    bodyScale.append(bodyScaleChild.text)
                                                            bodyScales.append(bodyScale)
                                                bodyScaleSet_bodyScales.append(bodyScales)
                                            measurement_bodyScaleSet.append(bodyScaleSet_name)
                                            measurement_bodyScaleSet.append(bodyScaleSet_bodyScales)
                                            
                                    #if( None == measurement_name ):
                                    #    measurement_name = " "
                                    measurement.append(measurement_name)
                                    measurement.append(measurement_apply)
                                    measurement.append(measurement_markerPairSet)
                                    measurement.append(measurement_bodyScaleSet)
                                    
                                    measurements.append(measurement)
                            
                            self.myConfig.scale_modelScaler_measurementSet_objects_measurements = measurements
                elif 'ScaleSet' == child.tag:
                    scaleSet_name = child.get('name')
                    scaleSet_scales = []
                    
                    for scaleSetChild in child:
                        if 'objects' == scaleSetChild.tag:
                            scales = []
                            for objectsChild in scaleSetChild:
                                if 'Scale' == objectsChild.tag:
                                    scale = []
                                    scale_name = objectsChild.get('name')
                                    scale_scales = ''
                                    scale_segment = ''
                                    scale_apply = ''
                                    
                                    for scaleChild in objectsChild:
                                        if 'scales' == scaleChild.tag:
                                            scale_scales = scaleChild.text
                                        elif 'segment' == scaleChild.tag:
                                            scale_segment = scaleChild.text
                                        elif 'apply' == scaleChild.tag:
                                            scale_apply = scaleChild.text
                                    
                                    scale.append(scale_name)
                                    scale.append(scale_scales)
                                    scale.append(scale_segment)
                                    scale.append(scale_apply)
                                    
                                    scales.append(scale)
                            scaleSet_scales.append(scales)
                    
                    if(None == scaleSet_name):
                        scaleSet_name = " "
                    self.myConfig.scale_modelScaler_scaleSet_name = scaleSet_name
                    self.myConfig.scale_modelScaler_scaleSet_objects_scale = scaleSet_scales
                    
                elif 'marker_file' == child.tag:
                    markerFile = child.text
                elif 'time_range' == child.tag:
                    timeRange = child.text
                elif 'preserve_mass_distribution' == child.tag:
                    preserveMassDistribution = child.text
                elif 'output_model_file' == child.tag:
                    outputModelFile = child.text
                elif 'output_scale_file' == child.tag:
                    outputScaleFile = child.text
                    if(outputScaleFile == None):
                        outputScaleFile = ''
            if( None == name ):
                name = " "
            self.myConfig.scale_modelScaler_name = name
            self.myConfig.scale_modelScaler_apply = apply
            self.myConfig.scale_modelScaler_scalingOrder = scalingOrder
            self.settingsTab_scaleModelGroup_markerDataMeasurementsFileTextField.text = markerFile
            
            splitTimes = timeRange.split(" ")
            timeFrom = float('inf')
            timeTo = float('inf')
            for time in splitTimes:
                try:
                    foundTime = float(time)
                    
                    if( timeFrom < float('inf') ):
                        timeTo = foundTime
                    else:
                        timeFrom = foundTime
                except ValueError:
                    pass
            
            self.settingsTab_scaleModelGroup_avgMeasurementBetweenTimesFromTextField.text = str(timeFrom)
            self.settingsTab_scaleModelGroup_avgMeasurementBetweenTimesToTextField.text = str(timeTo)
            self.myConfig.scale_modelScaler_preserveMassDistribution = preserveMassDistribution
            self.settingsTab_outputGroup_outputModelScaledFileTextField.text = outputModelFile
            self.myConfig.scale_modelScaler_outputModelFile = outputModelFile
            self.myConfig.scale_modelScaler_outputScaleFile = outputScaleFile
        elif('MarkerPlacer' == node.tag):
            name = node.get('name')
            apply = ''
            ikTaskSet_name = ''
            ikMarkerTasks = []
            ikCoordinateTasks = []
            markerFile = ''
            coordinateFile = ''
            timeRange = ''
            outputMotionFile = ''
            outputModelFile = ''
            outputMarkerFile = ''
            maxMarkerMovement = ''
            
            for markerPlacerChild in node:
                if 'apply' == markerPlacerChild.tag:
                    apply = markerPlacerChild.text
                elif 'IKTaskSet' == markerPlacerChild.tag:
                    ikTaskSet_name = markerPlacerChild.get('name')
                    if(None == ikTaskSet_name):
                        ikTaskSet_name = " "
                    for ikTaskSetChild in markerPlacerChild:
                        if 'objects' == ikTaskSetChild.tag:
                            for ikTaskSetObjectsChild in ikTaskSetChild:
                                if 'IKMarkerTask' == ikTaskSetObjectsChild.tag:
                                    ikMarkerTask = []
                                    ikMarkerTask_name = ikTaskSetObjectsChild.get('name').strip()
                                    ikMarkerTask_apply = ''
                                    ikMarkerTask_weight = ''
                                    
                                    print "[DEBUG-SU] Looking at IKMarkerTask tag"
                                    
                                    for ikMarkerTaskChild in ikTaskSetObjectsChild:
                                        if 'apply' == ikMarkerTaskChild.tag:
                                            ikMarkerTask_apply = ikMarkerTaskChild.text.strip()
                                        elif 'weight' == ikMarkerTaskChild.tag:
                                            ikMarkerTask_weight = ikMarkerTaskChild.text.strip()
                                            
                                    print "  Name: %s"   % (ikMarkerTask_name)
                                    print "  Apply: %s"  % (ikMarkerTask_apply)
                                    print "  Weight: %s" % (ikMarkerTask_weight)
                                    ikMarkerTask.append(ikMarkerTask_name)
                                    ikMarkerTask.append(ikMarkerTask_apply)
                                    ikMarkerTask.append(ikMarkerTask_weight)
                                    
                                    ikMarkerTasks.append(ikMarkerTask)
                                if 'IKCoordinateTask' == ikTaskSetObjectsChild.tag:
                                    ikCoordinateTask = []
                                    ikCoordinateTask_name = ikTaskSetObjectsChild.get('name').strip()
                                    ikCoordinateTask_apply = ''
                                    ikCoordinateTask_weight = ''
                                    ikCoordinateTask_valueType = ''
                                    ikCoordinateTask_value = ''
                                    
                                    for ikCoordinateTaskChild in ikTaskSetObjectsChild:
                                        if 'apply' == ikCoordinateTaskChild.tag:
                                            ikCoordinateTask_apply = ikCoordinateTaskChild.text.strip()
                                        elif 'weight' == ikCoordinateTaskChild.tag:
                                            ikCoordinateTask_weight = ikCoordinateTaskChild.text.strip()
                                        elif 'value_type' == ikCoordinateTaskChild.tag:
                                            ikCoordinateTask_valueType = ikCoordinateTaskChild.text.strip()
                                        elif 'value' == ikCoordinateTaskChild.tag:
                                            ikCoordinateTask_value = ikCoordinateTaskChild.text.strip()
                                    
                                    ikCoordinateTask.append(ikCoordinateTask_name)
                                    ikCoordinateTask.append(ikCoordinateTask_apply)
                                    ikCoordinateTask.append(ikCoordinateTask_weight)
                                    ikCoordinateTask.append(ikCoordinateTask_valueType)
                                    ikCoordinateTask.append(ikCoordinateTask_value)
                                    
                                    ikCoordinateTasks.append(ikCoordinateTask)
                elif 'marker_file' == markerPlacerChild.tag:
                    markerFile = markerPlacerChild.text
                elif 'coordinate_file' == markerPlacerChild.tag:
                    coordinateFile = markerPlacerChild.text
                elif 'time_range' == markerPlacerChild.tag:
                    timeRange = markerPlacerChild.text
                elif 'output_motion_file' == markerPlacerChild.tag:
                    outputMotionFile = markerPlacerChild.text
                    if(outputMotionFile == None):
                        outputMotionFile = ''
                elif 'output_model_file' == markerPlacerChild.tag:
                    outputModelFile = markerPlacerChild.text
                    if( outputModelFile == None):
                        outputModelFile = ''
                elif 'output_marker_file' == markerPlacerChild.tag:
                    outputMarkerFile = markerPlacerChild.text
                    if(outputMarkerFile == None):
                        outputMarkerFile = ''
                elif 'max_marker_movement' == markerPlacerChild.tag:
                    maxMarkerMovement = markerPlacerChild.text
                    
            if( None == name ):
                name = " "
            self.myConfig.scale_markerPlacer_name = name
            self.myConfig.scale_markerPlacer_apply = apply
            self.myConfig.scale_markerPlacer_ikTaskSet_name = ikTaskSet_name
            self.myConfig.scale_markerPlacer_ikTaskSet_objects_ikMarkerTask = ikMarkerTasks
            self.myConfig.scale_markerPlacer_ikTaskSet_objects_ikCoordinateTask = ikCoordinateTasks
            self.settingsTab_adjustModelMarkersGroup_markerDataStaticPoseFileTextField.text = markerFile
            self.settingsTab_adjustModelMarkersGroup_coordDataForStaticPoseFileTextField.text = coordinateFile
            
            splitTimes = timeRange.split(" ")
            timeFrom = float('inf')
            timeTo = float('inf')
            for time in splitTimes:
                try:
                    foundTime = float(time)
                    
                    if( timeFrom < float('inf') ):
                        timeTo = foundTime
                    else:
                        timeFrom = foundTime
                except ValueError:
                    pass
            
            self.settingsTab_adjustModelMarkersGroup_avgMarkersBetweenTimesFromTextField.text = str(timeFrom)
            self.settingsTab_adjustModelMarkersGroup_avgMarkersBetweenTimesToTextField.text = str(timeTo)
            self.myConfig.scale_markerPlacer_outputMotionFile = outputMotionFile
            self.settingsTab_outputGroup_outputModelFileTextField.text = outputModelFile
            self.myConfig.scale_markerPlacer_outputModelFile = outputModelFile
            self.myConfig.scale_markerPlacer_outputMarkerFile = outputMarkerFile
            self.myConfig.scale_markerPlacer_maxMarkerMovement = maxMarkerMovement
            
        ## TODO: Dig deeper
        ##if('MarkerPlacer' == node.tag):
        #if('output_model_file' == node.tag):
        #    self.settingsTab_outputGroup_outputModelFileTextField.text = node.text
        ##elif('use_scale' == node.tag):
        ##    self.myConfig.useScale = node.text
        
        # Look at any children of children
        for child in node:
            self.parseTree(child)
            
# end bessConfigUIScaleCard class

