

################################################################################
#
# This file creates the Configuration UI components and handles user input as
# it relates to BESS configuration.
#
################################################################################
from javax.swing import JFrame, JPanel, JButton, JFileChooser, JTextField, JLabel, JCheckBox, SwingConstants, JTabbedPane, JScrollPane, JComboBox, JTable
from javax.swing.border import TitledBorder
from java.awt import GridLayout, FlowLayout, Color, GridBagLayout, GridBagConstraints, CardLayout, BorderLayout

from xml.dom.minidom import parse, parseString, getDOMImplementation, Document
import xml.etree.cElementTree as ET

import os.path

from core.BessConfigUICardBase import bessCardBase
from core.BessUIFileChooser import bessFileChooser

'''
The bessConfigUIMultiRunCard is used as the Multi-run simulation Card UI component
It is used to allow the user to specify multiple runs on the simulation and
variances between the runs
'''
class bessConfigUIRRAToolCard(bessCardBase):
    def __init__(self, myConfig):
        self.myConfig = myConfig
        
        # Create the UI components
        self.rraToolCard = JPanel(BorderLayout())
        
        self.rraToolPanel = JPanel(GridBagLayout())
        self.rraToolPanel_GBLC = GridBagConstraints()
        self.rraToolPanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        # Use This Tool settings
        useThisToolGroup = JPanel(GridBagLayout())
        useThisToolGroup_GBLC = GridBagConstraints()
        useThisToolGroup_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        self.useThisToolGroup_useItCheckBox = JCheckBox("Perform Residuals Reduction Algorithm", True, actionPerformed=self.onUseItCheckBoxSelect)
        self.useThisToolGroup_loadSettingsButton = JButton("Load settings", actionPerformed = self.loadSettings)
        
        useThisToolGroup_GBLC.weightx = 0.25
        useThisToolGroup_GBLC.gridx = 0
        useThisToolGroup_GBLC.gridy = 0
        useThisToolGroup.add(self.useThisToolGroup_useItCheckBox, useThisToolGroup_GBLC)
        useThisToolGroup_GBLC.weightx = 0.60
        useThisToolGroup_GBLC.gridx = 1
        useThisToolGroup_GBLC.gridy = 0
        useThisToolGroup.add(JLabel(" "), useThisToolGroup_GBLC)
        useThisToolGroup_GBLC.weightx = 0.15
        useThisToolGroup_GBLC.gridx = 2
        useThisToolGroup_GBLC.gridy = 0
        useThisToolGroup.add(self.useThisToolGroup_loadSettingsButton, useThisToolGroup_GBLC)
        
        # Reduce Residuals group
        reduceResidualsGroup = JPanel(GridBagLayout())
        reduceResidualsGroup_GBLC = GridBagConstraints()
        reduceResidualsGroup_GBLC.fill = GridBagConstraints.HORIZONTAL
        reduceResidualsGroup.setBorder(TitledBorder("Reduce Residuals"))
        
        ## Adjust model (check box, text field, file chooser)
        self.reduceResidualsGroup_adjustModelCheckBox = JCheckBox("Adjust model", actionPerformed=self.onAdjustModelCheckBoxSelect)
        self.reduceResidualsGroup_adjustModelFileTextField = JTextField("", focusLost=self.setAdjustModelFileEvent)
        self.reduceResidualsGroup_adjustModelFileFileChooserButton = JButton("...", actionPerformed = self.selectAdjustModelFile)
        
        reduceResidualsGroup_adjustModelFilePanel = JPanel(GridBagLayout())
        reduceResidualsGroup_adjustModelFilePanel_GBLC = GridBagConstraints()
        reduceResidualsGroup_adjustModelFilePanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        reduceResidualsGroup_adjustModelFilePanel_GBLC.weightx = 0.99
        reduceResidualsGroup_adjustModelFilePanel_GBLC.gridx = 0
        reduceResidualsGroup_adjustModelFilePanel_GBLC.gridy = 0
        reduceResidualsGroup_adjustModelFilePanel.add(self.reduceResidualsGroup_adjustModelFileTextField, reduceResidualsGroup_adjustModelFilePanel_GBLC)
        reduceResidualsGroup_adjustModelFilePanel_GBLC.weightx = 0.01
        reduceResidualsGroup_adjustModelFilePanel_GBLC.gridx = 1
        reduceResidualsGroup_adjustModelFilePanel_GBLC.gridy = 0
        reduceResidualsGroup_adjustModelFilePanel.add(self.reduceResidualsGroup_adjustModelFileFileChooserButton, reduceResidualsGroup_adjustModelFilePanel_GBLC)
        
        ## Body COM to adjust (combo box including all bodies from model file)
        reduceResidualsGroup_bodyCOMLabel = JLabel("Body COM to adjust ")
        reduceResidualsGroup_bodyCOMComboBoxOptions = ["Bodies from model file"]
        reduceResidualsGroup_bodyCOMComboBox = JComboBox(reduceResidualsGroup_bodyCOMComboBoxOptions)
        
        # Finish Reduce Residuals group
        reduceResidualsGroup_GBLC.weightx = 0.1
        reduceResidualsGroup_GBLC.gridx = 0
        reduceResidualsGroup_GBLC.gridy = 0
        reduceResidualsGroup.add(JLabel(" "), reduceResidualsGroup_GBLC)
        reduceResidualsGroup_GBLC.weightx = 0.40
        reduceResidualsGroup_GBLC.gridx = 1
        reduceResidualsGroup_GBLC.gridy = 0
        reduceResidualsGroup.add(self.reduceResidualsGroup_adjustModelCheckBox, reduceResidualsGroup_GBLC)
        reduceResidualsGroup_GBLC.weightx = 0.40
        reduceResidualsGroup_GBLC.gridx = 2
        reduceResidualsGroup_GBLC.gridy = 0
        reduceResidualsGroup.add(reduceResidualsGroup_adjustModelFilePanel, reduceResidualsGroup_GBLC)
        reduceResidualsGroup_GBLC.weightx = 0.1
        reduceResidualsGroup_GBLC.gridx = 3
        reduceResidualsGroup_GBLC.gridy = 0
        reduceResidualsGroup.add(JLabel(" "), reduceResidualsGroup_GBLC)
        
        reduceResidualsGroup_GBLC.weightx = 0.1
        reduceResidualsGroup_GBLC.gridx = 0
        reduceResidualsGroup_GBLC.gridy = 1
        reduceResidualsGroup.add(JLabel(" "), reduceResidualsGroup_GBLC)
        reduceResidualsGroup_GBLC.weightx = 0.40
        reduceResidualsGroup_GBLC.gridx = 1
        reduceResidualsGroup_GBLC.gridy = 1
        reduceResidualsGroup.add(reduceResidualsGroup_bodyCOMLabel, reduceResidualsGroup_GBLC)
        reduceResidualsGroup_GBLC.weightx = 0.40
        reduceResidualsGroup_GBLC.gridx = 2
        reduceResidualsGroup_GBLC.gridy = 1
        reduceResidualsGroup.add(reduceResidualsGroup_bodyCOMComboBox, reduceResidualsGroup_GBLC)
        reduceResidualsGroup_GBLC.weightx = 0.1
        reduceResidualsGroup_GBLC.gridx = 3
        reduceResidualsGroup_GBLC.gridy = 1
        reduceResidualsGroup.add(JLabel(" "), reduceResidualsGroup_GBLC)
        
        # Output group
        outputGroup = JPanel(GridBagLayout())
        outputGroup_GBLC = GridBagConstraints()
        outputGroup_GBLC.fill = GridBagConstraints.HORIZONTAL
        outputGroup.setBorder(TitledBorder("Output"))
        
        ## Prefix (text field)
        outputGroup_prefixLabel = JLabel("Prefix")
        self.outputGroup_prefixTextField = JTextField("", focusLost=self.setPrefixEvent)
        
        ## Directory (text field, folder chooser)
        outputGroup_directoryLabel = JLabel("Directory")
        self.outputGroup_directoryTextField = JTextField("", focusLost=self.setDirectoryEvent)
        self.outputGroup_directoryFileChooserButton = JButton("...", actionPerformed = self.selectDirectoryFile)
        
        outputGroup_directoryPanel = JPanel(GridBagLayout())
        outputGroup_directoryPanel_GBLC = GridBagConstraints()
        outputGroup_directoryPanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        outputGroup_directoryPanel_GBLC.weightx = 0.99
        outputGroup_directoryPanel_GBLC.gridx = 0
        outputGroup_directoryPanel_GBLC.gridy = 0
        outputGroup_directoryPanel.add(self.outputGroup_directoryTextField, outputGroup_directoryPanel_GBLC)
        outputGroup_directoryPanel_GBLC.weightx = 0.01
        outputGroup_directoryPanel_GBLC.gridx = 1
        outputGroup_directoryPanel_GBLC.gridy = 0
        outputGroup_directoryPanel.add(self.outputGroup_directoryFileChooserButton, outputGroup_directoryPanel_GBLC)
        
        ## Precision (text field)
        outputGroup_precisionLabel = JLabel("Precision ")
        self.outputGroup_precisionTextField = JTextField("", focusLost=self.setPrecisionEvent)
        
        # Finish Output group
        outputGroup_GBLC.weightx = 0.1
        outputGroup_GBLC.gridx = 0
        outputGroup_GBLC.gridy = 0
        outputGroup.add(JLabel(" "), outputGroup_GBLC)
        outputGroup_GBLC.weightx = 0.40
        outputGroup_GBLC.gridx = 1
        outputGroup_GBLC.gridy = 0
        outputGroup.add(outputGroup_prefixLabel, outputGroup_GBLC)
        outputGroup_GBLC.weightx = 0.40
        outputGroup_GBLC.gridx = 2
        outputGroup_GBLC.gridy = 0
        outputGroup.add(self.outputGroup_prefixTextField, outputGroup_GBLC)
        outputGroup_GBLC.weightx = 0.1
        outputGroup_GBLC.gridx = 3
        outputGroup_GBLC.gridy = 0
        outputGroup.add(JLabel(" "), outputGroup_GBLC)
        
        outputGroup_GBLC.weightx = 0.1
        outputGroup_GBLC.gridx = 0
        outputGroup_GBLC.gridy = 1
        outputGroup.add(JLabel(" "), outputGroup_GBLC)
        outputGroup_GBLC.weightx = 0.40
        outputGroup_GBLC.gridx = 1
        outputGroup_GBLC.gridy = 1
        outputGroup.add(outputGroup_directoryLabel, outputGroup_GBLC)
        outputGroup_GBLC.weightx = 0.40
        outputGroup_GBLC.gridx = 2
        outputGroup_GBLC.gridy = 1
        outputGroup.add(outputGroup_directoryPanel, outputGroup_GBLC)
        outputGroup_GBLC.weightx = 0.1
        outputGroup_GBLC.gridx = 3
        outputGroup_GBLC.gridy = 1
        outputGroup.add(JLabel(" "), outputGroup_GBLC)
        
        outputGroup_GBLC.weightx = 0.1
        outputGroup_GBLC.gridx = 0
        outputGroup_GBLC.gridy = 2
        outputGroup.add(JLabel(" "), outputGroup_GBLC)
        outputGroup_GBLC.weightx = 0.40
        outputGroup_GBLC.gridx = 1
        outputGroup_GBLC.gridy = 2
        outputGroup.add(outputGroup_precisionLabel, outputGroup_GBLC)
        outputGroup_GBLC.weightx = 0.40
        outputGroup_GBLC.gridx = 2
        outputGroup_GBLC.gridy = 2
        outputGroup.add(self.outputGroup_precisionTextField, outputGroup_GBLC)
        outputGroup_GBLC.weightx = 0.1
        outputGroup_GBLC.gridx = 3
        outputGroup_GBLC.gridy = 2
        outputGroup.add(JLabel(" "), outputGroup_GBLC)
        
        # Finish card
        self.rraToolPanel_GBLC.weightx = 0.1
        self.rraToolPanel_GBLC.gridx = 0
        self.rraToolPanel_GBLC.gridy = 0
        self.rraToolPanel.add(JLabel(" "), self.rraToolPanel_GBLC)
        self.rraToolPanel_GBLC.weightx = 0.80
        self.rraToolPanel_GBLC.gridx = 1
        self.rraToolPanel_GBLC.gridy = 0
        self.rraToolPanel.add(reduceResidualsGroup, self.rraToolPanel_GBLC)
        self.rraToolPanel_GBLC.weightx = 0.1
        self.rraToolPanel_GBLC.gridx = 2
        self.rraToolPanel_GBLC.gridy = 0
        self.rraToolPanel.add(JLabel(" "), self.rraToolPanel_GBLC)
        
        self.rraToolPanel_GBLC.weightx = 0.1
        self.rraToolPanel_GBLC.gridx = 0
        self.rraToolPanel_GBLC.gridy = 1
        self.rraToolPanel.add(JLabel(" "), self.rraToolPanel_GBLC)
        self.rraToolPanel_GBLC.weightx = 0.80
        self.rraToolPanel_GBLC.gridx = 1
        self.rraToolPanel_GBLC.gridy = 1
        self.rraToolPanel.add(outputGroup, self.rraToolPanel_GBLC)
        self.rraToolPanel_GBLC.weightx = 0.1
        self.rraToolPanel_GBLC.gridx = 2
        self.rraToolPanel_GBLC.gridy = 1
        self.rraToolPanel.add(JLabel(" "), self.rraToolPanel_GBLC)
        
        self.rraToolCard.add(useThisToolGroup, BorderLayout.NORTH)
        self.rraToolCard.add(self.rraToolPanel, BorderLayout.CENTER)
        
    def getCard(self):
        return self.rraToolCard
    
    
    def onAdjustModelCheckBoxSelect(self, event):
        self.setAdjustModelFile()
    
    def onUseItCheckBoxSelect(self, event):
        if (self.useThisToolGroup_useItCheckBox.isSelected()):
            self.myConfig.useRraTool = 'true'
        else:
            self.myConfig.useRraTool = 'false'
    
    
    #################################################
    # Save the value in the UI to the config object #
    #################################################
    
    def setAdjustModelFile(self):
        if (self.reduceResidualsGroup_adjustModelCheckBox.isSelected()):
            self.myConfig.rraTool_outputModelFile = self.reduceResidualsGroup_adjustModelFileTextField.text
            #self.myConfig.rraTool_adjustComToReduceResiduals = 'true'
        else:
            self.myConfig.rraTool_outputModelFile = ''
            #self.myConfig.rraTool_adjustComToReduceResiduals = 'false'
        
    def setPrefix(self):
        self.myConfig.rraTool_prefix = self.outputGroup_prefixTextField.text
        
    def setDirectory(self):
        self.myConfig.rraTool_resultsDir = self.outputGroup_directoryTextField.text
        
    def setPrecision(self):
        self.myConfig.rraTool_outputPrecision = self.outputGroup_precisionTextField.text
    
    
    ###############################
    # Save contents of a text box #
    ###############################
    
    def setAdjustModelFileEvent(self, event):
        self.setAdjustModelFile()
        
    def setPrefixEvent(self, event):
        self.setPrefix()
        
    def setDirectoryEvent(self, event):
        self.setDirectory()
        
    def setPrecisionEvent(self, event):
        self.setPrecision()
        
    
    ###########################################################
    # Show file chooser dialog when a "..." button is pressed #
    ###########################################################
    
    def selectAdjustModelFile(self, event):
        # Create and show a file chooser (True = select file, False = select directory)
        myChooser = bessFileChooser(True)
        # Set the text field text to the results of the file chooser
        self.reduceResidualsGroup_adjustModelFileTextField.text = myChooser.fullPath.toString()
        # Save the value to the config class
        self.setAdjustModelFile()
        
    def selectDirectoryFile(self, event):
        # Create and show a file chooser (True = select file, False = select directory)
        myChooser = bessFileChooser(False)
        # Set the text field text to the results of the file chooser
        self.outputGroup_directoryTextField.text = myChooser.fullPath.toString()
        # Save the value to the config class
        self.setDirectory()
    
    
    # Save/load from config
    # https://docs.python.org/2/library/xml.etree.elementtree.html
    def loadSettings(self, event):
        # Create and show a file chooser (True = select file, False = select directory)
        myChooser = bessFileChooser(True)
        self.loadSettingsFromFile(myChooser.fullPath.toString())
        
    def loadSettingsFromFile(self, filePath):
        # Check if "use it" is checked
        if(self.myConfig.useRraTool == 'true'):
            self.useThisToolGroup_useItCheckBox.setSelected(True)
        else:
            self.useThisToolGroup_useItCheckBox.setSelected(False)
            
        # Open the xml file
        if(os.path.exists(filePath)):
            print 'Opening config file: %s' % (filePath)
            tree = ET.parse(filePath)
            
            # Parse the tree
            root = tree.getroot()
            self.parseTree(root)
            
            # Save values to BessConfig
            self.setAdjustModelFile()
            self.setPrefix()
            self.setDirectory()
            self.setPrecision()
            
            print 'Done loading settings'
        else:
            print 'Could not find file: %s' % (filePath)
        
    def parseTree(self, node):
        print 'Node tag: %s,  text: %s' % (node.tag, node.text)
        
        # TODO: Fix this so that when bodies are set in ui, the value switches to true
        self.myConfig.rraTool_adjustComToReduceResiduals = 'false'
        
        if('output_model_file' == node.tag):
            self.reduceResidualsGroup_adjustModelFileTextField.text = node.text
            if('' == self.reduceResidualsGroup_adjustModelFileTextField.text):
                self.reduceResidualsGroup_adjustModelCheckBox.setSelected(False)
            else:
                self.reduceResidualsGroup_adjustModelCheckBox.setSelected(True)
        #elif('' == node.tag):
        #    self.outputGroup_prefixTextField.text = node.text
        elif('results_directory' == node.tag):
            self.outputGroup_directoryTextField.text = node.text
        elif('adjusted_com_body' == node.tag):
            if('' == node.text):
                self.myConfig.rraTool_adjustComToReduceResiduals = 'false'
        #elif('adjust_com_to_reduce_residuals' == node.tag):
        #    if('true' == node.text):
        #        self.reduceResidualsGroup_adjustModelCheckBox.setSelected(True)
        #    else:
        #        self.reduceResidualsGroup_adjustModelCheckBox.setSelected(False)
        elif('output_precision' == node.tag):
            self.outputGroup_precisionTextField.text = node.text
        
        # Look at any children of children
        for child in node:
            self.parseTree(child)
            
# end bessConfigUIRRAToolCard class

