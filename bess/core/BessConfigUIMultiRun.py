

################################################################################
#
# This file creates the Configuration UI components and handles user input as
# it relates to BESS configuration.
#
################################################################################
from javax.swing import JFrame, JPanel, JButton, JFileChooser, JTextField, JLabel, JCheckBox, SwingConstants, JTabbedPane, JScrollPane, JComboBox
from javax.swing.border import TitledBorder
from java.awt import GridLayout, FlowLayout, Color, GridBagLayout, GridBagConstraints, CardLayout

from xml.dom.minidom import parse, parseString, getDOMImplementation, Document
import xml.etree.cElementTree as ET

import os.path

from core.BessConfigUICardBase import bessCardBase
from core.BessUIFileChooser import bessFileChooser

'''
The bessConfigUIMultiRunCard is used as the Multi-run simulation Card UI component
It is used to allow the user to specify multiple runs on the simulation and
variances between the runs
'''
class bessConfigUIMultiRunCard(bessCardBase):
    def __init__(self, myConfig):
        self.myConfig = myConfig
        
        # Create the UI components
        self.multiRunPanel = JPanel(GridBagLayout())
        self.multiRunPanel_GBLC = GridBagConstraints()
        self.multiRunPanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        self.multiRunEntryListIndex = 0
        
        # An array of JPanels (multi-run entry)
        self.multiRunEntryList = []
        
        self.numRunsTextFields = []
        self.variableToModifyComboBoxes = []
        self.lowRangeTextFields = []
        self.highRangeTextFields = []
        self.specificValsTextFields = []
        
        # Create multi-run entry (JPanel)
        self.addMultiRunEntry()
        
        # Make a scroll pane so user can see all available multi-run entries as more get added
        self.multiRunCard = JScrollPane(self.multiRunPanel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED)
        
    # Creates the JPanel containing all the multi-run entry ui elements
    def createMultiRunEntry(self):
        entryPanel = JPanel(GridBagLayout())
        entryPanel_GBLC = GridBagConstraints()
        entryPanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        ## Number of runs
        simulationPanel_NumRunsLabel = JLabel("Number of runs", SwingConstants.RIGHT)
        simulationPanel_NumRunsTextField = JTextField("", focusLost=self.checkForEmptyFieldsCallback)
        
        # Variable variance group
        variableVariancePanel = JPanel(GridBagLayout())
        variableVariancePanel_GBLC = GridBagConstraints()
        variableVariancePanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        variableVariancePanel.setBorder(TitledBorder("Variable Variance"))
        
        ## Variable to modify
        self.variableVariancePanel_variableToModifyComboBoxOptions = ["Choose a variable", "Scale - Mass"]
        variableVariancePanel_variableToModifyLabel = JLabel("Variable to modify ", SwingConstants.RIGHT)
        variableVariancePanel_variableToModifyComboBox = JComboBox(self.variableVariancePanel_variableToModifyComboBoxOptions, focusLost=self.checkForEmptyFieldsCallback)
        
        ## Low range
        variableVariancePanel_lowRangeLabel = JLabel("{ Low range ", SwingConstants.RIGHT)
        variableVariancePanel_lowRangeTextField = JTextField("", 5, focusLost=self.checkForEmptyFieldsCallback)
        
        ## High range
        variableVariancePanel_highRangeLabel = JLabel("High range ", SwingConstants.RIGHT)
        variableVariancePanel_highRangeTextField = JTextField("", 5, focusLost=self.checkForEmptyFieldsCallback)
        
        ## Specific values
        variableVariancePanel_specificValsLabel = JLabel("}   OR  enter Specific Values (CSV)", SwingConstants.RIGHT)
        variableVariancePanel_specificValsTextField = JTextField("", 5, focusLost=self.checkForEmptyFieldsCallback)
        
        # Finish creating variable variance group
        variableVariancePanel_GBLC.weightx = 0.1
        variableVariancePanel_GBLC.gridx = 0
        variableVariancePanel_GBLC.gridy = 0
        variableVariancePanel.add(variableVariancePanel_variableToModifyLabel, variableVariancePanel_GBLC)
        variableVariancePanel_GBLC.weightx = 0.15
        variableVariancePanel_GBLC.gridx = 1
        variableVariancePanel_GBLC.gridy = 0
        variableVariancePanel.add(variableVariancePanel_variableToModifyComboBox, variableVariancePanel_GBLC)
        variableVariancePanel_GBLC.weightx = 0.1
        variableVariancePanel_GBLC.gridx = 2
        variableVariancePanel_GBLC.gridy = 0
        variableVariancePanel.add(variableVariancePanel_lowRangeLabel, variableVariancePanel_GBLC)
        variableVariancePanel_GBLC.weightx = 0.15
        variableVariancePanel_GBLC.gridx = 3
        variableVariancePanel_GBLC.gridy = 0
        variableVariancePanel.add(variableVariancePanel_lowRangeTextField, variableVariancePanel_GBLC)
        variableVariancePanel_GBLC.weightx = 0.1
        variableVariancePanel_GBLC.gridx = 4
        variableVariancePanel_GBLC.gridy = 0
        variableVariancePanel.add(variableVariancePanel_highRangeLabel, variableVariancePanel_GBLC)
        variableVariancePanel_GBLC.weightx = 0.15
        variableVariancePanel_GBLC.gridx = 5
        variableVariancePanel_GBLC.gridy = 0
        variableVariancePanel.add(variableVariancePanel_highRangeTextField, variableVariancePanel_GBLC)
        variableVariancePanel_GBLC.weightx = 0.1
        variableVariancePanel_GBLC.gridx = 6
        variableVariancePanel_GBLC.gridy = 0
        variableVariancePanel.add(simulationPanel_NumRunsLabel, variableVariancePanel_GBLC)
        variableVariancePanel_GBLC.weightx = 0.15
        variableVariancePanel_GBLC.gridx = 7
        variableVariancePanel_GBLC.gridy = 0
        variableVariancePanel.add(simulationPanel_NumRunsTextField, variableVariancePanel_GBLC)
        variableVariancePanel_GBLC.weightx = 0.1
        variableVariancePanel_GBLC.gridx = 8
        variableVariancePanel_GBLC.gridy = 0
        variableVariancePanel.add(variableVariancePanel_specificValsLabel, variableVariancePanel_GBLC)
        variableVariancePanel_GBLC.weightx = 0.15
        variableVariancePanel_GBLC.gridx = 9
        variableVariancePanel_GBLC.gridy = 0
        variableVariancePanel.add(variableVariancePanel_specificValsTextField, variableVariancePanel_GBLC)
        
        # Add UI components to the arrays
        self.numRunsTextFields.append(simulationPanel_NumRunsTextField)
        self.variableToModifyComboBoxes.append(variableVariancePanel_variableToModifyComboBox)
        self.lowRangeTextFields.append(variableVariancePanel_lowRangeTextField)
        self.highRangeTextFields.append(variableVariancePanel_highRangeTextField)
        self.specificValsTextFields.append(variableVariancePanel_specificValsTextField)
        
        # Finish creating Entry Panel
        entryPanel_GBLC.weightx = 1.0
        entryPanel_GBLC.gridx = 0
        entryPanel_GBLC.gridy = 0
        entryPanel.add(variableVariancePanel, entryPanel_GBLC)
        
        return entryPanel
        
    # Calls createMultiRunEntry() and places the resulting JPanel into the UI
    def addMultiRunEntry(self):
        #print "-- Creating new multi-run entry"
        multiRunEntry = self.createMultiRunEntry()
        
        #print "-- 1"
        self.multiRunEntryList.append(multiRunEntry)
        
        #print "-- 2"
        self.multiRunPanel_GBLC.weightx = 0.1
        self.multiRunPanel_GBLC.gridx = 0
        self.multiRunPanel_GBLC.gridy = self.multiRunEntryListIndex
        self.multiRunPanel.add(JLabel(" "), self.multiRunPanel_GBLC)
        self.multiRunPanel_GBLC.weightx = 0.8
        self.multiRunPanel_GBLC.gridx = 1
        self.multiRunPanel_GBLC.gridy = self.multiRunEntryListIndex
        self.multiRunPanel.add(self.multiRunEntryList[self.multiRunEntryListIndex], self.multiRunPanel_GBLC)
        self.multiRunPanel_GBLC.weightx = 0.1
        self.multiRunPanel_GBLC.gridx = 2
        self.multiRunPanel_GBLC.gridy = self.multiRunEntryListIndex
        self.multiRunPanel.add(JLabel(" "), self.multiRunPanel_GBLC)
        self.multiRunEntryListIndex = self.multiRunEntryListIndex + 1
        
        #print "-- 3"
        self.multiRunPanel.revalidate()
        self.multiRunPanel.repaint()
        #print "-- Done"
        
    def removeMultiRunEntry(self, index):
        if(self.multiRunPanel.isEnabled()):
            #print ">> Disable all multiRunEntries"
            self.multiRunPanel.setEnabled(False)
            
            # Create temp variables
            numRunsValues = []
            variableToModifyIndices = []
            lowRangeValues = []
            highRangeValues = []
            #deltaValues = []
            specificValueValues = []
            
            # Gather the existing values so we can recreate them
            #print ">> Gathering existing values between indexes %d and %d" % (index+1, len(self.multiRunEntryList))
            for multiRunEntryIndex in range(index+1, len(self.multiRunEntryList)):
                numRunsValues.append(self.numRunsTextFields[multiRunEntryIndex].text)
                variableToModifyIndices.append(self.variableToModifyComboBoxes[multiRunEntryIndex].getSelectedIndex())
                lowRangeValues.append(self.lowRangeTextFields[multiRunEntryIndex].text)
                highRangeValues.append(self.highRangeTextFields[multiRunEntryIndex].text)
                #deltaValues.append(self.deltaTextFields[multiRunEntryIndex].text)
                specificValueValues.append(self.specificValsTextFields[multiRunEntryIndex].text)
            
            # Remove all multiRunEntries from the multiRunPanel and ui components in arrays starting from the given index
            multiRunEntryListLength = len(self.multiRunEntryList)
            #print ">> Removing all multiRunEntries between indexes %d and %d" % (index, multiRunEntryListLength)
            for multiRunEntryIndex in range(index, multiRunEntryListLength):
                #print ">>>> Removing entry %d" % (multiRunEntryIndex)
                self.multiRunPanel.remove(self.multiRunEntryList[index])
                self.multiRunEntryList.pop(index)
                self.numRunsTextFields.pop(index)
                self.variableToModifyComboBoxes.pop(index)
                self.lowRangeTextFields.pop(index)
                self.highRangeTextFields.pop(index)
                self.specificValsTextFields.pop(index)
                
            # Set the multi-run index variable
            self.multiRunEntryListIndex = index
            #print ">> Set multiRunEntryListIndex to %d" % (self.multiRunEntryListIndex)
            
            # Recreate the other multiRunEntries
            #print ">> Recreating %d multi-run entries" % (len(numRunsValues))
            for multiRunEntryIndex in range (len(numRunsValues)):
                #print ">>>> Adding new entry"
                # Add a new multi-run entry (blank)
                self.multiRunPanel.setEnabled(True)
                self.addMultiRunEntry()
                #print ">>>> Blank entry added, setting values"
                
                # Populate it with values
                self.numRunsTextFields[self.multiRunEntryListIndex-1].text = numRunsValues[multiRunEntryIndex]
                #print ">>>> value set"
                self.variableToModifyComboBoxes[self.multiRunEntryListIndex-1].setSelectedIndex(variableToModifyIndices[multiRunEntryIndex])
                #print ">>>> value set"
                self.lowRangeTextFields[self.multiRunEntryListIndex-1].text = lowRangeValues[multiRunEntryIndex]
                #print ">>>> value set"
                self.highRangeTextFields[self.multiRunEntryListIndex-1].text = highRangeValues[multiRunEntryIndex]
                #print ">>>> value set"
                self.specificValsTextFields[self.multiRunEntryListIndex-1].text = specificValueValues[multiRunEntryIndex]
                #print ">>>> value set"
            
            # Revalidate and repaint everything so Jython shows UI correctly on screen
            #print ">> Refreshing UI"
            self.multiRunPanel.revalidate()
            self.multiRunPanel.repaint()
            self.multiRunCard.revalidate()
            self.multiRunCard.repaint()
            
            self.multiRunPanel.setEnabled(True)
            #print "Finished removeMultiRunEntry()"
    '''
    https://stackoverflow.com/questions/1213295/handling-swing-focus-events-with-jython
    See frame stuff in above link to get the UI to refresh
    https://stackoverflow.com/questions/4279435/java-how-would-i-dynamically-add-swing-component-to-gui-on-click
    https://wiki.python.org/jython/SwingExamples
    '''
    def checkForEmptyFieldsCallback(self, event):
        #pass
        #print "==== Checking for empty fields ===="
        self.checkForEmptyFields()
    
    def checkForEmptyFields(self):
        if(self.multiRunPanel.isEnabled()):
            #print "==== Checking for empty fields ===="
            #print "There are %d multiRunEntries" % ( len(self.multiRunEntryList) )
                        
            emptyTextTypeA = " "
            emptyTextTypeB = ""
            isEmpty = True
            
            # Delete all values in the config (we'll update with new values later)
            del self.myConfig.multiRun_numberOfRuns    [:]
            del self.myConfig.multiRun_variableToModify[:]
            del self.myConfig.multiRun_lowRange        [:]
            del self.myConfig.multiRun_highRange       [:]
            del self.myConfig.multiRun_delta           [:]
            del self.myConfig.multiRun_specificValues  [:]
            
            # Loop through the entry list
            multiRunEntryListLength = len(self.multiRunEntryList)
            for i in range(multiRunEntryListLength):
                #print "[DEBUG] index: %d" % (i)
                
                isEmpty = True
                
                # Check if any of the fields are empty (for this item in entry list)
                numRunsText = self.numRunsTextFields[i].getText()
                tempValueComboBoxesText = self.variableToModifyComboBoxes[i].getSelectedItem()
                tempValueComboBoxesText = tempValueComboBoxesText.lower()
                tempValueComboBoxesText = tempValueComboBoxesText.replace(' ','_')
                lowRangeText = self.lowRangeTextFields[i].getText()
                highRangeText = self.highRangeTextFields[i].getText()
                specificValsText = self.specificValsTextFields[i].getText()
                
                if( (numRunsText != emptyTextTypeA) and (numRunsText != emptyTextTypeB) ):
                    #print "Found num run text"
                    isEmpty = False
                elif( (lowRangeText != emptyTextTypeA) and (lowRangeText != emptyTextTypeB) ):
                    #print "Found low range text"
                    isEmpty = False
                elif( (highRangeText != emptyTextTypeA) and (highRangeText != emptyTextTypeB) ):
                    #print "Found high range text"
                    isEmpty = False
                elif( (specificValsText != emptyTextTypeA) and (specificValsText != emptyTextTypeB) ):
                    #print "Found specific vals"
                    isEmpty = False
                else:
                    #print "No text found"
                    pass
                    
                # If all fields empty and not the last item in the entry list
                if( isEmpty ):
                    # if it is not the last one in the list and it is not the last thing TODO: same thing?
                    #if( (i != (self.multiRunEntryListIndex-1) ) and (self.multiRunEntryListIndex > 1) ):
                    if( i != (self.multiRunEntryListIndex-1) ):
                        # Remove the item from the entry list, UI
                        #print "Removing multiRunEntry %d" % (i)
                        self.removeMultiRunEntry(i)
                else:
                    # if not empty and is the last item in the entry list
                    if( i == (self.multiRunEntryListIndex-1) ):
                        # Add new item to the entry list, UI
                        #print "Adding multiRunEntry"
                        self.addMultiRunEntry()
                    
                    # Add values to config
                    self.myConfig.multiRun_numberOfRuns.append(numRunsText)
                    self.myConfig.multiRun_variableToModify.append(tempValueComboBoxesText)
                    self.myConfig.multiRun_lowRange.append(lowRangeText)
                    self.myConfig.multiRun_highRange.append(highRangeText)
                    self.myConfig.multiRun_specificValues.append(specificValsText)
                
        #print "Done looking for empty fields"
        
    def getCard(self):
        return self.multiRunCard
    
    ##################################################
    ## Save the value in the UI to the config object #
    ##################################################
    #
    #def setDirectory(self):
    #    self.myConfig.inverseDynTool_resultsDir = self.outputGroup_directoryTextField.text
    #    
    ################################
    ## Save contents of a text box #
    ################################
    #
    #def setDirectoryEvent(self, event):
    #    self.setDirectory()
    #    
    ############################################################
    ## Show file chooser dialog when a "..." button is pressed #
    ############################################################
    #
    #def selectDirectoryDir(self, event):
    #    # Create and show a file chooser (True = select file, False = select directory)
    #    myChooser = bessFileChooser(False)
    #    # Set the text field text to the results of the file chooser
    #    self.outputGroup_directoryTextField.text = myChooser.fullPath.toString()
    #    # Save the value to the config class
    #    self.setDirectory()
    #    
    ## Save/load from config
    ## https://docs.python.org/2/library/xml.etree.elementtree.html
    #def loadSettings(self, event):
    #    print 'Loading settings...'
    #    # Create and show a file chooser (True = select file, False = select directory)
    #    myChooser = bessFileChooser(True)
    #    self.loadSettingsFromFile(myChooser.fullPath.toString())
    
    def loadSettingsFromFile(self, filePath):
        # Open the xml file
        if(os.path.exists(filePath)):
            print 'Opening config file: %s' % (filePath)
            tree = ET.parse(filePath)
            print 'Config file loaded. Populating UI elements...'
            
            # Remove all current entries
            for multiRunEntryIndex in range(0, len(self.multiRunEntryList)-1):
                #print 'Emtying multi-run entry %d' % (multiRunEntryIndex)
                self.numRunsTextFields[multiRunEntryIndex].text = ''
                self.variableToModifyComboBoxes[multiRunEntryIndex].setSelectedIndex(0)
                self.lowRangeTextFields[multiRunEntryIndex].text = ''
                self.highRangeTextFields[multiRunEntryIndex].text = ''
                self.specificValsTextFields[multiRunEntryIndex].text = ''
            #print 'removing empty fields'
            self.checkForEmptyFields()
            
            # Parse the tree
            root = tree.getroot()
            self.parseTree(root)
            
            print 'Done loading settings'
        else:
            print 'Could not find file: %s' % (filePath)
        
    def parseTree(self, node):
        print 'Node tag: %s,  text: %s' % (node.tag, node.text)
        
        if('multiRunEntry' == node.tag):
            numRuns = ''
            variableToModify = ''
            lowRange = ''
            highRange = ''
            delta = ''
            specificValues = ''
            
            for child in node:
                if('number_of_runs' == child.tag):
                    numRuns = child.text
                elif('variable_to_modify' == child.tag):
                    variableToModify = child.text
                elif('low_range' == child.tag):
                    lowRange = child.text
                elif('high_range' == child.tag):
                    highRange = child.text
                elif('delta' == child.tag):
                    delta = child.text
                elif('specific_values' == child.tag):
                    specificValues = child.text
                    
            # Set the values in the UI fields
            self.numRunsTextFields[self.multiRunEntryListIndex-1].text = numRuns
            for index,item in enumerate(self.variableVariancePanel_variableToModifyComboBoxOptions):
                # Adjust the text to match the xml tag values
                item = item.lower()
                item = item.replace(' ','_')
                if(item == variableToModify):
                    self.variableToModifyComboBoxes[self.multiRunEntryListIndex-1].setSelectedIndex(index)
            self.lowRangeTextFields[self.multiRunEntryListIndex-1].text = lowRange
            self.highRangeTextFields[self.multiRunEntryListIndex-1].text = highRange
            self.specificValsTextFields[self.multiRunEntryListIndex-1].text = specificValues
            
            # Have the UI add another (empty) multi-run entry / group
            self.checkForEmptyFields()
        
        # Look at any children of children
        for child in node:
            self.parseTree(child)
    
# end bessConfigUIMultiRunCard class

