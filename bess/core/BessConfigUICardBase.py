

################################################################################
#
# Defines a base class for all the BESS card classes
#
################################################################################

class bessCardBase:
    """
    This class provides a set of "virtual" methods that may or may not be
    required to be overriden by a derived class.
    """
    
    def getCard(self):
        """
        Called when the main Config UI wants to get the JPanel from the
        individual card class.
        """
        
        # This function must be implemented by a derived class.
        raise NotImplementedError('getCard() function is not implemented')
        
    # End getCard.
    
    def loadSettingsFromFile(self, xmlPath):
        """
        Called when the root directory gets set and the specific config file
        for an individual card "tool" is identified.
        """
        
        # This function must be implemented by a derived class.
        raise NotImplementedError('loadSettingsFromFile() function is not '
            'implemented')
            
    # End loadSettingsFromFile.
    
    def onShow(self):
        """
        Called when a card is shown after a "Previous" or "Next" button click
        in the card layout.
        """
        
        # This function can be ignored by the derived class.
        pass
        
    # End onShow.
    
    def onCardCreationComplete(self):
        """
        Called when the ConfigUI has created all of the individual cards.
        """
        
        # Can be ignored by the derived class.
        pass
    
    # End onCardCreationComplete.
    
# End bessCardBase.

