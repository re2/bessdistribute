

################################################################################
#
#
################################################################################

from xml.dom.minidom import parse, parseString, getDOMImplementation, Document
import xml.etree.cElementTree as ET

class bessXmlCommonIO:
    def __init__(self, myConfig):
        print "Initializing bessXmlCommon"
        self.myConfig = myConfig
        print "Done initializing bessXmlCommon"
        
    # http://stackoverflow.com/questions/3605680/creating-a-simple-xml-file-using-python
    def writeConfigFile(self):
        print "Writing Common xml file"
        #<?xml version="1.0" encoding="UTF-8" ?>
        
        # TODO: Change version to re2version?
        #<BessConfig Version="1">
        xmlTag_OpenSimDocument = ET.Element("BessConfig", Version="1")
        
        #    <use_scale_tool />
        xmlTag_useScaleTool = ET.SubElement(xmlTag_OpenSimDocument, "use_scale_tool")
        xmlTag_useScaleTool.text = self.myConfig.useScale
        
        #    <use_cmc_tool />
        xmlTag_useCmcTool = ET.SubElement(xmlTag_OpenSimDocument, "use_cmc_tool")
        xmlTag_useCmcTool.text = self.myConfig.useCmcTool
        
        #    <use_ik_tool />
        xmlTag_useIkTool = ET.SubElement(xmlTag_OpenSimDocument, "use_ik_tool")
        xmlTag_useIkTool.text = self.myConfig.useIkTool
        
        #    <use_configure_forces_tool />
        xmlTag_useConfigureForcesTool = ET.SubElement(xmlTag_OpenSimDocument, "use_configure_forces_tool")
        xmlTag_useConfigureForcesTool.text = self.myConfig.useConfigureForcesTool
        
        #    <use_rra_tool />
        xmlTag_useRraTool = ET.SubElement(xmlTag_OpenSimDocument, "use_rra_tool")
        xmlTag_useRraTool.text = self.myConfig.useRraTool
        
        #    <use_static_optimization_tool />
        xmlTag_useStaticOptTool = ET.SubElement(xmlTag_OpenSimDocument, "use_static_optimization_tool")
        xmlTag_useStaticOptTool.text = self.myConfig.useStaticOptTool
        
        #    <use_inverse_dynamics_tool />
        xmlTag_useInverseDynTool = ET.SubElement(xmlTag_OpenSimDocument, "use_inverse_dynamics_tool")
        xmlTag_useInverseDynTool.text = self.myConfig.useInverseDynTool
        
        #    <use_jr_tool />
        xmlTag_useJrTool = ET.SubElement(xmlTag_OpenSimDocument, "use_jr_tool")
        xmlTag_useJrTool.text = self.myConfig.useJointReactionTool
        
        #    <root_directory />
        xmlTag_rootDir = ET.SubElement(xmlTag_OpenSimDocument, "root_directory")
        xmlTag_rootDir.text = self.myConfig.common_rootDir
        
        #    <model />
        xmlTag_model = ET.SubElement(xmlTag_OpenSimDocument, "model")
        xmlTag_model.text = self.myConfig.common_modelFile
        
        #    <motion />
        xmlTag_motion = ET.SubElement(xmlTag_OpenSimDocument, "motion")
        xmlTag_motion.text = self.myConfig.common_motionFile
                
        #    <ground_reactions />
        xmlTag_groundReactions = ET.SubElement(xmlTag_OpenSimDocument, "ground_reactions")
        xmlTag_groundReactions.text = self.myConfig.common_groundReactionsFile
        
        #    <desired_kinematics />
        xmlTag_desiredKinematics = ET.SubElement(xmlTag_OpenSimDocument, "desired_kinematics")
        xmlTag_desiredKinematics.text = self.myConfig.common_desiredKinematicsFile
        
        #    <tracking_tasks />
        xmlTag_trackingTasks = ET.SubElement(xmlTag_OpenSimDocument, "tracking_tasks")
        xmlTag_trackingTasks.text = self.myConfig.common_trackingTasksFile
        
        #    <replace_force_set />
        xmlTag_replaceForceSet = ET.SubElement(xmlTag_OpenSimDocument, "replace_force_set")
        xmlTag_replaceForceSet.text = self.myConfig.common_replaceForceSet
        
        #    <force_set />
        xmlTag_forceSet = ET.SubElement(xmlTag_OpenSimDocument, "force_set")
        xmlTag_forceSet.text = self.myConfig.common_forceSetFile
        
        #    <external_loads />
        xmlTag_externalLoads = ET.SubElement(xmlTag_OpenSimDocument, "external_loads")
        xmlTag_externalLoads.text = self.myConfig.common_externalLoadsSpecFile
        
        #    <integrator_error_tolerance />
        xmlTag_integratorErrTolerance = ET.SubElement(xmlTag_OpenSimDocument, "integrator_error_tolerance")
        xmlTag_integratorErrTolerance.text = self.myConfig.common_integratorErrTolerance
        
        #    <integrator_minimum_step_size />
        xmlTag_integratorMinStepSize = ET.SubElement(xmlTag_OpenSimDocument, "integrator_minimum_step_size")
        xmlTag_integratorMinStepSize.text = self.myConfig.common_integratorMinStepSize
        
        #    <integrator_maximum_step_size />
        xmlTag_integratorMaxStepSize = ET.SubElement(xmlTag_OpenSimDocument, "integrator_maximum_step_size")
        xmlTag_integratorMaxStepSize.text = self.myConfig.common_integratorMaxStepSize
        
        #    <integrator_maximum_steps />
        xmlTag_integratorMaxSteps = ET.SubElement(xmlTag_OpenSimDocument, "integrator_maximum_steps")
        xmlTag_integratorMaxSteps.text = self.myConfig.common_integratorMaxSteps
        
        #    <filter_kinematics />
        xmlTag_filterKinematics = ET.SubElement(xmlTag_OpenSimDocument, "filter_kinematics")
        xmlTag_filterKinematics.text = self.myConfig.common_filterKinematics
        
        #    <final_time />
        xmlTag_finalTime = ET.SubElement(xmlTag_OpenSimDocument, "final_time")
        xmlTag_finalTime.text = self.myConfig.common_timeRangeTo
        
        #    <initial_time />
        xmlTag_initialTime = ET.SubElement(xmlTag_OpenSimDocument, "initial_time")
        xmlTag_initialTime.text = self.myConfig.common_timeRangeFrom
        
        #    <in_degrees />
        xmlTag_inDegrees = ET.SubElement(xmlTag_OpenSimDocument, "in_degrees")
        xmlTag_inDegrees.text = self.myConfig.common_inDegrees
        
        #</BessConfig>
        
        # Fix the indentation
        #print "Correcting indentations..."
        self.indent(xmlTag_OpenSimDocument)
        #print "finished correcting indentations"
        tree = ET.ElementTree(xmlTag_OpenSimDocument)
        #print "created tree"
        
        # Write out the xml file
        xmlFileName = self.myConfig.commonConfigFile
        #print "perform writing"
        tree.write(xmlFileName, encoding='utf-8')
        print ("Wrote Common xml file: "+xmlFileName)
        
    def indent(self, elem, level=0):
        #print "fixing element: %s" % (elem)
        i = "\n" + level*"  "
        j = "\n" + (level-1)*"  "
        if len(elem):
            #print "1"
            if not elem.text or not elem.text.strip():
                #print "2"
                elem.text = i + "  "
            if not elem.tail or not elem.tail.strip():
                #print "3"
                elem.tail = i
            for subelem in elem:
                #print "4"
                self.indent(subelem, level+1)
            if not elem.tail or not elem.tail.strip():
                #print "5"
                elem.tail = j
        else:
            #print "6"
            if level and (not elem.tail or not elem.tail.strip()):
                #print "7"
                elem.tail = j
        #print "8"
        return elem        

