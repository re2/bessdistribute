

################################################################################
#
# This file creates the Configuration UI components and handles user input as
# it relates to BESS configuration.
#
################################################################################

from threading import Thread

from javax.swing import JFrame, JPanel, JButton, JFileChooser, JTextField, JLabel, JCheckBox, SwingConstants, JTextArea, JScrollPane
from javax.swing.border import TitledBorder
from java.awt import GridLayout, FlowLayout, Color, GridBagLayout, GridBagConstraints, CardLayout

from xml.dom.minidom import parse, parseString, getDOMImplementation, Document
import xml.etree.cElementTree as ET

from core.BessConfigUIScale import bessConfigUIScaleCard as scaleCardCreator
from core.BessConfigUICmc import bessConfigUICmcCard as cmcCardCreator
from core.BessConfigUICommon import bessConfigUICommonCard as commonCardCreator
from core.BessConfigUIMultiRun import bessConfigUIMultiRunCard as multiRunCardCreator
from core.BessConfigUIInvKineTool import bessConfigUIInvKineToolCard as invKineToolCardCreator
from core.BessConfigUIConfigureForces import bessConfigUIConfigureForcesCard as configureForcesCardCreator
from core.BessConfigUIRRATool import bessConfigUIRRAToolCard as rraToolCardCreator
from core.BessConfigUIStaticOptimTool import bessConfigUIStaticOptimToolCard as staticOptimToolCardCreator
from core.BessConfigUIInvDynTool import bessConfigUIInvDynToolCard as invDynToolCardCreator
from core.BessConfigUIJointReactTool import bessConfigUIJointReactToolCard as jointReactToolCardCreator

from core.BessXmlScale import bessXmlScaleIO as xmlScaleIO
from core.BessXmlCommon import bessXmlCommonIO as xmlCommonIO
from core.BessXmlMultiRun import bessXmlMultiRunIO as xmlMultiRunIO
from core.BessXmlCmc import bessXmlCmcIO as xmlCmcIO
from core.BessXmlIk import bessXmlIkIO as xmlIkIO
from core.BessXmlRra import bessXmlRraIO as xmlRraIO
from core.BessXmlStaticOpt import bessXmlStaticOptIO as xmlStaticOptIO
from core.BessXmlInvDyn import bessXmlInvDynIO as xmlInvDynIO
from core.BessXmlJr import bessXmlJrIO as xmlJrIO

from configure_forces import configureForces_Support
from utilities import toolsCommon as TC

'''
This class helps create all the individual UI cards / supporting classes and ties them together
'''
class bessConfigUI:

    def __init__(self, myConfig, mySim):
        self.myConfig = myConfig
        self.mySim = mySim
        self.myCardCreators = []

        # Create gui items
        self.bessInputFrame = JFrame("Input for BESS")
        #bessInputFrame.defaultCloseOperation  = swing.JFrame.EXIT_ON_CLOSE
        bessInputPanel = JPanel(GridBagLayout())
                
        self.cards_layout = CardLayout()
        self.cards = JPanel(self.cards_layout)
        
        # Card names
        self.cardIndex = 0
        self.WELCOME_CARD_NAME = "Welcome"
        self.COMMON_CARD_NAME = "Common"
        self.SCALE_CARD_NAME = "Scale"
        self.MULTI_RUN_CARD_NAME = "Multi-run"
        self.CMC_TOOL_CARD_NAME = "Computed Muscle Control"
        self.INV_KINE_TOOL_CARD_NAME = "Inverse Kinematics Tool"
        self.CONFIGURE_FORCES_TOOL_CARD_NAME = "Configure Forces Tool"
        self.RRA_TOOL_CARD_NAME = "Reduce Residuals Tool"
        self.STATIC_OPTIM_TOOL_CARD_NAME = "Static Optimization Tool"
        self.INV_DYN_TOOL_CARD_NAME = "Inverse Dynamics Tool"
        self.JOINT_REACT_TOOL_CARD_NAME = "Joint Reaction Analysis Tool"
        self.SUMMARY_CARD_NAME = "Summary"
        self.cardNames = []
        
        # Common card
        myCommonCardCreator = commonCardCreator(myConfig, self.myCardCreators)
        myCommonCard = myCommonCardCreator.getCard()
        myCommonCard.setName(self.COMMON_CARD_NAME)
        self.myCardCreators.append(myCommonCardCreator)
        
        # Scale card
        myScaleCardCreator = scaleCardCreator(myConfig)
        myScaleCard = myScaleCardCreator.getCard()
        myScaleCard.setName(self.SCALE_CARD_NAME)
        self.myCardCreators.append(myScaleCardCreator)
        
        # Inv Kinematics card
        myInvKineToolCardCreator = invKineToolCardCreator(myConfig)
        myInvKineToolCard = myInvKineToolCardCreator.getCard()
        myInvKineToolCard.setName(self.INV_KINE_TOOL_CARD_NAME)
        self.myCardCreators.append(myInvKineToolCardCreator)
        
        # Configure Forces card.
        myConfigureForcesCardCreator = configureForcesCardCreator(myConfig, 
            self.bessInputFrame)
        myConfigureForcesCard = myConfigureForcesCardCreator.getCard()
        myConfigureForcesCard.setName(self.CONFIGURE_FORCES_TOOL_CARD_NAME)
        self.myCardCreators.append(myConfigureForcesCardCreator)        
        
        # Inverse Dynamic
        myInvDynToolCardCreator = invDynToolCardCreator(myConfig)
        myInvDynToolCard = myInvDynToolCardCreator.getCard()
        myInvDynToolCard.setName(self.INV_DYN_TOOL_CARD_NAME)
        self.myCardCreators.append(myInvDynToolCardCreator)
        
        # Static Optimization
        myStaticOptimToolCardCreator = staticOptimToolCardCreator(myConfig)
        myStaticOptimToolCard = myStaticOptimToolCardCreator.getCard()
        myStaticOptimToolCard.setName(self.STATIC_OPTIM_TOOL_CARD_NAME)
        self.myCardCreators.append(myStaticOptimToolCardCreator)
        
        # RRA card
        myRRAToolCardCreator = rraToolCardCreator(myConfig)
        myRRAToolCard = myRRAToolCardCreator.getCard()
        myRRAToolCard.setName(self.RRA_TOOL_CARD_NAME)
        self.myCardCreators.append(myRRAToolCardCreator)
        
        # CMC card
        myCmcCardCreator = cmcCardCreator(myConfig)
        myCmcCard = myCmcCardCreator.getCard()
        myCmcCard.setName(self.CMC_TOOL_CARD_NAME)
        self.myCardCreators.append(myCmcCardCreator)
        
        # Joint Reaction
        myJointReactToolCardCreator = jointReactToolCardCreator(myConfig)
        myJointReactToolCard = myJointReactToolCardCreator.getCard()
        myJointReactToolCard.setName(self.JOINT_REACT_TOOL_CARD_NAME)
        self.myCardCreators.append(myJointReactToolCardCreator)
        
        # Multi-run card
        myMultiRunCardCreator = multiRunCardCreator(myConfig)
        myMultiRunCard = myMultiRunCardCreator.getCard()
        myMultiRunCard.setName(self.MULTI_RUN_CARD_NAME)
        self.myCardCreators.append(myMultiRunCardCreator)
        
        # Welcome and Summary cards
        self.welcomeCard = JPanel(GridBagLayout())
        welcomeCard_GBLC = GridBagConstraints()
        welcomeCard_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        welcomeCardLabel = JLabel("Welcome to BESS!", SwingConstants.CENTER)
        welcomeCardNotes1Label = JLabel("    Please be aware that BESS requires full file paths.")
        welcomeCardNotes2Label = JLabel("    When loading an existing configuration file that does not have full file paths, the behavior of BESS is undefined and may result in the application not functioning properly")
        welcomeCardLegalNoticeTextArea = JTextArea(15, 120)
        welcomeCardLegalNoticeTextArea.setLineWrap(True)
        welcomeCardLegalNoticeTextArea.setWrapStyleWord(True)
        welcomeCardLegalNoticeTextArea.setText("This Work is provided on an \"AS IS\" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied, including, without limitation, any warranties or conditions of TITLE, NON-INFRINGEMENT, MERCHANTABILITY, or FITNESS FOR A PARTICULAR PURPOSE. You are solely responsible for determining the appropriateness of using or redistributing the Work and assume any risks associated with Your exercise of permissions under this License.\n\nBy pressing the Next button, you agree to these terms.")
        welcomeCardLegalNoticeTextArea.setEditable(False)
        welcomeCardLegalNoticeScrollPane = JScrollPane(welcomeCardLegalNoticeTextArea)
        welcomeCardLegalNoticePanel = JPanel()
        welcomeCardLegalNoticePanel.add(welcomeCardLegalNoticeScrollPane)
        
        welcomeCardLayoutIndex = 0
        welcomeCard_GBLC.gridx = 0
        welcomeCard_GBLC.gridy = welcomeCardLayoutIndex
        welcomeCardLayoutIndex = welcomeCardLayoutIndex + 1
        self.welcomeCard.add(welcomeCardLabel, welcomeCard_GBLC)
        welcomeCard_GBLC.gridx = 0
        welcomeCard_GBLC.gridy = welcomeCardLayoutIndex
        welcomeCardLayoutIndex = welcomeCardLayoutIndex + 1
        self.welcomeCard.add(JLabel(" "), welcomeCard_GBLC)
        welcomeCard_GBLC.gridx = 0
        welcomeCard_GBLC.gridy = welcomeCardLayoutIndex
        welcomeCardLayoutIndex = welcomeCardLayoutIndex + 1
        self.welcomeCard.add(welcomeCardNotes1Label, welcomeCard_GBLC)
        welcomeCard_GBLC.gridx = 0
        welcomeCard_GBLC.gridy = welcomeCardLayoutIndex
        welcomeCardLayoutIndex = welcomeCardLayoutIndex + 1
        self.welcomeCard.add(welcomeCardNotes2Label, welcomeCard_GBLC)
        welcomeCard_GBLC.gridx = 0
        welcomeCard_GBLC.gridy = welcomeCardLayoutIndex
        welcomeCardLayoutIndex = welcomeCardLayoutIndex + 1
        self.welcomeCard.add(JLabel(" "), welcomeCard_GBLC)
        welcomeCard_GBLC.gridx = 0
        welcomeCard_GBLC.gridy = welcomeCardLayoutIndex
        welcomeCardLayoutIndex = welcomeCardLayoutIndex + 1
        self.welcomeCard.add(welcomeCardLegalNoticePanel, welcomeCard_GBLC)
        
        self.summaryCard = JPanel()
        
        summaryCardLabel = JLabel("TODO: Summary goes here")
        self.summaryCard.add(summaryCardLabel)
        
        self.welcomeCard.setName(self.WELCOME_CARD_NAME)
        self.summaryCard.setName(self.SUMMARY_CARD_NAME)
        
        # Cards
        # Welcome
        self.cards.add(self.welcomeCard, self.WELCOME_CARD_NAME)
        self.cardNames.append(self.WELCOME_CARD_NAME)
        # Common
        self.cards.add(myCommonCard, self.COMMON_CARD_NAME)
        self.cardNames.append(self.COMMON_CARD_NAME)
        # Scale
        self.cards.add(myScaleCard, self.SCALE_CARD_NAME)
        self.cardNames.append(self.SCALE_CARD_NAME)
        # IK
        self.cards.add(myInvKineToolCard, self.INV_KINE_TOOL_CARD_NAME)
        self.cardNames.append(self.INV_KINE_TOOL_CARD_NAME)
        # Configure Forces
        self.cards.add(myConfigureForcesCard, self.CONFIGURE_FORCES_TOOL_CARD_NAME)
        self.cardNames.append(self.CONFIGURE_FORCES_TOOL_CARD_NAME)
        # ID
        self.cards.add(myInvDynToolCard, self.INV_DYN_TOOL_CARD_NAME)
        self.cardNames.append(self.INV_DYN_TOOL_CARD_NAME)
        # Static Opt
        self.cards.add(myStaticOptimToolCard, self.STATIC_OPTIM_TOOL_CARD_NAME)
        self.cardNames.append(self.STATIC_OPTIM_TOOL_CARD_NAME)
        # RRA
        self.cards.add(myRRAToolCard, self.RRA_TOOL_CARD_NAME)
        self.cardNames.append(self.RRA_TOOL_CARD_NAME)
        # CMC
        self.cards.add(myCmcCard, self.CMC_TOOL_CARD_NAME)
        self.cardNames.append(self.CMC_TOOL_CARD_NAME)
        # JR
        self.cards.add(myJointReactToolCard, self.JOINT_REACT_TOOL_CARD_NAME)
        self.cardNames.append(self.JOINT_REACT_TOOL_CARD_NAME)
        # Multi-Run
        self.cards.add(myMultiRunCard, self.MULTI_RUN_CARD_NAME)
        self.cardNames.append(self.MULTI_RUN_CARD_NAME)
        # Summary
        self.cards.add(self.summaryCard, self.SUMMARY_CARD_NAME)
        self.cardNames.append(self.SUMMARY_CARD_NAME)
        
        # Progression Buttons
        self.prevCardButton = JButton("Prev", actionPerformed = self.displayPrevCard)
        self.prevCardButton.enabled = False
        self.nextCardButton = JButton("Next", actionPerformed = self.displayNextCard)
        startSimButton = JButton("Start sim", actionPerformed = self.startSimHandler)
        startSimButton.setBackground(Color.GREEN)
        
        buttonPanel = JPanel(GridBagLayout())
        buttonPanel_GBLC = GridBagConstraints()
        buttonPanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        
        buttonPanel_GBLC.weightx = 0.3
        buttonPanel_GBLC.gridx = 0
        buttonPanel_GBLC.gridy = 0
        buttonPanel.add(JLabel(" "), buttonPanel_GBLC)
        buttonPanel_GBLC.weightx = 0.15
        buttonPanel_GBLC.gridx = 1
        buttonPanel_GBLC.gridy = 0
        buttonPanel.add(self.prevCardButton, buttonPanel_GBLC)
        buttonPanel_GBLC.weightx = 0.15
        buttonPanel_GBLC.gridx = 2
        buttonPanel_GBLC.gridy = 0
        buttonPanel.add(self.nextCardButton, buttonPanel_GBLC)
        buttonPanel_GBLC.weightx = 0.15
        buttonPanel_GBLC.gridx = 3
        buttonPanel_GBLC.gridy = 0
        buttonPanel.add(JLabel(" "), buttonPanel_GBLC)
        buttonPanel_GBLC.weightx = 0.15
        buttonPanel_GBLC.gridx = 4
        buttonPanel_GBLC.gridy = 0
        buttonPanel.add(startSimButton, buttonPanel_GBLC)
        
        # Layout gui items.  Make the top panel get all the extra vertical
        # space since the bottom row are just fixed height buttons.
        bessInputPanel_GBLC = GridBagConstraints()        
        bessInputPanel_GBLC.weightx = 1.0
        bessInputPanel_GBLC.weighty = 1.0
        bessInputPanel_GBLC.gridx = 0
        bessInputPanel_GBLC.gridy = 0
        bessInputPanel_GBLC.fill = GridBagConstraints.BOTH
        bessInputPanel.add(self.cards, bessInputPanel_GBLC)
        bessInputPanel_GBLC.weightx = 1.0
        bessInputPanel_GBLC.weighty = 0.0
        bessInputPanel_GBLC.gridx = 0
        bessInputPanel_GBLC.gridy = 1
        bessInputPanel_GBLC.fill = GridBagConstraints.HORIZONTAL
        bessInputPanel.add(buttonPanel, bessInputPanel_GBLC)
        self.bessInputFrame.add(bessInputPanel)
        
        # Give the cards a chance to do anything not that all the cards have
        # been created.
        for cardCreator in self.myCardCreators:
            cardCreator.onCardCreationComplete()
        
        # Show the gui
        self.bessInputFrame.pack()
        self.bessInputFrame.visible = True
        self.bessInputFrame.setSize(self.bessInputFrame.getWidth(), 800) 

    def saveToXML(self):
        # Multi-run
        myXmlMultiRunIO = xmlMultiRunIO(self.myConfig)
        myXmlMultiRunIO.writeMultiRunConfigFiles()
        
        
    def displayNextCard(self, event):
        self.cards_layout.next(self.cards)
        self.cardIndex = self.cardIndex + 1
        
        if(self.welcomeCard.isVisible()):
            self.prevCardButton.enabled = False
        else:
            self.prevCardButton.enabled = True
        
        if(self.summaryCard.isVisible()):
            self.nextCardButton.enabled = False
        else:
            self.nextCardButton.enabled = True
        self.updateTitle()
        
        # Tell the card creator that we are showing the card.  This lets ability
        # card creator update any config parameters that may have changed on
        # a different card.  Subtract 1 and check on length to handle the fact
        # that the Welcome and Summary cards aren't in this list.
        if len(self.myCardCreators) > self.cardIndex - 1:
            self.myCardCreators[self.cardIndex - 1].onShow()
        
    def displayPrevCard(self, event):
        self.cards_layout.previous(self.cards)
        self.cardIndex = self.cardIndex - 1
        
        if(self.welcomeCard.isVisible()):
            self.prevCardButton.enabled = False
        else:
            self.prevCardButton.enabled = True
        
        if(self.summaryCard.isVisible()):
            self.nextCardButton.enabled = False
        else:
            self.nextCardButton.enabled = True
        self.updateTitle()
        
        # Tell the card creator that we are showing the card.  This lets ability
        # card creator update any config parameters that may have changed on
        # a different card.  Subtract 1 and check on length to handle the fact
        # that the Welcome and Summary cards aren't in this list.
        if len(self.myCardCreators) > self.cardIndex - 1:
            self.myCardCreators[self.cardIndex - 1].onShow()
        
    def updateTitle(self):
        self.bessInputFrame.setTitle(self.cardNames[self.cardIndex])
        
    def simSetup(self):
        """
        Called as the first step in simulation.
        
        This gives us the opportunity to do any preperatory work.
        """
        
        if(TC.boolFromLowerCaseString(self.myConfig.useConfigureForcesTool) and 
            self.myConfig.configureForcesTool_Model):
            
            # Adjust the file paths based on the overriding files selected
            # upstream in the BESS pipeline.  Note the model file to use is 
            # dependent upon whether or not the scaling tool is to be run.
            self.myConfig.configureForcesTool_Model.filePaths.modelPath = (
                self.myConfig.getPostScalingModelFilename())
            self.myConfig.configureForcesTool_Model.filePaths.ikPath = (
                self.myConfig.common_motionFile)
            self.myConfig.configureForcesTool_Model.filePaths.grfPath = (
                self.myConfig.common_groundReactionsFile)
            (self.myConfig.configureForcesTool_Model.filePaths.
                externalLoadsOutPath) = (
                    self.myConfig.common_externalLoadsSpecFile)
        
    # End simSetup.
        
    def startSimHandler(self, event):
        """
        Lightweight handler that allows us to govern the threading of the
        simulation.
        """
        
        # We're in the GUI thread right now.  Since simulation potentially 
        # takes a long time (certainly longer than 500 ms!) we don't want it
        # to run in the GUI thread -- this will block all other UI 
        # functionality (painting the screen, handling button clicks, etc.).
        # So run this is a worker thread.
        Thread(target=lambda: self.startSim()).start()
        
    # End startSimHandler.
        
    def startSim(self):
        try:
            # Before we perform checks we may need to do some preperatory work.
            self.simSetup()
            
            print '[DEBUG] performing checks'
            self.performChecks()
            print '[DEBUG] Done performing checks'
            
            if(self.inputOk == True):
                simStartPopUpFrame = JFrame("Running simulation...")
                simStartPopUpPanel = JPanel(GridLayout(0,1))
                simStartPopUpLabelOne = JLabel("The sim is running")
                simStartPopUpLabelTwo = JLabel("Please wait, this may take several minutes")
                
                simStartPopUpPanel.add(simStartPopUpLabelOne)
                simStartPopUpPanel.add(simStartPopUpLabelTwo)
                simStartPopUpFrame.add(simStartPopUpPanel)
                
                simStartPopUpFrame.pack()
                simStartPopUpFrame.size = (500,100)
                simStartPopUpFrame.visible = True
                self.bessInputFrame.visible = False
                #time.sleep(3)
                
                print "Saving configuration to file"
                self.saveToXML()
                
                print "Starting sim..."
                self.mySim.startSimulation()
                
                simStartPopUpFrame.visible = False
                print "Finished sim!"
            else:
                inputErrPopUpFrame = JFrame("Input Error")
                inputErrPopUpPanel = JPanel(GridLayout(0,1))
                inputErrPopUpLabelOne = JLabel("There was an issue with the values given to the UI")
                inputErrPopUpLabelTwo = JLabel("Please check the console to see a list of issues")
                inputErrPopUpLabelThree = JLabel("Once all issues are fixed, please run the simulation again")
                
                inputErrPopUpPanel.add(inputErrPopUpLabelOne)
                inputErrPopUpPanel.add(inputErrPopUpLabelTwo)
                inputErrPopUpPanel.add(inputErrPopUpLabelThree)
                inputErrPopUpFrame.add(inputErrPopUpPanel)
                
                inputErrPopUpFrame.pack()
                inputErrPopUpFrame.size = (500,100)
                inputErrPopUpFrame.visible = True
                #self.bessInputFrame.visible = False
        except:
            print "=== Begin error info  ======================================="
            import sys
            print "Something went wrong with BESS!"
            print("Unexpected error: ", sys.exc_info()[0])
            print "------------------------------------------"
            import traceback
            print traceback.format_exc()
            # TODO: PDB doesn't seem to be working...
            #print "------------------------------------------"
            #import pdb
            #pdb.post_mortem()
            print "=== End error info  ======================================="
            
            # If we had an exception we should close the simulation status 
            # pop-up window, since we're obviously done here.  If we wanted
            # graphical/window feedback to the user we should throw up a 
            # different error message window.
            simStartPopUpFrame.visible = False
            
            # Don't "raise" an exception from this point.  Either this will
            # be run by a worker thread, in which case raising will just cause
            # the thread to terminate (which it is about to do right now
            # anyway), or this code is being run by the GUI thread (if we are 
            # in a button handler).  Raising from here terminates the GUI 
            # thread which will lock up OpenSim.  Since this is in a 
            # try/except block, this exception has been "handled" and 
            # raising from here will not trigger the "unhandled exception" 
            # handler.
            
    # Perform checks on the input to ensure all things are connected, the proper format, etc.
    def performChecks(self):
        print 'Checking input...'
        self.inputOk = True
        
        if(self.myConfig.common_rootDir == None):
            print 'Missing Information: Please fill in the Common -> Root File Path. This is required for BESS to function properly.'
            self.inputOk = False
        
        if(TC.boolFromLowerCaseString(self.myConfig.useIkTool)):
            if(self.myConfig.ikTool_markerFile == None):
                print 'Missing Information: Please fill in the IK -> Marker File Path'
                self.inputOk = False
        
        if(TC.boolFromLowerCaseString(self.myConfig.useInverseDynTool)):
            ## If using InvDyn tool, use the results from IK or the supplied motion file
            #print '--------> inverseDynTool_coordinatesFile %s' % (self.myConfig.inverseDynTool_coordinatesFile)
            if(self.myConfig.inverseDynTool_coordinatesFile == None):
                if((TC.boolFromLowerCaseString(self.myConfig.useIkTool)) and (self.myConfig.ikTool_outputMotionFile != None)):
                    self.myConfig.inverseDynTool_coordinatesFile = self.myConfig.ikTool_outputMotionFile
                elif(self.myConfig.common_motionFile != None):
                    self.myConfig.inverseDynTool_coordinatesFile = self.myConfig.common_motionFile
                else:
                    print 'Missing Information: Please fill in the Common -> Motion File Path or the IK -> Output Motion File Path'
                    self.inputOk = False
                    
        if(TC.boolFromLowerCaseString(self.myConfig.useConfigureForcesTool)):
            # The configure forces support file has a validation method for us.
            # We can run it on the concrete objects or on the XML file (which
            # generates concrete objects).  At this point we haven't saved
            # XML yet, so we need to do it on concrete objects.
            cfErrorList = configureForces_Support.Config.validateModel(
                self.myConfig.configureForcesTool_Model.filePaths, 
                self.myConfig.configureForcesTool_Model.strideConfig,
                self.myConfig.configureForcesTool_Model.grfForceSets,
                self.myConfig.configureForcesTool_Model.trackedPositions,
                self.myConfig.configureForcesTool_Model.realLoadCellForceSets,
                (self.myConfig.configureForcesTool_Model.
                    syntheticLoadCellForceSets),
                self.myConfig.configureForcesTool_Model.syntheticDataConfig)
                
            if cfErrorList:
                print 'Errors validating Configure Forces tool settings:'
                for cfError in cfErrorList:
                    print '- ', cfError
                
                self.inputOk = False
                   
        
#end of bessConfigUI class

