

################################################################################
#
# This script provides functions for dealing with STO and MOT files.
#
################################################################################

import csv # For CSV reader.
import os
import sys

def readStoMotAsDict(filePath):
    """
    Reads a storage (STO) or motion (MOT) file and puts the data into a dictionary.
    
    Keys of the dictionary will be names of a data column, and the value will be
    a list of floats.
    """
    
    # Open the file path.  Python will throw if this fails.
    infile = open(filePath, 'r')
    
    # Throw away the header since it doesn't provide much extra information
    # from what we can get in the data itself.  I guess we could do some
    # checks on number of columns, etc.  Maybe later.
    while True:
        line = infile.readline()
        
        if not line:
            raise EOFError('Reached end of file without encountering end of header')
    
        if 'endheader' in line:
            break
            
    reader = csv.DictReader(infile, delimiter='\t')

    # Will be returned to the caller.
    outDict = dict()
    
    for row in reader:
        # Each row is a dictionary of key/value mappings.  Iterate through this
        # one row dictionary and add the values to our larger dictionary.
        for key, value in row.iteritems():
            # Handle the possibility of a tab at the end of a line, which will
            # show up as an empty value.  Note our upstream utilities shouldn't
            # have these tabs, but this condition was found before we fixed
            # that bug and it doesn't hurt to leave it in.
            if key == '':
                continue
        
            if key not in outDict:
                outDict[key] = []
            
            if value == '':
                outDict[key].append(None)
            else:
                outDict[key].append(float(value))
                
    infile.close()
    
    return outDict
    
# End readStoMotAsDict.

def readStoMotColumnNamesAsList(filePath):
    """
    Reads a storage (STO) or motion (MOT) file and puts the data column names
    into a list.
    
    This is useful if we just want to get the column names without reading the
    whole file.
    """
    
    # Open the file path.  Python will throw if this fails.
    infile = open(filePath, 'r')
    
    # Throw away the header since it doesn't provide much extra information
    # from what we can get in the data itself.  I guess we could do some
    # checks on number of columns, etc.  Maybe later.
    while True:
        line = infile.readline()
        
        if not line:
            raise EOFError('Reached end of file without encountering end of header')
    
        if 'endheader' in line:
            break
            
    reader = csv.DictReader(infile, delimiter='\t')
    
    # Advance one line in order to get the names.
    reader.next()
    return reader.fieldnames
    
# End readStoMotColumnNamesAsList.

def writeDictToStoMot(outDict, filePath, inDegrees=False):
    """
    Write a dictionary to a STO or MOT file.
    
    Choice of STO or MOT is made by the caller in the file path.
    """
    
    outfile = open(filePath, 'w')
    
    outfile.write(os.path.basename(filePath) + '\n')    
    outfile.write('version=1\n')
    
    # We assume that there will be a column named "time" in the dictionary.
    outfile.write('nRows=%d\n' % len(outDict['time']))
    
    # There is time, and then an unknown number of data columns.
    outfile.write('nColumns=%d\n' % len(outDict))
    
    # Let the caller decide if rotational units are in degrees or radians.
    outfile.write('inDegrees=%s\n' % ('yes' if inDegrees else 'no'))
    outfile.write('endheader\n')
    
    # Write out the header.  From here on in, all delimiters are tabs.  Make
    # sure "time" is the first column.
    headerList = ['time']
    for key in sorted(outDict):
        if key == 'time':
            continue
            
        headerList.append(key)
        
    outfile.write('\t'.join(headerList))
    outfile.write('\n')
    
    # Write out the data.  Oh, also note that we can assume that the
    # data length of the time column and all the data columns is the same.
    for ii, time in enumerate(outDict['time']):
        floatDataList = [time]
        for key in sorted(outDict):
            if key == 'time':
                continue
                
            floatDataList.append(outDict[key][ii])
        
        # Convert the list of floats to a list of strings.
        stringDataList = ['%f' % value for value in floatDataList]
        
        outfile.write('\t'.join(stringDataList))
        outfile.write('\n')
    
    outfile.close()

# End writeDictToStoMot.

