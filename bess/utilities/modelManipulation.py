

################################################################################
#
# This script contains common functions and types used when manipulating an
# OpenSim model.
#
################################################################################

import math

import org.opensim.console.gui as gui
import org.opensim.modeling as modeling

class SimObjects:
    """
    Just a bundle of common OpenSim objects.
    """
    def __init__(self):
        self.model = None
        self.state = None
        self.simEngine = None
        self.groundBody = None
        
# End SimObjects.

def getModelAndState(modelPath):    
    """
    Just loads a model and initializes the system.
    """
    
    simObjects = SimObjects()
    
    # Load a model using the API.  Don't use the loadModel() method -- this will
    # load into the UI which probably isn't what we want for this off-line/
    # background type script.  Also protect against the possibility that the
    # model path hasn't been provided -- fallback to trying to load from the
    # OpenSim GUI session.
    if not modelPath or modelPath == 'Unassigned':
        simObjects.model = gui.getCurrentModel()
    else:
        simObjects.model = modeling.Model(modelPath)
        
    if not simObjects.model:
        raise ValueError('Could not load model')
    
    # We need to initialize the system which (I think) actually builds concrete
    # objects in the model like bodies and coordinates).
    simObjects.state = simObjects.model.initSystem()
    
    # The simbody engine is necessary for transforming vectors and positions
    # between bodies.
    simObjects.simEngine = simObjects.model.getSimbodyEngine()
    
    # We often want to get positions in the ground frame, so we need a ground
    # body to do that.  OpenSim 4.0 treats the ground differently -- it's no
    # longer part of the body set.  The exception thrown is a Java
    # runtime exception, not a Python type, so just catch all and try with
    # the OpenSim 4.0 method.
    try:
        simObjects.groundBody = simObjects.model.getBodySet().get('ground')
    except:
        simObjects.groundBody = simObjects.model.ground
    
    return simObjects
    
# End getModelAndState.
    
def setModelCoordinates(simObjects, ikDict, ikIdx):
    """
    Sets the model to new coordinate positions as given by the dictionary
    holding the full IK solution, and a row index.
    """
    
    # Get the coordinate set for the model.
    coordSet = simObjects.model.getCoordinateSet()
    
    # Iterate through the IK dictionary, ignoring the time column.  Everything
    # else should be a coordinate.
    for name, dataList in ikDict.iteritems():
        # Ignore time.
        if name == 'time':
            continue
            
        # Ok, get the object for the named coordinate.
        coord = coordSet.get(name)
        
        # We can't just set the value since we need to distinguish between
        # rotational and translational coordinates, and do a degree to radian
        # conversion for the rotational ones (the IK file is in degrees).
        # If we ever want to bring in a non-degree IK file then we just need to
        # add that info to our readStoMot.py script as metadata.
        motionType = coord.getMotionType()
                    
        if motionType == modeling.Coordinate.MotionType.Rotational:
            coord.setValue(simObjects.state, math.radians(dataList[ikIdx]))
        else:
            coord.setValue(simObjects.state, dataList[ikIdx])
            
# End updateModelCoordinates.

def modifyModelMuscleForces(model, factor):
    """
    Note that the "factor" parameter should be viewed as a fractional decimal 
    percentage.  i.e. if we want to increase all the muscle forces by 10% we
    would pass 0.1.  Negative numbers are ok.
    """
    
    # Get the force set from the model which will contain the muscles.
    forceSet = model.getForceSet()
    
    # Force set contains muscels but also other stuff like actuators.
    muscleSet = forceSet.getMuscles()

    for ii in range(muscleSet.getSize()):
        # For each muscle we're going to get the optimal force and set it.
        muscle = muscleSet.get(ii)
        
        maxForce = muscle.getMaxIsometricForce()
        maxForce += maxForce * factor
        
        if maxForce < 0.0:
            raise ValueError('Muscle force cannot be less than 0.0')
            
        muscle.setMaxIsometricForce(maxForce)
        
    # End for.

# End modifyModelMuscleForces.

