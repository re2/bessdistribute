

################################################################################
#
# This script provides functions for dealing with TRC files.
#
################################################################################

from collections import defaultdict
import os
import sys

def readTrcAsDict(filePath):
    """
    Reads a TRC file and puts the data into a dictionary.
    
    Keys of the dictionary will be names of a data column, and the value will be
    a list of floats.  This function makes assumptions based on the input being
    a valid TRC file format -- it's a bit quick and dirty that way.  Column
    names will be a concatentation of the marker with something like "_x".
    """
        
    # Open the file path.  Python will throw if this fails.
    infile = open(filePath, 'r')
        
    # Throw away the header since it doesn't provide much extra information
    # from what we can get in the data itself.  I guess we could do some
    # checks on number of columns, etc.  Maybe later.  For now just skip the
    # first three lines.
    for idx in range(3):
        line = infile.readline()
        if not line:
            raise EOFError('Reached end of file without encountering end of header')
    
    # Read line 4 to get the marker names and other metadata (frame, time).
    line = infile.readline()
    headerList = line.split('\t')
    
    # Delete "Frame#" and "Time".  Get rid of the redundant tabs.
    del headerList[0:2]
    headerList = [name for name in headerList if name != '' and name != '\n']
        
    # Skip the next two lines in order to get to the data.
    for idx in range(2):
        line = infile.readline()
        if not line:
            raise EOFError('Reached end of file without reaching data')
    
    # This is the dictionary we will add data to.
    outDict = defaultdict(list)
    
    # Now read as much data as we can.
    for idx, line in enumerate(infile):    
        if line == '\n':
            # Handle a blank line gracefully.
            continue
            
        inList = line.split('\t')
        inList.remove('\n')
        
        # Verify there is enough data here.  3 elements for each marker plus
        # a frame and a time.            
        if len(inList) < 3 * len(headerList) + 2:
            raise ValueError('Not enough data on data line %d' % (idx + 1))
            
        outDict['time'].append(float(inList[1]))
        for markerIdx, markerName in enumerate(headerList):
            addValueToList(outDict[markerName + '_x'], inList[markerIdx * 3 + 2])
            addValueToList(outDict[markerName + '_y'], inList[markerIdx * 3 + 3])
            addValueToList(outDict[markerName + '_z'], inList[markerIdx * 3 + 4])
        
    infile.close()
    
    return outDict
    
# End readTrcAsDict.

def addValueToList(outList, value):
    if value == '':
        outList.append(None)
    else:
        outList.append(float(value))
    
# End addValueToList.

