

################################################################################
#
# This script contains common functions and types used when tracking strides
# in a subject's gait.
#
################################################################################

from collections import defaultdict
import copy
import math.sqrt
import os
import sys
from xml.etree import cElementTree as ET

# Add the parent "bess" directory to the Python module search path so that we
# can import modules from different directories.  Subsequent imports of BESS
# scripts may use reload() to ensure that any changes during development get
# reloaded by the interpreter.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
if bessDir not in sys.path:
    sys.path.append(bessDir)

import utilities.toolsCommon as TC; reload(TC)

class Stride:
    """
    A stride is when the foot goes from all the way back all the way forward,
    and vice-versa.
    """
    
    def __init__(self, startTime=None, endTime=None, full=None, forward=None):
        self.startTime = startTime
        self.endTime = endTime
        self.full = full
        self.forward = forward
        
    def isForward(self):
        return self.forward
        
    def getDumpString(self):
        """
        Debug code to printout stride info.
        """
        outString = ('start: %f, end: %f, full: %s, forward: %s' %
            (self.startTime,
            self.endTime,
            self.full,
            self.forward))
                
        return outString
        
    def getStartTime(self):
        return self.startTime
        
    def getEndTime(self):
        return self.endTime
        
    def getDuration(self):
        return self.getEndTime() - self.getStartTime()
        
    def toXmlElement(self):
        root = ET.Element('Stride')
        ET.SubElement(root, 'startTime').text = str(self.startTime)
        ET.SubElement(root, 'endTime').text = str(self.endTime)
        ET.SubElement(root, 'full').text = str(self.full)
        ET.SubElement(root, 'forward').text = str(self.forward)
        
        return root
        
    @staticmethod
    def createFromXmlElement(element):
        # For now the constructor arguments are optional.  So we can create the
        # object and then fill in the data.
        stride = Stride()
           
        stride.startTime = float(element.findtext('startTime'))
        stride.endTime = float(element.findtext('endTime'))
        
        # Be careful here to not try to evaluate the string as a bool() -- all
        # non-empty strings evaluate to True.
        stride.full = (element.findtext('full') == 'True')
        stride.forward = (element.findtext('forward') == 'True')
        
        return stride
        
# End Stride.
    
class StrideTracker:
    """
    Type used to identify and record strides in a gait.
    """
     
    def __init__(self, midpointHysteresis):
        """
        midpointHysteresis should be a tuple of two scalars, the first should 
        be less than the second.  A midpoint will be calculated as the average
        of the two values.
        """
        
        # Verify the hysteresis is a tuple.
        if midpointHysteresis[0] > midpointHysteresis[1]:
            raise ValueError('Midpoint hysteresis is not properly defined')
            
        self.strides = []
        self.midpoint = (midpointHysteresis[0] + midpointHysteresis[1]) / 2.0
        self.midpointHysteresis = midpointHysteresis
        self.currentStride = Stride()
        self.midpointCounter = 0
        self.lastValue = None
        self.hysteresisActive = False
        self.fullForwardStrideAverageDuration = 0.0
        self.numFullForwardStrides = 0
        self.fullReverseStrideAverageDuration = 0.0
        self.numFullReverseStrides = 0
        
        # From a validation standpoint it can be helpful to see what the
        # minimum stride duration is to see if we had any problems -- really
        # no stride should be less than 0.3 seconds I think.
        self.fullStrideMinDuration = None
        
    def toXmlElement(self):
        """
        Serialize this stride tracker to an XML ElementTree element.
        
        Useful for encoding strides to a file.
        """
        root = ET.Element('StrideTracker')
        
        # Ack, I hate this type of code.  It would be better (maybe) if all
        # these class members were just in a dictionary.  Oh well.
        ET.SubElement(root, 'midpoint').text = str(self.midpoint)
        ET.SubElement(root, 'midpointHysteresis').text = str(
            self.midpointHysteresis)        
        ET.SubElement(root, 'midpointCounter').text = str(self.midpointCounter)
        ET.SubElement(root, 'lastValue').text = str(self.lastValue)
        ET.SubElement(root, 'hysteresisActive').text = str(
            self.hysteresisActive)
        ET.SubElement(root, 'fullForwardStrideAverageDuration').text = str(
            self.fullForwardStrideAverageDuration)
        ET.SubElement(root, 'numFullForwardStrides').text = str(
            self.numFullForwardStrides)
        ET.SubElement(root, 'fullReverseStrideAverageDuration').text = str(
            self.fullReverseStrideAverageDuration)
        ET.SubElement(root, 'numFullReverseStrides').text = str(
            self.numFullReverseStrides)
        ET.SubElement(root, 'fullStrideMinDuration').text = str(
            self.fullStrideMinDuration)
        seStrides = ET.SubElement(root, 'strides')
        for stride in self.strides:
            seStrides.append(stride.toXmlElement())
            
        return root
        
    @staticmethod
    def createFromXmlElement(element):
        """
        Factory function for creating a StrideTracker from an XML element.
        
        We do this as a static factory since we may need to pass values to
        the constructor of the class so this needs to be a factory.
        """
        
        # We need to pass midpoint hysteresis to the StrideTracker constructor
        # so suss it out if we can.  It's encoded as a string of a tuple:
        # '(0.5 1.0)'.  We can use eval() to turn it into an actual tuple.
        midpointHysteresisString = element.findtext('midpointHysteresis')
        strideTracker = StrideTracker(eval(midpointHysteresisString))
        strideTracker.midpoint = float(element.findtext('midpoint'))
        strideTracker.midpointCounter = int(element.findtext('midpointCounter'))
        
        text = element.findtext('lastValue')
        if text == 'None':
            strideTracker.lastValue = None
        else:
            strideTracker.lastValue = float(text)
            
        strideTracker.hysteresisActive = (
            element.findtext('hysteresisActive') == 'True')
        strideTracker.fullForwardStrideAverageDuration = float(
            element.findtext('fullForwardStrideAverageDuration'))
        strideTracker.numFullForwardStrides = int(
            element.findtext('numFullForwardStrides'))
        strideTracker.fullReverseStrideAverageDuration = float(
            element.findtext('fullReverseStrideAverageDuration'))
        strideTracker.numFullReverseStrides = int(
            element.findtext('numFullReverseStrides'))
        
        text = element.findtext('fullStrideMinDuration')
        if text == 'None':
            strideTracker.fullStrideMinDuration = None
        else:
            strideTracker.fullStrideMinDuration = float(text)
        
        # Now let's find the subelement that holds our strides.
        stridesElement = element.find('strides')
        
        # Iterate over its subelements -- these are the strides.
        for strideElement in stridesElement.getchildren():
            strideTracker.strides.append(Stride.createFromXmlElement(
                strideElement))        
                
        return strideTracker
        
    @staticmethod
    def writeDictToXmlFile(strideDict, filePath):
        """
        Takes a dictionary of stride trackers and writes as XML to a file.
        """
        root = ET.Element('BESS_StrideTrackers', {'Version': '1'})
        for name, strideTracker in strideDict.iteritems():
            # Create an element for each instance.
            stElement = strideTracker.toXmlElement()
            # Add in the name of this stride tracker since we don't embed
            # that with the object itself.
            stElement.set('name', name)
            root.append(stElement)
        
        TC.indentXml(root)
        tree = ET.ElementTree(root)
        tree.write(filePath, encoding='UTF-8')
    
    @staticmethod
    def readDictFromXmlFile(filePath):
        """
        Reads an XML file to create a dictionary of stride trackers.
        """
        
        tree = ET.ElementTree(file=filePath)
        root = tree.getroot()
        outDict = dict()
        
        # Iterate over the children -- these are just serialized stride tracker
        # objects.
        for child in root.getchildren():
            # Pull out the name of this stride tracker and create a concrete
            # object from it.
            outDict[child.get('name')] = StrideTracker.createFromXmlElement(child)
    
        return outDict
        
    @staticmethod
    def writeDictToStoDict(strideTrackers):
        """
        Takes a dictionary of stride tracker objects and writes them to a
        dictionary appropriate for writing to a STO file.
        
        This is useful if we want to visualize the strides as a function of
        time to visually validate that the stride demarcations make sense.
        """
        
        # Let's make sure each of the stride trackers has valid data which
        # means it has strides and the start and end times are all the same.
        firstTime = lastTime = None
        for strideTracker in strideTrackers.values():
            if len(strideTracker.strides) == 0:
                raise ValueError('Stride tracker does not have any strides')
                
            if not firstTime:
                firstTime = strideTracker.getFirstTime()
            elif firstTime != strideTracker.getFirstTime():
                raise ValueError('Stride tracker does not have same first time')
                
            if not lastTime:
                lastTime = strideTracker.getLastTime()
            elif lastTime != strideTracker.getLastTime():
                raise ValueError('Stride tracker does not have same last time')
        
        outDict = defaultdict(list)
        
        # Track which stride we are on for each of the stride trackers as we
        # move forward in time.
        strideIndexDict = defaultdict(int)
        
        # We just use a simple integer that we increment or decrement based on
        # forward and reverse strides.  So a healthy stride tracker should have
        # a value bouncing between 0 and 1 or 0 and -1.
        strideValueDict = defaultdict(int)
        
        # Compose a list of all the possible times.  Start off with a set to
        # eliminate duplicates and then sort into a list to get a monotonically
        # increasing time.
        timeSet = set()
        for strideTracker in strideTrackers.values():
            timeSet = timeSet.union(strideTracker.getStrideStartTimes())
        
        outDict['time'] = sorted(list(timeSet))
        
        # Ok, we have all the times that we need to account for.  Let's iterate
        # through it and identify the stride index for each stride tracker.
        for time in outDict['time']:
            for name, strideTracker in strideTrackers.iteritems():
                strideIdx = strideIndexDict[name]
                stride = strideTracker.strides[strideIdx]
                
                if time >= stride.getEndTime():
                    strideIdx += 1
                    stride = strideTracker.strides[strideIdx]
                    if stride.forward:
                        strideValueDict[name] += 1
                    else:
                        strideValueDict[name] -= 1
                        
                outDict[name].append(strideValueDict[name])                    
                strideIndexDict[name] = strideIdx
          
        return outDict
        
    def addValue(self, time, value):
        # Don't do anything if this is the first time other than record the
        # value.
        if not self.lastValue:      
            self.lastValue = value
            self.currentStride.startTime = self.currentStride.endTime = time
            return
            
        # No matter what, extend the end time of the current stride.  One of
        # the side effects of this is that one stride's end time will equal
        # one stride's start time, which is by design (or at least assumed by
        # downstream users).
        self.currentStride.endTime = time
            
        # Look to see if we crossed the midpoint.
        if not self.hysteresisActive:
            # We only look for midpoint crossings when we haven't already 
            # crossed recently.
            if (value >= self.midpoint and self.lastValue < self.midpoint or
                value <= self.midpoint and self.lastValue > self.midpoint):
                # Ok, found a crossing.
                self.midpointCounter += 1
                self.hysteresisActive = True
                
                # Figure out if the current stride is forward or reverse.
                # A forward stride is when go from a high value to a low value
                # (since we are using a force plate to judge).
                self.currentStride.forward = (value >= self.midpoint and self.lastValue < self.midpoint)
                self.currentStride.full = (self.midpointCounter > 1)
                self.addStride(self.currentStride)
                self.currentStride = Stride(startTime=time, forward=(not self.currentStride.forward))
        else:
            # Ok, hysteresis is active.  That means we should look out to see
            # if we've left the hysteresis band.
            if (value > self.midpointHysteresis[1] or
                value < self.midpointHysteresis[0]):
                self.hysteresisActive = False
                
        # No matter what, update the last value for the next addition.
        self.lastValue = value
            
    def addStride(self, stride):
        """
        Just a gatekeeper function that makes sure a completed stride is
        added to the list properly.
        """
        self.strides.append(copy.copy(stride))
        
        if stride.isForward():
            newAvg = (((self.fullForwardStrideAverageDuration * 
                self.numFullForwardStrides) + stride.getDuration()) /
                (self.numFullForwardStrides + 1))
            self.fullForwardStrideAverageDuration = newAvg
            self.numFullForwardStrides += 1
        else:
            newAvg = (((self.fullReverseStrideAverageDuration * 
                self.numFullReverseStrides) + stride.getDuration()) /
                (self.numFullReverseStrides + 1))
            self.fullReverseStrideAverageDuration = newAvg
            self.numFullReverseStrides += 1
    
        if stride.full:
            self.fullStrideMinDuration = TC.minNone((stride.getDuration(),
                self.fullStrideMinDuration))
    
    def getFirstTime(self):
        if not self.strides:
            return None
            
        return self.strides[0].getStartTime()
        
    def getLastTime(self):
        if not self.strides:
            return None
            
        return self.strides[-1].getEndTime()
        
    def completeStrides(self):
        """
        Called at the end of motion to record the final stride.
        """
        self.currentStride.full = False
        self.addStride(self.currentStride)
        
    def getFullStrideAverageDurations(self):
        """
        For all full strides, averages the amount of time to complete the 
        stride.
        
        The return distinguishes between forward and reverse strides.
        """
                        
        if self.numFullForwardStrides == 0 or self.numFullReverseStrides == 0:
            raise ValueError('No full strides to get average duration')
            
        return (self.fullForwardStrideAverageDuration, 
            self.fullReverseStrideAverageDuration)
                
    def getStrideStartTimes(self):
        """
        Useful to get a list of times that each stride starts.
        """
        return [stride.getStartTime() for stride in self.strides]
        
    def dumpStrides(self):
        """
        Some debug code for writing out stride statistics.
        """
        
        for ii, stride in enumerate(self.strides):
            outString = '%d: %s' % (ii, stride.getDumpString())
            print outString
                    
    @staticmethod
    def getPercentCompleteByTime(stride,
        time,
        averageStrideDurations,
        firstStride,
        normalizeToFullGait=True):
        """
        Figure out what percentage of a stride is completed based on the time.
        """
        # Figure out if this is a partial stride.  If so we need to estimate
        # the percent complete from average stride durations.
        if stride.full:
            # Full stride.  This is an easy one.
            stridePercent = ((time - stride.getStartTime()) /
                (stride.getEndTime() - stride.getStartTime())) * 100.0
        else:
            # Partial stride.  Figure out what average stride duration to use:
            # forward and reverse strides tend to take different times.
            if stride.isForward():
                averageDuration = averageStrideDurations[0]
            else:
                averageDuration = averageStrideDurations[1]
                
            startTime = stride.getStartTime()
            endTime = stride.getEndTime()
            
            # Partial strides can be at the beginning or the end.  Depending
            # on which this is, we will get a different percent.
            if firstStride:
                startTime = endTime - averageDuration
                if startTime < 0:
                    # I doubt this can or will happen, but protect anyway.
                    startTime = 0                
            else:
                endTime = startTime + averageDuration
                
            stridePercent = ((time - startTime) / (endTime - startTime)) * 100.0
        
        # Ok, we have a percent completion for an individual stride.  If we
        # want the percent of a gait cycle, we need to "combine" a forward stride
        # and reverse stride together.  These aren't just halves, they are
        # unevenly distributed:  a forward stride is significantlyshorter in
        # time than a reverse stride.  By convention, it is the start of the
        # reverse stride that is the 0% point.
        if normalizeToFullGait:
            strideRatio = averageStrideDurations[1] / sum(averageStrideDurations)
            
            if not stride.isForward():
                stridePercent *= strideRatio
            else:
                stridePercent = strideRatio * 100.0 + (1.0 - strideRatio) * stridePercent
        
        return max(min(stridePercent, 100), 0)
        
# End StrideTracker.

class StrideHistogramBinStatistics:

    def __init__(self):
        self.numElements = 0
        self.minValue = self.maxValue = None
        self.M = self.S = 0
        
    def addValue(self, value):
        if self.numElements == 0:
            self.minValue = self.maxValue = value
        else:             
            self.minValue = TC.minNone((self.minValue, value))
            self.maxValue = TC.maxNone((self.maxValue, value))
            
        self.numElements += 1
        
        # Use Welford's algorithm to compute variance.  Who knew a single
        # pass variance calculation was so complicated?  In this notation,
        # M is mean.
        oldM = self.M
        self.M = self.M + (value - self.M) / self.numElements
        self.S = self.S + (value - self.M) * (value - oldM)
        
    def getAverage(self):
        if self.numElements == 0:
            return None
            
        return self.M
        
    def getVariance(self):
        if self.numElements == 0:
            return None
            
        return self.S / self.numElements
            
    def getStdDev(self):
        if self.numElements == 0:
            return None
            
        return math.sqrt(self.getVariance())
    
class StrideHistogramBin:
    """
    Collects/averages the load cell data for a small range of motion during
    a stride.
    """
    
    def __init__(self, minPercent, maxPercent):
        self.minPercent = minPercent
        self.maxPercent = maxPercent
        self.midPercent = (maxPercent + minPercent) / 2.0
        self.statistics = defaultdict(StrideHistogramBinStatistics)

    def addValues(self, valueList):
        """
        Takes a list of tuples (name, value) representing the data of the 
        current time instance.
        """        
        
        for name, newValue in valueList:
            self.statistics[name].addValue(newValue)
        
    def hasData(self):
        return bool(self.statistics)
        
    def copyAverageValuesFromBin(self, sourceBin):
        """
        Used during interpolation/extrapolation to set an empty bin to the
        contents of another bin.
        """
        
        self.statistics = defaultdict(StrideHistogramBinStatistics)
        for name in sourceBin.statistics:
            self.statistics[name].addValue(sourceBin.statistics[name].getAverage())
        
    def interpolateAverageValuesFromBins(self, leftBin, rightBin):
        """
        Used during interpolation to set an empty bin to a value between the
        contents of two surrounding bins.
        """
        
        # Calculate an interpolation ration based on the bin indices.
        ratio = (self.midPercent - leftBin.midPercent) / (rightBin.midPercent - 
            leftBin.midPercent)
        
        self.statistics = defaultdict(StrideHistogramBinStatistics)
        
        # We need to iterate over all the quantities binned in this histogram.
        # We assume that all bins will bin the same named quantities (i.e. the
        # dictionaries will all have the same keys).
        for name in leftBin.statistics:
            leftValue = leftBin.statistics[name].getAverage()
            rightValue = rightBin.statistics[name].getAverage()
            
            self.statistics[name].addValue(leftValue + (rightValue - leftValue) * ratio)
        
# End StrideHistogramBin.

class StrideHistogram:
    """
    Contains (potentially) irregularly spaced bins of data where bin size 
    is parameterized as a percentage of a stride.
    """
    
    def __init__(self, binList, regularSpacing=False):
    
        # binList should be a list of numbers sorted in ascending order, 
        # beginning with 0.  The last number should be equal to 100.  Each 
        # number is a percent value that defines how we break up bins.
        # Example:  [0, 25, 75, 100]
        if len(binList) < 2 or binList[0] != 0 or binList[-1] != 100:
            raise ValueError('Bin list is not formatted properly')
            
        self.binList = binList
        self.regularSpacing = regularSpacing
        if self.regularSpacing:
            self.binSpacing = binList[1] - binList[0]
            
        self.bins = []
        
        for binIndex in range(len(binList) - 1):
            minPercent = float(binList[binIndex])
            maxPercent = float(binList[binIndex + 1])
            self.bins.append(StrideHistogramBin(minPercent, maxPercent))
        
    def getBin(self, percent):
        if percent < 0.0 or percent > 100.0:
            raise ValueError('Stride percent must be in range of [0, 100]')
        
        if self.regularSpacing:
            binIndex = int(percent / self.binSpacing)
            if binIndex == len(self.bins):
                binIndex -= 1
                
            return self.bins[binIndex]
            
        # Since our bins are potentially irregular we need to search for it... :(
        for bin in self.bins:
            if percent >= bin.minPercent and percent <= bin.maxPercent:
                return bin
                
        raise ValueError('Could not find bin for stride percentage')

    def interpolateEmptyBins(self):
        """
        Go through the bins and populate empty bins based on linear 
        interpolation.
        
        Empty bins at the start and end won't be extrapolated, rather they will
        just take the value of the next bin.
        """
        
        # Use three indices:  right, current, and left.  The right index will
        # be the leader, seeking out for a non-empty bin.  The left index will
        # be the trailer, staying at the previous non-empty bin.  The current
        # index will be the one that determines when an interpolation is called
        # for.
        
        leftIdx = None
        currentIdx = -1
        for rightIdx in range(len(self.bins)):
            rightBin = self.bins[rightIdx]
            rightBinOccupied = rightBin.hasData()
                        
            if rightBinOccupied:
                # Bring the current and left indexes to the right index position.
                
                if not leftIdx:
                    # Handle the special case of starting with empty bins at 
                    # the start of the list.
                    while currentIdx < rightIdx - 1:
                        currentIdx += 1
                        
                        # Set the current bin to the left bin.
                        currentBin = self.bins[currentIdx]
                        currentBin.copyAverageValuesFromBin(rightBin)
                        
                    # Ok, bring the left index up to the right index.  All three
                    # indexes are aligned.
                    leftIdx = currentIdx = rightIdx
                    continue
                
                # Look to see if we need to interpolate at all.  If the
                # right index is only one place greater than the current
                # than we don't have any missing data.
                if rightIdx == leftIdx + 1:
                    leftIdx = currentIdx = rightIdx
                    continue
                    
                # Ok, we need to interpolate.
                while currentIdx < rightIdx - 1:
                    currentIdx += 1
                    currentBin = self.bins[currentIdx]
                    
                    currentBin.interpolateAverageValuesFromBins(
                        self.bins[leftIdx],
                        self.bins[rightIdx])

                # Ok, we've filled in all the values.  Back to advancing the
                # right index.
                leftIdx = currentIdx = rightIdx
                continue        
            else:
                # If the right index bin is not occupied we should just move
                # on, unless we are at the end of the bin list.
                if rightIdx != len(self.bins) - 1:
                    continue
                    
                # Make sure there is data somewhere in the bins!
                if not leftIdx:
                    # No data at all, just bail.
                    break
                    
                # Advance the current index, writing values as we go based on
                # the left index.
                leftBin = self.bins[leftIdx]
                while currentIdx < rightIdx:
                    currentIdx += 1
                    
                    # Set the current bin to the left bin.
                    currentBin = self.bins[currentIdx]
                    currentBin.copyAverageValuesFromBin(leftBin)
                    
                # Ok, we're done -- we've run out the list.
                break
    
# End StrideHistogram.

