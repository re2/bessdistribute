

################################################################################
#
# This script contains common functions and types used when analyzing data
# on a per-stride basis.
#
################################################################################

from collections import defaultdict
import os
import sys

# Add the parent "bess" directory to the Python module search path so that we
# can import modules from different directories.  Subsequent imports of BESS
# scripts may use reload() to ensure that any changes during development get
# reloaded by the interpreter.
import inspect
pathList = os.path.abspath(inspect.getfile(inspect.currentframe())).split(os.path.sep)
bessDir = os.path.sep.join(pathList[0:len(pathList)-pathList[::-1].index('bess')])
if bessDir not in sys.path:
    sys.path.append(bessDir)

import utilities.strideTracker as ST; reload(ST)
import utilities.toolsCommon as TC; reload(TC)

class StridifyInputData:
    """
    This class bundles together the data that defines what (and how) data
    should be put on a gait cycle basis.
    """
    def __init__(self, strideName=None, dataDict=None, dataNames=None):
        
        # Name of the stride we want to use for the underlying gait cycle.
        self.strideName = strideName
        
        # A data dictionary -- likely coming directly from a STO/MOT file (i.e.
        # has a column named "time", etc.).
        self.dataDict = dataDict
        
        # Key names in the data dictionary.  Ostensibly these are the column
        # headers in the STO/MOT file.  These variable names are the ones that
        # are going to be put on a gait cycle basis.
        self.dataNames = dataNames

# End StridifyInputData.

def stridifyData(strideTrackers, inputData):
    """
    Takes input data dictionaries and normalizes select data as a percentage of
    gait cycle.
    
    strideTrackers is expected to be a dictionary of StrideTracker objects.
    Presumably the keys in the dictionary will be "right" and "left".
    
    inputData is an iterable (list, tuple, etc.) of StridifyInputData objects.
    """
    
    # Determine what time bounds to apply to the common time base based on the
    # start and end times of the stride trackers.
    strideMaxStartTime = strideMinEndTime = None
    for st in strideTrackers.values():
        strideMaxStartTime = TC.maxNone((strideMaxStartTime, st.getFirstTime()))
        strideMinEndTime = TC.minNone((strideMinEndTime, st.getLastTime()))
        
    if strideMaxStartTime == None or strideMinEndTime == None:
        raise ValueError('Cannot determine start or end times of strides')
    
    # Come up with a common time base for all of our input datasets.
    commonTimeList = TC.createCommonTimeBase(
        [object.dataDict['time'] for object in inputData],
        allUnique=True,
        minTimeLimit=strideMaxStartTime,
        maxTimeLimit=strideMinEndTime
        )
                 
    # Use a worker to do the hardwork of binning all our input data.
    return binData(commonTimeList, strideTrackers, inputData)
         
# End stridifyData.

def stridifyDataFromFiles(strideFilePath, inputData):
    pass
    
# End stridifyDataFromFiles.

class StrideBinCallback:
    """
    Bundles together an input data dictionary with the callback to be called
    when new data is iterated to.
    """
    def __init__(self, inputData):
        self.inputData = inputData
                
        # Current copy of the data we're interested in which gets updated when
        # the iteration comes to a time for which we have new data.
        self.currentData = defaultdict(float)
        
    def dataCallback(self, commonTimeIdx, dataIdx):
        """
        Gets called by the iteration when it has a time for which we have new
        data.
        """        
        for name in self.inputData.dataNames:
            self.currentData[name] = self.inputData.dataDict[name][dataIdx]
        
# End StrideBinCallback.

class StrideBinBundler:
    """
    For each stride tracker we bundle a batch of data which brings together
    stride callbacks with data callbacks.
    """    
    def __init__(self, strideTracker, strideInputData):
        
        # There's one of these bin bundler objects for each stride tracker.
        self.strideTracker = strideTracker
        
        # One of the things this object tracks is what stride is under 
        # consideration at the current time in the iteration.
        self.currentStrideIdx = None
        
        # When computing percent completion of a gait cycle we need to be able
        # to normalize to how long the average strides take.
        self.averageStrideDurations = strideTracker.getFullStrideAverageDurations()
        
        # We partition the gait cycle by percent complete, hence the range of
        # [0 100]. 
        self.timeHistogram = ST.StrideHistogram(range(101))
        
        # Compose a list of callback objects from the input data.
        self.strideBinCallbacks = [StrideBinCallback(data) for data in strideInputData]        
        
    def strideCallback(self, commonTimeIdx, strideIdx):
        """
        As we iterate we are constantly checking to see if the current time
        point means we have moved onto the next stride.
        """        
        self.currentStrideIdx = strideIdx
                        
    def getTimePercentComplete(self, time):
        """
        For a given time (and the current stride) figure out what percentage
        of the gait cycle is completed).
        """        
        return ST.StrideTracker.getPercentCompleteByTime(
            self.strideTracker.strides[self.currentStrideIdx],
            time,
            self.averageStrideDurations,
            self.currentStrideIdx == 0,
            normalizeToFullGait=True)
        
# End StrideObject.
        
def binData(commonTimeList, strideTrackers, inputData):    
    
    # Create a "bin bundler" object for each stride tracker.  We'll likely
    # have two of these, one for each of "right" and "left".
    strideBinBundles = dict()
    for strideName, strideTracker in strideTrackers.iteritems():
        strideData = [sd for sd in inputData if sd.strideName == strideName]
        strideBinBundles[strideName] = StrideBinBundler(strideTracker, strideData)
               
                    
    def commonCallback(commonTimeIdx):
        commonTime = commonTimeList[commonTimeIdx]
        
        # Each time step in the iteration we put data into the histogram bins.
        for strideBinBundle in strideBinBundles.values():            
            timeBin = strideBinBundle.timeHistogram.getBin(
                strideBinBundle.getTimePercentComplete(commonTime))
                
            for binCallback in strideBinBundle.strideBinCallbacks:
                timeBin.addValues(binCallback.currentData.items())
            
    # End commonCallback.
                    
    # Compose the dynamic list of callbacks in preparation for iterating over
    # multiple data sets and the stride trackers.
    dataCallbackList = []
    for strideBinBundle in strideBinBundles.values():
        # Each stride will have a stride callback where we set which stride
        # matches the iteration time.
        dataCallbackList.append((
            strideBinBundle.strideTracker.getStrideStartTimes(),
            strideBinBundle.strideCallback))
            
        for strideBinCallback in strideBinBundle.strideBinCallbacks:
            dataCallbackList.append((
                strideBinCallback.inputData.dataDict['time'],
                strideBinCallback.dataCallback))
    
    # Do the actual iteration.
    TC.iterateMultipleDataSources(commonTimeList, 
        dataCallbackList,
        commonCallback)
   
    # Ok, we've put all our data into histograms.  Let's make sure that
    # all bins have data.
    for strideBinBundle in strideBinBundles.values():
        strideBinBundle.timeHistogram.interpolateEmptyBins()
   
    # Populate an output dictionary as we iterate through the finished
    # histograms.  The top-level dictionary will have an entry for each stride
    # and will hold a dictionary of data.
    outDict = defaultdict(dict)
    for strideName, strideBinBundle in strideBinBundles.iteritems():
        outDict[strideName] = defaultdict(list)
        
        for binIdx, bin in enumerate(strideBinBundle.timeHistogram.bins):
            # Write out an effective time for what chunk of motion this bin
            # corresponds to.  A STO/MOT file always needs to have a time column
            # and sometimes it is easier just to graph against a time x-axis than
            # a percent (since time pops up at the top of the variable list in
            # OpenSim).  We can use the average stride durations to get a normalized
            # time.
            outDict[strideName]['time'].append(bin.minPercent * 
                sum(strideBinBundle.averageStrideDurations) / 100.0)
            
            # Write out the percent of gait this bin corresponds to.  Let's just
            # use the minPercent -- with a histogram we're always going to leave out
            # 0% or 100% since we are effectively making a scatter plot.  NBD.
            outDict[strideName]['percent'].append(bin.minPercent)
            
            for itemName, statistic in bin.statistics.iteritems():
                outDict[strideName][itemName + '_avg'].append(statistic.getAverage())
                outDict[strideName][itemName + '_avg_psigma'].append(statistic.getAverage() + 
                    statistic.getStdDev())
                outDict[strideName][itemName + '_avg_nsigma'].append(statistic.getAverage() - 
                    statistic.getStdDev())
    
    return outDict
    
# End binData.



