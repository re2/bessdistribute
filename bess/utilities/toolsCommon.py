

################################################################################
#
# This script contains common functions and types used in other Python scripts.
#
################################################################################

import math
import os
import sys

import java.util.prefs.Preferences as JPreferences;

from javax.swing import JFileChooser
from javax.swing.filechooser import FileNameExtensionFilter

from org.opensim import modeling
import org.opensim.utils.FileUtils as FileUtils
import org.opensim.utils.Prefs as Prefs

class FileFilter:        
    """
    Type to represent how and when a file extension filter is to be used.
    """

    def __init__(self, filterDescriptionString=None, filterExtensionString=None):
        self.filterDescriptionString = filterDescriptionString
        
        # A comma separated list of extensions, without a ".".  For example:
        # "txt"
        # "txt,csv"
        # We also remove any whitespace in case someone write 'txt, csv'.
        self.filterExtensionString = filterExtensionString.replace(' ', '')
        
# End FileFilter.

def getSingleFilePath(promptString, fileFilter):
    """
    Gets a single file path by showing a file browser to the user.  
    
    Function is expected to raise if a non-empty path is not provided by the user.
    """
            
    # Create and setup the file chooser.  The "preferred" directory is the one
    # OpenSim (and our utilities) set after a successful file browse.  Without
    # this you would have to start in your home directory for every file browse
    # which is a pain.
    fileFullPath = ''
    initialDirectory = Prefs.getInstance().getPreferredDirectory()
    fileChooser = JFileChooser(initialDirectory)
    fileChooser.setDialogTitle(promptString)
    
    # Optionally filter based on a file type.
    if fileFilter.filterExtensionString:
        # The filter extensions needs to be a string list, even if it's a 
        # single element.
        filter = FileNameExtensionFilter(fileFilter.filterDescriptionString, 
            fileFilter.filterExtensionString.split(','))
        
        # SetFileFilter will default to filtering but the user can still click
        # on "All files".  This is preferred to addChoosableFileFilter() which
        # will default to "All files".
        fileChooser.setFileFilter(filter)
            
    # Show the file chooser and save the path.
    if(fileChooser.showOpenDialog(fileChooser) == JFileChooser.APPROVE_OPTION):
        fileFullPath = fileChooser.getSelectedFile().getCanonicalPath()
        
    # Do a basic sanity check -- right now an empty string which means they 
    # hit "Cancel".
    if not fileFullPath:
        raise ValueError('File path may not be empty')

    # Follow the OpenSim convention of changing the working directory based
    # on the user's last file browse so that they pick up in this directory
    # the next time.
    FileUtils.getInstance().setWorkingDirectoryPreference(
        fileChooser.getCurrentDirectory().getCanonicalPath())
        
    return fileFullPath
    
# End getSingleFilePath.

def getFilePaths(sourceFileFilter, destinationFileFilter):
    """
    Gets the full file paths for source and destination files.  
    """
    
    sourceFileFullPath = ''
    destinationFileFullPath = ''
    
    # Get SOURCE file path.
    sourcePromptString = 'Select SOURCE file to convert FROM'
    sourceFileFullPath = getSingleFilePath(sourcePromptString, sourceFileFilter)
            
    # Get DESTINATION file path.
    destinationPromptString = 'Select DESTINATION file to convert TO'
    destinationFileFullPath = getSingleFilePath(destinationPromptString, destinationFileFilter)
    
    # Check if the output file has the correct extension -- it's easy for the user
    # to neglect to type that in.
    if destinationFileFilter.filterExtensionString and os.path.splitext(destinationFileFullPath)[1] == '':
        destinationFileFullPath += '.' + destinationFileFilter.filterExtensionString   
        
    return sourceFileFullPath, destinationFileFullPath
    
# End getFilePaths.

def indentXml(elem, level=0):
    """
    Taken from http://effbot.org/zone/element-lib.htm#prettyprint
    
    Using this is helpful when using an ElementTree to write to a file.
    """
    
    i = "\n" + level*"  "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for elem in elem:
            indentXml(elem, level+1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i

# End indentXml.

def minNone(iterable):
    """
    Min numbers, accomodating for some of them being None.
    """
    return min(x for x in iterable if x is not None)
    
# End minNone.
    
def maxNone(iterable):
    """
    Max numbers, accomodating for some of them being None.
    """
    return max(x for x in iterable if x is not None)
            
# End maxNone.
            
def filterDataColumn(data, tau):
    """
    Filter a provided list of data and produce a new list of data containing the
    smoothed elements.
    """
    
    outList = []
    
    for index, value in enumerate(data):
        if index == 0:
            # The filter introducues a 1-element delay.
            outList.append(data[index])
            continue            
        else:
            # Use a basic exponential filter, which is a first-order system.
            # If x is the input series, and y is the output series, and n is
            # index, the filter is:
            # y[n] = y[n-1] * (1 - tau) + x[n] * tau.
            # Since we're doing this as an in-place operation, x and y are the
            # same here.
            # Tau is in the range of (0, 1].  So, if tau is 1, then we have a
            # passthrough and if it is 0 we have an output that never changes.
            x_n = data[index]
            y_n_1 = outList[index - 1]
            y_n = y_n_1 * (1 - tau) + x_n * tau
            outList.append(y_n)
            
    return outList
    
# End filterDataColumn.

def saturateDataColumn(data, minBound, maxBound):
    """
    Bound each value in the list of data to a specified range.
    """

    outList = []
    
    for index, value in enumerate(data):
        if value > maxBound:
            boundedValue = maxBound
        elif value < minBound:
            boundedValue = minBound
        else:
            boundedValue = value
            
        outList.append(boundedValue)
        
    return outList

# End saturateDataColumn.

def rectifyDataColumn(data, fullRectify=True):
    """
    Rectify the data column.  If fullRectify is true then negative values will
    become positive values.  If false, then negative values will become 0.
    """

    outList = []
    
    for value in data:
                
        if value >= 0:
            rectifiedValue = value
        elif fullRectify:
            rectifiedValue = -value
        else:
            rectifiedValue = 0
            
        outList.append(rectifiedValue)
        
    return outList
    
def rmsCentralMovingAverageDataColumn(data, maxWindow):
    """
    Average a column of data using a moving window.  The average of elements
    in the window is an RMS value and applied to the index in the center of
    the window.
    """

    # The window has to be an odd number.
    if maxWindow % 2 != 1:
        raise ValueError('Window size must be an odd positive integer')
        
    outList = []
    rightIdx = -1
    currentIdx = rightIdx - maxWindow / 2
    
    sumOfSquares = 0
    numElements = 0
    
    while currentIdx < len(data) - 1:
        # Advance our index.    
        rightIdx += 1
        currentIdx += 1
        leftIdx = rightIdx - maxWindow + 1
        
        # Figure out if we should add anything in to the sum from the right 
        # index.
        if rightIdx < len(data):
            sumOfSquares += data[rightIdx] * data[rightIdx]
            numElements += 1
            
        # Figure out if we need to remove anything from the sum from the left.
        if leftIdx >= 0:
            sumOfSquares -= data[leftIdx] * data[leftIdx]
            numElements -= 1
        
        # Using a moving window with a bunch of adding from the right end and
        # subtracting from the left end it looks like we can get very smallest
        # numerical errors.  If the window then goes over a large stretch of
        # zero-value data, we could be left with a sumOfSquares microscopically 
        # less than zero:  for example, when processing some data I saw 
        # a sum of squares of -1.53e-21 in a patch of zero-value data.  
        # So make sure the number is never less than zero to account for this 
        # use case.
        if sumOfSquares < 0:
            sumOfSquares = 0

        # Figure out if we are at the current index has reached the start point
        # yet.
        if currentIdx >= 0:
            rms = math.sqrt(sumOfSquares / numElements)
            outList.append(rms)
                
    return outList
    
# End rmsCentralMovingAverageColumn.

def calculateStatsDataColumn(data):
    """
    Does a single pass over a column of data and calculates stats like mean 
    and standard deviation.
    """
    
    numElements = 0
    minValue = maxValue = None
    M = S = 0
    sumSquares = 0
        
    for ii, value in enumerate(data):
        if numElements == 0:
            minValue = maxValue = value
        else:             
            minValue = min(minValue, value)
            maxValue = max(maxValue, value)
            
        numElements += 1
        sumSquares += value * value
        
        # Use Welford's algorithm to compute variance.  Who knew a single
        # pass variance calculation was so complicated?  In this notation,
        # M is mean.
        oldM = M
        M = M + (value - M) / numElements
        S = S + (value - M) * (value - oldM)

    rms = math.sqrt(sumSquares/ numElements)
    if numElements == 0:
        return None, None, None, None, None
    else:
        return M, math.sqrt(S / numElements), minValue, maxValue, rms
        
def createCommonTimeBase(timeLists, 
    allUnique=True, 
    settlingTimeOffset=0, 
    minTimeLimit=None,
    maxTimeLimit=None):
    """
    When working with multiple STO or MOT files it is helpful to identify a
    common time base among all files.  This function creates a new list of
    unique, monotonically increasing times such that the new time list will
    be withing the [min,max] of each individual time list.
    
    The new list can be an "all unique" list or not.  An "all unique" list 
    will contain all unique times from the input time lists.  If it is not
    an "all unique" list, then the new list will only contain the times found
    in the list with the highest average frequency of times (still subject to
    the common min/max).
    
    Take inputs A and B, and the resulting created time lists with "all unique"
    and "not all unique".
    
    A       B       AU      NAU
    -       0       -       -
    -       1       -       -
    -       2       -       -
    3       3       3       3
    3.75    -       3.75    3.75
    -       4       4       -
    4.5     -       4.5     4.5
    -       5       5       -
    5.25    -       5.25    5.25
    6       6(end)  6       6
    6.75    -       -       -
    7.5(end)-       -       -
    
    The timeLists input should be formatted as a list of lists (though other
    iterables are probably fine) where each list is monotonically increasing.
    timeLists = [[3,3.75,4.5,5.25,6,6.75,7.5], [0,1,2,3,4,5,6]]
    
    The function returns a new list, not a reference to one of the input lists.
    """
        
    # Identify the largest minimum time of the three data sets and do the same
    # for the smallest maximum time.
    minTime = max(tl[0] for tl in timeLists)
    maxTime = min(tl[-1] for tl in timeLists)
        
    # Adjust the minimum time forward by a "settling" period to handle datasets
    # where there is an initial transient we want to avoid.
    minTime += settlingTimeOffset
    
    # Add additional bounds if they are given.
    minTime = maxNone((minTime, minTimeLimit))
    maxTime = minNone((maxTime, maxTimeLimit))
    
    if (maxTime <= minTime):
        raise ValueError('Cannot find a shared time range for input data sets')
    
    # List we will manipulate.
    workingList = []
    
    if allUnique:
        # If we want all unique times then lets just throw all the lists
        # together.  To ensure uniqueness, use a set.
        workingSet = set()
        for tl in timeLists:
            workingSet.update(tl)
            
        # Now get a monotonically increasing set of times, since a set is
        # unordered.
        workingList = sorted(workingSet)
    else:
        # In this case we just want to get the highest frequency input list.
        highestIdx = None
        highestFreq = None
        for idx, tl in enumerate(timeLists):
            freq = (len(tl) - 1) / (tl[-1] - tl[0])
            
            if not highestIdx or freq > highestFreq:
                highestIdx = idx
                highestFreq = freq
    
        workingList = list(timeLists[highestIdx])
            
    # Ok, we have a monotonically increasing set of times.  But we need to slice
    # the list based on min and max times.
    minIdx = None
    maxIdx = None
    for idx, time in enumerate(workingList):
        if time >= minTime and minIdx is None:
            minIdx = idx
            
        if time < maxTime:
            maxIdx = idx
        else:
            break
            
    # Return the sliced list.  Add two to the max index to include that element
    # in the returned list -- when slicing we need to add at least one to the
    # right element of the slice.
    return workingList[minIdx : maxIdx + 2]
        
# End createCommonTimeBase.

def iterateMultipleDataSources(commonTimeBase, 
    dataSources, 
    commonExecFunction=None):
    """
    When we have multiple data sources we first agree on a common time base
    between them.  Then we often want to iterate through the multiple data
    sources along over the common time base.  This function handles the 
    looping/iteration over the common time base determines when each of the
    data sources has arrived at a new data entry and executes a callback 
    function for that data source.  In addition, the user can provide a common
    function will gets executed each time step of the common time base.
    
    commontTimeBase is a list of unique, monotonically increasing time values,
    more than likely generated by a call to createCommonTimeBase.
    
    dataSources is a list of 2-tuples.  Each 2-tuple consists of a list of time
    values for which the data source has data.  This like the commonTimeBase,
    this should be unique and monotonically increasing.  The second element of
    the 2-tuple is a callback function which returns nothing and takes in the
    index of the commonTimeBase iterator and the index of the data source time
    representing the newly advanced point in the data source.  More than likely
    the caller will use lambdas for these callbacks.
    
    commonExecFunction is a callback function which returns nothing and takes
    in the index of the commonTimeBase iterator.
    """
    
    # We're going to track indexes of the individual data sources, using an
    # "old data" and "new data" index to help detect when we've come onto 
    # new data.
    dataSourceIndices = []
    for ds in dataSources:
        # The first element will be the "old data" index value, and the 
        # second will be the "new data".
        dataSourceIndices.append([None, None])
        
    # Iterate over the common time base.
    for commonIdx, commonTime in enumerate(commonTimeBase):
        # Iterate over each of the data sources and acknowledge that we've
        # advanced a single time step in the common time base and the data
        # source may have new data.
        for dataSourceIdx, dataSource in enumerate(dataSources):
            # Update the data source index values from new to old.  Just use
            # a simple primitive temporarily to make code more readable.
            oldIdx = dataSourceIndices[dataSourceIdx][1]
            
            # Get some convenient references.
            dataSourceTimeList = dataSource[0]
            dataSourceCallback = dataSource[1]
            
            # For a data source, get the corresponding data index for the time
            # closest to the present common time base.
            newIdx = advanceDataSourceIndex(oldIdx, dataSourceTimeList, commonTime)
        
            # Ok, with this increment of the common iteration time we may have
            # come to a time where this data source has new data.
            if newIdx != oldIdx:
                # Tell the user about it.
                dataSourceCallback(commonIdx, newIdx)
                
            # Update the persistent object with our local variables.
            dataSourceIndices[dataSourceIdx][0] = oldIdx
            dataSourceIndices[dataSourceIdx][1] = newIdx
            
        # Each time step we call the general exec function.
        if commonExecFunction:
            commonExecFunction(commonIdx)
        
# End iterateMultipleDataSources.

def advanceDataSourceIndex(oldIdx, dataSourceTimeList, commonTime):
    """
    Find the index of the data source time list closest to the time from the
    overall common time base.
    """
    
    # Handle the starting condition where oldIdx is None.
    if not oldIdx:
        oldIdx = 0
        
    bestIdx = oldIdx
    for candidateIdx in range(oldIdx, len(dataSourceTimeList)):
        # Look at the time at the candidate index.  If it is greater than the
        # main time it is too far advanced and we punt.
        if dataSourceTimeList[candidateIdx] > commonTime:
            break
            
        bestIdx = candidateIdx
       
    return bestIdx
    
# End advanceDataSourceIndex.

def xmlStringTupleToTuple(xmlString):
    """
    This function takes a string read from an XML element that encodes a tuple
    of values and turns it into an actual tuple.
    
    In XML when we need to write out vectors and stuff, we will write out as
    <myElement>(0.3, 0.5, 0.7)</myElement>.  But that's a string and what we
    really want is to get (0.3, 0.5, 0.7) as a concrete tuple.  You could 
    use eval() to turn the string to a tuple but that is unsafe.
    """
    
    # Strip out parentheses.
    str = xmlString.lstrip('(')
    str = str.rstrip(')')
    strComponents = str.split(',')
    floatComponents = [float(x) for x in strComponents]
    return tuple(floatComponents)
    
# End xmlTupleToList.

def importJar(jarPath):
    """
    Imports a Java JAR file so classes can be imported by Jython.
    
    If we use custom Java code (like when using NetBeans for UI generation)
    we need the ability to import the JAR files.  When running Jython from
    the command line it is enough to just add the JAR filepath to sys.path.
    However, when running through OpenSim, this doesn't work for some uknown
    reason.  So we need to do the following.  This is taken from:
    https://stackoverflow.com/questions/3015059/jython-classpath-sys-path-and-jdbc-drivers
    
    Note that if you import the JAR into an OpenSim Jython session and then 
    make a change to the JAR file, you will need to restart OpenSim for the new
    JAR file to be used.  i.e. you can't "reload()" the JAR file in the current
    session like you can when importing a Python module (as far as I know).
    """
    
    from java.net import URL
    from java.net import URLClassLoader
    from java.lang import ClassLoader
    from java.io import File
    
    m = URLClassLoader.getDeclaredMethod("addURL", [URL])
    m.accessible = 1
    
    # One thing to consider here is whether we might try using a unique
    # instance of a class loader instead of the system class loader.  That
    # may make it possible that the loaded JAR will be unloaded at the end
    # of our program (via garbage collection) and that we don't have to
    # re-open OpenSim in order to load an updated JAR on the next run of
    # our script...  For future investigation.
    m.invoke(ClassLoader.getSystemClassLoader(), [File(jarPath).toURL()])
    
# End importJar.

def isTextValidFloat(text):
    """
    Useful for UI text fields to validate the text can be interpreted as a 
    floating point number.
    
    This also fails (by design) if text is empty.
    """
    
    try:
        # This test also handles the case where there is nothing in
        # the text field.
        float(text)
    except:
        return False
        
    return True
    
# End isTextValidFloat.
    
def boolToLowerCaseString(value):
    """
    Converts a True to 'true' and a False to 'false'.
    
    BESS uses 'true' and 'false' in its configuration object for some 
    boolean-like variables.
    """
    
    if value:
        return 'true'
    else:
        return 'false'
        
# End boolToLowerCaseString.

def boolFromLowerCaseString(value):
    """
    Converts a 'true' to True and a 'false' to False.
    
    BESS uses 'true' and 'false' in its configuration object for some 
    boolean-like variables.
    """
    
    if value == 'true':
        return True
    else:
        # Don't try to be too cute here, raising exceptions if it is an empty
        # string or something.  Just make the value == 'true' be the only test
        # and everything else False.
        return False
        
# End boolFromLowerCaseString.

def setPersistentString(rootName, keyName, value):
    """
    Uses Java preferences to set a persistent string value.
    
    On Windows this will be the registry.  Note you can save other primitive
    types but need a different function call.  This one is only for string
    values.
    """
    
    # We're not a Java class, so we can't use userNodeForPackage() like OpenSim
    # does.  Rather get the user root, which should put us at 
    # HKEY_CURRENT_USER/Software/JavaSoft/Prefs, which is the user root for
    # OpenSim.  I think that is the best we can do.
    userRoot = JPreferences.userRoot()
    
    # Get (or create) the root name for our persistent preferences.
    appRoot = userRoot.node(rootName)
    
    appRoot.put(keyName, value)
    
# End setPersistentString.

def getPersistentString(rootName, keyName, defaultValue):
    """
    Retrieves a persistent string from Java preferences.
    
    defaultValue must be a string -- a Python type like None is not acceptable.
    """
    
    userRoot = JPreferences.userRoot()
    appRoot = userRoot.node(rootName)
    return appRoot.get(keyName, defaultValue)
    
# End getPersistentString.
    
def listToMat33(input):
    """
    Utility function to create a 3x3 rotation matrix from a 1-dimensional
    iterable like a list or a tuple.
    """    
    outMat = modeling.Mat33()
    
    if len(input) != 9:
        raise ValueError('List is not correct size for rotation matrix')
        
    for idx, value in enumerate(input):
        r = idx / 3
        c = idx % 3
        outMat.set(r, c, value)

    return outMat
    
# End listToMat33.

def rotateVector(inVec, rMat, outVec):
    """
    Utility function to rotate an input vector according to a rotation matrix.
    
    The vector is a Vec3 and the matrix is a Mat33.
    """    
    for row in range(3):
        value = 0
        for col in range(3):
            value += rMat.get(row, col) * inVec.get(col)
            
        outVec.set(row, value)
        
# End rotateVector.

def getMat33Determinant(m):
    """
    Gets a determinant for a 3x3 matrix.
    """
    cofactors = 0
    
    for cofactor_col in range(3):
        
        if cofactor_col == 0:
            det2x2 = m.get(1,1) * m.get(2,2) - m.get(2,1) * m.get(1,2)
            sign = 1.0
        elif cofactor_col == 1:
            det2x2 = m.get(1,0) * m.get(2,2) - m.get(2,0) * m.get(1,2)
            sign = -1.0
        else:
            det2x2 = m.get(1,0) * m.get(2,1) - m.get(2,0) * m.get(1,1)
            sign = 1.0
            
        cofactors += sign * m.get(0, cofactor_col) * det2x2
            
    return cofactors

# End get3x3_MatrixDeterminant.    

